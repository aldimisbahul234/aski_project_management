<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });
Route::get('/logout', 'HomeController@logout');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/dashboard_detail_schedule/{id_project}', 'HomeController@dashboard_detail_schedule');
Route::get('/type_project_dashboard/{id_project}', 'HomeController@type_project_dashboard');
Route::get('/gant_chart_project/{id_project}/{part_number}', 'HomeController@gant_chart_project');


Route::group(['namespace' => 'User'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
          Route::get('/profile/{id}', 'UserController@profile');
         Route::get('/hapus/{id}', 'UserController@hapus');
        Route::get('/edit/{id}', 'UserController@edit');
         Route::get('/detail/{id}', 'UserController@profile');
        Route::post('/update/{id}', 'UserController@update');
        Route::any('/delete/{id}', 'UserController@delete');
    });
});
Route::group(['namespace' => 'Project'], function(){
    Route::get('/home', 'ProjectController@index');
    // Routing Group Menu Project
    Route::group(['prefix' => 'project'], function(){
        
        Route::get('/checktime', 'ProjectController@checktime');
        Route::get('/checkwa', 'ProjectController@checkwa');
        Route::get('/read_notif/{status}/{id}', 'ProjectController@read_notif');
        Route::get('/update_status_item/{id_item}/{status}', 'ProjectController@update_status_item');
        Route::get('/get_answer_pic', 'ProjectController@get_answer_pic');
        Route::get('/issue_remove/{id}', 'IssueController@issue_remove');
        Route::get('/', 'ProjectController@index');
        Route::post('/', 'ProjectController@store');
        Route::get('/create', 'ProjectController@create');
        Route::get('/get_email', 'ProjectController@get_email');
        
        Route::get('/get_list_name', 'ProjectController@get_list_name');
        Route::post('/update_status_document', 'ProjectController@update_status_document');
        Route::get('/get_document_note', 'ProjectController@get_document_note');
        
        Route::get('/customer/{id}', 'ProjectController@customer');
        Route::post('/upload_document_rfq', 'ProjectController@upload_document_rfq');
        Route::get('/customerDetail/{id}/{id_product}', 'ProjectController@customerDetail');
        Route::get('/export_process/{sectorID}/{projectID}/{processID}', 'ProjectController@export_process');
        Route::get('/export_all/{sectorID}/{projectID}/', 'ProjectController@export_all');
        Route::get('/edit_project/{sector}/{customer}/{id}', 'ProjectController@edit_project');
        Route::get('/edit_part_list/{sector}/{project}/{id}', 'ProjectController@edit_part_list');
        Route::get('/product_detail/{project}/{customer}', 'ProjectController@product_detail');
        Route::get('/product_category_control/{part_number}/{id_process}/{id_project}', 'ProjectController@product_category_control');
        Route::get('/get_item_list/{part_number}/{id_category}/{id_project}', 'ProjectController@get_item_list');
        Route::get('/item_control_detail/{id_project}/{id_category}/{part_number}/{id_process}', 'ProjectController@item_control_detail');
        Route::get('/display_project_detail/{id_customer}/{id_project}', 'ProjectController@display_project_detail');
        Route::get('/add_part/{projectID}/', 'ProjectController@add_part');
        Route::post('/add_event', 'ProjectController@add_event');
        Route::post('/add_rfq', 'ProjectController@add_rfq');
        Route::get('/create_rfq/{id}', 'ProjectController@create_rfq');
        Route::get('/edit_rfq/{id}', 'ProjectController@edit_rfq');
        Route::get('/export_rfq/{id}', 'ProjectController@export_rfq');
        Route::get('/add_part_die_go/{id}', 'ProjectController@add_part_die_go');
        Route::get('/get_prod/{id}', 'ProjectController@get_prod');
        Route::post('/post_product', 'ProjectController@post_product');
        Route::get('/edit_event/{id}', 'ProjectController@edit_event');
         Route::get('/delete_event/{id}', 'ProjectController@delete_event');
        Route::post('/update_event/{id}', 'ProjectController@update_event');
        Route::post('/update_general/{id}', 'ProjectController@update_general');
        Route::get('/detail_member/{id}/{part_number}', 'ProjectController@detail_member');
        Route::get('/add_new_member/{id}/{part_number}/{id_process}', 'ProjectController@add_new_member');
        Route::get('/analis/{IDproject}/{part_number}/{status}', 'ProjectController@analis');
        Route::get('/technical_review/{id}/{part_number}', 'ProjectController@technical_review');
        Route::get('/add_technical_review/{id}/{part_number}', 'ProjectController@add_technical_review');
        Route::get('/cost_performance/{id}', 'ProjectController@cost_performance');
        Route::get('/cogm/{id}', 'ProjectController@cogm');
        Route::get('/project_budget/{id}', 'ProjectController@project_budget');
        Route::get('/project_detail_gantt', 'ProjectController@project_detail_gantt');
        Route::get('/detail_event_grafik', 'ProjectController@detail_event_grafik');
        Route::get('/edit_member_detail/{id}', 'ProjectController@edit_member_detail');
        Route::get('/delete_member_detail/{id}', 'ProjectController@delete_member_detail');
        Route::get('/report_general/{id}', 'ProjectController@report_general');
        Route::get('/approval_line/{part_number}/{id_project}/{id_category_event_project}', 'ProjectController@approval_line');
        Route::get('/quality_add/{part_number}/{id}/{id_category_event_project}', 'ProjectController@quality_add');
        Route::get('/quality_detail/{id}/{part_number}', 'ProjectController@quality_detail');
        Route::get('/issue/{id_project}/{part_number}/{id_category}/{id_item}/{id_process}', 'IssueController@issue');
        Route::get('/task_detail/{id_project}/', 'ProjectController@task_detail');
        Route::post('/add_member', 'ProjectController@add_member');
        Route::post('/update_member/{id}', 'ProjectController@update_member');
        Route::get('/document/{id_project}/{part_number}', 'ProjectController@document');
        Route::get('/list_document/{id_project}/{part_number}', 'ProjectController@list_document');
        Route::post('/add_category_control', 'ProjectController@add_category_control');
        Route::get('/add_new_item/{id_category}/{id_project}/{part_number}/{categoris}/{id_process}', 'ProjectController@add_new_item');
        Route::post('/pos_add_new_item', 'ProjectController@pos_add_new_item');
        Route::get('/create_issue/{id_project}/{part_number}/{id_category}/{id_item}/{id_process}', 'IssueController@create_issue');
        Route::post('/pos_add_issue', 'IssueController@pos_add_issue');
        Route::get('/remove_category_control/{id_category}', 'ProjectController@remove_category_control');
        Route::get('/edit_item_control/{id_project}/{id_category}/{part_number}/{id_process}/{id_item}/', 'ProjectController@edit_item_control');
        Route::get('/delete_item_control/{id_item}/', 'ProjectController@delete_item_control');
        Route::post('/post_technical_review', 'ProjectController@post_technical_review');
        Route::post('/post_document_project', 'ProjectController@post_document_project');
        Route::post('/update_item_control/{id_item}', 'ProjectController@update_item_control');
         Route::get('/edit_technical_review/{id_tr}/{part_number}/{id}', 'ProjectController@edit_technical_review');
        Route::get('/download_document/{id}', 'ProjectController@download_document');
        Route::post('/update_technical_review/{no_document}/{part_number}', 'ProjectController@update_technical_review');
        Route::get('/delete_technical_review/{no_document}/{part_number}', 'ProjectController@delete_technical_review');
        Route::get('/print_technical_review/{id_project}/{no_document}/{part_number}', 'ProjectController@print_technical_review');
        Route::get('/download_excel_product/{id_project}/{part_number}', 'ProjectController@download_excel_product');
        Route::get('/checkEmail', 'ProjectController@checkEmail');
        Route::get('/delete_project_summary/{id_project}/{id_customer}', 'ProjectController@delete_project_summary');
            
        Route::get('/quality_detail_list/{id}', 'ProjectController@quality_detail_list');
        Route::post('/post_quality', 'ProjectController@post_quality');
        Route::post('/post_approval_lines', 'ProjectController@post_approval_lines');
        Route::post('/update_approval_lines/{id}', 'ProjectController@update_approval_lines');
        Route::post('/save_note', 'ProjectController@save_note');
         Route::get('/change_status_doc/{id}/{status}/{id_project}/{part_number}', 'ProjectController@change_status_doc');
         Route::post('/update_document', 'ProjectController@update_document');
    });
});
Route::group(['namespace' => 'Level'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
    });
});
Route::group(['namespace' => 'Masterdata'], function(){
    // Routing Group Menu Master data
    Route::group(['prefix' => 'dataSector'], function(){
        Route::get('/', 'DataSectorController@index');
        Route::post('/', 'DataSectorController@store');
        Route::get('/create', 'DataSectorController@create');
        Route::get('/edit/{id}', 'DataSectorController@edit');
        Route::any('/delete/{id}', 'DataSectorController@delete');
        Route::put('/update/{id}', 'DataSectorController@update');
    });
    Route::group(['prefix' => 'dataCustomer'], function(){
        Route::get('/', 'DataCustomerController@index');
        Route::post('/', 'DataCustomerController@store');
        Route::get('/create', 'DataCustomerController@create');
        Route::get('/edit/{id}', 'DataCustomerController@edit');

        Route::any('/delete/{id}', 'DataCustomerController@delete');
        Route::put('/update/{id}', 'DataCustomerController@update');
    });
    Route::group(['prefix' => 'typeProject'], function(){
        Route::get('/', 'TypeProjectController@index');
        Route::post('/', 'TypeProjectController@store');
        Route::get('/create', 'TypeProjectController@create');
        Route::get('/edit/{id}', 'TypeProjectController@edit');

        Route::any('/delete/{id}', 'TypeProjectController@delete');
        Route::put('/update/{id}', 'TypeProjectController@update');
    });
     Route::group(['prefix' => 'dataProduct'], function(){
        Route::get('/', 'DataProductController@index');
        Route::post('/', 'DataProductController@store');
        Route::get('/create', 'DataProductController@create');
        Route::get('/edit/{id}', 'DataProductController@edit');

        Route::any('/delete/{id}', 'DataProductController@delete');
        Route::put('/update/{id}', 'DataProductController@update');
    });
      Route::group(['prefix' => 'dataProcess'], function(){
        Route::get('/', 'DataProcessController@index');
        Route::post('/', 'DataProcessController@store');
        Route::get('/create', 'DataProcessController@create');
        Route::get('/edit/{id}', 'DataProcessController@edit');

        Route::any('/delete/{id}', 'DataProcessController@delete');
        Route::put('/update/{id}', 'DataProcessController@update');
    });
       Route::group(['prefix' => 'categoryControl'], function(){
        Route::get('/', 'CategoryControlController@index');
        Route::post('/', 'CategoryControlController@store');
        Route::get('/create', 'CategoryControlController@create');
        Route::get('/edit/{id}', 'CategoryControlController@edit');

        Route::any('/delete/{id}', 'CategoryControlController@delete');
        Route::put('/update/{id}', 'CategoryControlController@update');
    });
        Route::group(['prefix' => 'pi'], function(){
        Route::get('/', 'PiController@index');
        Route::post('/', 'PiController@store');
        Route::get('/create', 'PiController@create');
        Route::get('/edit/{id}', 'PiController@edit');

        Route::any('/delete/{id}', 'PiController@delete');
        Route::put('/update/{id}', 'PiController@update');
    });
     Route::group(['prefix' => 'seat'], function(){
        Route::get('/', 'SeatController@index');
        Route::post('/', 'SeatController@store');
        Route::get('/create', 'SeatController@create');
        Route::get('/edit/{id}', 'SeatController@edit');

        Route::any('/delete/{id}', 'SeatController@delete');
        Route::put('/update/{id}', 'SeatController@update');
    });
      Route::group(['prefix' => 'mirror'], function(){
        Route::get('/', 'MirrorController@index');
        Route::post('/', 'MirrorController@store');
        Route::get('/create', 'MirrorController@create');
        Route::get('/edit/{id}', 'MirrorController@edit');

        Route::any('/delete/{id}', 'MirrorController@delete');
        Route::put('/update/{id}', 'MirrorController@update');
    });
     Route::group(['prefix' => 'painting'], function(){
        Route::get('/', 'PaintingController@index');
        Route::post('/', 'PaintingController@store');
        Route::get('/create', 'PaintingController@create');
        Route::get('/edit/{id}', 'PaintingController@edit');

        Route::any('/delete/{id}', 'PaintingController@delete');
        Route::put('/update/{id}', 'PaintingController@update');
    });
     Route::group(['prefix' => 'eventProject'], function(){
        Route::get('/', 'EventProjectController@index');
        Route::post('/', 'EventProjectController@store');
        Route::get('/create', 'EventProjectController@create');
        Route::get('/edit/{id}', 'EventProjectController@edit');

        Route::any('/delete/{id}', 'EventProjectController@delete');
        Route::put('/update/{id}', 'EventProjectController@update');
    });
     Route::group(['prefix' => 'departement'], function(){
        Route::get('/', 'DepartementController@index');
        Route::post('/', 'DepartementController@store');
        Route::get('/create', 'DepartementController@create');
        Route::get('/edit/{id}', 'DepartementController@edit');

        Route::any('/delete/{id}', 'DepartementController@delete');
        Route::put('/update/{id}', 'DepartementController@update');
    });
});
Route::group(['namespace' => 'Cost'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'cost'], function(){
        Route::get('/', 'CostController@index');
        Route::post('/', 'CostController@store');
        Route::get('/create', 'CostController@create');
        Route::get('/edit/{id}', 'CostController@edit');
        Route::get('/cogm/{id}', 'CostController@cogm');
        Route::get('/budget/{id}', 'CostController@budget');
        Route::get('/cpi/{id}', 'CostController@cpi');
        Route::get('/create_cogm/{id}', 'CostController@create_cogm');
        Route::get('/budget_create/{id}', 'CostController@budget_create');
        Route::post('/post_cogm', 'CostController@post_cogm');
        Route::post('/post_budget', 'CostController@post_budget');
        Route::get('/get_qty/{id}', 'CostController@get_qty');
        Route::post('/post_budget', 'CostController@post_budget');
          Route::post('/save_budget', 'CostController@save_budget');
        Route::get('/hapus_budget/{id}', 'CostController@hapus_budget');
        Route::get('/save_all', 'CostController@save_all');
    });
});
Route::group(['namespace' => 'Email'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'email'], function(){
        Route::get('/', 'EmailController@index');
        Route::post('/', 'EmailController@store');
      
    });
});
Route::group(['namespace' => 'Whatsapp'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'whatsapp'], function(){
        Route::get('/', 'WhatsappController@index');
        Route::post('/', 'WhatsappController@store');
       Route::post('wa', 'WhatsappController@wa');
    });
});
Route::group(['namespace' => 'Pic'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'pic'], function(){
        
        Route::get('/', 'PicController@index');
         Route::get('download_document_issue/{id}', 'PicController@download_document_issue');
        Route::get('get_product_pic/{id}', 'PicController@get_product_pic');
        Route::get('open_rio', 'PicController@open_rio');
         Route::get('open_item_control', 'PicController@open_item_control');
         Route::get('open_item_control_detail/{id}', 'PicController@open_item_control_detail');
        Route::get('open_item_control_detail_product/{id}/{part_number}', 'PicController@open_item_control_detail_product');
        Route::get('project_open_rio/{id}', 'PicController@project_open_rio');
        Route::post('/', 'PicController@store');
      Route::get('get_category_product/{id}', 'PicController@get_category_product');

        Route::get('open_item_control_finish', 'PicController@open_item_control_finish');
        Route::get('open_item_control_detail_finish/{id}', 'PicController@open_item_control_detail_finish');
        Route::get('open_item_control_detail_product_finish/{id}/{part_number}', 'PicController@open_item_control_detail_product_finish');

         Route::get('my_profile', 'PicController@my_profile');
          Route::post('update_my_profile/{id}', 'PicController@update_my_profile');
          Route::get('my_document', 'PicController@my_document');
            Route::get('project_open_rio_get/{id}/{part_number}', 'PicController@project_open_rio_get');
              Route::get('project_open_rio_detail/{id}', 'PicController@project_open_rio_detail');
              Route::post('project_open_rio_update/{id}', 'PicController@project_open_rio_update');


        Route::get('open_rio_finish', 'PicController@open_rio_finish');
        Route::get('project_open_rio_finish/{id}', 'PicController@project_open_rio_finish');
         Route::get('project_open_rio_get_finish/{id}/{part_number}', 'PicController@project_open_rio_get_finish');
          Route::post('upload_document_pic', 'PicController@upload_document_pic');
         
    });
});
Route::group(['namespace' => 'Historyrio'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'history_rio'], function(){
        Route::get('/', 'HistoryRioController@index');
        Route::post('/', 'HistoryRioController@store');
       Route::post('wa', 'HistoryRioController@wa');
    });
});
Route::group(['namespace' => 'Historyescalation'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'history_escalation'], function(){
        Route::get('/', 'HistoryEscalationController@index');
        Route::post('/', 'HistoryEscalationController@store');
       Route::post('wa', 'HistoryEscalationController@wa');
    });
});
Route::group(['namespace' => 'Rfq'], function(){
    // Routing Group Menu Level
    Route::group(['prefix' => 'rfq'], function(){
        Route::get('/', 'RfqController@index');
        Route::post('/', 'RfqController@store');
    });
});
