<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('tbl_pengguna')->insert([
            [
				'firstname' => 'Quality',
				'email' => 'user_quality@gmail.com',
                'username' => 'user_quality',
				'password' => Hash::make('admin890'),
				'hak_akses'=>'quality'
			
            ], 
             [
                'firstname' => 'Cost',
                'email' => 'user_cost@gmail.com',
                'username' => 'user_cost',
                'password' => Hash::make('admin890'),
                'hak_akses'=>'cost'
               
            ]
             ]);
    }
}
