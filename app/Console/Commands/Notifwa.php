<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use Twilio\Rest\Client;
class Notifwa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:wa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $hp = [];
       $tomorrow = Carbon::tomorrow()->format('Y-m-d'); 
       $get_data = array();
       $fix_data = array();
       $id_control = DB::table('item_control')->whereDate('actual_finish_item',$tomorrow)->where('status_reminder',null)->pluck('id_control');
       $pic = DB::table('item_control')->whereIn('id_control',$id_control)->get();
       foreach ($pic as $key => $value) {

          $get_data[] = [
          'id_control'=>$value->id_control,
          'get_item_name'=>$get_item_name = DB::table('master_item_control')->where('id_item',$value->id_item)->first(),
          'item_name'=>$get_item_name == true ? $get_item_name->item_name : null,
          'get_part_name'=>$get_part_name = DB::table('product')->where('part_number',trim($value->part_number))->first(),
          'part_name'=>$get_part_name == true ? $get_part_name->part_name : null,
          'id_project'=>$value->id_project,
          'get_type_project'=> $type_project = DB::table('project_type')->where('id_project',$value->id_project)->first(),
          'type_project'=> $type_project == true ? $type_project->project_name : null,
          'pic_item'=>$value->pic_item,
          'get_phone'=>$get_phone = DB::table('tbl_pengguna')->where('username',$value->pic_item)->first(),
          'phone'=>$get_phone == true ? $get_phone->telepon : null
         ];
       }
       foreach ($get_data as $key => $values) {
         $get = str_replace(" ","",$values['phone']);
         $get = str_replace("(","",$values['phone']);
         $get = str_replace(")","",$values['phone']);
         $get = str_replace(".","",$values['phone']);

         // cek apakah no hp mengandung karakter + dan 0-9
         if(!preg_match('/[^+0-9]/',trim($get))){
             // cek apakah no hp karakter 1-3 adalah +62
             if(substr(trim($get), 0, 3)=='+62'){
                 $hp = trim($get);
             }
             // cek apakah no hp karakter 1 adalah 0
             elseif(substr(trim($get), 0, 1)=='0'){
                 $hp = '+62'.substr(trim($get), 1);
             }
         }
         $fix_data[] = [
          'id_control'=>$values['id_control'],
          'phone'=>$hp,
          'pic'=>$values['pic_item'],
          'type_project'=>$values['type_project'],
          'part_name'=>$values['part_name'],
          'item_name'=>$values['item_name']
         ];

       }

        $sid    = "ACe8f85166bc65189c3c377612f92a62a1";
        $token  = "b404fe2eafc7812343d964f2cccb4559";
        $wa_from= "+14155238886";
        foreach ($fix_data as $key => $da) {

            $twilio = new Client($sid, $token);
            $body = 'Hai, '.$da['pic'].' mohon segera lakukan update untuk item control ('.$da['item_name'].'), PART NAME('.str_replace(" ","",$da['part_name']).') - PROJECT('.$da['type_project'].') TERIMAKASIH';
            $send =  $twilio->messages->create("whatsapp:".$da['phone']."",["from" => "whatsapp:$wa_from", "body" => $body]);
             DB::table('cron')->insert(['name'=>$da['pic'],'create_by'=>Carbon::now(),'media'=>'whatsapp','tipe'=>'item_control','id_control'=>$da['id_control'],'send_to'=>$da['pic'],'phone'=>$da['phone']]);
        }
        DB::table('item_control')->whereIn('id_control',$id_control)->where('status_reminder',null)->update(['status_reminder'=>1]);
       
       return $send;

    }
}
