<?php

namespace App\Http\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Issue extends Model
{
    protected $table = "issue";

    protected $primaryKey = "id_issue";
    protected $fillable = [
      'id_project','part_number','category_issue','issue','pic','escalation','id_process','id_category','id_item','last_update','created','timing','status_rio','bukti'
    ];

public $timestamps = false;
}
