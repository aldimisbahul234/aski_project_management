<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Notification extends Model
{
    protected $table = "notifications";

    protected $primaryKey = "id_notification";
    protected $fillable = [
      'id_project','pengirim','penerima','tipe','part_number','status_read'
    ];

public $timestamps = false;
}
