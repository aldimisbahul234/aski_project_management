<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use Carbon\Carbon;
class ItemControl extends Model
{
    protected $table = "item_control";

    protected $primaryKey = "id_control";
    protected $fillable = [
      'part_number'
    ];

	public function item()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\MasterItemControl', 'id_item', 'id_item');
	}
	public function category()
	{
		return $this->belongsTo('App\Http\Models\Masterdata\CategoryControllName', 'id_category', 'id_category');
	}
	public static function CountAnalis($status,$IDproject,$part_number,$IDcategory)
	{
		$today = Carbon::now()->format('Y-m-d');
		$count = 0;
		if ($status =='today') 
		{
			if($IDcategory !=null){
			$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
				->where('plan_start_item',$today)
				->where('status_item',1)
				->where('id_category',$IDcategory)
				->count();
			}else{
			$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item',$today)
					->where('status_item',1)
					->count();
			}
		}elseif($status =='progress')
		{
			if($IDcategory !=null){
				$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','<',$today)
					->where('plan_finish_item','>',$today)
					->where('plan_progress_item','<',100)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->count();
			}else{
			$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','<',$today)
					->where('plan_finish_item','>',$today)
					->where('plan_progress_item','<',100)
					->where('status_item',1)
					->count();
				}
		}elseif($status =='next')
		{
			if($IDcategory !=null){
				$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','>',$today)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->count();
			}else{
			$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','>',$today)
					->where('status_item',1)
					->count();
				}
		}elseif($status =='delay')
		{
			if($IDcategory !=null){
				$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					 ->where('plan_finish_item','<',$today)
					 ->where('actual_progress_item','<',100)
					 ->where('status_item',1)
					 ->where('id_category',$IDcategory)
					 ->count();
			}else{
			$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					 ->where('plan_finish_item','<',$today)
					 ->where('actual_progress_item','<',100)
					 ->where('status_item',1)
					 ->count();
					}
		}else
		{
			if($IDcategory !=null){
				$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('actual_progress_item',100)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->count();
			}else{
				$count = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('actual_progress_item',100)
					->where('status_item',1)
					->count();
			}
			
		}
		return $count;
	}
	public static function GetAnalis($status,$IDproject,$part_number,$IDcategory)
	{
		$today = Carbon::now()->format('Y-m-d');
		$get = 0;
		if ($status =='today') 
		{
			if($IDcategory !=null){
			$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
				->where('plan_start_item',$today)
				->where('status_item',1)
				->where('id_category',$IDcategory)
				->get();
			}else{
			$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item',$today)
					->where('status_item',1)
					->get();
			}
		}elseif($status =='progress')
		{
			if($IDcategory !=null){
				$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','<',$today)
					->where('plan_finish_item','>',$today)
					->where('plan_progress_item','<',100)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->get();
			}else{
			$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','<',$today)
					->where('plan_finish_item','>',$today)
					->where('plan_progress_item','<',100)
					->where('status_item',1)
					->get();
				}
		}elseif($status =='next')
		{
			if($IDcategory !=null){
				$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','>',$today)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->get();
			}else{
			$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('plan_start_item','>',$today)
					->where('status_item',1)
					->get();
				}
		}elseif($status =='delay')
		{
			if($IDcategory !=null){
				$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					 ->where('plan_finish_item','<',$today)
					 ->where('actual_progress_item','<',100)
					 ->where('status_item',1)
					 ->where('id_category',$IDcategory)
					 ->get();
			}else{
			$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					 ->where('plan_finish_item','<',$today)
					 ->where('actual_progress_item','<',100)
					 ->where('status_item',1)
					 ->get();
					}
		}else
		{
			if($IDcategory !=null){
				$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('actual_progress_item',100)
					->where('status_item',1)
					->where('id_category',$IDcategory)
					->get();
			}else{
				$get = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])
					->where('actual_progress_item',100)
					->where('status_item',1)
					->get();
			}
			
		}
		return $get;
	}
	 
public $timestamps = false;
}
