<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class GuageCustom extends Model
{
    protected $table = "tbl_guage_custom";
    protected $primaryKey = "guage_custom_id";
    protected $fillable = [
      'no_document','part_number','name'
    ];
   
public $timestamps = false;

}
