<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class CategoryEventProject extends Model
{
    protected $table = "category_event_project";

    protected $primaryKey = "id_category_event_project";
    protected $fillable = [
      'event_project_name'
    ];

public $timestamps = false;
}
