<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class DocumentRfq extends Model
{
    protected $table = "rfq_document";

    protected $primaryKey = "id_rfq";
    protected $fillable = [
      'id_project','rfq_file','date_created','status'
    ];
    
public $timestamps = false;

}
