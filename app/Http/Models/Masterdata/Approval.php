<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Approval extends Model
{
    protected $table = "approval_line";

    protected $primaryKey = "id_approval_line";
    protected $fillable = [
     	'id_project',
		'part_number',
		'id_category_event_project',
		'attachment',
		'done1',
		'eject1',
		'silang1',
		'done2',
		'eject2',
		'silang2',
		'done3',
		'eject3',
		'silang3',
		'done4',
		'eject4',
		'silang4',
		'done5',
		'eject5',
		'silang5',
		'done6',
		'eject6',
		'silang6',
		'done7',
		'eject7',
		'silang7',
		'done8',
		'eject8',
		'silang8',
		'done9',
		'eject9',
		'silang9',
		'done10',
		'eject10',
		'silang10',
		'done11',
		'eject11',
		'silang11',
		'done12',
		'eject12',
		'silang12',
		'done13',
		'eject13',
		'silang13'
    ];

public $timestamps = false;
}




