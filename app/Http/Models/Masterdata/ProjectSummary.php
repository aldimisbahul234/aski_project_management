<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class ProjectSummary extends Model
{
    protected $table = "project_summary";

    protected $primaryKey = "id_project_summary";
    protected $fillable = [
      'id_project',
    ];
    
public $timestamps = false;

}
