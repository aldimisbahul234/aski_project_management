<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class EventProject extends Model
{
    protected $table = "event_project";

    protected $primaryKey = "id_event_project";
    protected $fillable = [
      'id_category_event_project'
    ];
	public function category_name()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\CategoryEventProject', 'id_category_event_project', 'id_category_event_project');
	}
	public function project_name()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\TypeProject', 'id_project', 'id_project');
	}
	public static function getJadwalEvent($id_project,$datetime)
	{
		$data = EventProject::where('id_project',$id_project)->where('jadwal_event','<',$datetime)->first();
		return $data;
	}
public $timestamps = false;
}
