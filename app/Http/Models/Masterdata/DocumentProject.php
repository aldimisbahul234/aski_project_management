<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class DocumentProject extends Model
{
    protected $table = "document_project";

    protected $primaryKey = "document_code";
    protected $fillable = [
      'id_category_event_project','id_project','part_number','document_name','date_create','pic','filename','status_document','id_process','reminder_count','create_by'
    ];
    
public $timestamps = false;
public static function CheckDocument($id_project,$id_category_event_project)
{
	$query = DocumentProject::where(['id_project'=>$id_project,'id_category_event_project'=>$id_category_event_project])->first();
	if ($query ==true) {
		return $query->id_category_event_project;
	}else{
		return false;
	}
}
public static function getDocument($id_project,$part_number,$category)
{
	$query = DocumentProject::where(['id_project'=>$id_project,'id_category_event_project'=>$category,'part_number'=>$part_number])->first();
	if ($query ==true) {
		return 'ada';
	}else{
		return false;
	}
}
public static function checkComplate($id_project,$part_number,$category)
{
	
		$query = DocumentProject::where(['id_project'=>$id_project,'id_category_event_project'=>$category,'part_number'=>$part_number,'filename'=>''])->count();
		if($query > 0){
			return true;
		}else{
			return false;
		}
	
}
public static function checkComplate2($id_project,$part_number)
{
	$check = DocumentProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->count();
	if ($check > 0) {
		$query = DocumentProject::where(['id_project'=>$id_project,'part_number'=>$part_number,'filename'=>''])->count();
		return $query;
	}
	return "kosong";
	
}
public function project()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\TypeProject', 'id_project', 'id_project');
	}
	public function product()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\Product', 'part_number', 'part_number');
	}
	public static function sumProgress($data,$totaldocument)
	{	$percentasi = 0;
		$get_data =  [];
		$getpercent = 0;
		$final = 0;
		if ($totaldocument > 0) {
			$percentasi = round(100 / $totaldocument,1);
		}
		foreach ($data as $key => $value) {
			if ($value->status_document ==1) {
				$getpercent = $percentasi; 
			}elseif($value->status_document ==2){
				$getpercent = $percentasi / 2;
			}else{
				$getpercent = 0;
			}
			$final += $getpercent; 
			$get_data[] = [
				'document_code'=>$value->document_code,
				'percentasi'=>$getpercent
			];

		}
		return round($final,1);
	}
}
