<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Customer extends Model
{
    protected $table = "customer";

    protected $primaryKey = "id_customer";
    protected $fillable = [
      'customer_name','id_product'
    ];

public $timestamps = false;
}
