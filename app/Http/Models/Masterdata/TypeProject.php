<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class TypeProject extends Model
{
    protected $table = "project_type";

    protected $primaryKey = "id_project";
    protected $fillable = [
      'project_name','id_customer'
    ];

public $timestamps = false;
}
