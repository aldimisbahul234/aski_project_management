<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Cogm extends Model
{
    protected $table = "cogm";

    protected $primaryKey = "id_cogm";
    protected $fillable = [
      'id_project','id_category_event_project','part_number','bom_cogm','process_cogm','depreciation_cogm','total_cogm'
    ];
	public function event()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\CategoryEventProject', 'id_category_event_project', 'id_category_event_project');
	}
	public function product()
	{
		return $this->belongsTo('App\Http\Models\Masterdata\Product', 'part_number', 'part_number');
	}
public $timestamps = false;
}
