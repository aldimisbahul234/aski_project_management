<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class HistoryEscalation extends Model
{
    protected $table = "history_escalation";

    protected $primaryKey = "id_escalation";
    protected $fillable = [
      'id_project','part_number'
    ];

public $timestamps = false;
}
