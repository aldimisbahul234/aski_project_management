<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class CategoryControllName extends Model
{
    protected $table = "category_control";

    protected $primaryKey = "id_category";
    protected $fillable = [
      'part_number','category_name','id_process','status','dateandtime'
    ];

public $timestamps = false;
}
