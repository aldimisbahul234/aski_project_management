<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class HistoryEvent extends Model
{
    protected $table = "event_project_history";

    protected $primaryKey = "id_event_project_history";
    protected $fillable = [
      'id_event_project','id_project','jadwal_event_current','update_on','jadwal_event_change'
    ];

public $timestamps = false;
}
