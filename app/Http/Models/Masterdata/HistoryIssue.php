<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class HistoryIssue extends Model
{
    protected $table = "history_issue";

    protected $primaryKey = "id_history";
    protected $fillable = [
      'id_project','part_number'
    ];


public function product()
{
	return $this->belongsTo('App\Http\Models\Masterdata\Product', 'part_number', 'part_number');
}
public function project()
{
	return $this->belongsTo('App\Http\Models\Masterdata\TypeProject', 'id_project', 'id_project');
}
public $timestamps = false;
}
