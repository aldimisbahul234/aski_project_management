<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class MasterItemControl extends Model
{
    protected $table = "master_item_control";

    protected $primaryKey = "id_item";
    protected $fillable = [
      'item_name'
    ];
public static function getName($id)
{
	$data = MasterItemControl::where('id_item',$id)->first();
	return $data->item_name;
}
public $timestamps = false;
}
