<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Product extends Model
{
    protected $table = "product";

  
    protected $fillable = [
      'part_name','plan_progress_part','actual_progress_part','id_process','tgl_masuk','id_project','status_product'
    ];
    public function process()
	{
    	return $this->belongsTo('App\Http\Models\Masterdata\Process', 'id_process', 'id_process');
	}
public $timestamps = false;
}
