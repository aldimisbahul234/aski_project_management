<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class ProductSummary extends Model
{
    protected $table = "product_summary";


    protected $fillable = [
      'part_number','id_project'
    ];
    
public $timestamps = false;
}
