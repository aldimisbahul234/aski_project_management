<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use DB;
use App\Http\Models\Masterdata\Customer;
class Sector extends Model
{
    protected $table = "product_type";

    protected $primaryKey = "id_product";
    protected $fillable = [
      'product_name','box_color','logo'
    ];

public $timestamps = false;

}
