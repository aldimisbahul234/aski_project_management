<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class ProjectBudget extends Model
{
    protected $table = "project_budget";

    protected $primaryKey = "id_project_budget";
    protected $fillable = [
      'id_project','part_number','item_invesment','funding','category','price_of_item_plan','qty_item_plan','price_of_item_plan','price_of_item_actual','uom_plan','uom_actual','total_price_plan','total_price_actual'
    ];

public $timestamps = false;
}
