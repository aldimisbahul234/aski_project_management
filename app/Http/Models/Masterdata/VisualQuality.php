<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class VisualQuality extends Model
{
    protected $table = "visual_quality";

    protected $primaryKey = "id_visual";
    protected $fillable = [
      	'id_quality',
		'notes',
		'pic_pe',
		'pic_mjtf',
		'pic_eng_process',
		'pic_eng_set_mold',
		'status',
		'trial_number',
		'part_number',
		'id_category_event_project'
    ];

public $timestamps = false;
}
