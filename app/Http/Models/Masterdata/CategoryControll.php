<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class CategoryControll extends Model
{
    protected $table = "category_control_list";

    protected $primaryKey = "id_category_list";
    protected $fillable = [
      'category_list_name','id_process'
    ];

public $timestamps = false;
}
