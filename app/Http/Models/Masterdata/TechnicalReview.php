<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use DB;
use App\Http\Models\Masterdata\Customer;
class TechnicalReview extends Model
{
    protected $table = "technical_review";

    protected $primaryKey = "id_tr";
    protected $fillable = [
      'id_project','part_number','tr_name','last_update_tr','update_tr_by','no_document'
    ];

public $timestamps = false;

}
