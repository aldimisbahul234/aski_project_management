<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class UtilityCustom extends Model
{
    protected $table = "tbl_utility_custom";
    protected $primaryKey = "utility_custom_id";
    protected $fillable = [
      'no_document','part_number','name','value'
    ];
   
public $timestamps = false;

}
