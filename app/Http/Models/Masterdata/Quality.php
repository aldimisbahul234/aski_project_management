<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Quality extends Model
{
    protected $table = "quality";

    protected $primaryKey = "id_quality";
    protected $fillable = [
      'id_project','part_number','id_category_event_project','trial_number','adjustment_visual','adjustment_dimensi','tanggal_masuk'
    ];
    
public $timestamps = false;

}
