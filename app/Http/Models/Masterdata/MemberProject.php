<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class MemberProject extends Model
{
    protected $table = "member_project";

    protected $primaryKey = "id_member";
    protected $fillable = [
      'username','id_position','id_project','id_process','availability','standar_rate','duration_rate','part_number'
    ];
    
public $timestamps = false;

}
