<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class DimensiQuality extends Model
{
    protected $table = "dimensi_quality";

    protected $primaryKey = "id_dimensi";
    protected $fillable = [
      'id_quality'
    ];

public $timestamps = false;
}
