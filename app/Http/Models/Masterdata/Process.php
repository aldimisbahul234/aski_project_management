<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Process extends Model
{
    protected $table = "process";

    protected $primaryKey = "id_process";
    protected $fillable = [
      'process_name','box_color'
    ];

public $timestamps = false;
}
