<?php

namespace App\Http\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Departement extends Model
{
    protected $table = "departement";

    protected $primaryKey = "id_departement";
    protected $fillable = [
      'departement',
    ];
    
public $timestamps = false;

}
