<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\User;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $user = User::all();

       return view('user.admin.index',compact('user'));

    }

    public function store(Request $request)
    {
       //return $request->all();
    	$user 			    = new \App\User;
    	$user->firstname 	= $request->firstname;
        $user->lastname     =  $request->lastname;
        $user->username     =  $request->username;
    	$user->email 		= $request->email;
    	$user->password	    = $request->password;
        $user->hak_akses    = $request->hak_akses;
        if ($request->HasFile('photo')) {
                $destination_path = "/public/admin/photo/";
                $image = $request->file('photo');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('photo')->storeAs($destination_path,$image_name);
                $user['photo'] = $image_name;
        }
        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

    	return redirect('user');

    }

    public function create()
    {

    	return view('user.admin.create');
    }

    public function edit($id)
    {
    	$user = \App\User::find($id);
    	return view('user.admin.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
    //	$this->authorize('edit', [ \App\User::class, $this->module ]);

    	$data = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'hak_akses' => $request->hak_akses,
            'password'=>$request->password
        ];
        if ($request->HasFile('avatar')) {
                $destination_path = "/public/avatar/";
                $image = $request->file('avatar');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('avatar')->storeAs($destination_path,$image_name);
                $data['photo'] = $image_name;
        }
    	
    	$update = \App\User::where('id_pengguna',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Admin');
        }

        if ($request->type == "profile") {
            return redirect('profile');
        }

        return redirect('user');
    }

    public function delete(Request $request, $id)
    {


    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Admin');
        }

    	return redirect('user');
    }

    public function profile($id)
    {
        $profile    = \App\User::find($id);

        return view('user.admin.detail', compact('profile'));
    }

    public function profileUpdate(Request $request)
    {
        $this->authorize('update', [ \App\User::class, $this->module ]);

        $id = \Auth::user()->id;

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'address' => $request->address,
        ];

        if ($request->password != "" || $request->password != NULL) {
            $data['password'] = bcrypt($request->password);
        }

        // return $data; exit;

        $update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

        return redirect('profile');
    }
    public function verifikasi(Request $request,$id)
    {

        $model  = \App\User::findOrFail($id);
        $model = [
            'verified' => 1,
        ];
        if ($model) {

           $update = \App\User::where('id',$id)->update($model);
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'approved has been edited');
       }
       return redirect()->back();
    }
    public function hapus($id)
    {
        $data = \App\User::find($id);
        $data->delete();

        if ($data) {
            return redirect()->back();
        }
    }

}
