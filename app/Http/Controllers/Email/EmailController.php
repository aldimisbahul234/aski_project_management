<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Redis;
use App\Http\Email\TestMail;
class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        
       return view('level.index');

    }
    public function store(Request $request)
    {
       
		Mail::to($request->email)->send(new TestMail());
 
		return "Email telah dikirim".$request->email;

    }

   
}
