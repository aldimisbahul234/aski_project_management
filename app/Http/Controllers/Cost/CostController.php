<?php

namespace App\Http\Controllers\Cost;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Masterdata\Customer;
use App\Http\Models\Masterdata\Sector;
use App\Http\Models\Masterdata\ProjectBudget;
use App\Http\Models\Masterdata\EventProject;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\CategoryEventProject;
use App\Http\Models\Masterdata\ProductSummary;

use DB;
use Carbon\Carbon;
use App\Http\Models\Masterdata\Cogm;
class CostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $data = TypeProject::all();
      foreach ($data as $key => $value) {
          $getData[] = [
            'id_project'=>$value->id_project,
            'project_name'=>$value->project_name,
            'get_customer'=>$get_customer = Customer::where('id_customer',$value->id_customer)->first(),
            'customer'=>$get_customer->customer_name,
            'get_sector'=>$get_sector = Sector::where('id_product',$get_customer->id_product)->first(),
            'sector'=>$get_sector->product_name
          ];
      }
         $countCogm = DB::table('cogm')->select('*')->count();
         $sumPlan = DB::table('project_budget')->select('*')->sum('total_price_plan');
         $sumActual = DB::table('project_budget')->select('*')->sum('total_price_actual');
         $cogm = Cogm::all();
        // return $cogm;
         foreach ($cogm as $key => $values) {
          $getCogm[] = [
            'get_project'=>$get_project = TypeProject::where('id_project',$values->id_project)->first(),
            'project'=>$get_project ==true ? $get_project->project_name : '-',
            'get_product'=>$get_product = Product::where('part_number',$values->part_number)->first(),
            'product'=>$get_product->part_name,
            'get_event'=>$get_event = CategoryEventProject::where('id_category_event_project',$values->id_category_event_project)->first(),
            'event'=>$get_event->event_project_name,
            'bom_cogm'=>$values->bom_cogm,
            'process_cogm'=>$values->process_cogm,
            'depreciation_cogm'=>$values->depreciation_cogm,
            'total_cogm'=>$values->total_cogm

          ];
         }

        return view('cost.index',compact('getData','countCogm','sumPlan','sumActual','cogm','getCogm'));
    }

    public function store(Request $request)
    {
  

        return redirect('cost');

    }
    public function edit($id)
    {
    

        return 404;
    }
   public function update(Request $request,$id){
     

       return redirect('cost');
   }
    public function delete(Request $request, $id)
    {
       
       return redirect()->back();
    }
    public function cogm($id)
    {
      $getData = [];
      $project = TypeProject::find($id);
      $data = DB::table('cogm')->where('id_project',$id)->get();
       
        foreach ($data as $key => $value) {
            $getData[] = [
                'id_cogm'=>$value->id_cogm,
                'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                'product'=>$get_poduct->part_name,
                'get_event'=>$get_event = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),
                
                'get_detail'=> $get_detail[] = [
                    'event'=>$get_event->event_project_name,
                    'event'=>$get_event->event_project_name,
                    'bom'=>$value->bom_cogm,
                    'process'=>$value->process_cogm,
                    'depreciation'=>$value->depreciation_cogm,
                    'total'=>$value->total_cogm,
                ],
                'detail'=>$get_detail,
            ];
        }

         return view('cost.cost',compact('getData','data','project'));
    }
    public function budget($id)
    {   
        $getData = [];
        $project = TypeProject::find($id);
        $datetime = Carbon::now()->format('d F Y');
        $event = EventProject::where('id_project',$id)->where('id_category_event_project',14)->first();
        $budget_plan = ProjectBudget::where('id_project',$id)->sum('total_price_plan');
        $budget_actual = ProjectBudget::where('id_project',$id)->sum('total_price_actual');
        if($budget_plan !=0 || $budget_actual !=0){
        $result = round($budget_actual / $budget_actual * 100);
        }else{
          $result = 0;
        }
        if ($result == 0) {
          $status = "Not available";
        }elseif($result > 100){
           $status = "Hight risk";
        }elseif($result <= 100 || $event->jadwal_event > $datetime){
          $status = "Potensial risk";

        }elseif($result <= 100 || $event->jadwal_event < $datetime){
          $status = "On time";
        }

         $projectBudget = DB::table('project_budget')->select('part_number')->where('id_project',$id)->where('status',1)->groupBy('part_number')->get();
            foreach ($projectBudget as $key => $value) {
                    $getData[] = [
                        'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                        'get_qty'=>$get_qty = ProductSummary::where(['id_project'=>$id,'part_number'=>$value->part_number])->first(),
                        'qty'=>$get_qty ==true ? $get_qty->qty_order:0,
                        'total_plan'=>$budget_plan,
                        'total_actual'=>$budget_actual,
                        'product'=>$get_poduct->part_name,
                        'status'=>$status,
                        'budget'=>ProjectBudget::where(['id_project'=>$id,'part_number'=>$value->part_number])->get()

                    ];
                }


          $part_name_opex_get = null;
          $part_name_opex = [];
          $plan_budget_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->sum('total_price_plan');
          $actual_budget_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->sum('total_price_actual');
          $part_number_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->first();
          if($part_number_opex ==true){
            $part_name_opex = Product::where('part_number',$part_number_opex->part_number)->first();
            $part_name_opex_get = $part_name_opex->part_name;
          }

           $part_name_capex = [];
           $total_plan_capex = 0;
           $total_actual_capex = 0;
          $plan_budget_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->sum('total_price_plan');
          $actual_budget_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->sum('total_price_actual');
          $part_number_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->first();
          if($part_number_capex ==true){
            $part_name_capex = Product::where('part_number',$part_number_capex->part_number)->first();
          }
          if ($plan_budget_capex || $actual_budget_capex > 0) {
            $total_plan_capex = round($plan_budget_capex / $actual_budget_capex * 100);
            if ($total_plan_capex > 100) {
              $total_plan_capex = 100;
            }
            $total_actual_capex = round($actual_budget_capex / $plan_budget_capex * 100);
            if ( $total_actual_capex > 100) {
               $total_actual_capex = 100;
            }
          }
    return view('cost.budget',compact('part_name_opex_get','part_name_capex','total_plan_capex','total_actual_capex','part_name_opex','plan_budget_opex','actual_budget_opex','project','getData','status','result','budget_actual','budget_plan'));

    }
    public function cpi($id)
    {
         $project = TypeProject::where('id_project',$id)->first();
    $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
    $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();

    $get_performance = [];
    $get_cost_category = [];
    $product_name = [];
    $total = 0;
    $cost_performance = Cogm::where(['id_project'=>$id,'status'=>1])->get();
    foreach ($cost_performance as $key => $performance) {
      $get_performance[] = [
        'part_number'=>$part_number = $performance->part_number,
        'part_number_on_product'=>$IDproduct = Product::where(['id_project'=>$id,'part_number'=>$part_number])->first(),
        'get_part_name'=>$IDproduct ==true ? $IDproduct->part_name : 0
      ];
    }
    $cost_performance_category = Cogm::where('id_project',$id)->get();
    foreach ($cost_performance_category as $key => $cost_category) {
      $get_cost_category[] = [
        'id_category_event_project'=>$IDcategory = $cost_category->id_category_event_project,
        'get_id_category_event_project'=>$ID = CategoryEventProject::where('id_category_event_project',$IDcategory)->first(),
        'event_project_name'=>$ID->event_project_name,
      ]; 
    }
    $sum_cost_performance_bom = Cogm::where('id_project',$id)->sum('bom_cogm');
    $sum_cost_performance_process_cogm = Cogm::where('id_project',$id)->sum('process_cogm');
    $depreciation_cogm = Cogm::where('id_project',$id)->sum('depreciation_cogm');
    $total =  $sum_cost_performance_bom +  $sum_cost_performance_process_cogm +  $depreciation_cogm;
    //

    $IDbudget = ProjectBudget::where('id_project',$id)->pluck('part_number');
    foreach ($IDbudget as $key => $value) {
     $product_name[] = [

      'get_product_name'=>$IDproduct = Product::where('part_number',$value)->first(),
      'product_name'=>$IDproduct->part_name
     ];
    }
    $actual_budget = ProjectBudget::where('id_project',$id)->sum('total_price_actual');
    $plan_budget = ProjectBudget::where('id_project',$id)->sum('total_price_plan');


    return view('cost.cpi',compact('project','IDsector','IDcustomer','product_name','actual_budget','plan_budget','total','get_cost_category','get_performance'));
    }
    public function create_cogm($id)
    {
      $project = TypeProject::find($id);
      $product = Product::all();
      $event = CategoryEventProject::all();
      $cogm = Cogm::where(['id_project'=>$id,'status'=>0])->get();
      $cogms = Cogm::where(['id_project'=>$id,'status'=>0])->get();
      //return $cogm;
      return view('cost.form.cogm_create',compact('project','product','event','cogm','cogms'));
    }
    public function budget_create($id)
    {
      $getBudget = [];
      $project = TypeProject::find($id);
      $product = Product::all();
      $budget = ProjectBudget::where(['id_project'=>$id,'status'=>0])->get();
      foreach ($budget as $key => $value) {
        $getBudget[] = [
            'id_budget'=>$value->id_project_budget,
            'part_number'=>$value->part_number,
            'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
            'product'=>$get_poduct->part_name,  
            'item_invesment'=>$value->item_invesment,
            'funding'=>$value->funding,
            'category'=>$value->category,
            'price_of_item_plan'=>$value->price_of_item_plan,
            'qty_item_actual'=>$value->qty_item_actual,
            'uom_actual'=>$value->uom_actual,
            'total_price_plan'=>$value->total_price_plan,

        ];
      }
      
      return view('cost.form.budget_create',compact('project','product','getBudget'));
    }
   
public function get_qty($id)
   {
    $product = ProductSummary::where('part_number',$id)->first();
    return $product->qty_order;
   }
   public function post_budget(Request $request)
   {
    $data = new ProjectBudget;
    $data->id_project = $request->id_project;
    $data->part_number = $request->part_number;
    $data->item_invesment = $request->item_invesment;
    $data->funding = $request->funding;
    $data->category = $request->category;
    $data->price_of_item_plan = $request->price_of_item_plan;
    $data->qty_item_plan = $request->qty_item_plan;
    $data->uom_plan = $request->uom_plan;
    $data->total_price_plan = $request->total_price_plan;
    $data->status = 1;

    $data->qty_item_actual = $request->qty_item_plan;
    $data->uom_actual = $request->uom_plan;
    $data->price_of_item_actual = $request->price_of_item_plan;
    $data->total_price_actual = $request->total_price_plan;

    $data->save();

    if ($data) {
      // DB::table('project_budget_created')->insert(['id_project'=>$request->id_project,'project_budget_created'=>Carbon::now()]);
    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Admin Has Add');
    }else{
    $request->session()->flash('status', '400');
    $request->session()->flash('msg', 'Failed To Add');
    }
    return redirect()->back();

   }
   public function hapus_budget(Request $request,$id)
   {
    $data = ProjectBudget::where('id_project_budget',$id)->first();

    if ($data) {
      $data->delete();
      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin Has Add');
    }else{
      $request->session()->flash('status', '400');
      $request->session()->flash('msg', 'Failed To Add');
    }
     return redirect()->back();
   }
    public function post_cogm(Request $request)
   {
    //return $request->all();
    $data = new Cogm;
    $data->id_project = $request->id_project;
    $data->part_number = $request->part_number;
    $data->id_category_event_project = $request->id_category_event_project;
    $data->bom_cogm = $request->bom_cogm;
    $data->process_cogm = $request->process_cogm;
    $data->depreciation_cogm = $request->depreciation_cogm;
    $data->total_cogm = 100;
    $data->status = 0;

    $data->save();

    if ($data) {
    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Admin Has Add');
    }else{
    $request->session()->flash('status', '400');
    $request->session()->flash('msg', 'Failed To Add');
    }
    return redirect()->back();

   }
   public function save_budget(Request $request)
   {


    return $request->all();
   }
   public function save_all(Request $request)
   {
   
     foreach ($request->id_cogm as $key => $value) {
        $update = Cogm::where('id_cogm',$value)->update([
            'id_project'=>$request->id_project,
            'part_number'=>$request->part_number[$key],
            'id_category_event_project'=>$request->id_category_event_project[$key],
            'bom_cogm'=>$request->bom_cogm[$key],
            'process_cogm'=>$request->process_cogm[$key],
            'depreciation_cogm'=>$request->depreciation_cogm[$key],
            'total_cogm'=>100,
            'status'=>1,
         ]);
      }
        return redirect()->back();
   }
}
