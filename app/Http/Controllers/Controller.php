<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Notification;
use App\Http\Models\Masterdata\Process;
use Auth;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


   public static function checkNotif($tipe)
   {
    if($tipe =="document"){
         $query = Notification::where(['penerima'=>Auth::user()->id_pengguna,'tipe'=>$tipe,'status_read'=>'un_read'])->count();
    }else{
         $query = Notification::where(['penerima'=>Auth::user()->id_pengguna,'tipe'=>$tipe,'status_read'=>'un_read'])->count();
    }
   
    return $query;
   }
    public static function getProcess($project,$process)
    {
    	$data = DB::table('product')->where(['id_project'=>$project,'id_process'=>$process])->count();
    	return $data;
    }
    public static function getProcessId($project,$process)
    {
    	$data = DB::table('product')->where(['id_project'=>$project,'id_process'=>$process])->get();
    	$id = [];
    	foreach ($data as $key => $value) {
    		$id = $value->id_process;
    	}
    	return $id;
    }
    public function getProduct($name,$id)
    {
        if ($name =="part_number") {
            $query = Product::where('part_number',$id)->first();
            if ($query ==true) {
                return $query->part_name;
            }else{
                return "-";
            }
           
        }elseif($name =="id_process"){
            $query = Product::where('part_number',$id)->first();
            if (!empty($query)) {
                 return $query->id_process;
            }else{
                return null;
            }
           
        }else{
            $query = Process::where('id_process',$id)->first();
            if ($query ==true) {
                return $query->process_name;
            }else{
                return "-";
            }
           
        }
        
    }
    public function getTechnical($id_project,$part_number)
    {
        $data = DB::table('technical_review')->where(['id_project'=>$id_project,'part_number'=>$part_number])->count();
        return $data;
    }
    public function getPic($name,$id_project,$part_number)
    {
        if ($name =="pic") {
            $query = DB::table('member_project')->where(['id_project'=>$id_project,'part_number'=>$part_number,'id_position'=>3])->count();
            return $query;
        }else{
             $query = DB::table('member_project')->where(['id_project'=>$id_project,'part_number'=>$part_number,'id_position'=>2])->get();
            return $query;
        }
    }
    public function getName($username){
        $data = DB::table('tbl_pengguna')->where('username',$username)->first();
        $get = $data->firstname;
        return $get;
    }
    public function getPosition($id){
        $data = DB::table('position')->where('id_position',$id)->first();
       
        return $data->position_name;
    }
     public function getDepartement($id){
        $data = DB::table('tbl_pengguna')->where('username',$id)->first();
        $departement = DB::table('departement')->where('id_departement',$data->id_departement)->first();
        return $departement->departement;
    }
    public function getQuality($id,$part_number,$category)
    {
        $query = DB::table('quality')->where(['id_project'=>$id,'part_number'=>$part_number,'id_category_event_project'=>$category])->first();
        if (!empty($query)) {
            return $query;
        }else{
            return null;
        }
    }
    public function CheckProject($username,$id_project)
    {
        if(Auth::user()->hak_akses =="guest_system"){
            $query = 1;
        }else{
             $query = DB::table('member_project')->where('username',$username)->where('id_project',$id_project)->first();
        }
       
        if (is_null($query)) {
            return 'null';
        }else{
            return 'true';
        }
    }
    // public function bulanindo($id,$status)
    // {
    //     $query = [];
    //     if ($status =="day") {
    //         $daysSpanish = [
    //         01 => 'lunes',
    //         02 => 'martes',
    //         03 => 'miércoles',
    //         04 => 'jueves',
    //         4 => 'viernes',
    //         5 => 'sábado',
    //         6 => 'domingo',
    //         ];
    //         $weekday = $daysSpanish[$date->dayOfWeek]
    //     }else{

    //     }
    //     return
    // }
}
