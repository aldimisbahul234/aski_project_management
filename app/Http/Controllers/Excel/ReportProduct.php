<?php

namespace App\Http\Controllers\Excel;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Http\Models\Masterdata\MemberProject;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\Process;
use App\Http\Models\Masterdata\ItemControl;
use App\Http\Models\Masterdata\CategoryControllName as CategoryName;
use App\Http\Models\Masterdata\MasterItemControl;
use App\User;
class ReportProduct implements FromView, WithTitle
{

    private $id_project;
    private $part_number;

    public function __construct($id_project,$part_number)
    {
        $this->id_project = $id_project;
        $this->part_number = $part_number;

    }
    use Exportable;

    public function view(): View
    {
    $id_project = $this->id_project;
    $part_number = $this->part_number;
     //header
      $get_item = [];
      $get_name = [];
      $product = Product::where(['id_project'=>$id_project,'part_number'=>$part_number])->first();
      $leader = MemberProject::where(['id_project'=>$id_project,'part_number'=>$part_number,'id_position'=>2])->first();
      if($leader ==true){
      $get_name_leader = User::where('username',$leader->username)->first();
      if ($get_name_leader ==true) {
        $get_name = $get_name_leader->firstname.'&nbsp;'.$get_name_leader->lastname;
      }
    }
      //end header

      //item
      $category = CategoryName::where('part_number',$part_number)->get();
      foreach ($category as $key => $value) {
        $get_item[] = [
          'id_category'=>$value->id_category,
          'get_item_control'=>ItemControl::where(['id_project'=>$id_project,'part_number'=>$part_number,'id_category'=>$value->id_category])->get()
        ];
      }
      //end item

  
        return view('export.product.index',compact('get_item','product','get_name','part_number','id_project'));
    }
public function title(): string
    {
        return 'Buku Besar';
    }

}
