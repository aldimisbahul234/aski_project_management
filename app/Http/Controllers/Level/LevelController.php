<?php

namespace App\Http\Controllers\Level;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;

class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        
       return view('level.index');

    }

   
}
