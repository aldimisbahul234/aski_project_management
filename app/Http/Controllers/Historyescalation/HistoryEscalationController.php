<?php

namespace App\Http\Controllers\Historyescalation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\HistoryEscalation;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\TypeProject;
class HistoryEscalationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $get_data = [];
       $data = HistoryEscalation::all();   
       foreach ($data as $key => $value) {
           $get_data[] = [
            'id_escalation'=>$value->id_escalation,
            'escalation_name'=>$value->escalation_name,
            'tgl_escalation'=>$value->tgl_escalation,
            'pic'=>$value->pic,
            'get_partname'=>$get_partname = Product::where('part_number',trim($value->part_number))->first(),
            'part_name'=>$get_partname ==true ? $get_partname->part_name : null,
            'get_project'=>$get_project = TypeProject::where('id_project',$value->id_project)->first(),
            'project'=>$get_project ==true ? $get_project->project_name : null
           ];
       }
            
       return view('project.leader.historyescalation.index',compact('data','get_data'));

    }

   
}
