<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\Sector;
class DataSectorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Sector::all();
       //return $data;
       
       return view('masterdata.datasector.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new Sector;
        $sector->product_name = $request->product_name;
        $sector->box_color    = $request->box_color;
        $sector->logo         = $request->logo;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();

    }
    public function edit($id)
    {
        $data = Sector::find($id);

         return view('masterdata.datasector.edit',compact('data'));
    }
   public function update(Request $request,$id){
     $data = [
            'product_name' => $request->product_name,
            'box_color' => $request->box_color,
            'logo' => $request->logo
        ];

        $update = Sector::where('id_product',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('dataSector');
   }
    public function delete(Request $request, $id)
    {
         $model  = Sector::findOrFail($id);

        if ($model) {
            $model->delete();
            $user = Auth::user()->id;

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
}
