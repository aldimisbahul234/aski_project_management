<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\CategoryEventProject as Event;
class EventProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Event::all();
       //return $data;

       return view('masterdata.eventcategory.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new Event;
        $sector->event_project_name = $request->event_project_name;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect('eventProject');

    }
    public function edit($id)
    {

         $data = Event::find($id);
        return view('masterdata.eventcategory.edit',compact('data'));
    }
    public function update(Request $request,$id){
     $data = [
            'event_project_name' => $request->event_project_name,
           
        ];

        $update = Event::where('id_category_event_project',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('eventProject');
   }
    public function delete(Request $request, $id)
    {
         $model  = Event::findOrFail($id);

        if ($model) {
            $model->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
   
}
