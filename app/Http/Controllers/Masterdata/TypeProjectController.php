<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Masterdata\Customer;
use App\Http\Models\Masterdata\MemberProject;
use Auth;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\ItemControl;
class TypeProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = TypeProject::all();
       $customer = Customer::all();
       //\return $data;
       
       return view('masterdata.typeproject.index',compact('data','customer'));

    }

    public function store(Request $request)
    {
        $sector               = new TypeProject;
        $sector->project_name = $request->project_name;
        $sector->id_customer    = $request->id_customer;
      
        $insert = $sector->save();

        if ($insert) {
            MemberProject::create(['id_project'=>$sector->id_project,'username'=>Auth::user()->username,'id_position'=>1]);
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();

    }
    public function edit($id)
    {
          $data = TypeProject::find($id);
          $customer = Customer::all();
            return view('masterdata.typeproject.edit',compact('customer','data'));

    }
   public function update(Request $request,$id){
     $data = [
            'project_name' => $request->project_name,
             'id_customer' => $request->id_customer,
           
        ];

        $update = TypeProject::where('id_project',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('eventProject');
   }
    public function delete(Request $request, $id)
    {
         $model  = TypeProject::findOrFail($id);

        if ($model) {
            $model->delete();
            $product = Product::where('id_project',$id)->delete();
            $item = ItemControl::where('id_project',$id)->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
}
