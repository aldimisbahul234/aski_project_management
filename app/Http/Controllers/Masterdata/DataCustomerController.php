<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\Sector;
use App\Http\Models\Masterdata\Customer;
class DataCustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Customer::all();
       //return $data;
       $sector = Sector::all();
       return view('masterdata.datacustomer.index',compact('data','sector'));

    }

    public function store(Request $request)
    {
        //return $request->all();
        $sector               = new Customer;
        $sector->customer_name = $request->customer_name;
        $sector->id_product    = $request->id_sector;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();

    }
     public function edit($id)
    {

        $data = Customer::find($id);
       // return $data->customer_name;
        $sector = Sector::all();
         return view('masterdata.datacustomer.edit',compact('data','sector'));
    }
   public function update(Request $request,$id){
     $data = [
            'customer_name' => $request->customer_name,
            'id_product' => $request->id_product
        ];

        $update = Customer::where('id_customer',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('dataSector');
   }
    public function delete(Request $request, $id)
    {
         $model  = Customer::findOrFail($id);

        if ($model) {
            $model->delete();
            $user = Auth::user()->id;

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
   
}
