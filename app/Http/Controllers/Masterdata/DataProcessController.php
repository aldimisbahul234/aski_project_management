<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\Process;
class DataProcessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Process::all();
       return view('masterdata.dataprocess.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new Process;
        $sector->process_name = $request->process_name;
        $sector->box_color    = $request->box_color;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect('dataProcess');

    }
     public function edit($id)
    {
        $data = Process::find($id);
        
         return view('masterdata.dataprocess.edit',compact('data'));
    }
   public function update(Request $request,$id){
     $data = [
            'process_name' => $request->process_name,
            'box_color' => $request->box_color
        ];

        $update = Process::where('id_process',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('dataSector');
   }
    public function delete(Request $request, $id)
    {
         $model  = Process::findOrFail($id);

        if ($model) {
            $model->delete();
            $user = Auth::user()->id;

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
   
   
}
