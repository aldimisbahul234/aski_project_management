<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\Departement;
class DepartementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Departement::all();
       //return $data;

       return view('masterdata.departement.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new Departement;
        $sector->departement = $request->departement;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect('departement');

    }
public function edit($id)
{
      $data = Departement::find($id);
     return view('masterdata.departement.edit',compact('data'));
}
    
    public function update(Request $request,$id){
     $data = [
            'departement' => $request->departement,
           
        ];

        $update = Departement::where('id_departement',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('departement');
   }
}
