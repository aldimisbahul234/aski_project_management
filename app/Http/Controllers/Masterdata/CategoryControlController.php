<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\CategoryControll;
use App\Http\Models\Masterdata\Process;
class CategoryControlController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = CategoryControll::all();
       //return $data;
      
       return view('masterdata.categorycontroll.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new CategoryControll;
        $sector->category_list_name = $request->category_list_name;
        $sector->id_process    = $request->id_process;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect('categoryControll');

    }
     public function edit($id)
    {

        $data = CategoryControll::find($id);
        $process = Process::all();
        //return $data;
       // return $data->customer_name;
       
         return view('masterdata.categorycontroll.edit',compact('data','process'));
    }
   public function update(Request $request,$id){
     $data = [
            'category_list_name' => $request->category_list_name,
            'id_process' => $request->id_process
        ];
return $data;
        $update = CategoryControll::where('id_category_list',$id)->update($data);
        if ($update) {
         $request->session()->flash('status', '200');
         $request->session()->flash('msg', 'Data berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Data has been fail');
        }

       return redirect('categoryControll');
   }
    public function delete(Request $request, $id)
    {
         $model  = CategoryControll::findOrFail($id);

        if ($model) {
            $model->delete();
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Data has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Data');
        }

       return redirect()->back();
    }
   
}
