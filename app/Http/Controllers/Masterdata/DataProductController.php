<?php

namespace App\Http\Controllers\Masterdata;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\Product;
class DataProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data = Product::all();
       //return $data;

       return view('masterdata.dataproduct.index',compact('data'));

    }

    public function store(Request $request)
    {
        $sector               = new Sector;
        $sector->product_name = $request->product_name;
        $sector->box_color    = $request->box_color;
        $sector->logo         = $request->logo;
    
        $insert = $sector->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect('dataSector');

    }
   
}
