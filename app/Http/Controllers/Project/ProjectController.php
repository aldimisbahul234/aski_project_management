<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Notification;
use App\Http\Models\Masterdata\Sector;
use App\Http\Models\Masterdata\Customer;
use App\Http\Models\Masterdata\MemberProject;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\Process;
use App\Http\Models\Masterdata\ItemControl;
use App\Http\Models\Masterdata\ProjectSummary as Summary;
use App\Http\Models\Masterdata\CategoryControllName as CategoryName;
use App\Http\Models\Masterdata\CategoryEventProject;
use App\Http\Models\Masterdata\EventProject;
use App\Http\Models\Masterdata\ProductSummary;
use App\Http\Models\Masterdata\HistoryEvent;
use App\Http\Models\Masterdata\MasterItemControl;
use App\Http\Models\Masterdata\Cogm;
use App\Http\Models\Masterdata\ProjectBudget;
use App\Http\Models\Masterdata\DocumentProject;
use App\Http\Models\Masterdata\CategoryControll;
use App\Http\Models\Masterdata\ProcessSpek;
use App\Http\Models\Masterdata\TechnicalReview;
use App\Http\Models\Masterdata\DocumentRfq;
use App\Http\Models\Masterdata\Quality;
use App\Http\Models\Masterdata\DimensiQuality;
use App\Http\Models\Masterdata\VisualQuality;
use App\Http\Models\Masterdata\Approval;
use App\Http\Models\Tr\MaterialSpec;
use App\Http\Models\Tr\BasicSpek;
use App\Http\Models\Tr\BuildingSpec;
use App\Http\Models\Tr\CatatanTr;
use App\Http\Models\Tr\ComponenInsert;
use App\Http\Models\Project\Issue;
use App\Http\Models\Tr\Guage;
use App\Http\Models\Tr\ListInvesment;
use App\Http\Models\Tr\OtherSpec;
use App\Http\Models\Tr\Packaging;
use App\Http\Models\Tr\TestingRequirement;
use App\Http\Models\Tr\UtilitySpec;
use App\Http\Models\Masterdata\GuageCustom;
use App\Http\Models\Masterdata\UtilityCustom;
use PDF;
use Carbon\Carbon;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use App\User;
use DateTime;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Excel\ReportProduct;
use Illuminate\Support\Str;
use PHPMailerPHPMailerPHPMailer;
use PHPMailerPHPMailerException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\POP3;
use Twilio\Rest\Client;
class ProjectController extends Controller
{
    /**
       * Code By :
       * @Aldi Misbahul Kabir :)
    */

    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    public function index(Request $request)
    {
      if (Auth::user()->hak_akses =='guest') {
        return redirect('/pic');
      }
      $data = Sector::all();

      $sector = [];
       foreach ($data as $key => $value) {
           $sector[] = [
            'id_product'=>$value->id_product,
            'customers'=>$customer = Customer::where('id_product',$value->id_product)->pluck('id_customer'),
            'project'=>$customer ==true ? $id_project = TypeProject::whereIn('id_customer',$customer)->pluck('id_project') : null,
            'notif'=> $id_project ==true ? Notification::whereIn('id_project',$id_project)->where('penerima',Auth::user()->id_pengguna)->where('status_read','un_read')->count() : null,
            'sector'=>$value->product_name,
            'color'=>$value->box_color,
            'logo'=>$value->logo,
            'customer'=>Customer::where('id_product',$value->id_product)->count()
           ];
       }
       //return $sector;
      return view('project.index',compact('sector'));
    }
    public function checktime()
    {
      $hp = [];
       $tomorrow = Carbon::tomorrow()->format('Y-m-d'); 
       $get_data = array();
       $fix_data = array();
       $id_control = DB::table('item_control')->whereDate('actual_finish_item',$tomorrow)->where('status_reminder',null)->pluck('id_control');
       $pic = DB::table('item_control')->whereIn('id_control',$id_control)->get();
       foreach ($pic as $key => $value) {

          $get_data[] = [
          'id_control'=>$value->id_control,
          'get_item_name'=>$get_item_name = DB::table('master_item_control')->where('id_item',$value->id_item)->first(),
          'item_name'=>$get_item_name == true ? $get_item_name->item_name : null,
          'get_part_name'=>$get_part_name = DB::table('product')->where('part_number',trim($value->part_number))->first(),
          'part_name'=>$get_part_name == true ? $get_part_name->part_name : null,
          'id_project'=>$value->id_project,
          'get_type_project'=> $type_project = DB::table('project_type')->where('id_project',$value->id_project)->first(),
          'type_project'=> $type_project == true ? $type_project->project_name : null,
          'pic_item'=>$value->pic_item,
          'get_phone'=>$get_phone = DB::table('tbl_pengguna')->where('username',$value->pic_item)->first(),
          'phone'=>$get_phone == true ? $get_phone->telepon : null
         ];
       }
       foreach ($get_data as $key => $values) {
         $get = str_replace(" ","",$values['phone']);
         $get = str_replace("(","",$values['phone']);
         $get = str_replace(")","",$values['phone']);
         $get = str_replace(".","",$values['phone']);

         // cek apakah no hp mengandung karakter + dan 0-9
         if(!preg_match('/[^+0-9]/',trim($get))){
             // cek apakah no hp karakter 1-3 adalah +62
             if(substr(trim($get), 0, 3)=='+62'){
                 $hp = trim($get);
             }
             // cek apakah no hp karakter 1 adalah 0
             elseif(substr(trim($get), 0, 1)=='0'){
                 $hp = '+62'.substr(trim($get), 1);
             }
         }
         $fix_data[] = [
          'id_control'=>$values['id_control'],
          'phone'=>$hp,
          'pic'=>$values['pic_item'],
          'type_project'=>$values['type_project'],
          'part_name'=>$values['part_name'],
          'item_name'=>$values['item_name']
         ];

       }

        $sid    = "ACe8f85166bc65189c3c377612f92a62a1";
        $token  = "b404fe2eafc7812343d964f2cccb4559";
        $wa_from= "+14155238886";
        foreach ($fix_data as $key => $da) {

            $twilio = new Client($sid, $token);
            $body = 'Hai, '.$da['pic'].' mohon segera lakukan update untuk item control ('.$da['item_name'].'), PART NAME ('.$da['pic'].') - PROJECT('.$da['type_project'].') TERIMAKASIH';
            $send =  $twilio->messages->create("whatsapp:".$da['phone']."",["from" => "whatsapp:$wa_from", "body" => $body]);
             DB::table('cron')->insert(['name'=>$da['pic'],'create_by'=>Carbon::now(),'media'=>'whatsapp','tipe'=>'item_control','id_control'=>$da['id_control'],'send_to'=>$da['pic'],'phone'=>$da['phone']]);
        }
        DB::table('item_control')->whereIn('id_control',$id_control)->where('status_reminder',null)->update(['status_reminder'=>1]);
       
       return $send;
   
      //return $fix_data;
    }
    public function checkwa()
    {
        $sid    = "ACe8f85166bc65189c3c377612f92a62a1";
        $token  = "b404fe2eafc7812343d964f2cccb4559";
        $wa_from= "+14155238886";
        $twilio = new Client($sid, $token);
        
        $body = "Hai, event project KSAJ* mu 2 hari lagi nih, segera lakukan pengecekan";

        return $twilio->messages->create("whatsapp:+6281572741935",["from" => "whatsapp:$wa_from", "body" => $body]);
    } 
    public function read_notif(Request $request, $status, $id)
    {
      if ($status =='item') {
       $update = Notification::where(['penerima'=>Auth::user()->id_pengguna,'id_item'=>$id])->update(['status_read'=>'read']);
      }elseif($status =='issue'){
         $update = Notification::where(['penerima'=>Auth::user()->id_pengguna,'id_issue'=>$id])->update(['status_read'=>'read']);
      }else{
         $update = Notification::where(['penerima'=>Auth::user()->id_pengguna,'document_code'=>$id])->update(['status_read'=>'read']);
      } 
      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been read');
      return redirect()->back();

    }
    public function update_status_item(Request $request, $id_item,$status)
    {
      if ($status =='finish') {
        $item = ItemControl::where('id_item',$id_item)->update(['status_item'=>2]);
      }else{
        $item = ItemControl::where('id_item',$id_item)->update(['status_item'=>1]);
      }
      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been added');
      return redirect()->back();
    }
    public function get_answer_pic(Request $request)
    {
      $data = Issue::where('id_issue',$request->id_issue)->first();

      return $data;
    }
    public function download_excel_product($id_project,$part_number)
    {

      $product = Product::where('part_number',$part_number)->first();

      return Excel::download(new ReportProduct($id_project,$part_number), 'Report product '.$product->part_name.'.xlsx');
      //return $get_name_leader;
    }
    public function customer($id)
    {
       $header = Sector::where('id_product',$id)->first();
      $customer = Customer::where('id_product',$id)->get();
      $data = [];
      foreach ($customer as $key => $value) {
       $data[] = [
        'id_customer'=>$value->id_customer,
        'customer'=>$value->customer_name,
        'project'=>TypeProject::where('id_customer',$value->id_customer)->count(),
        'get_project'=> $id_project = TypeProject::where('id_customer',$value->id_customer)->pluck('id_project'),
        'notif'=> $id_project ==true ? Notification::whereIn('id_project',$id_project)->where('penerima',Auth::user()->id_pengguna)->where('status_read','un_read')->count() : null,
       ];
      }
     // return $data;
      return view('project.customer',compact('data','header'));
    }
    public function customerDetail($id,$id_product)
    {

       $today = Carbon::now()->format('Y-m-d');
      if(Auth::user()->hak_akses =="administrator"){
          $header = Sector::where('id_product',$id_product)->first();
          $customer = Customer::where('id_customer',$id)->first();
          $project = TypeProject::where('id_customer',$id)->get();
          $process = Process::all();
          $proc = Process::all();

          foreach ($project as $key => $value) {
          $data[] = [
            'id'=>$value->id_project,
            'project_name'=>$value->project_name,
            'count_part'=>Product::where('id_project',$value->id_project)->count()
          ];
        }
       return view('project.customerDetail',compact('project','customer','data','process','proc','header'));

     }else{
          $header = Sector::where('id_product',$id_product)->first();
          $customer = Customer::where('id_customer',$id)->first();
          $data = [];
          $project = TypeProject::where('id_customer',$id)->get();
          $process = Process::all();
          $proc = Process::all();
          $get_item = [];
          $dateNow = Carbon::now()->format("Y-m-d");
          foreach ($project as $key => $value) {
          $data[] = [
              'id'=>$ids = $value->id_project,
              'check'=>$this->CheckProject(Auth::user()->username,$value->id_project),
              'customer'=>$customer->id_customer,
              'project_name'=>$value->project_name,
              'count_product'=>$count_product = ItemControl::where('id_project',$value->id_project)->count(),
              'plan_progress_part'=>$plan_progress_part = ItemControl::where('id_project',$value->id_project)->sum('plan_progress_item'),
              'result_plan'=> $plan_progress_part || $count_product > 0 ?round($plan_progress_part / $count_product) : 0,
              'actual_progress_part'=>$actual_progress_part = ItemControl::where('id_project',$value->id_project)->sum('actual_progress_item'),
              'result_actual'=>$actual_progress_part ||  $count_product > 0 ? round($actual_progress_part / $count_product) : 0,
              'hitung_product'=>$hitung_product = Product::where('id_project',$ids)->count(),
              'count_part'=>Product::where('id_project',$value->id_project)->count(),
              'part_number'=>$part_number = Product::where('id_project',$value->id_project)->first(),
              'get_item'=>$part_number ==true ? $get_item = ItemControl::where(['part_number'=>$part_number->part_number,'id_project'=>$value->id_project])->first() : null,
              'last_update_item'=>$get_item ==true ? $get_item->last_update_item : null,
              'notif'=>Notification::where(['id_project'=>$value->id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read'])->count()
            ];
          }
        // return $data;
          $get_grafik_progress_project = [];
          $get_grafik_progress_actual = [];
           $grafik_progress_actual = TypeProject::where('id_customer',$id)->get();
           foreach ($grafik_progress_actual as $key => $grafik_progress_actual) {
              $get_grafik_progress_project[] = [
                  'id_project'=>$grafik_progress_actual->id_project,
                  'project_name'=>$grafik_progress_actual->project_name,
              ];
               $get_grafik_progress_actual[] = [
                  'id_project'=>$grafik_progress_actual->id_project,
                  'count_product'=>$count_product = Product::where('id_project',$grafik_progress_actual->id_project)->where('status_product','!=',0)->count(),
                  'actual_product'=>$count_product > 0 ? $actual_product = Product::where('id_project',$grafik_progress_actual->id_project)->sum('actual_progress_part'):0,
                  'sum_actual'=>$count_product > 0  ? round($actual_product / $count_product) : 0
               ];
           }

           $get_event_name_current = null;
           $current = null;
           $event_name = null;
           $getProject = [];
            $project2 = TypeProject::where('id_customer',$id)->get();
            foreach ($project2 as $key => $value) {
                $getProject[] = [
                    'id_project'=>$value->id_project,
                    'get_product'=>$get_product = Product::where('id_project',$value->id_project)->first(),
                    'part_number'=>$get_product ==true ? $get_product->part_number:null,
                    'project_name'=>$value->project_name,
                    'get_event_current'=>$get_event_current = EventProject::where('id_project',$value->id_project)->where('jadwal_event','<',$today)->first(),
                    'get_event_name_current'=> $get_event_current ==true ? $get_event_name_current = CategoryEventProject::where('id_category_event_project',$get_event_current->id_category_event_project)->first() : "-",
                    'event_name_current'=>$get_event_name_current ==true ? $get_event_name_current->event_project_name : "-",
                    'current'=> $get_event_current ==true ? $current = $get_event_current->jadwal_event : "-",
                    'get_current'=>$today > $current ? "Until Now":"Today",


                    'get_event'=>$get_event = EventProject::where('id_project',$value->id_project)->where('jadwal_event','>',$today)->first(),
                    'event_name'=> $get_event ==true ? $event_name = CategoryEventProject::where('id_category_event_project',$get_event->id_category_event_project)->first() : "-",
                    'event_name'=>$event_name ==true ? $event_name->event_project_name : '-',
                    'current'=> $get_event ==true ? $current = $get_event->jadwal_event : "-",
                    'next'=> $get_event ==true ? $next = Carbon::now()->startOfDay()->diffInDays($get_event->jadwal_event, false) : "-",
                    "count_item"=>$count_item = ItemControl::where('id_project',$value->id_project)->count('part_number'),
                    "sum_actual"=>$sum_actual = ItemControl::where('id_project',$value->id_project)->sum('actual_progress_item'),
                    "total_actual"=>$count_item > 0 ? round($sum_actual / $count_item) : 0,
                    "get_product"=>Product::where('id_project',$value->id_project)->get(),
                ];
            }
//return $getProject;
             //grafik budget 
      $get_grafik_budget_sum = [];
      $get_grafik_budget = [];
      $grafik_budget = TypeProject::where('id_customer',$id)->get();
      foreach ($grafik_budget as $key => $grafik_budget) {
          $get_grafik_budget[] = [
            'id_project'=>$grafik_budget->id_project,
            'project_name'=>$grafik_budget->project_name,
             ];
        $get_grafik_budget_sum[] = [
            'price_plan'=>$price_plan = ProjectBudget::where('id_project',$grafik_budget->id_project)->sum('total_price_plan'),
            'price_actual'=>$price_actual = ProjectBudget::where('id_project',$grafik_budget->id_project)->sum('total_price_actual'),
            'total'=>$price_actual > 0 || $price_plan > 0 ? round($price_actual / $price_plan * 100) : 0
         ];
      }


      $get_grafik_cost = [];
      $grafik_cost = TypeProject::where('id_customer',$id)->get();
      foreach ($grafik_cost as $key => $grafik_cost) {
          $get_grafik_cost[] = [
            'id_project'=>$grafik_cost->id_project,
            'project_name'=>$grafik_cost->project_name
          ];
      }
      // return $get_grafik_progress_project;
       return view('project.leader.index',compact('get_grafik_budget_sum','get_grafik_cost','get_grafik_budget','getProject','get_grafik_progress_project','get_grafik_progress_actual','project','customer','data','process','proc','header','dateNow','today'));
     }

    }
    public function export_process($sectorID,$projectID,$processID)
    {
      $data = Product::where(['id_project'=>$projectID,'id_process'=>$processID])->get();
      $project = TypeProject::where('id_project',$projectID)->first();
      $customer = Customer::where('id_customer',$project->id_customer)->first();
      $sector = Sector::where('id_product',$sectorID)->first();
      $datetime = Carbon::now()->format('d F Y');

      $pdf = PDF::loadview('project.export.export_process',['datetime'=>$datetime,'sector'=>$sector,'project'=>$project,'data'=>$data,'customer'=>$customer]);
      return $pdf->download('laporan-part-pdf.pdf');
    }
    public function export_all($sectorID,$projectID)
    {
      $data = Product::where('id_project',$projectID)->get();
      $project = TypeProject::where('id_project',$projectID)->first();
      $customer = Customer::where('id_customer',$project->id_customer)->first();
      $sector = Sector::where('id_product',$sectorID)->first();
      $datetime = Carbon::now()->format('d F Y');

      $pdf = PDF::loadview('project.export.export_process',['datetime'=>$datetime,'sector'=>$sector,'project'=>$project,'data'=>$data,'customer'=>$customer]);
      return $pdf->download('laporan-part-pdf.pdf');
    }
    public function edit_project($sector,$customer,$id)
    {
      $header = Sector::where('id_product',$sector)->first();
      $customer = Customer::where('id_customer',$customer)->first();
      $project = TypeProject::where('id_project',$id)->first();
        $data = Product::where('id_project',$id)->get();
        return view('project.edit_part',compact('data','header','customer','project'));
    }
     public function edit_part_list($sector,$project,$id)
    {
        $header = Sector::where('id_product',$sector)->first();
        $project = TypeProject::where('id_project',$project)->first();
        $customer = Customer::where('id_customer',$project->id_customer)->first();
        $data = Product::where('part_number',$id)->first();

        return view('project.form_edit_part',compact('data','header','project','customer'));
    }
   public function product_detail($projectID,$customerID)
   {

    $customer = Customer::where('id_customer',$customerID)->first();
    $header = Sector::where('id_product',$customer->id_product)->first();

    $product = Product::where('id_project',$projectID)->get();
    $project = TypeProject::where('id_project',$projectID)->first();

    return view('project.leader.product_detail',compact('projectID','product','project','customer','header'));
   }

   public function product_category_control($part_number,$id_process,$id_project)
   {

 $datas = [];
 $category_list = CategoryControll::all();
  $product = Product::where('part_number',$part_number)->first();
  $project = TypeProject::where('id_project',$id_project)->first();
  $customer = Customer::where('id_customer',$project->id_customer)->first();
  $process = Process::where('id_process',$id_process)->first();

    $data = CategoryName::where(['part_number'=>$part_number,'id_process'=>$id_process])->get();
    $id_item_control = null;
   foreach ($data as $key => $value) {
     $datas[] = [
      'id_category'=>$value->id_category,
      'name'=>$value->category_name,
      'part_number'=>$value->part_number,
      'open_item'=>$open_item = ItemControl::where(['id_project'=>$id_project,'id_category'=>$value->id_category,'part_number'=>$part_number,'status_item'=>1])->where('actual_progress_item','<',100)->get(),
      'get_item'=>count($open_item) > 1? $open_item->pluck('id_item') : 0,
      'count_item'=>$countItem = ItemControl::where(['id_project'=>$id_project,'id_category'=>$value->id_category,'part_number'=>$part_number,'status_item'=>1])->count(),
      'sum_plan'=>$sum_plan = ItemControl::where(['id_project'=>$id_project,'id_category'=>$value->id_category,'part_number'=>$part_number])->sum('plan_progress_item'),
       'sum_actual'=>$sum_actual = ItemControl::where(['id_project'=>$id_project,'id_category'=>$value->id_category,'part_number'=>$part_number])->sum('actual_progress_item'),
      'result_plan'=> $sum_plan || $countItem  > 0 ? round($sum_plan / $countItem,2) : 0,
      'result_actual'=> $sum_actual || $countItem  > 0 ? round($sum_actual / $countItem,2) : 0,
       'pluck_id_item'=>$id_item_control = ItemControl::where(['part_number'=>$value->part_number,'id_category'=>$value->id_category])->pluck('id_item'),
      'get_notif'=>Notification::whereIn('id_item',$id_item_control)->where(['id_project'=>$id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'tipe'=>'item','status_read'=>'un_read'])->count()

     ];
   }
//return $datas;
   return view('project.leader.product_category_control',compact('customer','datas','id_project','product','project','process','id_process','part_number','category_list'));
   }
   public function add_category_control(Request $request)
   {
   $data = CategoryName::create([
    'part_number'=>$request->part_number,
    'id_process'=>$request->id_process,
    'category_name'=>$request->category_name,
    'status'=>1,
    'dateandtime'=>Carbon::now()
   ]);
    if ($data) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }
   return redirect()->back();
   }
   public function get_item_list($part_name,$id_category,$id_project)
   {

    $data = ItemControl::where(['id_project'=>$id_project,'id_category'=>$id_category,'part_number'=>$part_name])->get();
    $id_process = Product::where(['id_project'=>$id_project,'part_number'=>$part_name])->first();
    $get = CategoryName::where('id_category',$id_category)->first();
    $category  = CategoryControll::where('category_list_name',$get->category_name)->first();



     return view('project.leader.get_item_list',compact('id_process','id_category','data','category','id_project'));
   }
   public function item_control_detail($id_project,$id_category,$part_number,$id_process)
   {

    //return $id_category;
    $data = ItemControl::where(['id_project'=>$id_project,'id_category'=>$id_category,'part_number'=>$part_number])->get();
    $get = CategoryName::where('id_category',$id_category)->first();
    $category  = CategoryControll::where('category_list_name',$get->category_name)->first();
    $product = Product::where('part_number',$part_number)->first();
    $project = TypeProject::where('id_project',$id_project)->first();
  //]return $product;
    return view('project.leader.item_control_detail',compact('project','product','id_process','data','id_category','category','id_project','part_number'));
   }
   public function display_project_detail($id_customer,$id_project)
   {

    //dcoument rfq modal
    $modal_rfq_document = DocumentRfq::where('id_project',$id_project)->get();

     $datetime = Carbon::now()->format('d F Y');
    $manager = MemberProject::where(['id_project'=>$id_project,'id_position'=>1])->first();
    $getmanager = User::where('username',$manager->username)->first();

    $project = TypeProject::where('id_project',$id_project)->first();
    $customer = Customer::where('id_customer',$id_customer)->first();
    $header = Sector::where('id_product',$customer->id_product)->first();
    //general
    $summary = Summary::where(['id_project'=>$id_project,'id_customer'=>$id_customer])->get();
   //event
    $event = EventProject::where('id_project',$id_project)->orderBy('created_at','ASC')->get();
    $event_all = CategoryEventProject::all();

    //rfq
    $rfq = ProductSummary::where(['id_project'=>$id_project,'status_view'=>1])->get();
    $data_rfq = [];
      foreach ($rfq as $key => $value) {
      $data_rfq[] =[
        'part_number'=>$value->part_number,
        'part_name'=> $this->getProduct('part_number',$value->part_number),
        'id_process_product'=> $id_process = $this->getProduct('id_process',$value->part_number),
        'process_name'=> $this->getProduct('process_name',$id_process),
        'qty'=>$value->qty_order,
        'lifetime'=>$value->lifetime,
        'remarks'=>$value->remarks
      ];
      }
//technical
      $technical = ProductSummary::where(['id_project'=>$id_project,'status_view'=>1])->get();
      $data_technical = [];
      foreach ($technical as $key => $value) {
      $data_technical[] =[
        'part_number'=>$value->part_number,
        'part_name'=> $this->getProduct('part_number',$value->part_number),
        'technical_review'=> $this->getTechnical($id_project,$value->part_number),
      ];
      }
//
// structure
      $structure = Product::where('id_project',$id_project)->get();
      $data_structure = [];
      foreach ($structure as $key => $value) {
        $data_structure[] = [
          'part_number'=>$value->part_number,
          'part_name'=>$value->part_name,
          'id_process_product'=> $id_process = $this->getProduct('id_process',$value->part_number),
          'id_process'=>$id_process,
          'process_name'=> $this->getProduct('process_name',$id_process),
          'count_pic'=>$this->getPic('pic',$id_project,$value->part_number),
          'get_leader'=>$this->getPic('leader',$id_project,$value->part_number)
        ];
      }
  $all_member = MemberProject::where('id_project',$id_project)->get();
 // return $all_member;
      $all_member_get = [];
      foreach ($all_member as $key => $value) {
        $all_member_get[] = [
          'id_member'=>$value->id_member,
          'name'=>$this->getName($value->username),
          'process_name'=> $this->getProduct('process_name',$value->id_process),
          'position'=> $this->getPosition($value->id_position),
          'part_name'=> $this->getProduct('part_number',$value->part_number),
          'departement'=> $this->getDepartement($value->username),
          'availability'=>$value->availability,
          'standar_rate'=>$value->standar_rate
        ];
      }

//return $data_structure;

     //product
        $product = Product::where('id_project',$id_project)->get();
        $data_product = [];
      foreach ($product as $key => $value) {
        $data_product[] = [
          'part_number'=>$value->part_number,
          'part_name'=>$value->part_name,
          'id_process_product'=> $id_process = $this->getProduct('id_process',$value->part_number),
          'process_name'=> $this->getProduct('process_name',$id_process),
          'get_leader'=>$this->getPic('leader',$id_project,$value->part_number),
          'count_tem'=>$countItem = ItemControl::where('part_number',$value->part_number)->count(),
          'sum_plan'=>$sum_plan = ItemControl::where('part_number',$value->part_number)->sum('plan_progress_item'),
          'sum_actual'=>$sum_actual = ItemControl::where('part_number',$value->part_number)->sum('actual_progress_item'),
          'result_plan'=>$sum_plan || $countItem  > 0 ? round($sum_plan / $countItem,2) : 0,
          'result_actual'=>$sum_actual || $countItem  > 0 ? round($sum_actual / $countItem,2) : 0,
          'notif_issue'=>Notification::where(['id_project'=>$id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'tipe'=>'issue','status_read'=>'un_read'])->count(),
          'notif_item'=>Notification::where(['id_project'=>$id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'tipe'=>'item','status_read'=>'un_read'])->count()
        ];
      }
//return $data_product;
//quality
      $quality = [];
      $get_qualities = null;
      $approval_line = null;
      $line_done = [];
 $get_quality = Product::where('id_project',$id_project)->get();
 foreach ($get_quality as $key => $get_quality) {
    $quality[] = [
      'part_number'=>$get_quality->part_number,
      'part_name'=>$get_quality->part_name,
      'get_jadwal_event'=>$get_jadwal_event = EventProject::getJadwalEvent($id_project,$datetime),
      'id_category_event_project'=>$get_jadwal_event ==true ? $get_jadwal_event->id_category_event_project:null,
      'event_name'=>$get_jadwal_event ==true ? $get_jadwal_event->category_name->event_project_name:null,
      'current'=>$get_jadwal_event ==true ? $get_jadwal_event->jadwal_event > $datetime ? "Until Now" : "N/A":null,
      'quality'=> $get_jadwal_event ==true ? $get_qualities = Quality::where(['part_number'=>$get_quality->part_number,'id_project'=>$id_project,'id_category_event_project'=>$get_jadwal_event->id_category_event_project])->first() : null,
      'adjustment_visual'=>$get_qualities ==true ? $get_qualities->adjustment_visual : null,
      'adjustment_dimensi'=>$get_qualities ==true ? $get_qualities->adjustment_dimensi : null,
      'trial_number'=>$get_qualities ==true ? $get_qualities->trial_number : null,
      'approval_line'=>$get_jadwal_event ==true ? $approval_line = Approval::where(['part_number'=>$get_quality->part_number,'id_project'=>$id_project,'id_category_event_project'=>$get_jadwal_event->id_category_event_project])->first() : null,
      'line_done'=>$approval_line ==true ? $line_done = round((($approval_line->done1 + $approval_line->done2 + $approval_line->done3 + $approval_line->done4 + $approval_line->done5 + $approval_line->done6 + $approval_line->done7 + $approval_line->done8 + $approval_line->done9 + $approval_line->done10 + $approval_line->done11 + $approval_line->done12 + $approval_line->done13) / 52) * 100) : 0,
      'line_eject'=>$approval_line ==true ? round((($approval_line->eject1 + $approval_line->eject2 + $approval_line->eject3 + $approval_line->eject4 + $approval_line->eject5 + $approval_line->eject6 + $approval_line->eject7 + $approval_line->eject8 + $approval_line->eject9 + $approval_line->eject10 + $approval_line->eject11 + $approval_line->eject12 + $approval_line->eject13) / 52) * 100) : 0,
      'line_silang'=>$approval_line ==true ? round((($approval_line->silang1 + $approval_line->silang2 + $approval_line->silang3 + $approval_line->silang4 + $approval_line->silang5 + $approval_line->silang6 + $approval_line->silang7 + $approval_line->silang8 + $approval_line->silang9 + $approval_line->silang10 + $approval_line->silang11 + $approval_line->silang12 + $approval_line->silang13) / 52) * 100) : 0,
      'status_line_done'=> $line_done > 72 ? $status_line_done = "OK" : "NG",
    ];
  }
 //return $quality;
//document

       $document = Product::where('id_project',$id_project)->get();
       $data_document = [];
      foreach ($document as $key => $value) {
        $data_document[] = [
          'part_number'=> $part_number = $value->part_number,
          'part_name'=>$part_name = $value->part_name,
          'countdocument' =>$countdocument = DocumentProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->count(),
         'progress'=>DocumentProject::sumProgress(DocumentProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->get(),$countdocument),
         'notif'=>Notification::where('id_project',$id_project)->where('part_number',$value->part_number)->where('penerima',Auth::user()->id_pengguna)->where('status_read','un_read')->count()
        ];
      }
//return $data_document;

      //cost
      $cogm = DB::table('cogm')->where('id_project',$id_project)->count();
      $budget = DB::table('project_budget')->where(['id_project'=>$id_project,'status'=>1])->count();


      //analistyc
    $analistyc = Product::where('id_project',$id_project)->get();
    $data_analistyc = [];
      foreach ($analistyc as $key => $value) {
        $data_analistyc[] = [
          'part_number'=>$value->part_number,
          'part_name'=>$value->part_name,
        ];
      }
      $count_notif_product = Notification::where(['id_project'=>$id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read'])->count();
      $count_notif_document = Notification::where(['id_project'=>$id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'document'])->count();
// return $count_notif_product;

      return view('project.leader.display_project_detail',compact('project','customer','summary','event','data_rfq','technical','data_structure','data_technical','data_product','data_document','data_analistyc','cogm','budget','header','getmanager','event_all','all_member_get','quality','modal_rfq_document','count_notif_product','count_notif_document'));
   }
   public function delete_project_summary(Request $request,$id_project,$id_customer)
   {
      $summary = Summary::where(['id_project'=>$id_project,'id_customer'=>$id_customer])->delete();
      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been delete');
            return redirect()->back();
   }
   public function add_part($projectID)
   {
     $project = TypeProject::where('id_project',$projectID)->first();
       return view('project.add_part',compact('project'));
   }
   public function add_event(Request $request)
   {
     //return $request->all();
    $insert = DB::table('event_project')->insert([
      'id_category_event_project'=>$request->id_category_event_project,
      'id_project'=>$request->id_project,
      'jadwal_event'=>$request->jadwal_event,
      'actual_event'=>$request->actual_event,
      'finish_plan_event'=>$request->finish_plan_event,
      'finish_actual_event'=>$request->finish_actual_event
    ]);
       if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();
   }
   public function add_rfq(Request $request)
   {
    $project = TypeProject::where('id_project',$request->id_project)->first();
    $customer = Customer::where('id_customer',$project->id_customer)->first();
      DB::beginTransaction();
     try {

       DB::table('product_summary')->insert([
      'part_number'=>$request->part_number,
      'id_project'=>$request->id_project,
      'qty_order'=>$request->qty_order,
      'lifetime'=>$request->lifetime,
      'remarks'=>$request->remarks,
      'status_view'=>1,
     ]);

    DB::table('product')->insert([
       'part_number'=>$request->part_number,
       'part_name'=>$request->part_name,
       'id_project'=>$request->id_project,
       'id_process'=>$request->id_process,
       'tgl_masuk'=>Carbon::now()->format('Y-m-d'),
       'plan_progress_part'=>1,
       'actual_progress_part'=>1,
       'status_product'=>1
    ]);
      DB::commit();
     } catch (\Exception $e) {
      DB::rollback();
        $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Part number sudah ada');
         return redirect('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project);

    }

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added rfq');


        return redirect('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project);
   }
   public function create_rfq($id)
   {
      $project = TypeProject::where('id_project',$id)->first();
      $customer = Customer::where('id_customer',$project->id_customer)->first();
      $process = Process::all();
       return view('project.leader.form.create_rfq',compact('project','process','customer'));
   }
   public function edit_rfq($id)
   {
        $data = ProductSummary::where('part_number',$id)->first();
        $project = TypeProject::where('id_project',$data->id_project)->first();
        $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
        $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();
       return view('project.leader.form.edit_rfq',compact('data','project','IDcustomer','IDsector'));
   }
   public function update_rfq($id)
   {
        $data = ProductSummary::where('part_number',$id)->update($request->all());
         if ($data) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();
   }
   public function export_rfq($id)
   {

    $rfq = ProductSummary::where(['id_project'=>$id,'status_view'=>1])->get();
    $project = TypeProject::where('id_project',$id)->first();
    $customer = Customer::where('id_customer',$project->id_customer)->first();
    $sector = Sector::where('id_product',$customer->id_product)->first();
    $datetime = Carbon::now()->format('d F Y');
    $data = [];
      foreach ($rfq as $key => $value) {
      $data[] =[
        'part_number'=>$value->part_number,
        'part_name'=> $this->getProduct('part_number',$value->part_number),
        'id_process_product'=> $id_process = $this->getProduct('id_process',$value->part_number),
        'process_name'=> $this->getProduct('process_name',$id_process),
        'qty'=>$value->qty_order,
        'lifetime'=>$value->lifetime,
        'remarks'=>$value->remarks
      ];
      }

       $pdf = PDF::loadview('project.leader.export.export_rfq',['datetime'=>$datetime,'sector'=>$sector,'project'=>$project,'data'=>$data,'customer'=>$customer]);
      return $pdf->download('laporan-rfq-pdf.pdf');
   }
   public function add_part_die_go($id)
   {
    $project = TypeProject::where('id_project',$id)->first();
    $product = Product::all();

       return view('project.leader.form.add_part_die_go',compact('project','product'));
   }
   public function get_prod($id)
   {
    $product = Product::where('part_number',$id)->first();
    return $product->part_name;
   }
   public function post_product(Request $request)
   {

        $insert = DB::table('product')->insert([
           'part_number'=>$request->part_number,
           'part_name'=>$request->part_name,
           'id_project'=>$request->id_project,
           'id_process'=>1,
           'tgl_masuk'=>Carbon::now()->format('Y-m-d'),
           'plan_progress_part'=>1,
           'actual_progress_part'=>1,
           'status_product'=>1
        ]);
        if ($insert) {
            DB::table('product_summary')->insert([
              'username'=>Auth::user()->username,
              'part_number'=>$request->part_number,
              'id_project'=>$request->id_project,
              'qty_order'=>0,
              'lifetime'=>"0",
              'remarks'=>"0",
              'status_view'=>1,

            ]);
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();
   }
   public function edit_event($id)
   {
      $event = EventProject::where('id_event_project',$id)->first();
      $project = TypeProject::where('id_project',$event->id_project)->first();
      $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
      $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();
      $histori = HistoryEvent::where('id_event_project',$id)->orderBy('update_on','DESC')->get();
    
      return view('project.leader.form.edit_event',compact('event','histori','project','IDcustomer','IDsector'));
   }
   public function update_event(Request $request,$id)
   {
     DB::table('event_project')->where('id_event_project',$id)->update([
             'id_category_event_project'=>$request->id_category_event_project,
             'id_project'=>$request->id_project,
             'jadwal_event'=>$request->jadwal_event,
             'actual_event'=>$request->actual_event,
             'finish_plan_event'=>$request->finish_plan_event,
             'finish_actual_event'=>$request->finish_actual_event,
             'created_at'=>Carbon::now()->format('yy-m-d H:i:s')
          ]);
      $event =  EventProject::where('id_event_project',$id)->first();
      if ($event) {
          DB::table('event_project_history')->insert([
            'id_event_project'=>$id,
            'id_project'=>$request->id_project,
            'jadwal_event_current'=>$event->jadwal_event,
            'finish_plan_event_current'=>$event->finish_plan_event,
            'start_actual_event_current'=>$event->actual_event,
            'finish_actual_event_current'=>$event->finish_actual_event,
            'jadwal_event_change'=>$request->jadwal_event,
            'finish_plan_event_change'=>$request->finish_plan_event,
            'start_actual_event_change'=>$request->actual_event,
            'finish_actual_event_change'=>$request->finish_actual_event,
            'update_on'=>Carbon::now()->format('yy-m-d H:i:s')
          ]);
         
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();

   }
    public function delete_event(Request $request,$id)
    {
      $event =  EventProject::where('id_event_project',$id)->delete();
       if ($event) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

        return redirect()->back();
  }
  public function detail_member($id,$part_number)
  {
      $project = TypeProject::where('id_project',$id)->first();
      $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
      $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();

     $product = Product::where('part_number',$part_number)->first();
     $all_member = MemberProject::where(['id_project'=>$id,'part_number'=>$part_number])->get();
   //  return $project;
     $all_member_get = [];
      foreach ($all_member as $key => $value) {
        $all_member_get[] = [
          'id_member'=>$value->id_member,
          'name'=>$this->getName($value->username),
          'process_name'=> $this->getProduct('process_name',$value->id_process),
          'position'=> $this->getPosition($value->id_position),
          'part_name'=> $this->getProduct('part_number',$value->part_number),
          'departement'=> $this->getDepartement($value->username),
          'availability'=>$value->availability,
          'standar_rate'=>$value->standar_rate
        ];
      }

      return view('project.leader.detail.detail_member',compact('all_member_get','product','project','IDcustomer','IDsector'));
  }
  public function add_new_member($id,$part_number,$id_process)
  {
       $project = TypeProject::where('id_project',$id)->first();
        $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
        $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();
       $memberproject = MemberProject::all();
       foreach ($memberproject as $key => $value) {
         $data[] = [
          'get_user'=>$user = User::where('username',$value->username)->first(),
          'username'=>$value->username,
          'firstname'=>$user->firstname,
          'lastname'=>$user->lastname,
         ];
       }

       $position = DB::table('position')->get();
       $product = Product::where('part_number',$part_number)->first();
       $process = Process::where('id_process',$product->id_process)->first();
       $getmember = MemberProject::where(['id_project'=>$id,'id_process'=>$process->id_process,'part_number'=>$part_number,'id_position'=>2])->get();
       $getdata = [];
        foreach ($getmember as $key => $va) {
         $getdata[] = [
          'get_user'=>$user = User::where('username',$va->username)->first(),
          'get_position'=>$get = DB::table('position')->where('id_position',$va->id_position)->first(),
          'position'=>$get->position_name,
          'username'=>$va->username,
          'firstname'=>$user->firstname,
          'lastname'=>$user->lastname,
          'availability'=>$va->availability,
          'standar_rate'=>$va->standar_rate
         ];
       }

      return view('project.leader.form.add_new_member',compact('IDcustomer','IDsector','position','project','product','process','memberproject','data','getdata','id_process','part_number'));
  }
  public function update_general(Request $request,$id)
  {

      $delete = Summary::where('id_project',$id)->delete();
      for ($i = 0; $i < count($request->caption_info); $i++) {
        $answers[] = [
            'id_project'=>$id,
            'id_customer'=>$request->id_customer,
            'description' => $request->description[$i],
            'caption_info' => $request->caption_info[$i]
        ];
    }
    Summary::insert($answers);
     $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
    return redirect()->back();
  }
  public function analis($IDproject,$part_number,$status)
  {
    $project = TypeProject::where('id_project',$IDproject)->first();
    $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
    $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();

    $product = Product::where(['id_project'=>$IDproject,'part_number'=>$part_number])->first();
    $counttoday = 0;
    $countprogress = 0;
    $countnext = 0;
    $countdelay = 0;
    $countcomplate = 0;
    $total = 0;
    $today = Carbon::now()->format('Y-m-d');
    $item_names = [];
    $item_actual = [];
    $top_performance = 0;
    $parse_to_date_start = 0;
    $parse_to_date_finish = 0;
    $statistik_hasil = 0;
    $status_category = [];
    $category = CategoryName::where(['part_number'=>$part_number,'id_process'=>$product->id_process])->get();

    if ($status =="all") {
      $IDcategory = null;
      $counttoday = ItemControl::CountAnalis('today',$IDproject,$part_number,$IDcategory);
      $countprogress = ItemControl::CountAnalis('progress',$IDproject,$part_number,$IDcategory);
      $countnext = ItemControl::CountAnalis('next',$IDproject,$part_number,$IDcategory);
      $countdelay = ItemControl::CountAnalis('delay',$IDproject,$part_number,$IDcategory);
      $countcomplate = ItemControl::CountAnalis('complate',$IDproject,$part_number,$IDcategory);
      $total = $counttoday + $countprogress + $countnext + $countdelay + $countcomplate;
      $get_category = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])->get();
      $get_actual = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])->get();

      $gettoday = ItemControl::GetAnalis('today',$IDproject,$part_number,$IDcategory);
      $getprogress = ItemControl::GetAnalis('progress',$IDproject,$part_number,$IDcategory);
      $getnext = ItemControl::GetAnalis('next',$IDproject,$part_number,$IDcategory);
      $getdelay = ItemControl::GetAnalis('delay',$IDproject,$part_number,$IDcategory);
      $getcomplate = ItemControl::GetAnalis('complate',$IDproject,$part_number,$IDcategory);
//return $getdelay;
        foreach ($get_category as $key => $value) {
          $item_names[] = [
            'id_item'=>$IDitem = $value->id_item,
            'item_name'=>MasterItemControl::getName($IDitem),
          ];
        }
         foreach ($get_actual as $key => $value) {
          $item_actual[] = [
            'id_item'=>$IDitem = $value->id_item,
            'actual_progress_item'=>$value->actual_progress_item,
          ];
        }
        //statistik
         $get_performance = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number,'actual_progress_item'=>100])->first();


         $rencana_awal = 0;
         $parse_to_date_start = 0;
         $parse_to_date_finish = 0;
         if(!empty($get_performance)){
          $top_performance = User::where('username',$get_performance->pic_item)->first();
         $rencana_awal = new Carbon($get_performance->plan_start_item);
         $statistik_hasil = $rencana_awal->startOfDay()->diffInDays($get_performance->plan_finish_item, false);
         $parse_to_date_start =  date("d F Y",strtotime($rencana_awal));
         $parse_to_date_finish =  date("d F Y",strtotime($get_performance->plan_finish_item));
          }
         //end
      }else{
        $IDcategory = $status;
         $status_category = CategoryName::where(['part_number'=>$part_number,'id_process'=>$product->id_process])->where('id_category',$IDcategory)->first();

         $counttoday = ItemControl::CountAnalis('today',$IDproject,$part_number,$IDcategory);
        $countprogress = ItemControl::CountAnalis('progress',$IDproject,$part_number,$IDcategory);
        $countnext = ItemControl::CountAnalis('next',$IDproject,$part_number,$IDcategory);
        $countdelay = ItemControl::CountAnalis('delay',$IDproject,$part_number,$IDcategory);
        $countcomplate = ItemControl::CountAnalis('complate',$IDproject,$part_number,$IDcategory);

         $gettoday = ItemControl::GetAnalis('today',$IDproject,$part_number,$IDcategory);
        $getprogress = ItemControl::GetAnalis('progress',$IDproject,$part_number,$IDcategory);
        $getnext = ItemControl::GetAnalis('next',$IDproject,$part_number,$IDcategory);
        $getdelay = ItemControl::GetAnalis('delay',$IDproject,$part_number,$IDcategory);
        $getcomplate = ItemControl::GetAnalis('complate',$IDproject,$part_number,$IDcategory);

        $total = $counttoday + $countprogress + $countnext + $countdelay + $countcomplate;
        $get_category = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])->where('id_category',$IDcategory)->get();
        $get_actual = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number])->where('id_category',$IDcategory)->get();
          foreach ($get_category as $key => $value) {
            $item_names[] = [
              'id_item'=>$IDitem = $value->id_item,
              'item_name'=>MasterItemControl::getName($IDitem),
            ];
          }
           foreach ($get_actual as $key => $value) {
            $item_actual[] = [
              'id_item'=>$IDitem = $value->id_item,
              'actual_progress_item'=>$value->actual_progress_item,
            ];
          }
          //statistik
           $get_performance = ItemControl::where(['id_project'=>$IDproject,'part_number'=>$part_number,'actual_progress_item'=>100])->where('id_category',$IDcategory)->first();
           $top_performance = User::where('username',$get_performance->pic_item)->first();
           $rencana_awal = new Carbon($get_performance->plan_start_item);
           $statistik_hasil = $rencana_awal->startOfDay()->diffInDays($get_performance->plan_finish_item, false);
           $parse_to_date_start =  date("d F Y",strtotime($rencana_awal));
           $parse_to_date_finish =  date("d F Y",strtotime($get_performance->plan_finish_item));
      }


     return view('project.leader.analis',compact('parse_to_date_start','parse_to_date_finish','counttoday','countprogress','countnext','countdelay','countcomplate','total','item_names','item_actual','top_performance','statistik_hasil','project','product','status','category','status_category','IDcustomer','IDsector','gettoday','getprogress','getnext','getdelay','getcomplate','IDproject','part_number'));
  }
  public function technical_review($id,$part_number)
  {
    $data = DB::table('technical_review')->where(['id_project'=>$id,'part_number'=>$part_number])->get();
    $product = Product::where('part_number',$part_number)->first();
    $project = TypeProject::where('id_project',$id)->first();
    return view('project.leader.document.technical_review',compact('data','product','project'));
  }
  public function add_technical_review($id,$part_number)
  {

    $project = TypeProject::where('id_project',$id)->first();
    $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
    $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();
    $product = Product::where('part_number',$part_number)->first();
    $process = Process::where('id_process',$product->id_process)->first();
     return view('project.leader.form.add_technical_review',compact('process','project','IDcustomer','IDsector','product'));
  }
  public function cost_performance($id)
  {
    $project = TypeProject::where('id_project',$id)->first();
    $IDcustomer = Customer::where('id_customer',$project->id_customer)->first();
    $IDsector = Sector::where('id_product',$IDcustomer->id_product)->first();

    $get_performance = [];
    $get_cost_category = [];
    $total = 0;
    $cost_performance = Cogm::where('id_project',$id)->get();
    foreach ($cost_performance as $key => $performance) {
      $get_performance[] = [
        'part_number'=>$part_number = $performance->part_number,
        'part_number_on_product'=>$IDproduct = Product::where(['id_project'=>$id,'part_number'=>$part_number])->first(),
        'get_part_name'=>$IDproduct ==true ? $IDproduct->part_name:null
      ];
    }
    $cost_performance_category = Cogm::where('id_project',$id)->get();
    foreach ($cost_performance_category as $key => $cost_category) {
      $get_cost_category[] = [
        'id_category_event_project'=>$IDcategory = $cost_category->id_category_event_project,
        'get_id_category_event_project'=>$ID = CategoryEventProject::where('id_category_event_project',$IDcategory)->first(),
        'event_project_name'=>$ID->event_project_name,
      ];
    }
    $sum_cost_performance_bom = Cogm::where('id_project',$id)->sum('bom_cogm');
    $sum_cost_performance_process_cogm = Cogm::where('id_project',$id)->sum('process_cogm');
    $depreciation_cogm = Cogm::where('id_project',$id)->sum('depreciation_cogm');
    $total =  $sum_cost_performance_bom +  $sum_cost_performance_process_cogm +  $depreciation_cogm;
    //
    $product_name = [];
    $IDbudget = ProjectBudget::where('id_project',$id)->pluck('part_number');
    foreach ($IDbudget as $key => $value) {
     $product_name[] = [

      'get_product_name'=>$IDproduct = Product::where('part_number',$value)->first(),
      'product_name'=>$IDproduct->part_name
     ];
    }
    $actual_budget = ProjectBudget::where('id_project',$id)->sum('total_price_actual');
    $plan_budget = ProjectBudget::where('id_project',$id)->sum('total_price_plan');

    return view('project.leader.cost_performance',compact('project','IDsector','IDcustomer','product_name','actual_budget','plan_budget','total','get_cost_category','get_performance'));
  }
  public function cogm($id)
  {
     $getData = [];
     $getDatas = [];
     $getDatasi = [];
      $project = TypeProject::find($id);
      $data = DB::table('cogm')->where('id_project',$id)->get();

        foreach ($data as $key => $value) {
            $getData[] = [
                'id_cogm'=>$value->id_cogm,
                'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                'product'=>$get_poduct->part_name,
                'get_event'=>$get_event = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),

                'get_detail'=> $get_detail = [
                    'event'=>$get_event->event_project_name,
                    'event'=>$get_event->event_project_name,
                    'bom'=>$value->bom_cogm,
                    'process'=>$value->process_cogm,
                    'depreciation'=>$value->depreciation_cogm,
                    'total'=>$value->total_cogm,
                ],
                'detail'=>$get_detail,
            ];
             $getDatas[] = [
                'id_cogm'=>$value->id_cogm,
                'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                'product'=>$get_poduct->part_name,
                'get_event'=>$get_event = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),

                'get_detail'=> $get_detail = [
                    'event'=>$get_event->event_project_name,
                    'event'=>$get_event->event_project_name,
                    'bom'=>$value->bom_cogm,
                    'process'=>$value->process_cogm,
                    'depreciation'=>$value->depreciation_cogm,
                    'total'=>$value->total_cogm,
                ],
                'detail'=>$get_detail,
            ];
             $getDatasi[] = [
                'id_cogm'=>$value->id_cogm,
                'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                'product'=>$get_poduct->part_name,
                'get_event'=>$get_event = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),

                'get_detail'=> $get_detail = [
                    'event'=>$get_event->event_project_name,
                    'event'=>$get_event->event_project_name,
                    'bom'=>$value->bom_cogm,
                    'process'=>$value->process_cogm,
                    'depreciation'=>$value->depreciation_cogm,
                    'total'=>$value->total_cogm,
                ],
                'detail'=>$get_detail,
            ];
          }
        //  return $getData;
     return view('project.leader.cogm',compact('getData','data','project','getDatas','getDatasi'));
  }
  public function project_budget($id)
  {
        $getData = [];
        $project = TypeProject::find($id);
        $datetime = Carbon::now()->format('d F Y');
        $event = EventProject::where('id_project',$id)->where('id_category_event_project',14)->first();
        $budget_plan = ProjectBudget::where('id_project',$id)->sum('total_price_plan');
        $budget_actual = ProjectBudget::where('id_project',$id)->sum('total_price_actual');
        if($budget_plan !=0 || $budget_actual !=0){
        $result = round($budget_actual / $budget_actual * 100);
        }else{
          $result = 0;
        }
        if ($result == 0) {
          $status = "Not available";
        }elseif($result > 100){
           $status = "Hight risk";
        }elseif($result <= 100 || $event->jadwal_event > $datetime){
          $status = "Potensial risk";

        }elseif($result <= 100 || $event->jadwal_event < $datetime){
          $status = "On time";
        }

         $projectBudget = DB::table('project_budget')->select('part_number')->where('id_project',$id)->where('status',1)->groupBy('part_number')->get();
            foreach ($projectBudget as $key => $value) {
                    $getData[] = [
                        'get_poduct'=>$get_poduct = Product::where('part_number',$value->part_number)->first(),
                        'get_qty'=>$get_qty = ProductSummary::where(['id_project'=>$id,'part_number'=>$value->part_number])->first(),
                        'qty'=>$get_qty ==true ? $get_qty->qty_order:0,
                        'total_plan'=>$budget_plan,
                        'total_actual'=>$budget_actual,
                        'product'=>$get_poduct->part_name,
                        'status'=>$status,
                        'budget'=>ProjectBudget::where(['id_project'=>$id,'part_number'=>$value->part_number])->get()

                    ];
                }


          $part_name_opex_get = null;
          $part_name_opex = [];
          $plan_budget_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->sum('total_price_plan');
          $actual_budget_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->sum('total_price_actual');
          $part_number_opex = ProjectBudget::where(['id_project'=>$id,'category'=>'Opek'])->first();
          if($part_number_opex ==true){
            $part_name_opex = Product::where('part_number',$part_number_opex->part_number)->first();
            $part_name_opex_get = $part_name_opex->part_name;
          }

           $part_name_capex = [];
           $total_plan_capex = 0;
           $total_actual_capex = 0;
          $plan_budget_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->sum('total_price_plan');
          $actual_budget_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->sum('total_price_actual');
          $part_number_capex = ProjectBudget::where(['id_project'=>$id,'category'=>'Capex'])->first();
          if($part_number_capex ==true){
            $part_name_capex = Product::where('part_number',$part_number_capex->part_number)->first();
          }
          if ($plan_budget_capex || $actual_budget_capex > 0) {
            $total_plan_capex = round($plan_budget_capex / $actual_budget_capex * 100);
            $total_actual_capex = round($actual_budget_capex / $plan_budget_capex * 100);
          }
         //return $plan_budget_capex;
    return view('project.leader.project_budget',compact('part_name_opex_get','part_name_capex','total_plan_capex','total_actual_capex','part_name_opex','plan_budget_opex','actual_budget_opex','project','getData','status','result','budget_actual','budget_plan'));
  }
  public function project_detail_gantt()
  {


     return view('project.leader.project_detail_gantt');
  }
  public function detail_event_grafik()
  {

      return view('project.leader.detail.detail_event_grafik');
  }
  public function delete_member_detail(Request $request, $id)
  {
      $data = MemberProject::where('id_member',$id)->first();
      if ($data) {
        $data->delete();
      }
      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been delete member');
      return redirect()->back();
  }
  public function edit_member_detail($id)
  {
    $data = MemberProject::where('id_member',$id)->first();
    $project = TypeProject::where('id_project',$data->id_project)->first();
     $position = DB::table('position')->get();

    return view('project.leader.form.edit_member',compact('data','project','position'));
  }
  public function update_member(Request $request,$id){

      $data = MemberProject::where('id_member',$id)->update([
      'username'=>$request->username,
      'id_project'=>$request->id_project,
      'id_position'=>$request->id_position,
      'id_process'=>$request->id_process,
      'part_number'=>$request->part_number,
      'availability'=>$request->availability,
      'standar_rate'=>$request->standar_rate,
      'duration_rate'=>$request->duration_rate
    ]);
       $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been update');
    return redirect()->back();
  }
  public function report_general($id_project)
  {
    $project = TypeProject::where('id_project',$id_project)->first();
    $customer = Customer::where('id_customer',$project->id_customer)->first();
    $IDsector = Sector::where('id_product',$customer->id_product)->first();
    $data = Summary::where(['id_project'=>$id_project,'id_customer'=>$customer->id_customer])->get();
      $datetime = Carbon::now()->format('d F Y');

       $pdf = PDF::loadview('project.export.export_general',['datetime'=>$datetime,'project'=>$project,'data'=>$data,'customer'=>$customer]);
      return $pdf->download('laporan-part-pdf.pdf');

  }
  public function approval_line($part_number, $id_project, $id_category_event_project)
  {
    $check = Approval::where(['part_number'=>$part_number,'id_project'=>$id_project,'id_category_event_project'=>$id_category_event_project])->first();
    if ($check ==true) {
      $project = TypeProject::where('id_project',$check->id_project)->first();
      $event = CategoryEventProject::where('id_category_event_project',$check->id_category_event_project)->first();
      return view('project.leader.form.approval_line_update',compact('check','project','event'));
    }else{
          $event = CategoryEventProject::where('id_category_event_project',$id_category_event_project)->first();
          $project = TypeProject::where('id_project',$id_project)->first();
          return view('project.leader.form.approval_line',compact('project','event','part_number','id_project','id_category_event_project'));
    }

  }
   public function quality_add($part_number, $id, $id_category_event_project)
  {
    $event = CategoryEventProject::where('id_category_event_project',$id_category_event_project)->first();
    $product = Product::where('part_number',$part_number)->first();
    $trial = Quality::where(['id_project'=>$id,'part_number'=>$part_number])->count();
    $trial_count = $trial + 1;
    $visual = VisualQuality::where(['trial_number'=>$trial_count,'part_number'=>$part_number])->get();
    $dimensi = DimensiQuality::where(['trial_number'=>$trial_count,'part_number'=>$part_number])->get();
   // return $trial_count;
    return view('project.leader.form.quality_add',compact('event','product','trial_count','visual','dimensi','id'));
  }
  public function update_approval_lines($id, Request $request)
  {
    if(($request->done1 + $request->eject1 + $request->silang1) > 2){
        $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Incoming & Suplier Development tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done2 + $request->eject2 + $request->silang2) > 3){
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', ' Data Drawing tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done3 + $request->eject3 + $request->silang3) > 3){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Material & Komponen tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done4 + $request->eject4 + $request->silang4) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Man tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done5 + $request->eject5 + $request->silang5) > 9){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Document tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done6 + $request->eject6 + $request->silang6) > 6){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Machine tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done7 + $request->eject7 + $request->silang7) > 7){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Environment tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done8 + $request->eject8 + $request->silang8) > 3){
            echo "<b>--></b><br>";
            $status = false;
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', ' Data Inspection tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done9 + $request->eject9 + $request->silang9) > 1){
            echo "<b>--> Data Visual tidak valid, cek kembali jumlah kapasitas item tersebut</b><br>";
            $status = false;
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Visual tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done10 + $request->eject10 + $request->silang10) > 1){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Dimensi tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done11 + $request->eject11 + $request->silang11) > 2){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Process Performance tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done12 + $request->eject12 + $request->silang12) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Material & Komponen incoming tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done13 + $request->eject13 + $request->silang13) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Storage FG tidak valid, cek kembali jumlah kapasitas item tersebut');
        }else{
          $request->session()->flash('status', '200');
          $request->session()->flash('msg', ' Data Approval Line Berhasil di Update');
           DB::beginTransaction();
            try {
               $image_name = null;
               if ($request->HasFile('attachment')) {
                    $destination_path = "/public/document/";
                    $image = $request->file('attachment');
                    $image_name = $image->getClientOriginalName();
                    $path = $request->file('attachment')->storeAs($destination_path,$image_name);
                    $data['attachment'] = $image_name;
              }
              $create = Approval::where('id_approval_line',$id)->update([
                'id_project'=>$request->id_project ==true ? $request->id_project : 0,
                'part_number'=>$request->part_number ==true ? $request->part_number : 0,
                'id_category_event_project'=>$request->id_category_event_project ==true ? $request->id_category_event_project : 0,
                'attachment'=>$image_name ==true ? $image_name : 0,
                'done1'=>$request->done1 ==true ? $request->done1 : 0,
                'eject1'=>$request->eject1 ==true ? $request->eject1 : 0,
                'silang1'=>$request->silang1 ==true ? $request->silang1 : 0,
                'done2'=>$request->done2 ==true ? $request->done2 : 0,
                'eject2'=>$request->eject2 ==true ? $request->eject2 : 0,
                'silang2'=>$request->silang2 ==true ? $request->silang2 : 0,
                'done3'=>$request->done3 ==true ? $request->done3 : 0,
                'eject3'=>$request->eject3 ==true ? $request->eject3 : 0,
                'silang3'=>$request->silang3 ==true ? $request->silang3 : 0,
                'done4'=>$request->done4 ==true ? $request->done4 : 0,
                'eject4'=>$request->eject4 ==true ? $request->eject4 : 0,
                'silang4'=>$request->silang4 ==true ? $request->silang4 : 0,
                'done5'=>$request->done5 ==true ? $request->done5 : 0,
                'eject5'=>$request->eject5 ==true ? $request->eject5 : 0,
                'silang5'=>$request->silang5 ==true ? $request->silang5 : 0,
                'done6'=>$request->done6 ==true ? $request->done6 : 0,
                'eject6'=>$request->eject6 ==true ? $request->eject6 : 0,
                'silang6'=>$request->silang6 ==true ? $request->silang6 : 0,
                'done7'=>$request->done7 ==true ? $request->done7 : 0,
                'eject7'=>$request->eject7 ==true ? $request->done7 : 0,
                'silang7'=>$request->silang7 ==true ? $request->silang7 : 0,
                'done8'=>$request->done8 ==true ? $request->done8 : 0,
                'eject8'=>$request->eject8 ==true ? $request->eject8 : 0,
                'silang8'=>$request->silang8 ==true ? $request->silang8 : 0,
                'done9'=>$request->done9 ==true ? $request->done9 : 0,
                'eject9'=>$request->eject9 ==true ? $request->eject9 : 0,
                'silang9'=>$request->silang9 ==true ? $request->silang9 : 0,
                'done10'=>$request->done10 ==true ? $request->done10 : 0,
                'eject10'=>$request->eject10 ==true ? $request->eject10 : 0,
                'silang10'=>$request->silang10 ==true ? $request->silang10 : 0,
                'done11'=>$request->done11 ==true ? $request->done11 : 0,
                'eject11'=>$request->eject11 ==true ? $request->eject11 : 0,
                'silang11'=>$request->silang11 ==true ? $request->silang11 : 0,
                'done12'=>$request->done12 ==true ? $request->done12 : 0,
                'eject12'=>$request->eject12 ==true ? $request->eject12 : 0,
                'silang12'=>$request->silang12 ==true ? $request->silang12 : 0,
                'done13'=>$request->done13 ==true ? $request->done13 : 0,
                'eject13'=>$request->eject13 ==true ? $request->eject13 : 0,
                'silang13'=>$request->silang13 ==true ? $request->silang13 : 0
              ]);
            DB::commit();
            } catch (Exception $e) {
               DB::rollback();
            }
        }
        return redirect()->back();
  }
  public function post_approval_lines(Request $request)
  {


      if(($request->done1 + $request->eject1 + $request->silang1) > 2){
        $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Incoming & Suplier Development tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done2 + $request->eject2 + $request->silang2) > 3){
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', ' Data Drawing tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done3 + $request->eject3 + $request->silang3) > 3){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Material & Komponen tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done4 + $request->eject4 + $request->silang4) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Man tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done5 + $request->eject5 + $request->silang5) > 9){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Document tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done6 + $request->eject6 + $request->silang6) > 6){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Machine tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done7 + $request->eject7 + $request->silang7) > 7){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Environment tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done8 + $request->eject8 + $request->silang8) > 3){
            echo "<b>--></b><br>";
            $status = false;
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', ' Data Inspection tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done9 + $request->eject9 + $request->silang9) > 1){
            echo "<b>--> Data Visual tidak valid, cek kembali jumlah kapasitas item tersebut</b><br>";
            $status = false;
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Visual tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done10 + $request->eject10 + $request->silang10) > 1){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Dimensi tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done11 + $request->eject11 + $request->silang11) > 2){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Process Performance tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done12 + $request->eject12 + $request->silang12) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Material & Komponen incoming tidak valid, cek kembali jumlah kapasitas item tersebut');
        }

        elseif(($request->done13 + $request->eject13 + $request->silang13) > 5){
             $request->session()->flash('status', '400');
        $request->session()->flash('msg', 'Data Storage FG tidak valid, cek kembali jumlah kapasitas item tersebut');
        }else{
          $request->session()->flash('status', '200');
          $request->session()->flash('msg', 'Data Approval Line Berhasil di Simpan');
           DB::beginTransaction();
            try {
              $image_name = null;
               if ($request->HasFile('attachment')) {
                    $destination_path = "/public/document/";
                    $image = $request->file('attachment');
                    $image_name = $image->getClientOriginalName();
                    $path = $request->file('attachment')->storeAs($destination_path,$image_name);
                    $data['attachment'] = $image_name;
              }
              $create = Approval::create([
                'id_project'=>$request->id_project ==true ? $request->id_project : 0,
                'part_number'=>$request->part_number ==true ? $request->part_number : 0,
                'id_category_event_project'=>$request->id_category_event_project ==true ? $request->id_category_event_project : 0,
                'attachment'=>$image_name ==true ? $image_name : 0,
                'done1'=>$request->done1 ==true ? $request->done1 : 0,
                'eject1'=>$request->eject1 ==true ? $request->eject1 : 0,
                'silang1'=>$request->silang1 ==true ? $request->silang1 : 0,
                'done2'=>$request->done2 ==true ? $request->done2 : 0,
                'eject2'=>$request->eject2 ==true ? $request->eject2 : 0,
                'silang2'=>$request->silang2 ==true ? $request->silang2 : 0,
                'done3'=>$request->done3 ==true ? $request->done3 : 0,
                'eject3'=>$request->eject3 ==true ? $request->eject3 : 0,
                'silang3'=>$request->silang3 ==true ? $request->silang3 : 0,
                'done4'=>$request->done4 ==true ? $request->done4 : 0,
                'eject4'=>$request->eject4 ==true ? $request->eject4 : 0,
                'silang4'=>$request->silang4 ==true ? $request->silang4 : 0,
                'done5'=>$request->done5 ==true ? $request->done5 : 0,
                'eject5'=>$request->eject5 ==true ? $request->eject5 : 0,
                'silang5'=>$request->silang5 ==true ? $request->silang5 : 0,
                'done6'=>$request->done6 ==true ? $request->done6 : 0,
                'eject6'=>$request->eject6 ==true ? $request->eject6 : 0,
                'silang6'=>$request->silang6 ==true ? $request->silang6 : 0,
                'done7'=>$request->done7 ==true ? $request->done7 : 0,
                'eject7'=>$request->eject7 ==true ? $request->done7 : 0,
                'silang7'=>$request->silang7 ==true ? $request->silang7 : 0,
                'done8'=>$request->done8 ==true ? $request->done8 : 0,
                'eject8'=>$request->eject8 ==true ? $request->eject8 : 0,
                'silang8'=>$request->silang8 ==true ? $request->silang8 : 0,
                'done9'=>$request->done9 ==true ? $request->done9 : 0,
                'eject9'=>$request->eject9 ==true ? $request->eject9 : 0,
                'silang9'=>$request->silang9 ==true ? $request->silang9 : 0,
                'done10'=>$request->done10 ==true ? $request->done10 : 0,
                'eject10'=>$request->eject10 ==true ? $request->eject10 : 0,
                'silang10'=>$request->silang10 ==true ? $request->silang10 : 0,
                'done11'=>$request->done11 ==true ? $request->done11 : 0,
                'eject11'=>$request->eject11 ==true ? $request->eject11 : 0,
                'silang11'=>$request->silang11 ==true ? $request->silang11 : 0,
                'done12'=>$request->done12 ==true ? $request->done12 : 0,
                'eject12'=>$request->eject12 ==true ? $request->eject12 : 0,
                'silang12'=>$request->silang12 ==true ? $request->silang12 : 0,
                'done13'=>$request->done13 ==true ? $request->done13 : 0,
                'eject13'=>$request->eject13 ==true ? $request->eject13 : 0,
                'silang13'=>$request->silang13 ==true ? $request->silang13 : 0
              ]);
            DB::commit();
            } catch (Exception $e) {
               DB::rollback();
            }
        }
        return redirect()->back();
  }
  public function post_quality(Request $request)
  {

    if ($request->type =="save_all") {
     DB::beginTransaction();
      try {
        $quality = new Quality;
        $quality->id_project = $request->id_project;
        $quality->part_number = $request->part_number;
        $quality->id_category_event_project = $request->id_category_event_project;
        $quality->trial_number = $request->trial_number;
        $quality->adjustment_visual = $request->adjustment_visual;
        $quality->adjustment_dimensi = $request->adjustment_dimensi;
        $quality->tanggal_masuk =Carbon::now();
        $quality->save();
        $visual = VisualQuality::where(['part_number'=>$request->part_number,'trial_number'=>$request->trial_number])->update([
        'status'=>1,
        'id_quality'=>$quality->id_quality
        ]);
        $dimensi = DimensiQuality::where(['part_number'=>$request->part_number,'trial_number'=>$request->trial_number])->update([
        'status'=>1,
        'id_quality'=>$quality->id_quality
        ]);
      DB::commit();
      } catch (Exception $e) {
         DB::rollback();
      }


    }elseif($request->type =="save_visual"){

      foreach ($request->pic_terkait as $i) {
        $pic_terkait2 = $i;

        //PE
        if($pic_terkait2 == '1'){
            $pic_pe = '1';
        }else{
             $pic_pe = '0';
        }

        //MTJF
        if($pic_terkait2 == '2'){
            $pic_mjtf = '2';
        }else{
           $pic_mjtf = '0';
        }

        //ENGPROCESS
        if($pic_terkait2 == '3'){
            $pic_eng_process = '3';
        }else{
            $pic_eng_process = '0';
        }

        //ENGSETMOLD
        if($pic_terkait2 == '4'){
            $pic_eng_set_mold = '4';
        }else{
           $pic_eng_set_mold = '0';
        }

    }
//return $pic_eng_set_mold;
     $visual = new VisualQuality;
     $visual->id_quality = 0;
     $visual->notes = $request->note_visual;
     $visual->pic_pe  = $pic_pe;
     $visual->pic_mjtf  = $pic_mjtf;
     $visual->pic_eng_process = $pic_eng_process;
     $visual->pic_eng_set_mold  = $pic_eng_set_mold;
     $visual->status  = 0;
     $visual->trial_number  = $request->trial_number;
     $visual->part_number = $request->part_number;
     $visual->id_category_event_project = $request->id_category_event_project;
     $visual->save();

    }else{
     foreach ($request->pic_terkait as $i) {
        $pic_terkait2 = $i;

        //PE
        if($pic_terkait2 == '1'){
            $pic_pe = '1';
        }else{
             $pic_pe = '0';
        }

        //MTJF
        if($pic_terkait2 == '2'){
            $pic_mjtf = '2';
        }else{
           $pic_mjtf = '0';
        }

        //ENGPROCESS
        if($pic_terkait2 == '3'){
            $pic_eng_process = '3';
        }else{
            $pic_eng_process = '0';
        }

        //ENGSETMOLD
        if($pic_terkait2 == '4'){
            $pic_eng_set_mold = '4';
        }else{
           $pic_eng_set_mold = '0';
        }

    }

       $dimensi = new DimensiQuality;
     $dimensi->id_quality = 0;
     $dimensi->notes = $request->note_dimensi;
     $dimensi->pic_pe  = $pic_pe;
     $dimensi->pic_mjtf  = $pic_mjtf;
     $dimensi->pic_eng_process = $pic_eng_process;
     $dimensi->pic_eng_set_mold  = $pic_eng_set_mold;
     $dimensi->status  = 0;
     $dimensi->trial_number  = $request->trial_number;
     $dimensi->part_number = $request->part_number;
     $dimensi->id_category_event_project = $request->id_category_event_project;
     $dimensi->save();

    }
     $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been add');
    return redirect('project/quality_detail/'.$request->id_project.'/'.$request->part_number);
  }

   public function quality_detail($id,$part_number)
  {

    $event = EventProject::where('id_project',$id)->get();
    $project = TypeProject::where('id_project',$id)->first();
    $product = Product::where('part_number',$part_number)->first();
    foreach ($event as $key => $value) {
      $data[] = [
        'id_category_event_project'=>$value->id_category_event_project,
        'event_get'=>$event_get = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),
        'part_number'=>$part_number,
        'id_project'=>$id,
        'event'=>$event_get->event_project_name,
        'get_quality'=>$get_quality = $this->getQuality($id,$part_number,$value->id_category_event_project),
        'id_quality'=>$get_quality ==true ? $get_quality->id_quality : null,
        'trial_number'=>$get_quality ==true ? $get_quality->trial_number : null,
        'adjustment_visual'=>$get_quality ==true ? $get_quality->adjustment_visual : null,
        'adjustment_dimensi'=>$get_quality ==true ? $get_quality->adjustment_dimensi : null,
      ];
    }
//return $data;
    return view('project.leader.form.quality_detail',compact('data','project','product'));
  }
  public function quality_detail_list($id)
  {
      return view('project.leader.form.quality_detail_list');
  }
  public function task_detail($id_project)
  {
    $current = Carbon::now();
    // add 30 days to the current time
    $addnextweek = $current->addDays(6);
    $nextweek = $addnextweek->format('Y-m-d');
    $today = Carbon::now()->format('Y-m-d');
    $name_week = $addnextweek->format('d-M');
    $name_today = Carbon::now()->format('d-M');
     $get_task_today = [];
      $get_task_next_week = [];
    $task_today = ItemControl::where('id_project',$id_project)->where('plan_start_item','=',$today)->get();
    foreach ($task_today as $key => $value) {
      $get_task_today[] = [
        'id_item'=>$value->id_item,
        'part_number'=>$value->part_number,
        'id_project'=>$value->id_project,
        'pic_item'=>$value->pic_item,
       'get_part_name'=>$get_part_name = Product::where(['part_number'=>trim($value->part_number),'id_project'=>$value->id_project])->first(),
        'part_name'=>$get_part_name ==true ? $get_part_name->part_name : null,
         'id_category'=>$value->id_category,
        'get_category'=>$get_category = CategoryName::where('id_category',$value->id_category)->first(),
        'category_name'=>$get_category ==true ? $get_category->category_name : null
      ];
    }

    $task_nextweek = ItemControl::where('id_project',$id_project)->where('plan_start_item','=',$nextweek)->get();
     foreach ($task_nextweek as $key => $values) {
      $get_task_next_week[] = [
        'id_item'=>$values->id_item,
        'part_number'=>$values->part_number,
        'id_project'=>$values->id_project,
        'pic_item'=>$values->pic_item,
        'get_part_name'=>$get_part_name = Product::where(['part_number'=>trim($values->part_number),'id_project'=>$values->id_project])->first(),
        'part_name'=>$get_part_name ==true ? $get_part_name->part_name : null,
        'id_category'=>$values->id_category,
        'get_category'=>$get_category = CategoryName::where('id_category',$values->id_category)->first(),
        'category_name'=>$get_category ==true ? $get_category->category_name : null
      ];
    }

  //return $get_task_next_week;
    return view('Dashboard.task_detail',compact('task_today','get_task_next_week','get_task_today','name_today','name_week'));
  }
  public function add_member(Request $request)
  {
      $project = TypeProject::where('id_project',$request->id_project)->first();
    $customer = Customer::where('id_customer',$project->id_customer)->first();

   // return $request->all();
    $data = MemberProject::create([
      'username'=>$request->username,
      'id_project'=>$request->id_project,
      'id_position'=>$request->id_position,
      'id_process'=>$request->id_process,
      'part_number'=>$request->part_number,
      'availability'=>$request->availability,
      'standar_rate'=>$request->standar_rate,
      'duration_rate'=>$request->duration_rate
    ]);
     if ($data) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added member');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }
     return redirect('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project);
  }
  public function document($id_project,$part_number)
  {
    $get_event = [];
    $data = ItemControl::where('id_project',$id_project)->first();
    $event = EventProject::where('id_project',$id_project)->get();
    $category = CategoryEventProject::all();
    $project = TypeProject::where('id_project',$id_project)->first();

    foreach ($event as $key => $value) {
      $get_event[] = [
        'event_get'=>$event_get = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),
        'id_process'=>$event_get->id_process,
        'id_category_event_project'=>$value->id_category_event_project,
        'event'=>$event_get->event_project_name,
        'id_project'=>$id_project,
        'part_number'=>$part_number,
        'get_document'=>DocumentProject::getDocument($id_project,$part_number,$value->id_category_event_project),
        'check'=>DocumentProject::checkComplate($id_project,$part_number,$value->id_category_event_project)
      ];
    }
  //return $get_event;
    return view('project.leader.document',compact('id_project','project','get_event'));
  }
  public function list_document($id_project,$part_number)
  {
    $id_category_event_project = 1;
    $project = TypeProject::where('id_project',$id_project)->first();
    //return $project;
    $data = DocumentProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->orderBy('date_create','DESC')->get();
    //return $data;
    $member = MemberProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->get();
    // Notification::where('id_project',$id_project)->where('part_number',$part_number)->where('penerima',Auth::user()->id_pengguna)->where('status_read','un_read')->where('tipe','document')->update([
    //   'status_read'=>'read'
    // ]);
    return view('project.leader.list_document',compact('member','data','project','id_category_event_project','part_number','id_project'));
  }
  public function post_document_project(Request $request)
  {
        $pool = '0123456789ABCDEFGHIJKLMNOPRSTU';
        $length = 16;
    try {
       DB::beginTransaction();

      $id_pengguna = User::where('username',$request->pic)->first();
      $penerima = $request->email;
      $data = new DocumentProject;
      $data->id_category_event_project = $request->id_category_event_project;
      $data->id_project = $request->id_project;
      $data->part_number = $request->part_number;
      $data->document_name = $request->document_name;
      $data->date_create = Carbon::now();
      $data->pic = $request->pic;
      $data->status_document = 0;
      $data->id_process = 1;
      $data->reminder_count = 1;
      $data->create_by = Auth::user()->id_pengguna;
   
     if ($request->HasFile('filename')) {
            $destination_path = "/public/document/";
            $image = $request->file('filename');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('filename')->storeAs($destination_path,$image_name);
            $data['filename'] = $image_name;
      }
     $data->save();

 
        $notif = new Notification;
        $notif->id_project = $request->id_project;
        $notif->part_number = $request->part_number;
        $notif->pengirim = Auth::user()->id_pengguna;
        $notif->penerima = $id_pengguna->id_pengguna;
        $notif->tipe = 'document';
        $notif->status_read = 'un_read';
        $notif->document_code = $data->document_code;
        $notif->save();
    
       // $files = $request->file('filename');
       //      $mail = Mail::send('email.document_project', ['penerima'=>$penerima], function($message) use ($penerima,$files)
       //      {
       //          $message->to($penerima)->subject('NOTIFICATION ADD DOCUMENT');

       //             if ($files ==true) {
       //               $message->attach($files->getRealPath(), array(
       //                      'as' => $files->getClientOriginalName(), // If you want you can chnage original name to custom name
       //                      'mime' => $files->getMimeType())
       //                  );

       //             }


       //      });
      DB::commit();
    } catch (Exception $e) {
      DB::rollback();
      return "email error";
    }

      $request->session()->flash('status', '200');
      $request->session()->flash('msg', 'Admin has been added');
      return redirect()->back();
  }
  public function download_document($id)
  {
    try {
       $path =  storage_path('app/public/document/'.$id);
    } catch (Exception $e) {

    }


    return response()->download($path);
  }
  public function add_new_item($id_category,$id_project,$part_number,$categoris,$id_process)
  {


    $category = DB::table('master_item_control')->where('id_category',$id_category)->get();
   // return $category;
    $member = User::get();
    $product = Product::where('id_project',$id_project)->first();
    $data = null;
    $check = '';
     $email_pic['email_pic'] = null;
    return view('project.leader.form.add_new_item',compact('email_pic','check','data','id_process','categoris','category','member','product','id_project','id_category','part_number'));
  }
  public function pos_add_new_item(Request $request)
  {
$image_name = null;
$duration_plan = 0;
if ($request->plan_finish_item ==true) {
 $plan_start = Carbon::parse($request->plan_start_item);
$plan_finish = Carbon::parse($request->plan_finish_item);

$duration_plan = $plan_start->diffInDays($plan_finish);
}
$duration_actual = 0;
if ($request->actual_finish_item ==true) {
$actual_start = Carbon::parse($request->actual_start_item);
$actual_finish = Carbon::parse($request->actual_finish_item);

$duration_actual = $actual_start->diffInDays($actual_finish);
}
   if ($request->HasFile('bukti_item')) {
            $destination_path = "/public/document/";
            $image = $request->file('bukti_item');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('bukti_item')->storeAs($destination_path,$image_name);
            $data['bukti_item'] = $image_name;
      }

   // return $request->all();
    $data = CategoryName::where('part_number',$request->part_number)->first();

    $addItem = new  MasterItemControl;
    $addItem->item_name = $request->id_category;
    $addItem->id_category = $data->id_category;
    $addItem->part_number = $request->part_number;
    $addItem->save();
    $insert = DB::table('item_control')->insert([
        'id_item'=>$addItem->id_item,
        'id_project'=>$request->id_project,
        'part_number'=>$request->part_number,
        'id_category'=>$data->id_category,
        'pic_item'=>$request->pic_item,
        'id_status'=>1,
        'plan_start_item'=>$request->plan_start_item,
        'plan_finish_item'=>$request->plan_finish_item,
        'plan_duration_item'=>$duration_plan,
        'plan_progress_item'=>1,
        'actual_start_item'=>$request->actual_start_item,
        'actual_finish_item'=>$request->actual_finish_item,
        'actual_duration_item'=>$duration_actual,
        'actual_progress_item'=>$request->actual_progress_item,
        'last_update_item'=>Carbon::now(),
        'bukti_item'=>$image_name,
        'status_item'=>1,
        'created_by'=>Auth::user()->username,
        'date_duration_auto_email'=>$request->date_duration_auto_email,
        'catatan'=>$request->catatan,
        'reminder_count'=>1,
        'create_by'=>Auth::user()->id_pengguna
    ]);
    $get = User::where('username',$request->pic_item)->first();
    $notif                    = new Notification;
    $notif->id_project        = $request->id_project;
    $notif->part_number       = $request->part_number;
    $notif->pengirim          = Auth::user()->id_pengguna;
    $notif->penerima          = $get->id_pengguna;
    $notif->tipe              = 'item';
    $notif->id_item           = $addItem->id_item;
    $notif->status_read       = 'un_read';
    $notif->save();

$pengirim = $request->email_pengirim;
$password_pengirim = $request->password_pengirim;
$project = TypeProject::where('id_project',$request->id_project)->first();
$product = Product::where('part_number',$request->part_number)->first();
$manager = MemberProject::where('id_project',$request->id_project)->first();
   $body = '<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>
<body>
    <div style="float: center;margin-right: 10px;">
        <img src="cid:logoaski" alt="Logo" style="height: 50px;width: 250px">
    </div>
    <br>
    <h2 style="margin-bottom: 0;">PROJECT MANAGEMENT SYSTEM</h2>


    <div style="clear: both"></div>
    <hr />

    <div style="text-align: justify">


        Kepada Yth.
        <br><p>
        "'.$manager->username.'"

        <p>
        Berikut saya sampaikan bahwa terdapat Item Control Baru pada : <p>
        <table>
            <tr>
                <td width="40%">Project</td>
                <td> : </td>
                <td>"'.$project->project_name.'"</td>
            </tr>
            <tr>
                <td width="40%">Part Number</td>
                <td> : </td>
                <td>"'.$product->part_number.'"</td>
            </tr>
            <tr>
                <td width="40%">Part Name</td>
                <td> : </td>
                <td>"'.$product->part_name.'"</td>
            </tr>

        </table>
        <p>
            Mohon segera diperiksa, terimakasih atas waktunya


            Hormat Saya,<br>
            <?php echo $nama_pengirim;?>
        <p>
        <a href="http://10.14.39.75:3000>Klik disini untuk menindak lanjuti Item Control</a>
        <p>


    </div>
</body>
</html>';

try {
$pop = new POP3();
$pop->Authorise('aski.component.astra.co.id', 110, 25, $pengirim, $password_pengirim, 1);

      require '../vendor/autoload.php';
      $mail = new PHPMailer();

      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
      $mail->IsSMTP();
      $mail->Host     = 'aski.component.astra.co.id';
      $mail->setFrom('noreply.aldi@gmail.com', 'PROJECT-MANAGEMENT');
      $mail->addAddress($request->email_penerima);
      if ($_FILES['bukti_item']) {
          $mail->addAttachment($_FILES['bukti_item']['tmp_name'], $_FILES['bukti_item']['name']);

         }
      $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html
      $mail->Subject = 'RFQ DOCUMENT';
      $mail->Body = $body;
      $mail->send();
      } catch (phpmailerException $e) {
       $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      } catch (Exception $e) {
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      }


            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');

      return redirect('project/item_control_detail/'.$request->id_project.'/'.$data->id_category.'/'.$request->part_number.'/'.$request->id_process);
  }
  public function remove_category_control(Request $request,$id)
  {
    $category = CategoryName::where('id_category',$id)->first();
    if ($category) {
      $item_control = ItemControl::where('id_category',$id)->delete();
      $category->delete();
    }
    if ($category) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been delete');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }
      return redirect()->back();
  }
  public function delete_item_control(Request $request,$id_item)
  {
    $data = ItemControl::where('id_item',$id_item)->delete();
       if ($data) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been delete');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }
      return redirect()->back();
  }
  public function edit_item_control($id_project,$id_category,$part_number,$id_process,$id_item)
  {
    
    $categoris = $id_category;
    $data = ItemControl::where('id_item',$id_item)->first();
    $get_leader = User::where('id_pengguna',$data->create_by)->first();
    $get_email_pic = User::where('username',$data->pic_item)->first();
    $email_leader['email_leader'] = $get_leader ==true ? $get_leader->email : null;
    $email_pic['email_pic'] = $get_email_pic ==true ? $get_email_pic->email : null;
    $category = DB::table('master_item_control')->where('id_item',$data->id_item)->first();
    //  return $category;
     // return $category;
    $member = User::get();
    $product = Product::where('id_project',$id_project)->first();
    $check = '';
    if (Auth::user()->hak_akses =='project_manager' || Auth::user()->hak_akses =='project_leader') {
      $check = '';
    }else{
      $check = 'readonly';
    }

      return view('project.leader.form.add_new_item',compact('email_pic','email_leader','check','category','member','product','categoris','data','id_project','id_category','part_number','id_process','id_item'));
    
  }

public function post_technical_review(Request $request)
{

  //set document
  $count_technical_review = DB::table('technical_review')->where([
    'id_project'=>$request->id_project,
    'part_number'=>$request->part_number
  ])->count();
  $project = TypeProject::where('id_project',$request->id_project)->first();
  $sum_technical = $count_technical_review+1;
  $no_document  = 'FR-ENGG.0'.$sum_technical.'-00'.$sum_technical;
  $tr_name = 'TECHNICAL REVIEW '.$sum_technical;
  //



//return $request->all();
  DB::beginTransaction();

  try {

      DB::table('technical_review')->insert([
    'id_project'=>$request->id_project,
    'part_number'=>$request->part_number,
    'tr_name'=>$tr_name,
    'last_update_tr'=>Carbon::now(),
    'update_tr_by'=>Auth::user()->username,
    'no_document'=>$no_document
   ]);


     DB::table('tbl_basic_spec')->insert([
     'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'targetmasspro'=>$request->target_masspro,
      'forecasttahun'=>$request->forcase_year,
      'forecastbulan'=>$request->forcase_month,
      'forecasthari'=>$request->forcase_day
    ]);


   //PERINTAH INSERT CUSTOM UTILITY INSERT
if(!empty($request->utility_custome_name)) {
    foreach ($request->utility_custome_name as $key => $value) {
      UtilityCustom::create([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$value,
        'value'=>$request->utility_custome_value[$key],
        ]);
      }
  }

    //PERINTAH INSERT CUSTOM GUAGE INSERT
if(!empty($request->guage_custom_name)) {
    foreach ($request->guage_custom_name as $key => $value) {
       GuageCustom::create([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$value,
        ]);
      }
  }
 DB::table('tbl_process_spec')->insert([
      'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'beratpartpi'=>$request->beratpartpi,
      'beratrunnerpi'=>$request->beratrunnerpi,
      'nocavitypi'=>$request->nocavitypi,
      'mesininjectionpi'=>$request->mesininjectionpi,
      'finishing'=>$request->finishing,
      'assygeneral'=>$request->assygeneral,
      'targetctimepi'=>$request->targetctimepi,
      'targetctimeassypi'=>$request->targetctimeassypi,
      'estimaterejectionpi'=>$request->estimaterejectionpi,
      'estimatepemakaianmesinharipi'=>$request->estimatepemakaianmesinharipi,
      'nomor_mesin'=>$request->nomor_mesin,
      'kapasitasmesintersediapi'=>$request->kapasitasmesintersediapi,
      'linerearview'=>$request->linerearview,
      'targetctimerearview'=>$request->targetctimerearview,
      'estimaterejectionrearview'=>$request->estimaterejectionrearview,
      'estimatepemakaianrearview'=>$request->estimatepemakaianrearview,
      'kapasitasrearview'=>$request->kapasitasrearview,
      'kapasitasterpakairearview'=>$request->kapasitasterpakairearview,
      'kapasitastersediarearview'=>$request->kapasitastersediarearview,
      'materialfloatglass'=>$request->materialfloatglass,
      'qtyfloatglass'=>$request->qtyfloatglass,
      'lineassyoutermirror'=>$request->lineassyoutermirror,
      'targetctimeassyoutermirror'=>$request->targetctimeassyoutermirror,
      'estimaterejectionassyoutermirror'=>$request->estimaterejectionassyoutermirror,
      'estimatepemakaianassyoutermirror'=>$request->estimatepemakaianassyoutermirror,
      'kapasitasassyoutermirror'=>$request->kapasitasassyoutermirror,
      'kapasitasterpakaiassyoutermirror'=>$request->kapasitasterpakaiassyoutermirror,
      'kapasitastersediaassyoutermirror'=>$request->kapasitastersediaassyoutermirror,
      'linefoaming'=>$request->linefoaming,
      'beratpartfoaming'=>$request->beratpartfoaming,
      'beratrunnerfoaming'=>$request->beratrunnerfoaming,
      'targetctimefoaming'=>$request->targetctimefoaming,
      'estimaterejectionfoaming'=>$request->estimaterejectionfoaming,
      'estimatepemakaianfoaming'=>$request->estimatepemakaianfoaming,
      'kapasitasfoaming'=>$request->kapasitasfoaming,
      'kapasitasterpakaifoaming'=>$request->kapasitasterpakaifoaming,
      'kapasitastersediafoaming'=>$request->kapasitastersediafoaming,
      'materialpolyol'=>$request->materialpolyol,
      'materialisocyanalk'=>$request->materialisocyanalk,
      'materialmoldrelease'=>$request->materialmoldrelease,
      'lineassyseatbottom'=>$request->lineassyseatbottom,
      'targetctimeassyseatbottom'=>$request->targetctimeassyseatbottom,
      'estimaterejectionassyseatbottom'=>$request->estimaterejectionassyseatbottom,
      'estimatepemakaianassyseatbottom'=>$request->estimatepemakaianassyseatbottom,
      'kapasitasassyseatbottom'=>$request->kapasitasassyseatbottom,
      'kapasitasterpakaiassyseatbottom'=>$request->kapasitasterpakaiassyseatbottom,
      'kapasitastersediaassyseatbottom'=>$request->kapasitastersediaassyseatbottom,
      'linepainting'=>$request->linepainting,
      'targetctimepainting'=>$request->targetctimepainting,
      'estimaterejectionbuffing'=>$request->estimaterejectionbuffing,
      'estimaterejectionsanding'=>$request->estimaterejectionsanding,
      'estimaterejectiontouchup'=>$request->estimaterejectiontouchup,
      'estimaterejectionrepaint'=>$request->estimaterejectionrepaint,
      'estimaterejectionouttotal'=>$request->estimaterejectionouttotal,
      'estimatepemakaianpainting'=>$request->estimatepemakaianpainting,
      'kapasitaspainting'=>$request->kapasitaspainting,
      'kapasitasterpakaipainting'=>$request->kapasitasterpakaipainting,
      'kapasitastersediapainting'=>$request->kapasitastersediapainting,
      'catundercoat'=>$request->catundercoat,
      'satuancatundercoat'=>$request->satuancatundercoat,
      'cattopcoat'=>$request->cattopcoat,
      'satuancattopcoat'=>$request->satuancattopcoat,
      'thinnerundercoat'=>$request->thinnerundercoat,
      'satuanthinnerundercoat'=>$request->satuanthinnerundercoat,
      'thinnertopcoat'=>$request->thinnertopcoat,
      'satuanthinnertopcoat'=>$request->satuanthinnertopcoat,
      'hardener'=>$request->hardener,
      'satuanhardener'=>$request->satuanhardener,
      'estimatepemakaianmesinbulanpi'=>$request->estimatepemakaianmesinbulanpi,
      'satuanpolyol'=>$request->satuanpolyol,
      'satuanisocyanalk'=>$request->satuanisocyanalk,
      'satuanmoldrelease'=>$request->satuanmoldrelease,
      'satuanqtyfloatglass'=>$request->satuanqtyfloatglass,
      'namecatundercoat'=>$request->namecatundercoat,
      'namecattopcoat'=>$request->namecattopcoat,
      'namethinnerundercoat'=>$request->namethinnerundercoat,
      'namethinnertopcoat'=>$request->namethinnertopcoat,
      'namehardener'=>$request->namehardener,
      'statusmoldpi'=>$request->statusmoldpi
  ]);

      DB::table('tbl_utility_spec')->insert([
         'part_number'=>$request->part_number,
         'no_document'=>$no_document,
         'hotrunner'=>$request->hotrunner,
         'chiller'=>$request->chiller,
         'mtcair'=>$request->mtcair,
         'mtcoil'=>$request->mtcoil,
         'rendaman'=>$request->rendaman,
         'pendingin'=>$request->pendingin,
         'anginbertekanan'=>$request->anginbertekanan,
         'mejaassy'=>$request->mejaassy,
         'ovenconveyor'=>$request->ovenconveyor,
         'staples'=>$request->staples
      ]);


  DB::table('tbl_material_spec')->insert([
       'part_number'=>$request->part_number,
       'no_document'=>$no_document,
       'typematerial'=>$request->typematerial,
       'gradematerial'=>$request->gradematerial,
       'statusmaterial'=>$request->statusmaterial,
       'qtymonthmaterial'=>$request->qtymonthmaterial,
       'warnaaditif'=>$request->warnaaditif,
       'gradeaditif'=>$request->gradeaditif,
       'dosageaditif'=>$request->dosageaditif,
       'preheating'=>$request->preheating,
       'hopperpreheating'=>$request->hopperpreheating,
       'menggunakandehumidifiying'=>$request->menggunakandehumidifiying,
       'dehumidifiyingtersedia'=>$request->dehumidifiyingtersedia,
       'satuantypematerial'=>$request->satuantypematerial
    ]);


  DB::table('tbl_gauge')->insert([
    'part_number'=>$request->part_number,
    'no_document'=>$no_document,
    'jig'=>$request->jig,
    'gonogo'=>$request->gonogo,
    'cf'=>$request->cf,
    'errorprofing'=>$request->errorprofing
   ]);



  DB::table('tbl_packaging')->insert([
    'part_number'=>$request->part_number,
    'no_document'=>$no_document,
    'typeboxpackaging'=>$request->typeboxpackaging,
    'panjangdimensiboxpackaging'=>$request->panjangdimensiboxpackaging,
    'lebardimensiboxpackaging'=>$request->lebardimensiboxpackaging,
    'tinggidimensiboxpackaging'=>$request->tinggidimensiboxpackaging,
    'isipartbox'=>$request->isipartbox,
    'totalkebutuhanbox'=>$request->totalkebutuhanbox,
    'kantongpackaging'=>$request->kantongpackaging,
    'typekantongpackaging'=>$request->typekantongpackaging,
    'panjangdimensikantongpackaging'=>$request->panjangdimensikantongpackaging,
    'lebardimensikantongpackaging'=>$request->lebardimensikantongpackaging,
    'qtykantongpackaging'=>$request->qtykantongpackaging,
    'layerskat'=>$request->layerskat,
    'typekartonskat'=>$request->typekartonskat,
    'typecorrugatedskat'=>$request->typecorrugatedskat,
    'typeplastikskat'=>$request->typeplastikskat,
    'panjangdimensiskat'=>$request->panjangdimensiskat,
    'lebardimensiskat'=>$request->lebardimensiskat,
    'keretahandling'=>$request->keretahandling,
    'jumlahtumpukankeretahandling'=>$request->jumlahtumpukankeretahandling,
    'totalkebutuhankeretahandling'=>$request->totalkebutuhankeretahandling

   ]);



   DB::table('tbl_building_spec')->insert([

     'part_number'=>$request->part_number,
     'no_document'=>$no_document,
     'dimensiruangrawmaterial'=>$request->dimensiruangrawmaterial,
     'ruangtersediarawmaterial'=>$request->ruangtersediarawmaterial,
     'dimensiruangboxkosong'=>$request->dimensiruangboxkosong,
     'ruangtersediaboxkosong'=>$request->ruangtersediaboxkosong,
     'dimensiruangpartfinishgood'=>$request->dimensiruangpartfinishgood,
     'ruangtersediapartfinishgood'=>$request->ruangtersediapartfinishgood,

   ]);


  DB::table('tbl_catatan_tr')->insert([
      'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'note'=>$request->note,
      'keterangan'=>$request->keterangan,
      'rekomendasi'=>$request->rekomendasi,
      'produkok'=>$request->produkok,
      'produkng'=>$request->produkng
     ]);




//use array
   //PERINTAH INSERT COMPONENT INSERT
if(!empty($request->qty_component)) {
    foreach ($request->qty_component as $key => $value) {
      DB::table('tbl_component_insert')->insert([

        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'nama_component'=>$request->qty_component[$key],
        'qty_component'=>$request->qty_component[$key],
        'satuancomponent'=>$request->satuancomponent[$key]

        ]);
      }
  }
//PERINTAH INSERT ETC UTILITY

//INPUTAN ETC UTILITY

      //   DB::table('tbl_etc_utility')->insert([
      //   'part_number'=>$request->part_number,
      //   'no_document'=>$no_document,
      //   'etc_description'=>$request->etc_description

      // ]);



   //PERINTAH INSERT TESTING REQUIREMENT
    if(!empty($request->jenis_pengujian)) {
      foreach ($request->jenis_pengujian as $key => $value) {
        DB::table('tbl_testing_requirement')->insert([
            'part_number'=>$request->part_number,
            'no_document'=>$no_document,
            'jenis_pengujian'=>$value,
            'tempat_pengujian'=>$request->jenis_pengujian[$key]
          ]);
      }

    }
   //PERINTAH INSERT OTHER SPECIFICATION
     if(!empty($request->description_other_spec)) {
      foreach ($request->description_other_spec as $key => $value) {
          DB::table('tbl_other_spec')->insert([
          'part_number'=>$request->part_number,
          'no_document'=>$no_document,
          'description_other_spec'=>$value
        ]);
      }

    }
  //PERINTAH INSERT LIST OF INVESMENT
     if(!empty($request->list_invesment)) {
      foreach ($request->list_invesment as $key => $value) {

       DB::table('tbl_list_invesment')->insert([
          'part_number'=>$request->part_number,
          'no_document'=>$no_document,
          'list_invesment'=>$value,
          'qty_list'=>$request->qty_list[$key],
          'satuaninvestasi'=>$request->satuaninvestasi[$key]
        ]);

        }
    }
    DB::commit();
    // all good
  } catch (\Exception $e) {
    DB::rollback();
    return $e;
  }
    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Admin has been add technical review');
    return redirect('project/display_project_detail/'.$project->id_customer.'/'.$project->id_project);

}

public function update_item_control(Request $request,$id_item)
{
// return $request->all();
$membernew                = new MemberProject;
$membernew->username      = $request->pic_item;
$membernew->id_project    = $request->id_project;
$membernew->id_position   = 3;
$membernew->id_process    = $request->id_process;
$membernew->availability  = $request->availability;
$membernew->standar_rate  = $request->standar_rate;
$membernew->duration_rate = $request->duration_rate;
$membernew->part_number   = $request->part_number;
$membernew->save(); 

$duration_plan = 0;
if ($request->plan_finish_item ==true) {
 $plan_start = Carbon::parse($request->plan_start_item);
$plan_finish = Carbon::parse($request->plan_finish_item);

$duration_plan = $plan_start->diffInDays($plan_finish);
}
$duration_actual = 0;
if ($request->actual_finish_item ==true) {
$actual_start = Carbon::parse($request->actual_start_item);
$actual_finish = Carbon::parse($request->actual_finish_item);

$duration_actual = $actual_start->diffInDays($actual_finish);
}
//return $duration_actual;
//return $diff;
//return $request->all();
$image_name = null;
$datas = CategoryName::where('part_number',$request->part_number)->first();
 if ($request->HasFile('bukti_item')) {
            $destination_path = "/public/document/";
            $image = $request->file('bukti_item');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('bukti_item')->storeAs($destination_path,$image_name);
            $data['bukti_item'] = $image_name;
      }
      $checkitem = MasterItemControl::where('item_name',$request->id_category)->count();
      $addItem = 0;
if ($checkitem > 0) {
  MasterItemControl::where('item_name',$request->id_category)->update([
    'item_name'=>$request->id_category,
  ]);
}else{
    $id_item = new  MasterItemControl;
    $id_item->item_name = $request->id_category;
    $id_item->id_category = $datas->id_category;
    $id_item->part_number = $request->part_number;
    $id_item->save();
}
$data = DB::table('item_control')->where('id_item',$id_item)->update([
'id_item'=>$id_item,
'id_category'=>$request->categoris,
'part_number'=>$request->part_number,
'id_project'=>$request->id_project,
'pic_item'=>$request->pic_item,
 'id_status'=>1,
'plan_start_item'=>$request->plan_start_item,
'plan_finish_item'=>$request->plan_finish_item,
'plan_duration_item'=>$duration_plan,
'plan_progress_item'=>1,
'actual_start_item'=>$request->actual_start_item,
'actual_finish_item'=>$request->actual_finish_item,
'actual_duration_item'=>$duration_actual,
'actual_progress_item'=>$request->actual_progress_item,
'last_update_item'=>Carbon::now(),
'bukti_item'=>$image_name,
'status_item'=>1,
'created_by'=>Auth::user()->username,
'date_duration_auto_email'=>$request->date_duration_auto_email,
'catatan'=>$request->catatan,
'reminder_count'=>1
]);
 $get = User::where('username',$request->pic_item)->first();
    $get = User::where('username',$request->pic_item)->first();
    $notif                    = new Notification;
    $notif->id_project        = $request->id_project;
    $notif->part_number       = $request->part_number;
    $notif->pengirim          = Auth::user()->id_pengguna;
    $notif->penerima          = $get->id_pengguna;
    $notif->tipe              = 'item';
    $notif->id_item           = $id_item;
    $notif->status_read       = 'un_read';
    $notif->save();
  if($get->id_pengguna == Auth::user()->id_pengguna){
   $check_create = ItemControl::where(['id_item'=>$id_item,'id_project'=>$request->id_project,'part_number'=>$request->part_number])->first();
  
  Notification::create([
    'id_project'=>$request->id_project,
    'part_number'=>$request->part_number,
    'pengirim'=>Auth::user()->id_pengguna,
    'penerima'=>$check_create ==true ? $check_create->create_by : Auth::user()->id_pengguna,
    'tipe'=>'item',
    'status_read'=>'un_read'
  ]);
}
$pengirim = $request->email_pengirim;
$password_pengirim = $request->password_pengirim;
$project = TypeProject::where('id_project',$request->id_project)->first();
$product = Product::where('part_number',$request->part_number)->first();
   $body = '<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>
<body>
    <div style="float: center;margin-right: 10px;">
        <img src="cid:logoaski" alt="Logo" style="height: 50px;width: 250px">
    </div>
    <br>
    <h2 style="margin-bottom: 0;">PROJECT MANAGEMENT SYSTEM</h2>


    <div style="clear: both"></div>
    <hr />

    <div style="text-align: justify">


        Kepada Yth.
        <br><p>


        <p>
        Berikut saya sampaikan bahwa Item Control telah di perbaharui oleh saya sendiri, berikut detail pembaharuannya : <p>
        <table>
            <tr>
                <td width="40%">Project</td>
                <td> : </td>
                <td>"'.$project->project_name.'"</td>
            </tr>
            <tr>
                <td width="40%">Part Number</td>
                <td> : </td>
                <td>"'.$product->part_number.'"</td>
            </tr>
            <tr>
                <td width="40%">Part Name</td>
                <td> : </td>
                <td>"'.$product->part_name.'"</td>
            </tr>

        </table>
        <p>
            Mohon segera diperiksa, terimakasih atas waktunya


            Hormat Saya,<br>
            <?php echo $nama_pengirim;?>
        <p>
        <a href="http://10.14.39.75:3000>Klik disini untuk menindak lanjuti Item Control</a>
        <p>


    </div>
</body>
</html>';

try {
$pop = new POP3();
$pop->Authorise('aski.component.astra.co.id', 110, 25, $pengirim, $password_pengirim, 1);

      require '../vendor/autoload.php';
      $mail = new PHPMailer();

      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
      $mail->IsSMTP();
      $mail->Host     = 'aski.component.astra.co.id';
      $mail->setFrom('noreply.aldi@gmail.com', 'PROJECT-MANAGEMENT');
      $mail->addAddress($request->email);
      if ($_FILES['bukti_item']) {
          $mail->addAttachment($_FILES['bukti_item']['tmp_name'], $_FILES['bukti_item']['name']);

         }
      $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html
      $mail->Subject = 'RFQ DOCUMENT';
      $mail->Body = $body;
      $mail->send();
      } catch (phpmailerException $e) {
       $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      } catch (Exception $e) {
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      }



    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Admin has been update item');
$data = CategoryName::where('part_number',$request->part_number)->first();
  return redirect('project/item_control_detail/'.$request->id_project.'/'.$data->id_category.'/'.$request->part_number.'/'.$request->id_process);

}
public function edit_technical_review($id_tr,$part_number,$id)
{
  $data = TechnicalReview::where('no_document',$id_tr)->where('part_number',$part_number)->first();
  $project = TypeProject::where('id_project',$data->id_project)->first();
  $product = Product::where(['id_project'=>$data->id_project,'part_number'=>$data->part_number])->first();
  $process = Process::where('id_process',$product->id_process)->first();
  $customer =  Customer::where('id_customer',$project->id_customer)->first();

  $basic = BasicSpek::where('no_document',$data->no_document)->where('part_number',$data->part_number)->first();
  //return $basic;

  $process_spect = ProcessSpek::where('no_document',$data->no_document)->where('part_number',$data->part_number)->first();
  if (empty($process_spect)) {
    $process_spect =null;
  }
//return $process_spect;
   $material_spec = MaterialSpec::where('no_document',$data->no_document)->where('part_number',$part_number)->first();

//custom
   $utility_custom = UtilityCustom::where('no_document',$data->no_document)->where('part_number',$part_number)->get();
   $guage_custom = GuageCustom::where('no_document',$data->no_document)->where('part_number',$part_number)->get();

$building_spec = BuildingSpec::where('no_document',$data->no_document)->where('part_number',$part_number)->first();
$catatan_tr = CatatanTr::where('no_document',$data->no_document)->where('part_number',$part_number)->first();
$componen_insert = ComponenInsert::where('no_document',$data->no_document)->where('part_number',$part_number)->get();
$guage = Guage::where('no_document',$data->no_document)->where('part_number',$part_number)->first();
//return $guage;
$list_invesment = ListInvesment::where('no_document',$data->no_document)->where('part_number',$part_number)->get();
$other_spec = OtherSpec::where('no_document',$data->no_document)->where('part_number',$part_number)->get();
$packaging = Packaging::where('no_document',$data->no_document)->where('part_number',$part_number)->first();
$testing_requirement = TestingRequirement::where('no_document',$data->no_document)->where('part_number',$part_number)->get();
$utility_spec = UtilitySpec::where('no_document',$data->no_document)->where('part_number',$part_number)->first();
//return $list_invesment;
 return view('project.leader.form.edit_technical_review',compact('catatan_tr','list_invesment','other_spec','building_spec','testing_requirement','packaging','guage','utility_spec','componen_insert','material_spec','process_spect','basic','data','project','product','process','customer','part_number','id','utility_custom','guage_custom'));
}
public function upload_document_rfq(Request $request)
{
  $pengirim = $request->email;
  $password_pengirim = $request->password;
  $files = $request->rfq_file;
  $data = new DocumentRfq;
  $data->id_project = $request->id_project;
  $data->date_created = Carbon::now();
  $data->status     = 0;
      if ($request->HasFile('rfq_file')) {
            $destination_path = "/public/document/";
            $image = $request->file('rfq_file');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('rfq_file')->storeAs($destination_path,$image_name);
            $data['rfq_file'] = $image_name;
      }
      $data->save();
$penerima = User::where('hak_akses','administrator')->first();
$project = TypeProject::where('id_project',$request->id_project)->first();

     $body = '    <html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>
<body>
    <div style="float: center;margin-right: 10px;">
        <img src="cid:logoaski" alt="Logo" style="height: 50px;width: 250px">
    </div>
    <br>
    <h2 style="margin-bottom: 0;">PROJECT MANAGEMENT SYSTEM</h2>


    <div style="clear: both"></div>
    <hr />

    <div style="text-align: justify">
        Kepada Yth.
        <br><p>
         "'.$penerima->firstname.'" "'.$penerima->lastname.'"<br>
        <p>
        Dengan ini saya sampaikan bahwa Dokumen RFQ Project "'.$project->project_name.'" baru saja saya Upload ke Sistem dan Terlampir di Email yang saya kirim.<p>
            Mohon segera diperiksa dan Beritahukan kepada saya jika sudah selesai, terimakasih atas waktunya <?php
        echo $firstname;echo " ";echo $lastname;?>
            <p>
            Hormat Saya,<br>
            "'.$penerima->firstname.'" "'.$penerima->lastname.'"
        <p>
        <a href="http://10.14.39.75:3000">Klik disini untuk menindak lanjutinya</a>
</body>
</html>';
try {
$pop = new POP3();
$pop->Authorise('aski.component.astra.co.id', 110, 25, $pengirim, $password_pengirim, 1);

      require '../vendor/autoload.php';
      $mail = new PHPMailer();

      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
      $mail->IsSMTP();
      $mail->Host     = 'aski.component.astra.co.id';
      $mail->setFrom('noreply.aldi@gmail.com', 'PROJECT-MANAGEMENT');
      $mail->addAddress('aldimisbahul234@gmail.com');
      if ($files ==true) {
          $mail->addAttachment($_FILES['rfq_file']['tmp_name'], $_FILES['rfq_file']['name']);

         }
      $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html
      $mail->Subject = 'RFQ DOCUMENT';
      $mail->Body = $body;
      $mail->send();
      } catch (phpmailerException $e) {
       $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      } catch (Exception $e) {
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      }
          $request->session()->flash('status', '200');
          $request->session()->flash('msg', 'Admin has been added');


      return redirect()->back();
}
public function update_technical_review(Request $request,$no_document,$part_number)
{
// $data = ProcessSpek::where(['part_number'=>$part_number,'no_document'=>$no_document])->first();
// return $data;
 // return $request->all();
  DB::beginTransaction();

  try {

      DB::table('technical_review')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
    'id_project'=>$request->id_project,
    'part_number'=>$request->part_number,
    'tr_name'=>$request->id_tr,
    'last_update_tr'=>Carbon::now(),
    'update_tr_by'=>Auth::user()->username,
    'no_document'=>$no_document
   ]);


     DB::table('tbl_basic_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
     'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'targetmasspro'=>$request->target_masspro,
      'forecasttahun'=>$request->forcase_year,
      'forecastbulan'=>$request->forcase_month,
      'forecasthari'=>$request->forcase_day
    ]);

 DB::table('tbl_process_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
      'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'beratpartpi'=>$request->beratpartpi,
      'beratrunnerpi'=>$request->beratrunnerpi,
      'nocavitypi'=>$request->nocavitypi,
      'mesininjectionpi'=>$request->mesininjectionpi,
      'finishing'=>$request->finishing,
      'assygeneral'=>$request->assygeneral,
      'targetctimepi'=>$request->targetctimepi,
      'targetctimeassypi'=>$request->targetctimeassypi,
      'estimaterejectionpi'=>$request->estimaterejectionpi,
      'estimatepemakaianmesinharipi'=>$request->estimatepemakaianmesinharipi,
      'nomor_mesin'=>$request->nomor_mesin,
      'kapasitasmesintersediapi'=>$request->kapasitasmesintersediapi,
      'linerearview'=>$request->linerearview,
      'targetctimerearview'=>$request->targetctimerearview,
      'estimaterejectionrearview'=>$request->estimaterejectionrearview,
      'estimatepemakaianrearview'=>$request->estimatepemakaianrearview,
      'kapasitasrearview'=>$request->kapasitasrearview,
      'kapasitasterpakairearview'=>$request->kapasitasterpakairearview,
      'kapasitastersediarearview'=>$request->kapasitastersediarearview,
      'materialfloatglass'=>$request->materialfloatglass,
      'qtyfloatglass'=>$request->qtyfloatglass,
      'lineassyoutermirror'=>$request->lineassyoutermirror,
      'targetctimeassyoutermirror'=>$request->targetctimeassyoutermirror,
      'estimaterejectionassyoutermirror'=>$request->estimaterejectionassyoutermirror,
      'estimatepemakaianassyoutermirror'=>$request->estimatepemakaianassyoutermirror,
      'kapasitasassyoutermirror'=>$request->kapasitasassyoutermirror,
      'kapasitasterpakaiassyoutermirror'=>$request->kapasitasterpakaiassyoutermirror,
      'kapasitastersediaassyoutermirror'=>$request->kapasitastersediaassyoutermirror,
      'linefoaming'=>$request->linefoaming,
      'beratpartfoaming'=>$request->beratpartfoaming,
      'beratrunnerfoaming'=>$request->beratrunnerfoaming,
      'targetctimefoaming'=>$request->targetctimefoaming,
      'estimaterejectionfoaming'=>$request->estimaterejectionfoaming,
      'estimatepemakaianfoaming'=>$request->estimatepemakaianfoaming,
      'kapasitasfoaming'=>$request->kapasitasfoaming,
      'kapasitasterpakaifoaming'=>$request->kapasitasterpakaifoaming,
      'kapasitastersediafoaming'=>$request->kapasitastersediafoaming,
      'materialpolyol'=>$request->materialpolyol,
      'materialisocyanalk'=>$request->materialisocyanalk,
      'materialmoldrelease'=>$request->materialmoldrelease,
      'lineassyseatbottom'=>$request->lineassyseatbottom,
      'targetctimeassyseatbottom'=>$request->targetctimeassyseatbottom,
      'estimaterejectionassyseatbottom'=>$request->estimaterejectionassyseatbottom,
      'estimatepemakaianassyseatbottom'=>$request->estimatepemakaianassyseatbottom,
      'kapasitasassyseatbottom'=>$request->kapasitasassyseatbottom,
      'kapasitasterpakaiassyseatbottom'=>$request->kapasitasterpakaiassyseatbottom,
      'kapasitastersediaassyseatbottom'=>$request->kapasitastersediaassyseatbottom,
      'linepainting'=>$request->linepainting,
      'targetctimepainting'=>$request->targetctimepainting,
      'estimaterejectionbuffing'=>$request->estimaterejectionbuffing,
      'estimaterejectionsanding'=>$request->estimaterejectionsanding,
      'estimaterejectiontouchup'=>$request->estimaterejectiontouchup,
      'estimaterejectionrepaint'=>$request->estimaterejectionrepaint,
      'estimaterejectionouttotal'=>$request->estimaterejectionouttotal,
      'estimatepemakaianpainting'=>$request->estimatepemakaianpainting,
      'kapasitaspainting'=>$request->kapasitaspainting,
      'kapasitasterpakaipainting'=>$request->kapasitasterpakaipainting,
      'kapasitastersediapainting'=>$request->kapasitastersediapainting,
      'catundercoat'=>$request->catundercoat,
      'satuancatundercoat'=>$request->satuancatundercoat,
      'cattopcoat'=>$request->cattopcoat,
      'satuancattopcoat'=>$request->satuancattopcoat,
      'thinnerundercoat'=>$request->thinnerundercoat,
      'satuanthinnerundercoat'=>$request->satuanthinnerundercoat,
      'thinnertopcoat'=>$request->thinnertopcoat,
      'satuanthinnertopcoat'=>$request->satuanthinnertopcoat,
      'hardener'=>$request->hardener,
      'satuanhardener'=>$request->satuanhardener,
      'estimatepemakaianmesinbulanpi'=>$request->estimatepemakaianmesinbulanpi,
      'satuanpolyol'=>$request->satuanpolyol,
      'satuanisocyanalk'=>$request->satuanisocyanalk,
      'satuanmoldrelease'=>$request->satuanmoldrelease,
      'satuanqtyfloatglass'=>$request->satuanqtyfloatglass,
      'namecatundercoat'=>$request->namecatundercoat,
      'namecattopcoat'=>$request->namecattopcoat,
      'namethinnerundercoat'=>$request->namethinnerundercoat,
      'namethinnertopcoat'=>$request->namethinnertopcoat,
      'namehardener'=>$request->namehardener,
      'statusmoldpi'=>$request->statusmoldpi
  ]);

      DB::table('tbl_utility_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
         'part_number'=>$request->part_number,
         'no_document'=>$no_document,
         'hotrunner'=>$request->hotrunner,
         'chiller'=>$request->chiller,
         'mtcair'=>$request->mtcair,
         'mtcoil'=>$request->mtcoil,
         'rendaman'=>$request->rendaman,
         'pendingin'=>$request->pendingin,
         'anginbertekanan'=>$request->anginbertekanan,
         'mejaassy'=>$request->mejaassy,
         'ovenconveyor'=>$request->ovenconveyor,
         'staples'=>$request->staples
      ]);


  DB::table('tbl_material_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
       'part_number'=>$request->part_number,
       'no_document'=>$no_document,
       'typematerial'=>$request->typematerial,
       'gradematerial'=>$request->gradematerial,
       'statusmaterial'=>$request->statusmaterial,
       'qtymonthmaterial'=>$request->qtymonthmaterial,
       'warnaaditif'=>$request->warnaaditif,
       'gradeaditif'=>$request->gradeaditif,
       'dosageaditif'=>$request->dosageaditif,
       'preheating'=>$request->preheating,
       'hopperpreheating'=>$request->hopperpreheating,
       'menggunakandehumidifiying'=>$request->menggunakandehumidifiying,
       'dehumidifiyingtersedia'=>$request->dehumidifiyingtersedia,
       'satuantypematerial'=>$request->satuantypematerial
    ]);


  DB::table('tbl_gauge')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
    'part_number'=>$request->part_number,
    'no_document'=>$no_document,
    'jig'=>$request->jig,
    'gonogo'=>$request->gonogo,
    'cf'=>$request->cf,
    'errorprofing'=>$request->errorprofing
   ]);



  DB::table('tbl_packaging')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
    'part_number'=>$request->part_number,
    'no_document'=>$no_document,
    'typeboxpackaging'=>$request->typeboxpackaging,
    'panjangdimensiboxpackaging'=>$request->panjangdimensiboxpackaging,
    'lebardimensiboxpackaging'=>$request->lebardimensiboxpackaging,
    'tinggidimensiboxpackaging'=>$request->tinggidimensiboxpackaging,
    'isipartbox'=>$request->isipartbox,
    'totalkebutuhanbox'=>$request->totalkebutuhanbox,
    'kantongpackaging'=>$request->kantongpackaging,
    'typekantongpackaging'=>$request->typekantongpackaging,
    'panjangdimensikantongpackaging'=>$request->panjangdimensikantongpackaging,
    'lebardimensikantongpackaging'=>$request->lebardimensikantongpackaging,
    'qtykantongpackaging'=>$request->qtykantongpackaging,
    'layerskat'=>$request->layerskat,
    'typekartonskat'=>$request->typekartonskat,
    'typecorrugatedskat'=>$request->typecorrugatedskat,
    'typeplastikskat'=>$request->typeplastikskat,
    'panjangdimensiskat'=>$request->panjangdimensiskat,
    'lebardimensiskat'=>$request->lebardimensiskat,
    'keretahandling'=>$request->keretahandling,
    'jumlahtumpukankeretahandling'=>$request->jumlahtumpukankeretahandling,
    'totalkebutuhankeretahandling'=>$request->totalkebutuhankeretahandling

   ]);



   DB::table('tbl_building_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([

     'part_number'=>$request->part_number,
     'no_document'=>$no_document,
     'dimensiruangrawmaterial'=>$request->dimensiruangrawmaterial,
     'ruangtersediarawmaterial'=>$request->ruangtersediarawmaterial,
     'dimensiruangboxkosong'=>$request->dimensiruangboxkosong,
     'ruangtersediaboxkosong'=>$request->ruangtersediaboxkosong,
     'dimensiruangpartfinishgood'=>$request->dimensiruangpartfinishgood,
     'ruangtersediapartfinishgood'=>$request->ruangtersediapartfinishgood,

   ]);


  DB::table('tbl_catatan_tr')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
      'part_number'=>$request->part_number,
      'no_document'=>$no_document,
      'note'=>$request->note,
      'keterangan'=>$request->keterangan,
      'rekomendasi'=>$request->rekomendasi,
      'produkok'=>$request->produkok,
      'produkng'=>$request->produkng
     ]);



 
//use array

  //PERINTAH INSERT CUSTOM UTILITY INSERT
if(!empty($request->utility_custome_name_add)) {
    foreach ($request->utility_custome_name_add as $key => $value) {
      UtilityCustom::create([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$value,
        'value'=>$request->utility_custome_value_add[$key],
        ]);
      }
  }

    //PERINTAH INSERT CUSTOM GUAGE INSERT
if(!empty($request->guage_custom_name_add)) {
    foreach ($request->guage_custom_name_add as $key => $value) {
       GuageCustom::create([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$value,
        ]);
      }
  }
 // PERINTAH INSERT CUSTOM UTILITY INSERT
if(!empty($request->utility_custom_id)) {
    foreach ($request->utility_custom_id as $key => $value) {
     
     UtilityCustom::where('utility_custom_id',$value)->update([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$request->utility_custom_name[$key],
        'value'=>$request->utility_custom_value[$key],
        ]);
      }
  }

    //PERINTAH INSERT CUSTOM GUAGE INSERT
if(!empty($request->guage_custom_id)) {
    foreach ($request->guage_custom_id as $key => $value) {
     GuageCustom::where('guage_custom_id',$value)->update([
        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'name'=>$request->guage_custom_name[$key],
        ]);
      }
  }
   //PERINTAH INSERT COMPONENT INSERT
if(!empty($request->qty_component)) {
    foreach ($request->qty_component as $key => $value) {
      DB::table('tbl_component_insert')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([

        'part_number'=>$request->part_number,
        'no_document'=>$no_document,
        'nama_component'=>$request->nama_component[$key],
        'qty_component'=>$request->qty_component[$key],
        'satuancomponent'=>$request->satuancomponent[$key]

        ]);
      }
  }
//PERINTAH INSERT ETC UTILITY

//INPUTAN ETC UTILITY

      //   DB::table('tbl_etc_utility')->insert([
      //   'part_number'=>$request->part_number,
      //   'no_document'=>$no_document,
      //   'etc_description'=>$request->etc_description

      // ]);



   //PERINTAH INSERT TESTING REQUIREMENT
    if(!empty($request->jenis_pengujian)) {
      foreach ($request->jenis_pengujian as $key => $value) {
        DB::table('tbl_testing_requirement')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
            'part_number'=>$request->part_number,
            'no_document'=>$no_document,
            'jenis_pengujian'=>$value,
            'tempat_pengujian'=>$request->jenis_pengujian[$key]
          ]);
      }

    }
   //PERINTAH INSERT OTHER SPECIFICATION
     if(!empty($request->description_other_spec)) {
      foreach ($request->description_other_spec as $key => $value) {
          DB::table('tbl_other_spec')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
          'part_number'=>$request->part_number,
          'no_document'=>$no_document,
          'description_other_spec'=>$value
        ]);
      }

    }
  //PERINTAH INSERT LIST OF INVESMENT
     if(!empty($request->list_invesment)) {
      foreach ($request->list_invesment as $key => $value) {

       DB::table('tbl_list_invesment')->where(['part_number'=>$part_number,'no_document'=>$no_document])->update([
          'part_number'=>$request->part_number,
          'no_document'=>$no_document,
          'list_invesment'=>$value,
          'qty_list'=>$request->qty_list[$key],
          'satuaninvestasi'=>$request->satuaninvestasi[$key]
        ]);

        }
    }
    DB::commit();
    // all good
  } catch (\Exception $e) {
    DB::rollback();
    return $e;
  }

  $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been update');
  return redirect('project/technical_review/'.$request->id_project.'/'.$request->part_number);
}
public function delete_technical_review(Request $request, $no_document,$part_number)
{
  $data = TechnicalReview::where('no_document',$no_document)->where('part_number',$part_number)->delete();


  $basic = DB::table('tbl_basic_spec')->where('no_document',$no_document)->where('part_number',$part_number)->delete();
  //return $basic;
  $process_spect = ProcessSpek::where('no_document',$no_document)->delete();

   $material_spec = MaterialSpec::where('no_document',$no_document)->where('part_number',$part_number)->delete();

$building_spec = BuildingSpec::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$catatan_tr = CatatanTr::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$componen_insert = ComponenInsert::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$guage = Guage::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$list_invesment = ListInvesment::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$other_spec = OtherSpec::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$packaging = Packaging::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$testing_requirement = TestingRequirement::where('no_document',$no_document)->where('part_number',$part_number)->delete();
$utility_spec = UtilitySpec::where('no_document',$no_document)->where('part_number',$part_number)->delete();

  $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been delete');
  return redirect()->back();
}
public function print_technical_review($id_project,$no_document,$part_number)
{

    $product = Product::where(['id_project'=>$id_project,'part_number'=>$part_number])->first();
    $project = TypeProject::where('id_project',$id_project)->first();
    $customer = Customer::where('id_customer',$project->id_customer)->first();

  return view('project.leader.print_technical_review',compact('project','customer','product','no_document','part_number'));
}
public function checkEmail()
{
   $dt = Carbon::now('Asia/Jakarta')->toDateString();
   $item_control = ItemControl::where('id_project',62)->get();
   // $users = Users::where('status_id', 'active')
   //         ->where( 'created_at', '>', date('Y-m-d', strtotime("-30 days"))
   //         ->get();
   foreach ($item_control as $key => $value) {
     $get_item[] = [
      'id_item'=>$value->id_item,
      'plan_start_item'=>$value->plan_start_item,
      'plan_finish_item'=>$value->plan_finish_item,
      'date_duration_auto_email'=>$value->date_duration_auto_email,
     ];
   }
   return $get_item;
}
public function get_email(Request $request)
{
  $data = User::where('username',$request->username)->first();
  return $data->email;
}
public function get_list_name(Request $request)
{
  $html ="<div>";
        $data = DB::table('list_upload_document')->where('document_code',$request->document_code)->get();
        foreach ($data as $key => $value) {
          $html .="<h4>".$value->list_name."</h4>";
        }
  $html .="</div>";
if (count($data) > 0) {
  return $html;
  
}else{
return "tidak ada list <br />";
  
}

}
public function update_status_document(Request $request)
{
  $data = DB::table('document_project')->where('document_code',$request->document_code)->update(['status_document'=>$request->status_document,'note'=>$request->note]);
   $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been update');
  return redirect()->back();
}

public function get_document_note(Request $request)
{
  $data = DB::table('document_project')->where('document_code',$request->document_code)->first();

 
  $get_data['result'] = $data;
  return $get_data;
}
public function save_note(Request $request)
{
  $data = DB::table('document_project')->where('document_code',$request->document_code)->update(['note'=>$request->note,'date_create'=>Carbon::now()->format('yy-m-d h:i:s')]);

  $get = DocumentProject::where('document_code',$request->document_code)->first();
  $id_pengguna = User::where('username',$get->pic)->first();
    $notif = new Notification;
    $notif->id_project =  $get->id_project;
    $notif->part_number = $get->part_number;
    $notif->pengirim =  Auth::user()->id_pengguna;
    $notif->penerima =  $id_pengguna->id_pengguna;
    $notif->tipe =  'document';
    $notif->document_code = $request->document_code;
    $notif->status_read = 'un_read';
    $notif->save();
  
  $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been update');
  return redirect()->back();
}
public function change_status_doc(Request $request, $id, $status, $id_project, $part_number)
{
  $data = DB::table('document_project')->where('document_code',$id)->update(['status_document'=>$status,'date_create'=>Carbon::now()->format('yy-m-d h:i:s')]);
  $get = DocumentProject::where('document_code',$id)->first();
  $id_pengguna = User::where('username',$get->pic)->first();

    $notif                =   new Notification;
    $notif->id_project    =   $get->id_project;
    $notif->part_number   =   $get->part_number;
    $notif->pengirim      =   Auth::user()->id_pengguna;
    $notif->penerima      =   $id_pengguna->id_pengguna;
    $notif->tipe          =   'document';
    $notif->document_code =   $get->document_code;
    $notif->status_read   =   'un_read';
    $notif->save();

  $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been update');
  return redirect()->back();
}
public function update_document(Request $request)
{


     $image = null;
     if ($request->HasFile('filename')) {
            $destination_path = "/public/document/";
            $image = $request->file('filename');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('filename')->storeAs($destination_path,$image_name);
            $image= $image_name;
      }
    

 
 $data = DB::table('document_project')->where('document_code',$request->document_code)->update(
  [
    'id_category_event_project'=>$request->id_category_event_project,
    'id_project'=>$request->id_project,
    'part_number'=>$request->part_number,
    'document_name'=>$request->document_name,
    'date_create'=>Carbon::now()->format('yy-m-d h:i:s'),
    'pic'=>$request->pic,
    'reminder_count'=>1,
    'filename' => $image,
    'create_by' => Auth::user()->id_pengguna
  ]
  );
        $id_pengguna = User::where('username',$request->pic)->first();
        $notif = new Notification;
        $notif->id_project = $request->id_project;
        $notif->part_number = $request->part_number;
        $notif->pengirim = Auth::user()->id_pengguna;
        $notif->penerima = $id_pengguna->id_pengguna;
        $notif->tipe = 'document';
        $notif->status_read = 'un_read';
        $notif->document_code = $request->document_code;
        $notif->save();

  $request->session()->flash('status', '200');
  $request->session()->flash('msg', 'Admin has been update');
  return redirect()->back();
}

}
