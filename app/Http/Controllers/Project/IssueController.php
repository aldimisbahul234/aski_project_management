<?php

namespace App\Http\Controllers\Project;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Project\Issue;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\Customer;
use App\Http\Models\Masterdata\Sector;
use App\Http\Models\Masterdata\MemberProject;
use App\Http\Models\Masterdata\CategoryEventProject;
use Illuminate\Support\Facades\Mail;
use App\Http\Models\Masterdata\ItemControl;
use App\Http\Models\Masterdata\MasterItemControl;
use App\User;
use PHPMailerPHPMailerPHPMailer;
use PHPMailerPHPMailerException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\POP3;
use Auth;
use App\Http\Models\Notification;
class IssueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function issue($id_project,$part_number,$id_category,$id_item,$id_process)
    {
      // headeer
      $project = TypeProject::where('id_project',$id_project)->first();
      $customer = Customer::where('id_customer',$project->id_customer)->first();
      $sector = Sector::where('id_product',$customer->id_product)->first();
     // return $sector;
         $data = [];
         if ($id_process =="dashboard") {
            $data = Issue::where(['id_project'=>$id_project])->get();
            $get_part = TypeProject::where('id_project',$id_project)->first();
            
         }elseif($id_process =="part_number"){
            $data = Issue::where(['id_project'=>$id_project,'part_number'=>$part_number])->get();
            $get_part = Product::where('id_project',$id_project)->first();
         }else{
         
            $data = Issue::where(['id_project'=>$id_project,'part_number'=>$part_number])->get();
            $get_part = Product::where(['id_project'=>$id_project,'part_number'=>$part_number])->first();
           
         }
      
     
       //return $data;
      
       return view('project.leader.rio',compact('project','data','id_project','part_number','id_category','id_item','id_process','get_part','customer'));

    }

    public function store(Request $request)
    {
      

    }
     public function edit($id)
    {

       
      
       
    }
   public function update(Request $request,$id){
   
       return redirect('categoryControll');
   }
    public function delete(Request $request, $id)
    {
        

       return redirect()->back();
    }
    public function create_issue($id_project,$part_number,$id_category,$id_item,$id_process)
    {
      
     
        $item_control = ItemControl::where('part_number',$part_number)->pluck('id_item');
        $get_master_item = MasterItemControl::whereIn('id_item',$item_control)->get();
       // return $get_master_item;
        $member = MemberProject::where(['id_project'=>$id_project])->get();
        $project = TypeProject::where('id_project',$id_project)->first();
       $customer = Customer::where('id_customer',$project->id_customer)->first();
       $product = Product::where(['part_number'=>$part_number,'id_project'=>$id_project])->first();
        return view('project.leader.form.add_issue',compact('product','get_master_item','id_process','id_project','part_number','id_category','member','id_item','project','customer'));
    }
   public function pos_add_issue(Request $request)
   {
    $id_item = MasterItemControl::where('id_item',$request->id_item)->first();
    $pic = User::where('username',$request->pic)->first();
    $get_pic['get_pic'] = $pic ==true ? $pic->id_pengguna : null;
    $files = $request->bukti;
    $merge = null;
    $image = 'kosong';
    $pool = '0123456789ABCDEFGHIJKLMNOPRSTU';
    $length = 16;
    if ($request->HasFile('bukti')) {
        $destination_path = "/public/document";
        $image = $request->file('bukti');
        $image_name = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        $path = $request->file('bukti')->storeAs($destination_path,$image_name);
        $image = $image_name;
    }
    $pengirim = $request->email;
    $password_pengirim = $request->password;
        $issue                    = new Issue;
        $issue->id_project        = $request->id_project;
        $issue->part_number       = $request->part_number;
        $issue->category_issue    = $request->category_issue;
        $issue->issue             = $request->issue;
        $issue->pic               = $request->pic;
        $issue->escalation        = 'EX 0';
        $issue->id_process        = $request->id_process;
        $issue->id_category       = $id_item->id_category;
        $issue->id_item           = $request->id_item;
        $issue->last_update       = Carbon::now();
        $issue->created           = Carbon::now();
        $issue->timing            = 1;
        $issue->bukti             = $image;
        $issue->status_rio        = 0;
        $issue->create_by         = Auth::user()->id_pengguna;
        $issue->save();
        
        $notif = new Notification;
        $notif->id_project = $request->id_project;
        $notif->part_number = $request->part_number;
        $notif->pengirim = Auth::user()->id_pengguna;
        $notif->penerima = $get_pic['get_pic'];
        $notif->tipe = 'issue';
        $notif->status_read = 'un_read';
        $notif->id_issue = $issue->id_issue;
        $notif->save();
 
       $penerima = User::where('username',$request->pic)->first();


     $body = '    <html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>
<body>
    <div style="float: center;margin-right: 10px;">
        <img src="cid:logoaski" alt="Logo" style="height: 50px;width: 250px">
    </div>
    <br>
    <h2 style="margin-bottom: 0;">PROJECT MANAGEMENT SYSTEM</h2>
    

    <div style="clear: both"></div>
    <hr />

    <div style="text-align: justify">
        Kepada Yth.
        <br><p>
         "'.$penerima->firstname.'" "'.$penerima->lastname.'"<br>
        <p>
        Dengan ini saya sampaikan bahwa .<p>
            Mohon segera diperiksa dan Beritahukan kepada saya jika sudah selesai, terimakasih atas waktunya <?php 
        echo $firstname;echo " ";echo $lastname;?>
            <p>
            Hormat Saya,<br>
            "'.$penerima->firstname.'" "'.$penerima->lastname.'"
        <p>
        <a href="http://10.14.39.75:3000">Klik disini untuk menindak lanjutinya</a>
</body>
</html>';
try {
$pop = new POP3();
$pop->Authorise('aski.component.astra.co.id', 110, 25, $pengirim, $password_pengirim, 1);
 
      require '../vendor/autoload.php';
      $mail = new PHPMailer(); 

      // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
      $mail->IsSMTP();
      $mail->Host     = 'aski.component.astra.co.id';                   
      $mail->setFrom('noreply.aldi@gmail.com', 'PROJECT-MANAGEMENT');                       
      $mail->addAddress('aldimisbahul234@gmail.com');
      if ($files ==true) {
          $mail->addAttachment($_FILES['bukti']['tmp_name'], $_FILES['bukti']['name']);
        
         }
      $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html
      $mail->Subject = 'RFQ DOCUMENT';
      $mail->Body = $body;
      $mail->send();
      } catch (phpmailerException $e) {
       $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      } catch (Exception $e) {
          $request->session()->flash('status', '400');
          $request->session()->flash('msg', 'Mail error');
      }

   
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
      
    return redirect()->back();
   }
   public function issue_remove(Request $request, $id)
   {
    $data = Issue::where('id_issue',$id)->delete();
    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Admin has been delete');
      
    return redirect()->back();
   }
}
