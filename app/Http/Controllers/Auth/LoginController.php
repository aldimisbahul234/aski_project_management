<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
 protected $redirectTo = '/project';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
     protected function validateLogin(Request $request)
    {
        // $request->validate([
        //     $this->username() => 'required|string',
        //     'password' => 'required|string',
        // ]);
    }
      protected function attemptLogin(Request $request)
    {

        if ($request->guest =="oke") {
            $user = User::where('username', 'guest_system')->first();
        }else{
          $user = User::where('username', $request->email)
          ->where('password', $request->password)
        ->where('hak_akses', $request->hak_akses)
        ->first();
        }
        

    if(!isset($user)){
        return false;
    }

    Auth::login($user);

    return true;
    }
     protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field      => $request->get($this->username()),
            'password'  => $request->password,
            'hak_akses'  =>$request->hak_akses,
        ];
    }
}
