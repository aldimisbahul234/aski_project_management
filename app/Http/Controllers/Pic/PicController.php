<?php

namespace App\Http\Controllers\Pic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Masterdata\TypeProject;
use App\Http\Models\Masterdata\Product;
use App\Http\Models\Masterdata\MemberProject;
use App\Http\Models\Masterdata\CategoryControllName;
use Auth;
use App\Http\Models\Project\Issue;
use Redis;
use App\Http\Models\Masterdata\ItemControl;
use App\Http\Models\Masterdata\MasterItemControl;
use Response;
use App\Http\Models\Masterdata\DocumentProject;
use App\User;
use DB;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Http\Models\Notification;
class PicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $user = Auth::user()->username;
       $count_issue_not_complate = Issue::where('pic',$user)->where('status_rio',0)->count();
       $count_issue_complate = Issue::where('pic',$user)->where('status_rio',1)->count();
       $get_my_id_project = MemberProject::where('username',$user)->whereNotNull('id_project')->pluck('id_project');
       $get_my_part_number = MemberProject::where('username',$user)->whereNotNull('part_number')->pluck('part_number');
       $get_my_product = Product::whereIn('part_number',$get_my_part_number)->get();
       $my_project = TypeProject::whereIn('id_project',$get_my_id_project)->get();
       $count_my_product = Product::whereIn('part_number',$get_my_part_number)->count();
       $count_my_project =TypeProject::whereIn('id_project',$get_my_id_project)->count();
       $select_projects = TypeProject::whereIn('id_project',$get_my_id_project)->get();
       $count_item_not_complate = ItemControl::where('pic_item',Auth::user()->username)->where('status_item','!=',2)->count();
       $count_item_complate = ItemControl::where('pic_item',Auth::user()->username)->where('status_item',2)->count();
     
       $project = TypeProject::pluck('id_project');
        $get_notif_item =  Notification::whereIn('id_project',$project)->where(['penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'item'])->count();
      $get_notif_issue =  Notification::where(['penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->whereIn('id_project',$project)->count();
     //  return $get_notif_item;
       //return $count_item_complate;

       $my_category = [];
        $pluck_item = ItemControl::where('pic_item',Auth::user()->username)->get();
        foreach ($pluck_item as $key => $value) {
           $my_category[] = [
            'part_number'=>$part_number = trim($value->part_number),
            'get_category'=>$get_category = CategoryControllName::where('part_number',$part_number)->first(),
            'category_name'=>$get_category ==true ? $get_category->category_name : null
           ];
        }
       // return $my_category;

        $get_item = null;
        if ($request->id_project) {
            $get_item = ItemControl::where(['id_project'=>$request->id_project,'part_number'=>$request->part_number,'pic_item'=>Auth::user()->username,'id_category'=>$request->id_category])->get();
        }
        $count_rio_before = DB::table('issue')->where('pic',trim(Auth::user()->username))->where('status_rio',0)->count();
        //return $count_rio_before;
       return view('pic.index',compact('count_rio_before','count_issue_not_complate','count_issue_complate','count_my_product','count_my_project','count_item_complate','count_item_not_complate','my_category','get_item','my_project','get_my_product','select_projects','get_notif_item','get_notif_issue'));

    }
    public function open_rio()
    {   
        $get_data = [];
        $data = TypeProject::all();
        foreach ($data as $key => $value) {
           $get_data[] = [
            'id_project'=>$value->id_project,
            'project_name'=>$value->project_name,
            'count_product'=>Product::where('id_project',$value->id_project)->count(),
            'get_notif'=> Notification::where(['id_project'=>$value->id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->count()
           ];
        }
        //return $get_data;
        return view('pic.open_rio',compact('get_data'));
    }
    public function project_open_rio($id)
    {
        $get_data = [];
        $project = TypeProject::where('id_project',$id)->first();
        $product = Product::where('id_project',$id)->get();
        foreach ($product as $key => $value) {
            $get_data[] = [
                'part_number'=>$part_number = $value->part_number,
                'part_name'=>$part_name = $value->part_name,
                'get_issue'=>$get_issue = Issue::where('part_number',trim($part_number))->where('status_rio',0)->where('id_project',$id)->count(),
                 'get_notif'=> Notification::where(['id_project'=>$value->id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->count()
            ];
        }
       // return $get_data;
          return view('pic.project_open_rio',compact('get_data','project'));
    }
    public function project_open_rio_get($id,$part_number)
    {
      $data = Issue::where('part_number',trim($part_number))->where('status_rio',0)->where('id_project',$id)->get();
      $get_data = [];
      foreach ($data as $key => $value) {
        $get_data[] = [
          'id_issue'=>$value->id_issue,
          'issue'=>$value->issue,
          'part_number'=>$value->part_number,
          'status_rio'=>$value->status_rio,
          'bukti'=>$value->bukti,
          'get_notif'=> Notification::where(['id_issue'=>$value->id_issue,'id_project'=>$value->id_project,'part_number'=>$part_number,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->count()
        ];
      }
    //return $get_data;
      return view('pic.project_open_rio_get',compact('get_data'));
    }
    public function project_open_rio_detail($id)
    {
      $data = Issue::find($id);
      $email = User::where('username',$data->pic)->first();
      $email_project_manager = User::where('id_pengguna',$data->create_by)->first();
      $get_mail['get_email'] = $email_project_manager == true ? $email_project_manager->email : 'email not found';
      $master_item = MasterItemControl::where('id_item',$data->id_item)->first();
      $get_item['get_items'] = $master_item == true ? $master_item->item_name : 'item not found';
      $get_answer['get_answer'] = $data == true ? $data->answer_pic : 'answer pic not found';
      //return $get_email;
       return view('pic.project_open_rio_detail',compact('get_answer','data','email','get_mail','get_item'));
    }
    public function project_open_rio_update(Request $request, $id)
    {
     // return $request->all();
      $pic = User::where('username',$request->pic)->first();
      $get_pic['get_pic'] = $pic ==true ? $pic->id_pengguna : null;
      $penerima = $request->email;
      $files = $request->file('bukti');
      $images = Issue::where('id_issue',$id)->first();
      $get_project_manager['get_project_manager'] = $images ==true ? $images->create_by : null;
     // return $get_project_manager['get_project_manager'];
      $image = $images->bukti;
     if ($request->HasFile('bukti')) {
            $destination_path = "/public/document/";
            $image = $request->file('bukti');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('bukti')->storeAs($destination_path,$image_name);
            $image= $image_name;
      }
    
        $issue = Issue::where('id_issue',$id)->update([
              'id_project'        => $request->id_project,
              'part_number'       => $request->part_number,
              'category_issue'    => $request->category_issue,
              'issue'             => $request->issue,
              'pic'               => $request->pic,
              'escalation'        => 'EX 0',
              'id_process'        => $request->id_process,
              'id_category'       => $request->id_category,
              'id_item'           => $request->id_item,
              'last_update'       => Carbon::now(),
              'created'           => Carbon::now(),
              'timing'            => 1,
              'bukti'             => $image,
              'status_rio'        => 0,
              'answer_pic'        => $request->answer_pic
        ]);            
        
        $notif = new Notification;
        $notif->id_project = $request->id_project;
        $notif->part_number = $request->part_number;
        $notif->pengirim = Auth::user()->id_pengguna;
        $notif->penerima = $get_project_manager['get_project_manager'];
        $notif->tipe = 'issue';
        $notif->status_read = 'un_read';
        $notif->id_issue = $images->id_issue;
        $notif->save();
            // $mail = Mail::send('email.testmail', ['penerima'=>$penerima], function($message) use ($penerima,$files)
            // {    
            //     $message->to($penerima)->subject('NOTIFICATION RIO');    
             
            //        if ($files ==true) {
            //          $message->attach($files->getRealPath(), array(
            //                 'as' => $files->getClientOriginalName(), // If you want you can chnage original name to custom name      
            //                 'mime' => $files->getMimeType())
            //             );
                  
            //        }
                       
               
            // });
       if ($notif) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been update');
        }
    return redirect('pic/project_open_rio_get/'.$request->id_project.'/'.$request->part_number);
   
    }
public function open_item_control()
{
    $get_data = [];
    $member = MemberProject::where('username',Auth::user()->username)->pluck('id_project');
    $project = TypeProject::whereIn('id_project',$member)->get();
    foreach ($project as $key => $value) {
        $get_data[] = [
            'id_project'=>$value->id_project,
            'project_name'=>$value->project_name,
             'count_product'=>Product::where('id_project',$value->id_project)->count(),
             'notif'=>Notification::where(['id_project'=>$value->id_project,'tipe'=>'item','penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read'])->count()

        ];
    }
   // return $get_data;
     return view('pic.open_item_control',compact('get_data','project'));
}
public function open_item_control_detail($id)
{
    $username = Auth::user()->username;
    $get_data = [];
    $project = TypeProject::where('id_project',$id)->first();
    $product = Product::where('id_project',$id)->get();
    foreach ($product as $key => $value) {
          $get_data[] = [
            'id_project'=>$value->id_project,
            'part_number'=>$value->part_number,
            'part_name'=>$value->part_name,
            'count_item'=>ItemControl::where(['id_project'=>$id,'part_number'=>$value->part_number,'pic_item'=>$username])->where('status_item','!=',2)->count(),
             'notif'=>Notification::where(['id_project'=>$value->id_project,'part_number'=>$value->part_number,'tipe'=>'item','penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read'])->count()
          ];
    }
   // return $get_data;
   return view('pic.open_item_control_detail',compact('get_data','project'));
}
public function open_item_control_detail_product($id,$part_number)
{
    $data = ItemControl::where(['id_project'=>$id,'part_number'=>$part_number,'pic_item'=>Auth::user()->username])->where('status_item','!=',2)->get();
    // Notification::where(['id_project'=>$id,'part_number'=>$part_number,'tipe'=>'item','penerima'=>Auth::user()->id_pengguna])->update([
    //     'status_read'=>'read'
    //   ]);
    return view('pic.open_item_control_detail_product',compact('data'));
}
   



   public function open_item_control_finish()
{
    $get_data = [];
    $member = MemberProject::where('username',Auth::user()->username)->pluck('id_project');
    $project = TypeProject::whereIn('id_project',$member)->get();
    foreach ($project as $key => $value) {
        $get_data[] = [
            'id_project'=>$value->id_project,
            'project_name'=>$value->project_name,
             'count_product'=>Product::where('id_project',$value->id_project)->count()
        ];
    }
     return view('pic.open_item_control_finish',compact('get_data','project'));
}
public function open_item_control_detail_finish($id)
{
    $username = Auth::user()->username;
    $get_data = [];
    $project = TypeProject::where('id_project',$id)->first();
    $product = Product::where('id_project',$id)->get();
    foreach ($product as $key => $value) {
          $get_data[] = [
            'id_project'=>$value->id_project,
            'part_number'=>$value->part_number,
            'part_name'=>$value->part_name,
            'count_item'=>ItemControl::where(['id_project'=>$id,'part_number'=>$value->part_number,'pic_item'=>$username])->where('status_item',2)->count()
          ];
    }
   return view('pic.open_item_control_detail_finish',compact('get_data','project'));
}
public function open_item_control_detail_product_finish($id,$part_number)
{
    $data = ItemControl::where(['id_project'=>$id,'part_number'=>$part_number,'pic_item'=>Auth::user()->username])->where('status_item',2)->get();
    return view('pic.open_item_control_detail_product_finish',compact('data'));
}
public function get_product_pic($id)
{
    $data = Product::where('id_project',$id)->pluck('part_number','part_name');

    return Response::json($data);
}
public function get_category_product($id)
{
    $data = CategoryControllName::where('part_number',$id)->pluck('id_category','category_name');

     return Response::json($data);
}
public function my_profile()
{

$data = User::where('id_pengguna',Auth::user()->id_pengguna)->first();
$departement = DB::table('departement')->get();
return view('pic.my_profile.index',compact('data','departement'));
}
public function my_document()
{
$data =  DocumentProject::where('pic',Auth::user()->username)->get();

// Notification::where('penerima',Auth::user()->id_pengguna)->where('tipe','document')->update([
//   'status_read'=>'read'
// ]);
//return $data;
return view('pic.my_document.index',compact('data'));
}
public function update_my_profile(Request $request, $id)
{
  $data = User::where('id_pengguna',$id)->update([
    'password'=>$request->password,
    'firstname'=>$request->firstname,
    'lastname'=>$request->lastname,
    'email'=>$request->email,
    'telepon'=>$request->telepon,
    'id_departement'=>$request->id_departement
  ]);
  if ($data) {
     $request->session()->flash('status', '200');
     $request->session()->flash('msg', 'Admin has been added');
  }
  return redirect()->back();
}
public function open_rio_finish()
{
 $get_data = [];
        $data = TypeProject::all();
        foreach ($data as $key => $value) {
           $get_data[] = [
            'id_project'=>$value->id_project,
            'project_name'=>$value->project_name,
            'count_product'=>Product::where('id_project',$value->id_project)->count()
           ];
        }
        return view('pic.open_rio_finish',compact('get_data'));
}
 public function project_open_rio_finish($id)
    {
        $get_data = [];
        $project = TypeProject::where('id_project',$id)->first();
        $product = Product::where('id_project',$id)->get();
        foreach ($product as $key => $value) {
            $get_data[] = [
                'part_number'=>$part_number = $value->part_number,
                'part_name'=>$part_name = $value->part_name,
                'get_issue'=>$get_issue = Issue::where('part_number',trim($part_number))->where('status_rio',1)->where('id_project',$id)->count(),
            ];
        }
       // return $get_data;
          return view('pic.project_open_rio_finish',compact('get_data','project'));
    }
     public function project_open_rio_get_finish($id,$part_number)
    {
      $data = Issue::where('part_number',trim($part_number))->where('status_rio',1)->where('id_project',$id)->get();
      return view('pic.project_open_rio_get_finish',compact('data'));
    }
   public function upload_document_pic(Request $request)
   {
     $pool = '0123456789ABCDEFGHIJKLMNOPRSTU';
        $length = 16;
       $get = DocumentProject::where('document_code',$request->document_code)->first();
//return $get;
      if($request->HasFile('filename')) {
        $destination_path = "/public/document/";
        $image = $request->file('filename');
        $image_name =  $image->getClientOriginalName();
        $path = $request->file('filename')->storeAs($destination_path,$image_name);
        $data['filename'] = $image_name;
      }
     // return $data;
        $notif                = new Notification;
        $notif->id_project    = $get->id_project;
        $notif->part_number   = $get->part_number;
        $notif->pengirim      = Auth::user()->id_pengguna;
        $notif->penerima      = $get->create_by;
        $notif->tipe          = 'document';
        $notif->document_code = $get->document_code;
        $notif->status_read   = 'un_read';
        $notif->save();
    $update = DocumentProject::where('document_code',$request->document_code)->update(['filename'=>$data['filename']]);

    $request->session()->flash('status', '200');
    $request->session()->flash('msg', 'Pic has been add upload document');
    return redirect()->back();
   }
    public function download_document_issue($id)
  {
    try {
       $path =  storage_path('app/public/document/'.$id);
    } catch (Exception $e) {

    }


    return response()->download($path);
  }
}
