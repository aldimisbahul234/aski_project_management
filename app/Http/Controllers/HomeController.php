<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\MasterData\Customer;
use App\Http\Models\MasterData\Sector;
use App\Http\Models\MasterData\EventProject;
use App\Http\Models\MasterData\TypeProject;
use App\Http\Models\MasterData\Product;
use App\Http\Models\MasterData\ItemControl;
use App\Http\Models\MasterData\DocumentProject;
use App\Http\Models\MasterData\CategoryEventProject;
use App\Http\Models\MasterData\ProjectBudget;
use Carbon\Carbon;
use App\User;
use DB;
use App\Http\Models\MasterData\CategoryControllName;
use App\Http\Models\MasterData\MemberProject;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    $today = Carbon::now()->format('Y-m-d');
    //count dasboard  
    $countCustomer = Customer::all()->count();
    $getCustomer = Customer::all();
    $countProjectSignOff = EventProject::where('jadwal_event','<',$today)->where('id_category_event_project',14)->count();
    $countProjectSuccess = EventProject::where('jadwal_event','>',$today)->where('id_category_event_project',14)->count();
    $getProjectSignOff = EventProject::where('jadwal_event','<',$today)->where('id_category_event_project',14)->get();
    $getProjectSuccess = EventProject::where('jadwal_event','>',$today)->where('id_category_event_project',14)->get();
    $current =null;
    $getProjectSuccessData = [];
//modal dev
    foreach ($getProjectSuccess as $key => $success) {
        $getProjectSuccessData[] = [
            'id_project'=>$success->id_project,
            'get_project_name'=>$get_project_name = TypeProject::where('id_project',$success->id_project)->first(),
            'project_name'=>$get_project_name ==true ? $get_project_name->project_name : null,
            'id_category_event_project'=>$success->id_category_event_project,
            'get_event'=>$get_event = EventProject::where('id_project',$success->id_project)->where('jadwal_event','>',$today)->first(),
            'get_event_name'=>$get_event_name = CategoryEventProject::where('id_category_event_project',$get_event->id_category_event_project)->first(),
            'event_name'=>$get_event_name ==true ? $get_event_name->event_project_name : null,
            'current'=>$today > $get_event_name->jadwal_event ? "Until Now":"today",

        ];
    }
    //return $getProjectSuccessData;
    //end count
    $project = TypeProject::all();
    foreach ($project as $key => $value) {
        $getProject[] = [
            'id_project'=>$value->id_project,
            'get_product'=>$get_product = Product::where('id_project',$value->id_project)->first(),
            'part_number'=>$get_product ==true ? $get_product->part_number:null,
            'project_name'=>$value->project_name,
            'get_event_current'=>$get_event_current = EventProject::where('id_project',$value->id_project)->where('jadwal_event','<',$today)->first(),
            'get_event_name_current'=> $get_event_current ==true ? $get_event_name_current = CategoryEventProject::where('id_category_event_project',$get_event_current->id_category_event_project)->first() : "-",
            'event_name_current'=>$get_event_name_current->event_project_name,
            'current'=> $get_event_current ==true ? $current = $get_event_current->jadwal_event : "-",
            'get_current'=>$today > $current ? "Until Now":"Today",


            'get_event'=>$get_event = EventProject::where('id_project',$value->id_project)->where('jadwal_event','>',$today)->first(),
            'event_name'=> $get_event ==true ? $event_name = CategoryEventProject::where('id_category_event_project',$get_event->id_category_event_project)->first() : "-",
            'event_name'=>$event_name->event_project_name,
            'current'=> $get_event ==true ? $current = $get_event->jadwal_event : "-",
            'next'=> $get_event ==true ? $next = Carbon::now()->startOfDay()->diffInDays($get_event->jadwal_event, false) : "-",
            "count_item"=>$count_item = ItemControl::where('id_project',$value->id_project)->count('part_number'),
            "sum_actual"=>$sum_actual = ItemControl::where('id_project',$value->id_project)->sum('actual_progress_item'),
            "total_actual"=>$count_item > 0 ? round($sum_actual / $count_item) : 0,
            "get_product"=>Product::where('id_project',$value->id_project)->get(),
        ];
    }
    //return $getProject;
    //task
     $tasks = TypeProject::all();
     foreach ($tasks as $key => $task) {
         $getTask[] = [
            'id_project'=>$task->id_project,
            'project_name'=>$task->project_name,
            'count_product'=>Product::where('id_project',$task->id_project)->count(),
         ];
     }
     //grafik project customer
     $sectors = Sector::all();
     foreach ($sectors as $key => $sector) {
         $getSector[] = [
            'id_sector'=>$sector->id_product,
            'sector_name'=>$sector->product_name,
            'get_customer'=>$get_customer = Customer::where('id_product',$sector->id_product)->first(),
            'customer'=>$get_customer ==true ? $get_customer->customer_name : null,
            'count_project'=>$get_customer ==true ? TypeProject::where('id_customer',$get_customer->id_customer)->count() : 0,
         ];
     }

     //grafik rfq 
     $grafik_rfq = TypeProject::all();
     foreach ($grafik_rfq as $key => $grafik_rfq) {
         $get_grafik_rfq[] = [
            'id_project'=>$grafik_rfq->id_project,
            'project_name'=>$grafik_rfq->project_name
         ];
         $get_grafik_rfq_product[] = [
            'id_project'=>$grafik_rfq->id_project,
            'product'=>Product::where(['id_project'=>$grafik_rfq->id_project,'status_product'=>0])->count(),
           
         ];
         $get_grafik_rfq_product_die_go[] = [
            'id_project'=>$grafik_rfq->id_project,
             'product'=>Product::where('id_project',$grafik_rfq->id_project)->where('status_product','!=',0)->count(),
         ];
     }

//grafik progress actual
     $grafik_progress_actual = TypeProject::all();
     foreach ($grafik_progress_actual as $key => $grafik_progress_actual) {
        $get_grafik_progress_project[] = [
            'id_project'=>$grafik_progress_actual->id_project,
            'project_name'=>$grafik_progress_actual->project_name,
        ];
         $get_grafik_progress_actual[] = [
            'id_project'=>$grafik_progress_actual->id_project,
            'count_product'=>$count_product = Product::where('id_project',$grafik_progress_actual->id_project)->where('status_product','!=',0)->count(),
            'actual_product'=>$count_product > 0 ? $actual_product = Product::where('id_project',$grafik_progress_actual->id_project)->sum('actual_progress_part'):0,
            'sum_actual'=>$count_product > 0  ? round($actual_product / $count_product) : 0
         ];
     }

     //grafik budget 
      $grafik_budget = TypeProject::all();
      foreach ($grafik_budget as $key => $grafik_budget) {
          $get_grafik_budget[] = [
            'id_project'=>$grafik_budget->id_project,
            'project_name'=>$grafik_budget->project_name,
             ];
        $get_grafik_budget_sum[] = [
            'price_plan'=>$price_plan = ProjectBudget::where('id_project',$grafik_budget->id_project)->sum('total_price_plan'),
            'price_actual'=>$price_actual = ProjectBudget::where('id_project',$grafik_budget->id_project)->sum('total_price_actual'),
            'total'=>$price_actual > 0 || $price_plan > 0 ? round($price_actual / $price_plan * 100) : 0
         ];
      }
    //  return $get_grafik_budget;
     //grafik cost
      $grafik_cost = TypeProject::all();
      $grafik_cost = TypeProject::all();
      foreach ($grafik_cost as $key => $grafik_cost) {
          $get_grafik_cost[] = [
            'id_project'=>$grafik_cost->id_project,
            'project_name'=>$grafik_cost->project_name
          ];
      }


     //schedule
      $schedules = TypeProject::all();
     foreach ($schedules as $key => $schedule) {
         $getSchedule[] = [
            'id_project'=>$schedule->id_project,
            'project_name'=>$schedule->project_name,
            'get_id_category_event_project'=>$get_id_category_event_project = EventProject::where('id_project',$schedule->id_project)->get()->pluck('id_category_event_project'),
            'get_event_name'=>CategoryEventProject::whereIn('id_category_event_project',$get_id_category_event_project)->get(),

        ];
        }
//return $getSchedule;
     //event
     $events = TypeProject::all();
     foreach ($events as $key => $event) {
         $getEvent[] = [
            'id_project'=>$event->id_project,
            'project_name'=>$event->project_name,
             'get_event_current'=>$get_event_current = EventProject::where('id_project',$value->id_project)->where('jadwal_event','<',$today)->first(),
            'get_event_name_current'=> $get_event_current ==true ? $get_event_name_current = CategoryEventProject::where('id_category_event_project',$get_event_current->id_category_event_project)->first() : "-",
            'event_name_current'=>$get_event_name_current->event_project_name,
            'current'=> $get_event_current ==true ? $current = $get_event_current->jadwal_event : "-",
            'get_current'=>$today > $current ? "Until Now":"Today",


            'get_event'=>$get_event = EventProject::where('id_project',$value->id_project)->where('jadwal_event','>',$today)->first(),
            'event_name'=> $get_event ==true ? $event_name = CategoryEventProject::where('id_category_event_project',$get_event->id_category_event_project)->first() : "-",
            'event_name'=>$event_name->event_project_name,
            'current'=> $get_event ==true ? $current = $get_event->jadwal_event : "-",
            'next'=> $get_event ==true ? $next = Carbon::now()->startOfDay()->diffInDays($get_event->jadwal_event, false) : "-",
            'pluck_part'=>$pluck_part = DocumentProject::where('id_project',$event->id_project)->pluck('part_number'),
            'get_product'=>Product::whereIn('part_number',$pluck_part)->where('id_project',$event->id_project)->get(),
            'check'=>DocumentProject::where('id_project',$event->id_project)->where('filename','')->count(),
            'checkdocument'=>DocumentProject::where('id_project',$event->id_project)->count()
         ];
     }
 
///return $getEvent;
        return view('home',compact('getProjectSuccessData','getCustomer','getProjectSuccess','getProjectSignOff','countCustomer','countProjectSuccess','countProjectSignOff','getProject','getTask','getSector','getEvent','getSchedule','get_grafik_rfq','get_grafik_rfq_product','get_grafik_rfq_product_die_go','get_grafik_progress_project','get_grafik_progress_actual','get_grafik_budget','get_grafik_budget_sum'));
    }
     public function logout () {
    //logout user
    auth()->logout();
    return redirect('/');
    }
    public function dashboard_detail_schedule($id_project)
    {
        $project = TypeProject::where('id_project',$id_project)->first();
        $event = EventProject::where('id_project',$id_project)->get();
        $member = MemberProject::where('id_project',$id_project)->where('id_position',1)->first();
        $pengguna = User::where('username',$member->username)->first();
        $get_event = [];

        // return $event;
        foreach ($event as $key => $value) {
            $get_event[] = [
                'get_event'=> $data = CategoryEventProject::where('id_category_event_project',$value->id_category_event_project)->first(),
                'event_name'=>$data->event_project_name,
                'jadwal_event'=>$jadwal_event = $value->jadwal_event,
                'actual_event'=>$actual_event = $value->actual_event,
                'start_day'=>Carbon::create($jadwal_event)->format('d'),
                'start_month'=>Carbon::create($jadwal_event)->format('m') - 1,
                'showing_start_month'=>Carbon::create($jadwal_event)->format('m'),
                'end_month'=>$actual_event ==true ? Carbon::create($actual_event)->format('m') : Carbon::now()->format('m') ,
                'end_day'=>$actual_event ==true ? Carbon::create($actual_event)->format('d') : Carbon::now()->format('d'),
                 'end_year'=>$actual_event ==true ? Carbon::create($actual_event)->format('Y') : Carbon::now()->format('Y'),
                'start_year'=>Carbon::create($jadwal_event)->format('Y'),

                'actual_day_check'=> $actual_event ==true ? Carbon::create($actual_event)->format('d') : 'harap isi actual tanggal',
                'actual_month_check'=>$actual_event ==true ? Carbon::create($actual_event)->format('m') : 'harap isi actual bulan',
                'actual_year_check'=>$actual_event ==true ? Carbon::create($actual_event)->format('Y') :  'harap isi actual tahun',
            ];

        }
         $events = EventProject::where('id_project',$id_project)->get();
         foreach ($events as $key => $values) {
            $get_events[] = [
                'get_event'=> $data = CategoryEventProject::where('id_category_event_project',$values->id_category_event_project)->first(),
                'event_name'=>$data->event_project_name,
                'jadwal_event'=>$jadwal_event = $values->jadwal_event,
                'actual_event'=>$actual_event = $values->actual_event,
                'start_day'=>Carbon::create($jadwal_event)->format('d'),
                'start_month'=>Carbon::create($jadwal_event)->format('m'),
                'end_month'=>$actual_event ==true ? Carbon::create($actual_event)->format('m') : Carbon::now()->format('m') ,
                'end_day'=>$actual_event ==true ? Carbon::create($actual_event)->format('d') : Carbon::now()->format('d'),
                 'end_year'=>$actual_event ==true ? Carbon::create($actual_event)->format('Y') : Carbon::now()->format('Y'),
                'start_year'=>Carbon::create($jadwal_event)->format('Y'),

                'actual_day_check'=> $actual_event ==true ? Carbon::create($actual_event)->format('d') : 'harap isi actual tanggal',
                'actual_month_check'=>$actual_event ==true ? Carbon::create($actual_event)->format('m') : 'harap isi actual bulan',
                'actual_year_check'=>$actual_event ==true ? Carbon::create($actual_event)->format('Y') :  'harap isi actual tahun',
            ];

        }
      // return $pengguna;
        return view('Dashboard.grafik.grafik_gantt_schedule_project',compact('get_events','project','get_event','pengguna'));
    }
     public function gant_chart_project(Request $request,$id_project, $part_number)
    {
        $date = Carbon::now()->formatLocalized('%A %d %B %Y');
   
        $firstname = null;
        $lastname = null;
        $product = [];
        $get_category = [];
        $get_all_product = Product::where('id_project',$id_project)->get();
        if ($part_number =='null') {
              $product = Product::where('id_project',$id_project)->first();
        }
     

      //  return $part_number;
       $pengguna = [];
       if ($part_number !='null') {
         $product = Product::where(['id_project'=>$id_project,'part_number'=>$part_number])->first();
         //return $product;
          $member = MemberProject::where(['id_project'=>$id_project,'part_number'=>$part_number])->where('id_position',2)->first();
          if ($member ==true) {
               $pengguna = User::where('username',$member->username)->first();
               $firstname = $pengguna->firstname;
               $lastname = $pengguna->lastname;
          }
       

           $category = CategoryControllName::where('part_number',$product->part_number)->get();
           foreach ($category as $key => $value) {
            $get_category[] = [
                'id_category'=>$value->id_category,
                'category_name'=>$value->category_name,
                'get_item_data'=>$get_item_data = ItemControl::where(['part_number'=>$value->part_number,'id_category'=>$value->id_category])->first(),
                'plan_start_item'=>$get_item_data ==true ? $get_item_data->plan_start_item : null,
                'plan_finish_item'=>$get_item_data ==true ? $get_item_data->plan_finish_item : null,
                'actual_start_item'=>$get_item_data ==true ? $get_item_data->actual_start_item : null,
                'actual_finish_item'=>$get_item_data ==true ? $get_item_data->actual_finish_item : null,
                'plan_start_year'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_start_item)->format('Y') : 0,
                'plan_start_month'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_start_item)->format('m') : 0,
                 'plan_start_day'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_start_item)->format('d') : 0,
                'plan_finish_year'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_finish_item)->format('Y') : 0,
                'plan_finish_month'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_finish_item)->format('m') : 0,
                 'plan_finish_day'=>$get_item_data ==true ? Carbon::create($get_item_data->plan_finish_item)->format('d') : 0,
                 'count_progress'=>$get_item_data ==true ? $count_progress = round($get_item_data->plan_progress_item + $get_item_data->actual_progress_item)  : 0,
                  'get_item'=>ItemControl::where(['id_category'=>$value->id_category,'part_number'=>$value->part_number])->get(),

            ];
            }
       }

//return $lastname;
  
        return view('project.leader.grafik.gant_chart_project',compact('firstname','lastname','pengguna','date','part_number','get_category','product','id_project','get_all_product'));
    }
    public function type_project_dashboard($id)
    {
        $project = TypeProject::find($id);
        $customer = Customer::where('id_customer',$project->id_customer)->first();
        $query = TypeProject::where('id_project',$id)->get();
           $dateNow = Carbon::now()->format("Y-m-d");
        foreach ($query as $key => $value) {
          $data[] = [
              'id'=>$value->id_project,
              'customer'=>$customer->id_customer,
              'project_name'=>$value->project_name,
              'count_item'=>$count_item = ItemControl::where('id_project',$value->id_project)->count(),
              'plan_progress_item'=>$plan_progress_item = ItemControl::where('id_project',$value->id_project)->sum('plan_progress_item'),
              'result_plan'=> $plan_progress_item || $count_item > 0 ?round($plan_progress_item / $count_item) : 0,
              'actual_progress_item'=>$actual_progress_item = ItemControl::where('id_project',$value->id_project)->sum('actual_progress_item'),
              'result_actual'=>$actual_progress_item ||  $count_item > 0 ? round($actual_progress_item / $count_item) : 0,
              'hitung_product'=>$hitung_product = ItemControl::where('id_project',$id)->count(),
              'count_part'=>ItemControl::where('id_project',$value->id_project)->count(),

              
            ];
          }
         // return $data;
        return view('project_dashboard',compact('data','customer','dateNow'));
    }
}
