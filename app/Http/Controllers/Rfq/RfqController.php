<?php

namespace App\Http\Controllers\Rfq;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redis;
use App\Http\Models\Masterdata\DocumentRfq;
use App\Http\Models\Masterdata\TypeProject;
class RfqController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 1000;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $getData = [];
      $data = DocumentRfq::all();
      foreach ($data as $key => $value) {
        $getData[] = [
          'id_rfq'=>$value->id_rfq,
          'id_project'=>$value->id_project,
          'rfq_file'=>$value->rfq_file,
          'date_created'=>$value->date_created,
          'status'=>$value->status,
          'get_project'=>$get_project = TypeProject::where('id_project',$value->id_project)->first(),
          'project_name'=>$get_project ==true ? $get_project->project_name : null
        ];
      }
      //return $getData;
      return view('rfq.index',compact('getData'));
    }

}
