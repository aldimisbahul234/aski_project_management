<?php
 
namespace App\Http\Email;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class TestMail extends Mailable
{
    use Queueable, SerializesModels;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->from('pengirim@gmail.com')
                   ->view('email.testmail')
                   ->to('jareerzeenam.29@gmail.com', 'Email Title')
                   ->with(
                    [
                        'nama' => 'AldiMisbahulKabir',
                        'website' => 'www.aldi.com',
                    ]);
    }
}