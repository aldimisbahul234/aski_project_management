<div class="modal fade text-left" id="testmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
<div class="modal-dialog" role="document">
<form class="form-participant" action="{{ url('email') }}" method="post" enctype="multipart/form-data">
      @csrf
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     @csrf
    <div class="row">
        <div class="col-sm-12 modal-text">
          <label>EMAIL</label>
           <input type="email" name="email" class="form_technical" style="width: 100%">
        </div>
    </div>
    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-success">SEND MAIL</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</form>
</div>
</div>