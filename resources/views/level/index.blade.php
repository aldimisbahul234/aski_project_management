
@extends('layouts/root')
@section('main')
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <a href="{{ url('user/create') }}" class="btn btn-gradient-primary">Tambah Level</a>
        <h4 class="card-title">Table Basic</h4>
      </div>
      <div class="card-body">
      </div>
  		<div class="col-12">
  			  <div class="card">
    	  <table class="table">
          <thead>
            <tr>
              <th>Project</th>
              <th>Client</th>
              <th>Users</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          	 <tr>
              <td>
                <img
                  src="../../../app-assets/images/icons/angular.svg"
                  class="mr-75"
                  height="20"
                  width="20"
                  alt="Angular"
                />
                <span class="font-weight-bold">Angular Project</span>
              </td>
              <td>Peter Charls</td>
              <td>
                <div class="avatar-group">
                  <div
                    data-toggle="tooltip"
                    data-popup="tooltip-custom"
                    data-placement="top"
                    title=""
                    class="avatar pull-up my-0"
                    data-original-title="Lilian Nenez"
                  >
                    <img
                      src="../../../app-assets/images/portrait/small/avatar-s-5.jpg"
                      alt="Avatar"
                      height="26"
                      width="26"
                    />
                  </div>
                
                </div>
              </td>
              <td><span class="badge badge-pill badge-light-primary mr-1">Active</span></td>
              <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
    	</div>

      </div>
    </div>


@endsection
@push('script')
@endpush