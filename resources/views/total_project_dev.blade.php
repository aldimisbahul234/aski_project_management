<div class="modal fade text-left" id="total_project_dev" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     @csrf
     <table class="datatables-basic table" id="datatable_dev">
       <thead>
         <tr>
           <th>Project</th>
         </tr>
       </thead>
       <tbody>
         @foreach($getProjectSuccessData as $ps)
         <tr>
           <td>{{ $ps['project_name'] }}</td>
         </tr>
          @endforeach
       </tbody>
     </table>






    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
</div>