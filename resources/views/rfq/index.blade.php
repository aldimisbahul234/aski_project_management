@extends('layouts/root')
@section('main')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<h3 style="text-align: center;">RFQ FILE</h3>
				<table class="datatables-basic table" id="datatable">
					<thead>
					<tr>
						<th>NO</th>
						<th>PROJECT</th>
						<th>RFQ FILE</th>
						<th>DATE CREATED</th>
						<th>STATUS</th>
						<th>CONFIRMATION</th>
						<th>DELETE</th>
					</tr>
					</thead>
					<tbody>
						@foreach($getData as $value)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $value['project_name'] }}</td>
							<td>{{ $value['rfq_file'] }}</td>
							<td>{{ $value['date_created'] }}</td>
							<td>
								@if($value['status'] ==1)
								<button class="btn btn-success"> CONFIRMED</button>
								@else
								<button class="btn btn-danger"> UNCONFIRMED</button>
								@endif
							</td>
							<td>
								<a href="" class="btn btn-primary"><i data-feather="file"></i> </a>
							</td>
							<td>
								<a href="" class="btn btn-danger"> <i data-feather="trash"></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				
				</table>
		</div>
	</div>
</div>
@endsection
@push('script')
<script type="text/javascript">
	   $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,

            });
          });
</script>
@endpush