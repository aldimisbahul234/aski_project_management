@extends('layouts/root')
@section('main')
<div class="col-lg-12">
 <div class="row">

   
        <div class="col-lg-4">
          <div class="card-counter primary customer total_customer" style="margin-bottom: 30px;">
           <i class="fa fa-award fa-5x"></i>
            <span class="count-numbers">{{ $countCustomer }}</span>
            <span class="count-name">TOTAL CUSTOMER</span>
          </div>
        </div>

        <div class="col-lg-4">
            <div class="card-counter primary customer total_project_so" style="margin-bottom: 30px;">
              <i class="fa fa-database"></i>
              <span class="count-numbers">{{ $countProjectSignOff }}</span>
              <span class="count-name">TOTAL PROJECT SIGN OFF</span>
            </div>
          </div>

        <div class="col-lg-4">
          <div class="card-counter primary customer total_project_dev" style="margin-bottom: 30px;">
              <i class="fa fa-cogs fa-5x"></i>
            <span class="count-numbers">{{ $countProjectSuccess }}</span>
            <span class="count-name">TOTAL PROJECT DEVELOPMENT</span>
          </div>
        </div>
    

  </div>
</div>

<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
        <table class="datatables-basic table" id="datatable">
           <thead>
            
            <tr>
              <th style="text-align: center;">PROJECT NAME</th>
              <th style="text-align: center;">CURRENT</th>
              <th style="text-align: center;">NEXT EVENT</th>
              <!-- <th style="text-align: center;">TIME</th> -->
              <th style="text-align: center;">QUALITY</th>
              <th style="text-align: center;">COST</th>
              <th style="text-align: center;">BUDGET</th>
              <th style="text-align: center;">OPEN R.I.O</th>
              <th style="text-align: center;">ANALISTYIC</th>
              <th style="text-align: center;">PROGRESS ACTUAL</th>
            </tr>
            </thead>
           <tbody>
            @foreach($getProject as $project)
            <tr>
              <td>{{ $project['project_name'] }}</td>
              <td>
                <button disabled type="button" class="btn btn-info" style="width: 165px;height: 80px;">{{ $project['event_name_current'] }} - {{ $project['get_current'] }}</button>
              </td>
              <td>
                <button disabled type="button" class="btn btn-warning" style="width: 165px;height: 80px;">{{ $project['event_name'] }} {{ $project['next'] }} Days Left</button>
              </td>
             <!--  <td><a href="{{ url('type_project_dashboard/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-danger waves-effect"><i data-feather="eye"></i></a></td> -->
              <td>
               
                   <button class="btn btn-primary dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 100%">APPROVAL LINE 
                            N/A</button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item"></a>
                  @foreach($project['get_product'] as $vals)
                    <a class="dropdown-item" href="{{ url('project/quality_detail/'.$vals->id_project.'/'.$vals->part_number) }}">{{ $vals->part_name }}</a>
                  @endforeach
              </div>
                        <!--     <a class="btn btn-primary" href="" style="width: 100%">
                            APPROVAL LINE 
                            N/A
                          </a> -->
                           <a class="btn btn-primary" style="margin-top: 10px;width: 100%">
                            TRIAL
                            N/A
                          </a>
              
                     
                
                     
                              
               
              </td>
              <td>
                  <a href="{{ url('cost/cogm/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-warning waves-effect"><i data-feather="eye"></i></a></td>
              </td>
              <td>
                <a href="{{ url('project/project_budget/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-warning waves-effect"><i data-feather="eye"></i></a></td>
              </td>
              <td>
                <a href="{{ url('project/issue/'.$project['id_project'].'/'.'null'.'/'.'null'.'/'.'null'.'/'.'dashboard') }}" class="btn btn-danger">R.I.O</a>
              </td>
              <td>
                @if($project['part_number'] ==true)
                <a class="btn btn-primary" href="{{ url('project/analis/'.$project['id_project'].'/'.$project['part_number'].'/'.'all') }}">ANALYTIC</a>
                @else
                <button class="btn btn-primary" disabled>ANALYTIC</button>
                @endif
              </td>
              <td>
                <a href="{{ url('type_project_dashboard/'.$project['id_project']) }}">
                  @if($project['total_actual'] == 100 || $project['total_actual'] > 100)
               <div class="progress progress-bar-success">
                 <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      >{{ $project['total_actual'] }} %</div>
                    
                    </div>
                    @else
                    <div class="progress progress-bar-warning">
                 <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{ $project['total_actual'] }}%"
                      >{{ $project['total_actual'] }} %</div>
                    
                    </div>

                    @endif

                  </a>
              </td>
            </tr>
            @endforeach
           </tbody>
         </table>
    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
      <h3 style="text-align: center;">TASK</h3>
        <table class="datatables-basic table" id="datatable_task">
           <thead>
            
            <tr>
              <th width="2%">NO</th>
              <th width="10%">PROJECT</th>
              <th width="15%">PRODUCT</th>
            </tr>
            </thead>
           <tbody>
          @foreach($getTask as $task)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $task['project_name']}}</td>
            <td>
            <a href="{{ url('project/task_detail/'.$task['id_project']) }}" class="btn btn-info" style="width: 100%">{{ $task['count_product'] }}</a>
            </td>
          </tr>
          @endforeach
           </tbody>
         </table>
    </div>
  </div>
</div>

<!-- <div class="col-lg-12">
  <div class="card">
    <div class="card-body">
        <button class="btn btn-success email">TEST MAIL</button>
        <button class="btn btn-primary wa">TEST WA</button>
    </div>
  </div>
</div>
 -->


<div class="col-lg-12">
	<div class="row">

    <div class="col-lg-6">
       
            <div id="grafik_customer"></div>
          
    </div>
	
   
    <div class="col-lg-6">
      
          <div id="grafik_rfq"></div>
      
    </div>
    <br><br>

    <div class="col-lg-4" style="top: 15px;">
     
          <div id="progress_project"></div>
      
    </div>

     <div class="col-lg-4" style="top: 15px;">
     
          <div id="project_budget"></div>
        
    </div>

     <div class="col-lg-4" style="top: 15px;">
     
          <div id="project_cost"></div>
       
    </div>

 	</div>
</div>

<div class="col-lg-12" style="top: 30px;">
  <div class="card">
    <div class="card-body">
      <h3 style="text-align: center;">SCHEDULE PROJECT EVENT</h3>
        <table class="datatables-basic table" id="datatable_schedule">
           <thead>
            
            <tr>
              <th width="2%">NO</th>
              <th width="10%">PROJECT</th>
              <th width="15%"></th>
            
            </tr>
            </thead>
           <tbody>
              @foreach($getSchedule as $schedule)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $schedule['project_name'] }}</td>
                  <td>
                    <a href="{{ url('dashboard_detail_schedule/'.$schedule['id_project']) }}" target="_blank">
                     <div class="progress progress-bar-success"  style="height: 34px;">
                      @foreach($schedule['get_event_name'] as $event)
                      @php
                      $checkdocument = App\Http\Models\Masterdata\DocumentProject::CheckDocument($schedule['id_project'],$event['id_category_event_project']);
                      @endphp 
                      
                    
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      >{{$event['id_category_event_project']}} - {{$event['event_project_name']}} - {{ $checkdocument }}</div>
                    
                      @endforeach
                    </div>
                  </a>
                  </td>

                 
                </tr>
              @endforeach
           </tbody>
         </table>
    </div>
  </div>
</div>

<!-- <div class="col-lg-12" style="top: 30px;">
  <div class="card">
    <div class="card-body">
      <h3 style="text-align: center;">TIME LINE EVENT</h3>
        <table class="datatables-basic table" id="datatable_time">
           <thead>
            
            <tr>
              <th width="2%">NO</th>
              <th width="10%">PROJECT NAME</th>
              <th width="10%">CURRENT EVENT</th>
              <th width="15%">NEXT EVENT</th>
              <th width="15%">STATUS DOCUMENT CURRENT EVENT</th>
            </tr>
            </thead>
           <tbody>
            @foreach($getEvent as $event)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $event['project_name'] }}</td>
              <td>
                <button type="button" class="btn btn-info" style="width: 100%;"><p>{{ $event['event_name_current'] }}</p> <p>{{ $event['get_current'] }}</p></button>
              </td>
              <td>
                <button type="button" class="btn btn-warning" style="width: 100%;"><p>{{ $event['event_name'] }}</p> <p>{{ $event['next'] }} Days Left</p></button>
              </td>
              <td>
                @if($event['checkdocument'] > 0)
                    @if($event['check'] ==0)
                        <button class="btn btn-success dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 100%;height: 93px;">COMPLATE</button>
                    @else
                     <button class="btn btn-danger dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 100%;height: 93px;">NOT COMPLATE</button>
                    @endif
                  @else
                     <button class="btn btn-secondary dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 100%;height: 93px;">NOT AVAILABLE</button>
                @endif
                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                   <a class="dropdown-item" >--SELECT PRODUCT--</a>
                  @foreach($event['get_product'] as $vals)
                    <a class="dropdown-item" href="{{ url('project/document/'.$vals->id_project.'/'.$vals->part_number) }}">{{ $vals->part_name }}</a>
                  @endforeach
              </div>
              </td>
            </tr>
            @endforeach
           </tbody>
         </table>
    </div>
  </div>
</div> -->


@endsection
@push('script')
@include('testmail')
@include('testwa')
@include('total_customer')
@include('total_project_dev')
@include('total_project_so')
<script type="text/javascript">
	// Create the chart
Highcharts.chart('grafik_customer', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Grafik Project Customer'
  },
  subtitle: {
    text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
  },
  accessibility: {
    announceNewData: {
      enabled: true
    }
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Total Project'
    }

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y:.1f}%'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> project<br/>'
  },

  series: [
    {
      name: "Setor",
      colorByPoint: true,
      data: [
      <?php foreach ($getSector as $key => $sector): ?>
        {
          name: "<?php echo $sector['sector_name'];?>",
          y: <?php echo $sector['count_project'];?>,
          drilldown: "Chrome_<?php echo $key;?>"
        },
      <?php endforeach ?>
        
       
      ]
    }
  ],
  drilldown: {
    series: [
      {
        name: "Chrome",
        id: "Chrome",
        data: [
          [
            "v65.0",
            0.1
          ],
          [
            "v64.0",
            1.3
          ]
        ]
      },
      {
        name: "Firefox",
        id: "Firefox",
        data: [
          [
            "v58.0",
            1.02
          ],
          [
            "v57.0",
            7.36
          ]
        ]
      }

    ]
  }
});
Highcharts.chart('grafik_rfq', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik RFQ vs DIE-GO'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories:  [
        <?php foreach ($get_grafik_rfq as $key => $get_grafik_rfq): ?>
           '<?php echo $get_grafik_rfq['project_name'];?>',
        <?php endforeach ?>

        ]
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total RFQ & DIE-GO'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [ {
        name: 'NOT DIE-GO',
        color : 'red',
        data: [   
        <?php foreach ($get_grafik_rfq_product as $key => $get_grafik_rfq_product): ?>
           <?php echo $get_grafik_rfq_product['product'];?>,
        <?php endforeach ?>
        ]
    },
    {
        name: 'DIE-GO',
        color : 'green',
        data: [ 
        <?php foreach ($get_grafik_rfq_product_die_go as $key => $get_grafik_rfq_product_die_go): ?>
           <?php echo $get_grafik_rfq_product_die_go['product'];?>,
        <?php endforeach ?>
        ]
    }
    ]
});
Highcharts.chart('progress_project', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Progress Project'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories: [
         <?php foreach ($get_grafik_progress_project as $key => $get_grafik_progress_project): ?>
           '<?php echo $get_grafik_progress_project['project_name'];?>',
        <?php endforeach ?>
        ]
    },
    yAxis: {
        min: 0,
        max:100,
        title: {
            text: 'PROGRESS PROJECT ACTUAL'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}%</b><br/>'
    },
    series: [ {
        name: 'Complete',
        color : "skyblue",
        data: [ 
        <?php foreach ($get_grafik_progress_actual as $key => $get_grafik_progress_actual): ?>
           <?php echo $get_grafik_progress_actual['sum_actual'];?>,
        <?php endforeach ?>
        ]
    }]
});
Highcharts.chart('project_budget', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Budget Complete Project'
    },
    credits:{
        enabled : false
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        <?php foreach ($get_grafik_budget as $key => $get_grafik_budget): ?>
           '<?php echo $get_grafik_budget['project_name'];?>',
        <?php endforeach ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Budget Complete'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">Project {point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },
    series: [{
        name: 'Budget Complete',
        color : "skyblue",
        data: [
            <?php foreach ($get_grafik_budget_sum as $key => $get_grafik_budget_sum): ?>
           <?php echo $get_grafik_budget_sum['total'];?>,
            <?php endforeach ?>
        ]

    }]
});

// Create the chart
Highcharts.chart('project_cost', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Cost Current Event'
    },
    subtitle: {
        text: 'Click Name of Project Below to see Detail'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Cost'
        }

    },
    credits:{
        enabled : false
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: ''
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">Project {point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0"> Percentage : </td>' +
            '<td style="padding:0"><b>{point.y}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },

    series: [
        {
            name: "PROJECT",
            colorByPoint: true,
            data: [
              
            ]
        }
    ],
    drilldown: {
        series: [
           
        ]
    }
});
  $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
                
            });
         
          
            $('#datatable_task').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });
             $('#datatable_schedule').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });
              $('#datatable_time').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });
               $('#datatable_dev').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });

              $('.wa').click(function(){
                  $('#testwa').modal('show');
                  $('.modal-title').html('Test Wa');
              });
              $('.email').click(function(){
                  $('#testmail').modal('show');
                  $('.modal-title').html('Tes Email');
              });

               $('.total_customer').click(function(){
                  $('#total_customer').modal('show');
                  $('.modal-title').html('Total Customer');
              });
                 $('.total_project_dev').click(function(){
                  $('#total_project_dev').modal('show');
                  $('.modal-title').html('Total Project Development');
              });
                 $('.total_project_so').click(function(){
                  $('#total_project_so').modal('show');
                  $('.modal-title').html('Total Project Sign Off');
              });
          });

</script>


@endpush



