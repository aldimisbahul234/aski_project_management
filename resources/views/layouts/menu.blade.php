
     <!-- BEGIN: Main Menu-->
     <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
          <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="../../../html/ltr/vertical-menu-template/index.html"><span class="brand-logo">
                  <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                    <defs>
                      <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                        <stop stop-color="#000000" offset="0%"></stop>
                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                      </lineargradient>
                      <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                      </lineargradient>
                    </defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                        <g id="Group" transform="translate(400.000000, 178.000000)">
                          <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill:currentColor"></path>
                          <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                          <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                          <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                          <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                        </g>
                      </g>
                    </g>
                  </svg></span>
                <h2 class="brand-text">A S K I - PM</h2></a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
          </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
          <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" {{ Request::is('home') || Request::is('home/*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('/project')}}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span></a>
            </li>
              @if(Auth::user()->hak_akses =="administrator")
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">SYSTEM &amp; SETTING</span><i data-feather="more-horizontal"></i>
            </li>
          <!--   <li class="{{ Request::is('level') || Request::is('level*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('level')}}"><i data-feather="settings"></i><span class="menu-item text-truncate" data-i18n="Account Settings">Level User</span></a>
            </li> -->
            <li class="{{ Request::is('user') || Request::is('user*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('user')}}"><i data-feather="user"></i><span class="menu-item text-truncate" data-i18n="Account Settings">User</span></a>
            </li>
            @endif
              @if(Auth::user()->hak_akses =="administrator" || Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader" || Auth::user()->hak_akses =="quality")
               @php
              $check_notis = App\Http\Controllers\Controller::checkNotif('all');
              @endphp
            <li class=" navigation-header"><span data-i18n="User Interface">PROJECT</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ Request::is('project') || Request::is('project*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('project')}}"><i data-feather="clipboard"></i><span class="menu-title text-truncate" data-i18n="Typography">Project</span></a>
              @if($check_notis > 0)
              <span class="notif" style="right: 12px;height: 57%;padding-top: 0px;padding-left: 10px;padding-right: 20px;">{{ $check_notis }}</span>
              @endif
            </li>
            @endif
              @if(Auth::user()->hak_akses =="project_leader" || Auth::user()->hak_akses =="project_manager")
               <li class="navigation-header"><span data-i18n="User Interface">History</span><i data-feather="more-horizontal"></i>
            </li>
               <li class="nav-item {{ Request::is('history_rio') || Request::is('history_rio*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('history_rio')}}"><i data-feather="clipboard"></i><span class="menu-title text-truncate" data-i18n="Typography">History R.I.O</span></a>
            </li>
             <li class="nav-item {{ Request::is('history_escalation') || Request::is('history_escalation*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('history_escalation')}}"><i data-feather="clipboard"></i><span class="menu-title text-truncate" data-i18n="Typography">History ESCALATION</span></a>
            </li>
              @endif
              @if(Auth::user()->hak_akses =="administrator")
            <li class="navigation-header"><span data-i18n="User Interface">Master Data</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ Request::is('dataSector') || Request::is('dataSector/*') || Request::is('dataCustomer') || Request::is('dataCustomer/*') || Request::is('typeProject') || Request::is('typeProject/*') || Request::is('dataProduct') || Request::is('dataProduct/*') || Request::is('dataProcess') || Request::is('dataProcess/*') || Request::is('categoryControl') || Request::is('categoryControl/*') || Request::is('pi') || Request::is('pi/*') || Request::is('seat') || Request::is('seat/*') || Request::is('mirror') || Request::is('mirror/*') || Request::is('painting') || Request::is('painting/*') || Request::is('eventProject') || Request::is('eventProject/*') || Request::is('departement') || Request::is('departement/*') ? 'open' : '' }}"><a class="d-flex align-items-center" href="#"><i data-feather="book"></i><span class="menu-title text-truncate" data-i18n="User">Master Data</span></a>
              <ul class="menu-content">
                <li class="{{ Request::is('dataSector') || Request::is('dataSector*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('dataSector')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Data Sector</span></a>
                </li>
                <li class="{{ Request::is('dataCustomer') || Request::is('dataCustomer*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('dataCustomer')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View">Data Customer</span></a>
                </li>
                <li class="{{ Request::is('typeProject') || Request::is('typeProject*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('typeProject')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Data Type Project</span></a>
                </li>
                  <li class="{{ Request::is('dataProduct') || Request::is('dataProduct*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('dataProduct')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Data Product</span></a>
                </li>
                <li class="{{ Request::is('dataProcess') || Request::is('dataProcess*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('dataProcess')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View">Data Process</span></a>
                </li>
                <li class="{{ Request::is('categoryControl') || Request::is('categoryControl*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('categoryControl')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Data Category Control</span></a>
                </li>
                <li class="nav-item {{ Request::is('pi') || Request::is('pi/*') || Request::is('seat') || Request::is('seat/*') || Request::is('mirror') || Request::is('mirror/*') || Request::is('painting') || Request::is('painting/*') ? 'open' : '' }}"><a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="User">Data Item Control</span></a>
                    <ul class="menu-content">
                         <li class="{{ Request::is('pi') || Request::is('pi*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('pi')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">PI</span></a>
                         </li>
                         <li class="{{ Request::is('seat') || Request::is('seat*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('seat')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">SEAT</span></a>
                         </li>
                         <li class="{{ Request::is('mirror') || Request::is('mirror*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('mirror')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">MIRROR</span></a>
                         </li>
                         <li class="{{ Request::is('painting') || Request::is('painting*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('painting')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">PAINTING</span></a>
                         </li>
                    </ul>
                <li class="{{ Request::is('eventProject') || Request::is('eventProject*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('eventProject')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View">Data Event Project</span></a>
                </li>
                <li class="{{ Request::is('departement') || Request::is('departement*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('departement')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Data Departement</span></a>
                </li>     

              </ul>
            </li>
            <li class=" navigation-header"><span data-i18n="User Interface">DOCUMENT</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ Request::is('rfq') || Request::is('rfq*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{  url('rfq') }}"><i data-feather="file"></i><span class="menu-item text-truncate" data-i18n="Account Settings">RFQ DOCUMENT</span></a>
            </li>
            @endif

             @if(Auth::user()->hak_akses =="guest_system")
            <li class=" navigation-header"><span data-i18n="User Interface">PROJECT</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ Request::is('project') || Request::is('project*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('project')}}"><i data-feather="clipboard"></i><span class="menu-title text-truncate" data-i18n="Typography">Project</span></a>
            </li>
            @endif
            
            @if(Auth::user()->hak_akses =="cost")
              <li class=" navigation-header"><span data-i18n="User Interface">COST</span><i data-feather="more-horizontal"></i>
            </li>
            <li><a class="d-flex align-items-center" href="{{url('cost')}}"><i data-feather="file"></i><span class="menu-item text-truncate" data-i18n="Account Settings">Mangement Cost</span></a>
            </li>
            </li>
            @endif
              @if(Auth::user()->hak_akses =="guest")
              @php
              $check_notif = App\Http\Controllers\Controller::checkNotif('document');
               $check_notif_project = App\Http\Controllers\Controller::checkNotif('item');
              @endphp
             
              <li class=" navigation-header"><span data-i18n="User Interface">MASTER PIC</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="{{ Request::is('pic')  ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('pic')}}"><i data-feather="file"></i><span class="menu-item text-truncate" data-i18n="Account Settings">Project</span></a>
               @if($check_notif_project > 0)
            <span class="notif" style="right: 12px;height: 57%;padding-top: 0px;padding-left: 10px;padding-right: 20px;">{{ $check_notif_project }}</span>
            @endif
            </li>
              <li class="{{ Request::is('pic/my_document') || Request::is('pic/my_document*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('pic/my_document')}}"><i data-feather="file"></i><span class="menu-item text-truncate" data-i18n="Account Settings">Document</span></a>
                @if($check_notif > 0)
            <span class="notif" style="right: 12px;height: 57%;padding-top: 0px;padding-left: 10px;padding-right: 20px;">{{ $check_notif }}</span>
            @endif
            </li>
              <li class="{{ Request::is('pic/my_profile') || Request::is('pic/my_profile*') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{url('pic/my_profile')}}"><i data-feather="file"></i><span class="menu-item text-truncate" data-i18n="Account Settings">My profile</span></a>
            </li>
            </li>
            @endif
          </ul>
        </div>
      </div>
      <!-- END: Main Menu-->
