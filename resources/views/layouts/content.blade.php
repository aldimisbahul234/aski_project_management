  <div class="app-content content ">

      <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>

          <div class="content-wrapper">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                     @if(str_contains(url()->current(), '/level'))
                    <h2 class="content-header-title float-left mb-0">Level user</h2>
                        @elseif(str_contains(url()->current(), '/user'))
                          <h2 class="content-header-title float-left mb-0">User list</h2>
                      @endif
                        <div class="breadcrumb-wrapper">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Table Bootstrap
                            </li>
                          </ol>
                        </div>

                  </div>
                </div>
              </div>
          </div>
        </div>
