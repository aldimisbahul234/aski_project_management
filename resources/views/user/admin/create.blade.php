 @extends('layouts/root')
@section('main')
 <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tambah User</h4>
        </div>
        <div class="card-body">
          <form class="needs-validation" novalidate>

            <div class="row">
              <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Nama Depan</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                required
              />
            </div>
          </div>
            <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Nama Belakang</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                required
              />
            </div>
          </div>
           </div>
          
             <div class="row">
              <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Username</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                required
              />
            </div>
          </div>
            <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Telpon</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                required
              />
            </div>
          </div>
           </div>

        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-default-email1">Email</label>
              <input
                type="email"
                id="basic-default-email1"
                class="form-control"
                placeholder="john.doe@email.com"
                aria-label="john.doe@email.com"
                required
              />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-default-password1">Password</label>
              <input
                type="password"
                id="basic-default-password1"
                class="form-control"
                name="password"
                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                required
              />
            </div>
          </div>
          </div>


            <div class="form-group">
              <label for="select-country1">Hak Akses</label>
              <select class="form-control" id="select-country1" required>
                <option value="">Select Country</option>
                <option value="usa">USA</option>
                <option value="uk">UK</option>
                <option value="france">France</option>
                <option value="australia">Australia</option>
                <option value="spain">Spain</option>
              </select>
            </div>

            <div class="form-group">
              <label for="customFile1">Profile pic</label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1" required />
                <label class="custom-file-label" for="customFile1">Choose profile pic</label>
              </div>
            </div>
      
            <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@push('script')

@endpush