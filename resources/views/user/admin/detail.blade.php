 @extends('layouts/root')
@section('main')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<div class="col-lg-12">
				 <div class="form-group">
				<input type="text" name="firstname" value="{{ Auth::user()->firstname }}" class="form-control"> 
			</div>
			</div>
			<div class="col-lg-12">
				 <div class="form-group">
				<input type="text" name="lastname" value="{{ Auth::user()->lastname }}" class="form-control">
				</div> 
			</div>
			<div class="col-lg-12">
				 <div class="form-group">
				<input type="text" name="username" value="{{ Auth::user()->username }}" class="form-control"> 
			</div>
			</div>
			<div class="col-lg-12">
				 <div class="form-group">
				<input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control"> 
			</div>
			</div>
			<div class="col-lg-12">
				 <div class="form-group">
				<input type="password" name="password" value="{{ Auth::user()->password }}" class="form-control"> 
			</div>
			</div>
			<div class="col-lg-12">
				 <div class="form-group">
				<button class="btn btn-success"> UPDATE</button> 
			</div>
			</div>
		</div>
	</div>
</div>
@endsection