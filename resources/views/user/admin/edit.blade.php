 @extends('layouts/root')
@section('main')
 <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Edit {{ $user->firstname }}</h4>
        </div>
        <div class="card-body">
          <form class="needs-validation" action="{{ url('user/update/'.$user->id_pengguna) }}" method="post" enctype="multipart/form-data">
      @csrf
            <div class="row">
              <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Nama Depan</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                value="{{ $user->firstname }}"
                name="firstname"
              />
            </div>
          </div>
            <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Nama Belakang</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                value="{{ $user->lastname }}"
                name="lastname"
              />
            </div>
          </div>
           </div>
          
             <div class="row">
              <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Username</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                value="{{ $user->username }}"
                name="username"
              />
            </div>
          </div>
            <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Telpon</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Name"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                value="{{ $user->telepon }}"
                name="telpon"
              />
            </div>
          </div>
           </div>

        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-default-email1">Email</label>
              <input
                type="email"
                id="basic-default-email1"
                class="form-control"
                placeholder="john.doe@email.com"
                aria-label="john.doe@email.com"
                value="{{ $user->email }}"
                name="email"
              />
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label class="form-label" for="basic-default-password1">Password</label>
              <input
                type="password"
                id="basic-default-password1"
                class="form-control"
                name="password"
                   value="{{ $user->password }}"
                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
               
              />
            </div>
          </div>
          </div>

          @if(Auth::user()->hak_akses =='administrator')
           <div class="form-group">
            <label for="select-country1">Hak Akses</label>
            <select class="form-control" id="select-country1" name="hak_akses" required>
              <option value="">-Pilih Akses-</option>
              <option value="administrator" {{ $user->hak_akses =="administrator" ? "selected":"" }}>Administrator</option>
              <option value="project_manager" {{ $user->hak_akses =="project_manager" ? "selected":"" }}>Project Manager</option>
              <option value="project_leader" {{ $user->hak_akses =="project_leader" ? "selected":"" }}>Project Leader</option>
              <option value="cost" {{ $user->hak_akses =="cost" ? "selected":"" }}>Cost</option>
              <option value="quality" {{ $user->hak_akses =="quality" ? "selected":"" }}>Quality</option>
            </select>
          </div>
          @endif
          <!--   <div class="form-group">
              <label for="customFile1">Profile pic</label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1" value="" />
                <label class="custom-file-label" for="customFile1">Choose profile pic</label>
              </div>
            </div> -->
      
            <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@push('script')

@endpush