             
              <div
                            class="modal fade text-left"
                            id="default"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="myModalLabel1"
                            aria-hidden="true"
                          >
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Tambah Sector</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                          
                                   <div class="col-md-12 col-12">
                 
    <form class="form-participant" action="{{ url('dataSector') }}" method="post" enctype="multipart/form-data">
      @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Sector</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Sector"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="product_name"
                            required
                          />
                        </div>
                     



                        <div class="form-group">
                          <label for="select-country1">Box Color</label>
                          <select class="form-control" id="select-country1" name="box_color" required>
                            <option value="">-Box Color-</option>
                            <option value="bg-danger">Red</option>
                            <option value="bg-success">Green</option>
                          </select>
                        </div>

                         <div class="form-group">
                          <label for="select-country1">Logo</label>
                          <select class="form-control" name="logo" required>
                            <option value="">-Pilih Logo-</option>
                            <option value="fa fa-motorcycle fa-5x">Motorcycle</option>
                            <option value="fa fa-car fa-5x">Car</option>
                            <option value="fa fa-truck fa-5x">Truck</option>
                            <option value="fa fa-bus fa-5x">Bus</option>
                            <option value="fa fa-plane fa-5x">Plane</option>
                            <option value="fa fa-subway fa-5x">Train</option>
                            <option value="fa fa-ship fa-5x">Ship</option>
                            <option value="fa fa-cog fa-5x">Non Automotive</option>
                          </select>
                        </div>

              
                    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
