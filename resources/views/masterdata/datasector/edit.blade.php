 
@extends('layouts/root')
@section('main')
@include('masterdata.datasector.header.header')
@include('masterdata.datasector.flash.flash')
 <div class="col-12">
    <div class="card">
      <div class="card-header">

         <div class="col-lg-12">
            <div class="card" style="background-color: rgba(115,103,240,.7)">
              <div class="card-body">
                  <h2 style="text-align: center;" class="text-white">EDIT SECTOR</h2>
              </div>
            </div>
        </div>

      </div>
      <div class="card-body">

 <form class="form-participant" action="{{ url('dataSector/update/'.$data->id_product) }}" method="post" enctype="multipart/form-data">
      @csrf
               <input type="hidden" value="PUT" name="_method">            
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Sector</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Sector"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="product_name"
                            value="{{ $data->product_name }}"
                            required
                          />
                        </div>
                        <div class="form-group">
                          <label for="select-country1">Box Color</label>
                          <select class="form-control" id="select-country1" name="box_color" required>
                            <option value="">-Box Color-</option>
                            <option value="bg-danger" {{ $data->box_color == "bg-danger" ? "selected" : "" }}>Red</option>
                            <option value="bg-primary" {{ $data->box_color == "bg-primary" ? "selected" : "" }}>Purple</option>
                          </select>
                        </div>

                         <div class="form-group">
                          <label for="select-country1">Logo</label>
                          <select class="form-control" name="logo" required>
                            <option value="">-Pilih Logo-</option>
                            <option value="fa fa-motorcycle fa-5x" {{ $data->logo == "fa fa-motorcycle fa-5x" ? "selected" : "" }}>Motorcycle</option>
                            <option value="fa fa-car fa-5x" {{ $data->logo == "fa fa-car fa-5x" ? "selected" : "" }}>Car</option>
                            <option value="fa fa-truck fa-5x" {{ $data->logo == "fa fa-truck fa-5x" ? "selected" : "" }}>Truck</option>
                            <option value="fa fa-bus fa-5x" {{ $data->logo == "fa fa-bus fa-5x" ? "selected" : "" }}>Bus</option>
                            <option value="fa fa-plane fa-5x" {{ $data->logo == "fa fa-plane fa-5x" ? "selected" : "" }}>Plane</option>
                            <option value="fa fa-subway fa-5x" {{ $data->logo == "fa fa-subway fa-5x" ? "selected" : "" }}>Train</option>
                            <option value="fa fa-ship fa-5x" {{ $data->logo == "fa fa-ship fa-5x" ? "selected" : "" }}>Ship</option>
                            <option value="fa fa-cog fa-5x" {{ $data->logo == "fa fa-cog fa-5x" ? "selected" : "" }}>Non Automotive</option>
                          </select>
                        </div>
                    </div>
                  </div>
   

                    
                     <div class="form-group">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                </form>

</div>
</div>
</div>

@endsection
@push('script')
@endpush
