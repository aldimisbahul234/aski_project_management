
@extends('layouts/root')
@section('main')
@include('masterdata.datasector.header.header')
@include('masterdata.datasector.flash.flash')
 
  <div class="col-12">
    <div class="card">
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
    	  <table class="datatables-basic table" id="datatable">
          <thead>
            <tr>
             
              <th style="text-align: center;">No</th>
              <th style="text-align: center;">Sector Name</th>
              <th style="text-align: center;">Box Color</th>
              <th style="text-align: center;">Logo</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
           <tbody>
          @foreach($data as $value)
            <tr>
              <td style="text-align: center;">{{ $loop->iteration }}</td>
              <td style="text-align: center;">{{ $value->product_name }}</td>
              <td style="text-align: center;"><button class="btn {{ $value->box_color }}"></button></td>
              <td style="text-align: center;"><i class="{{ $value->logo }}"></i></td>
              <td style="text-align: center;">
               
                 
                    <a class="btn" href="{{url('dataSector/edit/'.$value->id_product)}}">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="btn" href="{{url('dataSector/delete/'.$value->id_product)}}">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  
              </td>
            </tr>
          @endforeach
         </tbody>
        </table>
    </div>
    	</div>

      </div>
    </div>
</div>

@include('masterdata.datasector.modal_data_sector')

@endsection
@push('script')
    <script src="../../../app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 dom: 'Bfrtip',
   
                   
              buttons: [
        {
          extend: 'collection',
          className: 'btn btn-outline-secondary dropdown-toggle mr-2',
          text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
          buttons: [
            {
              extend: 'print',
              text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
              className: 'dropdown-item',
             
            },
            {
              extend: 'csv',
              text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
              className: 'dropdown-item',
              
            },
            {
              extend: 'excel',
              text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
              className: 'dropdown-item',
              
            },
            {
              extend: 'pdf',
              text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
              className: 'dropdown-item',
              
            },
            {
              extend: 'copy',
              text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
              className: 'dropdown-item',
              
            }
          ],
          init: function (api, node, config) {
            $(node).removeClass('btn-secondary');
            $(node).parent().removeClass('btn-group');
            setTimeout(function () {
              $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
            }, 50);
          }
        },
        {
          text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Add New Record',
          className: 'create-new btn btn-primary',
          attr: {
            'data-toggle': 'modal',
            'data-target': '#default'
          },
          init: function (api, node, config) {
            $(node).removeClass('btn-secondary');
          }
        }
      ],
        });
          });
    </script>
@endpush