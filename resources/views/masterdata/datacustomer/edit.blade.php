@extends('layouts/root')
@section('main')
@include('masterdata.datacustomer.header.header')
<div class="col-lg-12">
<div class="card">
	<div class="card-header">

		 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">EDIT CUSTOMER</h2>
		    			</div>
		  			</div>
				</div>

	</div>
	<div class="card-body">
      <form class="form-participant" action="{{ url('dataCustomer/update/'.$data->id_customer) }}" method="post" enctype="multipart/form-data">
                      @csrf
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Customer</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Sector"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="customer_name"
                            value="{{ $data->customer_name }}"
                            required
                          />
                        </div>
                        <div class="form-group">
                          <label for="select-country1">Sector Name</label>
                          <select class="form-control" id="select-country1" name="id_product" required>
                            <option value="">-Pilih Sector-</option>
                            @foreach($sector as $value)
                            <option value="{{ $value->id_product }}" {{ $data->id_product == $value->id_product ? "selected" : "" }}>{{$value->product_name}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                  </div>
                    
                     <div class="form-group">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                 </form>
             </div>
</div>
</div>
@endsection