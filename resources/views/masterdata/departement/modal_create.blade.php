             
<div class="modal fade text-left"
              id="default"
              tabindex="-1"
              role="dialog"
              aria-labelledby="myModalLabel1"
              aria-hidden="true"
>
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel1">Tambah Departement</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
          
                   <div class="col-md-12 col-12">
                 
                        <form class="form-participant" action="{{ url('departement') }}" method="post" enctype="multipart/form-data">
                           @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Departement</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Departement"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="departement"
                            required
                          />
                        </div>
                    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
