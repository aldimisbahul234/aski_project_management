@extends('layouts/root')
@section('main')
@include('masterdata.departement.header.header')
<div class="col-lg-12">
<div class="card">
	<div class="card-header">

		 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">EDIT DEPARTEMENT</h2>
		    			</div>
		  			</div>
				</div>

	</div>
<div class="card-body">
                        <form class="form-participant" action="{{ url('departement/update/'.$data->id_departement) }}" method="post" enctype="multipart/form-data">
                           @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Departement</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Departement"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="departement"
                            value="{{ $data->departement }}"
                            required
                          />
                        </div>
                    
                    </div>
                  </div>
   

                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                      </form>
                  </div>
              </div>

@endsection