             
              <div
                            class="modal fade text-left"
                            id="default"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="myModalLabel1"
                            aria-hidden="true"
                          >
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Tambah Customer</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                          
                                   <div class="col-md-12 col-12">
                 
    <form class="form-participant" action="{{ url('typeProject') }}" method="post" enctype="multipart/form-data">
      @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Project</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Sector"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="project_name"
                            required
                          />
                        </div>
                     



                        <div class="form-group">
                          <label for="select-country1">Customer Name</label>
                          <select class="form-control" id="select-country1" name="id_customer" required>
                            <option value="">-Pilih Customer-</option>
                            @foreach($customer as $value)
                            <option value="{{ $value->id_customer }}">{{ $value->customer_name }}</option>
                            @endforeach
                          </select>
                        </div>
              
                    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
