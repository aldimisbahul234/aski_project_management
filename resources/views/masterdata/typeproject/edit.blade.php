@extends('layouts/root')
@section('main')
@include('masterdata.typeproject.header.header')
 <div class="col-12">
    <div class="card">
      <div class="card-header">

         <div class="col-lg-12">
            <div class="card" style="background-color: rgba(115,103,240,.7)">
              <div class="card-body">
                  <h2 style="text-align: center;" class="text-white">EDIT TYPE PROJECT</h2>
              </div>
            </div>
        </div>

      </div>
      <div class="card-body">

<form class="form-participant" action="{{ url('typeProject/update/'.$data->id_project) }}" method="post" enctype="multipart/form-data">
      @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Project</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Sector"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="project_name"
                            value="{{ $data->project_name }}"
                            required
                          />
                        </div>
                     



                        <div class="form-group">
                          <label for="select-country1">Customer Name</label>
                          <select class="form-control" id="select-country1" name="id_customer" required>
                            <option value="">-Pilih Customer-</option>
                            @foreach($customer as $value)
                            <option value="{{ $value->id_customer }}" {{ $data->id_customer == $value->id_customer ? "selected" : "" }}>{{ $value->customer_name }}</option>
                            @endforeach
                          </select>
                        </div>
              
                    
                    </div>
                  </div>
   

                    
                     <div class="form-group">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
              </div>
          </div>

@endsection