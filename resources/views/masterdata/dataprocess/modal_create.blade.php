             
              <div
                            class="modal fade text-left"
                            id="default"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="myModalLabel1"
                            aria-hidden="true"
                          >
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Tambah Process</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                          
                                   <div class="col-md-12 col-12">
                 
    <form class="form-participant" action="{{ url('dataProcess') }}" method="post" enctype="multipart/form-data">
      @csrf
                        
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Process</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Process"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="process_name"
                            required
                          />
                        </div>
                     
                        <div class="form-group">
                          <label for="select-country1">Box Color</label>
                          <select class="form-control" id="select-country1" name="box_color" required>
                            <option value="">-Box Color-</option>
                            <option value="red">Red</option>
                            <option value="yellow">Yellow</option>
                            <option value="pink">Pink</option>
                            <option value="orange">Orange</option>
                             <option value="purple">Purple</option>
                             <option value="green">Green</option>
                              <option value="blue">Blue</option>
                          </select>
                        </div>

              
                    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
