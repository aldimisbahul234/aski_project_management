@extends('layouts/root')
@section('main')
@include('masterdata.typeproject.header.header')
<div class="col-lg-12">
<div class="card">
	<div class="card-header">

		 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">EDIT PROCESS</h2>
		    			</div>
		  			</div>
				</div>

	</div>
	<div class="card-body">

 <form class="form-participant" action="{{ url('dataProcess') }}" method="post" enctype="multipart/form-data">
      @csrf
            
            <div class="form-group">
              <label class="form-label" for="basic-addon-name">Nama Process</label>
              <input
                type="text"
                id="basic-addon-name"
                class="form-control"
                placeholder="Nama Process"
                aria-label="Name"
                aria-describedby="basic-addon-name"
                name="process_name"
                value="{{ $data->process_name }}"
                required
              />
            </div>
         
            <div class="form-group">
                          <label for="select-country1">Box Color</label>
                          <select class="form-control" id="select-country1" name="box_color" required>
                            <option value="">-Box Color-</option>
                            <option value="bg-danger" {{ $data->box_color == "bg-danger" ? "selected" : "" }}>Red</option>
                            <option value="bg-primary" {{ $data->box_color == "bg-primary" ? "selected" : "" }}>Purple</option>
                              <option value="pink" {{ $data->box_color == "pink" ? "selected" : "" }}>Pink</option>
                          </select>
                        </div>
        
        </div>
      </div>
   

                    
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
        </div>
    </div>

@endsection