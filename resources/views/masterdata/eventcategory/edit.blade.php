@extends('layouts/root')
@section('main')
@include('masterdata.eventcategory.header.header')
<div class="col-lg-12">
<div class="card">
	<div class="card-header">

		 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">EDIT EVENT PROJECT</h2>
		    			</div>
		  			</div>
				</div>

	</div>
	<div class="card-body">

       <form class="form-participant" action="{{ url('eventProject/update/'.$data->id_category_event_project) }}" method="post" enctype="multipart/form-data">
                           @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Event Project</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Event Project"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="event_project_name"
                            value="{{ $data->event_project_name }}"
                            required
                          />
                        </div>
                    
                    </div>
                  </div>
   

                    
                         <div class="form-group">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>


                  </div>
              </div>

@endsection