 
@extends('layouts/root')
@section('main')
@include('masterdata.categorycontroll.header.header')
@include('masterdata.categorycontroll.flash.flash')
 <div class="col-12">
    <div class="card">
      <div class="card-header">

         <div class="col-lg-12">
            <div class="card" style="background-color: rgba(115,103,240,.7)">
              <div class="card-body">
                  <h2 style="text-align: center;" class="text-white">EDIT CATEGORY CONTROL</h2>
              </div>
            </div>
        </div>

      </div>
      <div class="card-body">

 <form class="form-participant" action="{{ url('categoryControl/update/'.$data->id_category_list) }}" method="post" enctype="multipart/form-data">
      @csrf
               <input type="hidden" value="PUT" name="_method">            
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Category</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Category"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="category_list_name"
                            value="{{ $data->category_list_name }}"
                            required
                          />
                        </div>
                        <div class="form-group">
                          <label for="select-country1">Process</label>
                          <select class="form-control" id="select-country1" name="id_process" required>
                            <option value="">-Pilih process-</option>
                            @foreach($process as $value)
                            <option value="{{$value->id_process}}" {{ $data->id_process == $value->id_process ? "selected" : "" }}>{{$value->process_name}}</option>
                            @endforeach
                          </select>
                        </div>

                    </div>
                  </div>
   

                    
                     <div class="form-group">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                </form>

</div>
</div>
</div>

@endsection
@push('script')
@endpush
