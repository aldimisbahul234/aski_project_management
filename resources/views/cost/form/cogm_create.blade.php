@extends('layouts/root')
@section('main')
@include('project.flash.flash')


 <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <h2 style="text-align: center">ADD COGM</h2>
        <hr>

        <form class="form-participant" action="{{ url('cost/post_cogm/') }}" method="post" enctype="multipart/form-data">
                      @csrf
                       <input type="hidden" name="id_project" value="{{ $project->id_project }}">

                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">EVENT</label> :
                      <div class="col-sm-9">
                        <select class="form-control" value="" name="id_category_event_project" placeholder="Normal Input" style="width: 100%"/ >
                          <option>-Pilih Event-</option>
                        @foreach($event as $event)
                          <option value="{{ $event->id_category_event_project }}">{{ $event->event_project_name }}</option>
                        @endforeach

                        </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PRODUCT</label> :
                      <div class="col-sm-9">
                        <select class="form-control" placeholder="Normal Input" style="width: 100%"/ id="part_number" name="part_number">
                          <option>-Pilih Product-</option>
                        @foreach($product as $product)
                          <option value="{{ $product->part_number }}">{{ $product->part_number }}</option>
                        @endforeach

                        </select>
                    </div>
                  </div>

                 <span id="get"></span>

                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">BOM COST(%)</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="bom_cogm" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PROCESS COST(%)</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="process_cogm" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">DEPRECIATION COST(%)</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="depreciation_cogm" />
                    </div>
                  </div>

                     <div class="col-sm-1 offset-sm-10">
                    <button type="submit" class="btn btn-success">ADD</button>
                  </div>

                </div>
               

              </div>
            </div>
</form>

<div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <h2 style="text-align: center">SHOW COGM</h2>
        <form action="{{ url('cost/save_all') }}">
         @csrf
          <button type="submit" class="btn btn-success">SAVE ALL</button>
       
        <hr>

<table class="table table-striped table-hover nowrap index" id="datatable">
<thead>
  <tr>
  <th>NO</th>
  <th>EVENT COGM</th>
  <th>PART NUMBER</th>
  <th>PART NAME</th>
  <th>BOM COST</th>
  <th>PROCESS COST</th>
  <th>DEPRECIATION COST</th>
  <th>TOTAL COST </th>
 
  </tr>
</thead>
<tbody>
@foreach($cogm as $cogm)
  <tr>
    <td>{{ $loop->iteration }}</td>
    <td>{{ $cogm->event->event_project_name }}</td>
    <td>{{ $cogm->part_number }}</td>
    <td>{{ $cogm->product->part_name }}</td>
    <td>{{ $cogm->bom_cogm }}</td>
    <td>{{ $cogm->process_cogm }}</td>
    <td>{{ $cogm->depreciation_cogm }}</td>
    <td>{{ $cogm->total_cogm }}</td>
  </tr>
@endforeach
</tbody>
</table>
@foreach($cogms as $cogms)
<input type="hidden" name="id_project" value="{{ $project->id_project }}">
<input type="hidden" name="id_category_event_project[]" value="{{ $cogms->id_category_event_project }}">
<input type="hidden" name="part_number[]" value="{{ $cogms->part_number }}">
<input type="hidden" name="bom_cogm[]" value="{{ $cogms->bom_cogm }}">
<input type="hidden" name="process_cogm[]" value="{{ $cogms->process_cogm }}">
<input type="hidden" name="id_cogm[]" value="{{ $cogms->id_cogm }}">
<input type="hidden" name="depreciation_cogm[]" value="{{ $cogms->depreciation_cogm }}">
@endforeach
 </form>
      </div>
    </div>
  </div>
@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
              $('#part_number').on('change',function(e){
                   $('#get').hide();
                var brand_id =  e.target.value;
                console.log(brand_id);

                $.ajax  ({
                url: "{{ url('project/get_prod') }}/"+brand_id,
                success: function(brand_id){
                    console.log(brand_id);
                       $("#get").show();
                       $("#get").html('<div class="form-group row">'+
                    '<label for="colFormLabel" class="col-sm-2 col-form-label">PART NAME</label> :'+
                      '<div class="col-sm-9">'+
                        '<input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value="'+brand_id+'" name="part_name" />'+
                    '</div>'+
                  '</div>');
                    }
                });
             });

                $('.index').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });
 </script>
 @endpush
 