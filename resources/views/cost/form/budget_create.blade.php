@extends('layouts/root')
@section('main')
@include('project.flash.flash')


 <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <h2 style="text-align: center">ADD BUDGET</h2>
        <hr>
 <form class="form-participant" action="{{ url('cost/post_budget/') }}" method="post" enctype="multipart/form-data">
                      @csrf

                <input type="hidden" name="id_project" value="{{ $project->id_project }}">

                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PROJECT NAME</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value="{{ $project->project_name }}" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PROJECT BUDGET DATE</label> :
                      <div class="col-sm-9">
                        <input type="date" class="form_technical" name="budget_date" placeholder="Normal Input" style="width: 100%" value="" />
                    </div>
                  </div>

                    <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PRODUCT</label> :
                      <div class="col-sm-9">
                        <select class="form-control" value="" name="part_number" placeholder="Normal Input" style="width: 100%"/ id="part_number">
                          <option>-Pilih Process-</option>
                        @foreach($product as $product)
                          <option value="{{ $product->part_number }}">{{ $product->part_name }} ({{ $product->part_number }}) </option>
                        @endforeach

                        </select>
                    </div>
                  </div>

                 <span id="get"></span>

                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"> ITEM INVESMENT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="item_invesment" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">FUNDING</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="funding" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">CATEGORY</label> :
                      <div class="col-sm-9">
                        <select class="form-control" value="" name="category" placeholder="Normal Input" style="width: 100%"/ id="part_number">
                          <option>-Pilih Category-</option>
                       
                          <option value="Opek">Opex</option>
                          <option value="Capex">Copex</option>
                        </select>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">PRICE ITEM (Rp)</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="price_of_item_plan" />
                    </div>
                  </div>

                    <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">QTY / ITEM</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="qty_item_plan" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">UOM</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="uom_plan" />
                    </div>
                  </div>


                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TOTAL PRICE(Rp)</label> :
                      <div class="col-sm-9">
                        <input type="number" class="form_technical" placeholder="Normal Input" style="width: 100%" value="" name="total_price_plan" />
                    </div>
                  </div>

                     <div class="col-sm-1 offset-sm-10">
                    <button type="submit" class="btn btn-success">ADD</button>
                  </div>
</form>
                </div>
               

              </div>
            </div>
<!-- 
<div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <h2 style="text-align: center">ADD BUDGET</h2>
        <form class="form-participant" action="{{ url('cost/save_budget/') }}" method="post" enctype="multipart/form-data">
                      @csrf
         
       
        <hr>

<table class="table table-striped table-hover nowrap index" id="datatable">
<thead>
  <tr>
  <th>NO</th>
  <th>PART NAME</th>
  <th>PART NUMBER</th>
  <th>ITEM INVESMENT</th>
  <th>FUNDING</th>
  <th>CATEGORY</th>
  <th>PRICE OF ITEM</th>
  <th>QTY ITEM </th>
  <th>OUM</th>
  <th>TOTAL PRICE</th>
  </tr>
</thead>
<tbody>
@foreach($getBudget as $value)
<tr>
  <td>{{ $loop->iteration }}</td>
  <td>{{ $value['product'] }}</td>
  <td>{{ $value['part_number'] }}</td>
  <td>{{ $value['item_invesment'] }}</td>
  <td>{{ $value['funding'] }}</td>
  <td>{{ $value['category'] }}</td>
  <td>{{ $value['price_of_item_plan'] }}</td>
  <td>{{ $value['qty_item_actual'] }}</td>
  <td>{{ $value['uom_actual'] }}</td>
  <td>{{ $value['total_price_plan'] }}</td>
</tr>

@endforeach
</tbody>
</table>

 <button type="submit" class="btn btn-success">SAVE ALL</button>
 </form> -->

      </div>
    </div>
  </div>
@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
              $('#part_number').on('change',function(e){
                   $('#get').hide();
                var brand_id =  e.target.value;
                console.log(brand_id);

                $.ajax  ({
                url: "{{ url('cost/get_qty') }}/"+brand_id,
                success: function(brand_id){
                    console.log(brand_id);
                       $("#get").show();
                       $("#get").html('<div class="form-group row">'+
                    '<label for="colFormLabel" class="col-sm-2 col-form-label">QTY ORDER</label> :'+
                      '<div class="col-sm-9">'+
                        '<input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value="'+brand_id+'" name="part_name" />'+
                    '</div>'+
                  '</div>');
                    }
                });
             });

              $('.index').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });
   

 </script>
 @endpush
 