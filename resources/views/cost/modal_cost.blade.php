<div class="modal fade bd-example-modal-lg" id="modalCost" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
   
     
              <h2 style="text-align: center;">LIST COGM</h2>
                <table class="table table-striped table-hover nowrap list" id="datatable">
                    <thead>
                    <tr>
                    <th>NO</th>
                    <th>PROJECT NAME</th>
                    <th>PRODUCT NAME</th>
                    <th>EVENT COGM</th>
                    <th>BOM COST</th>
                    <th>PROCESS COST</th>
                    <th>DEPRECIATION COST</th>
                    <th>TOTAL COST</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($getCogm as $values)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $values['project'] }}</td>
                           <td>{{ $values['product'] }}</td>
                            <td>{{ $values['event'] }}</td>
                            <td>{{ $values['bom_cogm'] }}</td>
                             <td>{{ $values['process_cogm'] }}</td>
                              <td>{{ $values['depreciation_cogm'] }}</td>
                               <td>{{ $values['total_cogm'] }}</td>
                        </tr>
                      @endforeach
                  </tbody>
                </table>
            


        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>

      </div>
  </div>
</div>
</div>