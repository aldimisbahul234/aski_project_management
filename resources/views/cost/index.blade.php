@extends('layouts/root')
@section('main')
<div class="col-lg-12">
  <div class="row">

    <div class="col-lg-4">
        <div class="card-box bg-danger" id="cost">
            <div class="inner">
                <h3 style="color: white"> COGM </h3>
                <p style="color: white"> {{ $countCogm }} COGM </p>
            </div>
            <div class="icon">
                <i class="fa fa-cog" aria-hidden="true"></i>
            </div>
          
        </div>
    </div>

     <div class="col-lg-4">
        <div class="card-box bg-success">
            <div class="inner">
                <h3 style="color: white"> TOTAL PLAN BUDGET </h3>
                <p style="color: white">Rp. {{ number_format($sumPlan,2) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-calculator" aria-hidden="true"></i>
            </div>
          
        </div>
    </div>
     <div class="col-lg-4">
        <div class="card-box bg-info">
            <div class="inner">
                <h3 style="color: white">TOTAL ACTUAL BUDGET</h3>
                <p style="color: white">Rp. {{ number_format($sumActual,2) }}</p>
            </div>
            <div class="icon">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
          
        </div>
    </div>
  </div>
</div>

<div class="col-lg-12">
<div class="card">
<div class="card-body">
    <h2 style="text-align: center;">LIST PROJECT</h2>
<table class="table table-striped table-hover nowrap index" id="datatable">
<thead>
	<tr>
	<th>NO</th>
	<th>PROJECT NAME</th>
	<th>SECTOR</th>
	<th>COSTOMER</th>
	<th>COGM</th>
	<th>BUDGET</th>
	<th>CPI</th>
	</tr>
</thead>
<tbody>
@foreach($getData as $value)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $value['project_name'] }}</td>
        <td>{{ $value['sector'] }}</td>
        <td>{{ $value['customer'] }}</td>
        <td><a href="{{ url('cost/cogm/'.$value['id_project']) }}" class="btn btn-danger btn-sm">COGM</a></td>
        <td><a href="{{ url('cost/budget/'.$value['id_project']) }}" class="btn btn-danger btn-sm">BUDGET</a></td>
        <td><a href="{{ url('cost/cpi/'.$value['id_project']) }}" class="btn btn-danger btn-sm">CPI</a></td>
    </tr>
@endforeach
</tbody>
</table>
</div>
</div>
@endsection
@include('cost.modal_cost')
@push('script')
<script type="text/javascript">
	 $(document).ready(function() {
            $('.index').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });
      $(document).ready(function() {
            $('.list').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true, 
                
            });
          });
       $(document).ready(function(){
        $('#cost').click(function(){
            $('#modalCost').modal('show');
            $('.modal-title').html('List Cogm');

        });
        });
</script>
@endpush