@extends('layouts/root')
@section('main')
<div class="col-lg-12">

			
	
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h2 class="text-title">COST PERFORMANCE INDEX (CPI)</h2>
					 
				</div>
				<div class="card-body">
					<div id="grafik_cost"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div id="cpi"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div id="plus_minus"></div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
@push('script')
    @include('project.cost.grafik.grafik_cost_cpi')
@endpush

