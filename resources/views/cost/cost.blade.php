@extends('layouts/root')
@section('main')

    <div class="col-sm-12">
      <div class="card collapse-icon">
        <div class="card-header">

        </div>
        <div class="card-body">
         <h2 style="text-align: center;">COGM</h2>
       
       @if(Auth::user()->hak_akses =="cost")
         <a class="btn btn-info" href="{{ url('cost/create_cogm/'.$project->id_project) }}"><i class="fa fa-plus fx-5"></i>ADD COGM</a><br><br>
         @else
          <button class="btn btn-info" href="#" disabled><i class="fa fa-plus fx-5"></i>ADD COGM (user cost only)</button><br><br>
         @endif
          @foreach($getData as $key=>$value)
          <div class="collapse-default">
            <div class="card" style="background-color: orange">
              <div
                id="headingCollapse1"
                class="card-header"
                data-toggle="collapse"
                role="button"
                data-target="#collapse_{{ $key }}"
                aria-expanded="false"
                aria-controls="collapse1"
              >
                <span class="lead collapse-title text-white"> {{ $value['product'] }} </span>
              </div>

              <div id="collapse_{{ $key }}" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                <div class="card-body" style="background-color: white">
                  
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table table-striped table-hover nowrap index_{{ $key }}" id="datatable">
                      <thead>
                      <tr>
                      <th>NO</th>
                      <th>EVENT COGM</th>
                      <th>BOM COST</th>
                      <th>PROCESS COST</th>
                      <th>DEPRECATION COST</th>
                      <th>TOTAL COST</th>
                      <th>ACTION</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($value['detail'] as $detail)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $detail['event'] }}</td>
                            <td>{{ $detail['bom'] }}</td>
                            <td>{{ $detail['process']}}</td>
                            <td>{{ $detail['depreciation']}}</td>
                            <td>{{ $detail['total'] }}</td>
                            <td>
                              <a href="{{ url('cost/edit/'.$value['id_cogm']) }}" class="btn btn-info btn-sm"><i data-feather="edit"></i>EDIT</a>
                              <a href="{{ url('cost/hapus/'.$value['id_cogm']) }}" class="btn btn-danger btn-sm"><i data-feather="trash"></i>HAPUS</a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                      </table>
                  </div>
                </div>
              </div>

                </div>
              </div>
            </div>
            @endforeach


          </div>
        </div>
      </div>
    </div>
 
@endsection
@push('script')
<script type="text/javascript">
   $(document).ready(function() {
<?php foreach ($data as $key => $value): ?>
          $('.index_{{ $key }}').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
<?php endforeach ?>
           

          });
</script>
@endpush