<html>
<head>
	<title>Export</title>
</head>
<body>
	<center>
		<h1>Report Product  with Detail Item Control Astra Komponen Indonesia</h1>
	</center>
  <table>
		<tr>
   
			<td >Part Number : </td>
			<td colspan="13">{{ $part_number }}</td>
     
		</tr>
    <tr>
   
			<td >Part Name : </td>
			<td colspan="13">{{ $product->part_name }}</td>
     
		</tr>
    <tr>
   
			<td >Process : </td>
     		<td colspan="13">{{ $product->process->process_name }}</td>
		</tr>
    <tr>
   
			<td >Project Leader : </td>
   			<td colspan="13">@if($get_name ==true) {{ $get_name }} @else - @endif</td>
		</tr>
    <tr>
   
			<td >Tanggal Pendaftaran Product :</td>
     		<td colspan="13"> {{ $product->tgl_masuk }}</td>
		</tr>
    <tr>
   
			<td >Plan Progress Product : </td>
     		<td colspan="13">{{ $product->plan_progress_part }} %</td>
		</tr>
    <tr>
   
			<td >Actual Progress Product : </td>
     		<td colspan="13">{{ $product->actual_progress_part }} %</td>
		</tr>
    <tr>
   
			<td >Open R.I.O </td>
    		<td colspan="13"></td>
		</tr>
    <tr>
   	
			<td >Status Product :  </td>
			@if($product->status_product ==1)
			<td colspan="1" style='background-color:red;color:white;'>High Risk</td>
			@elseif($product->status_product ==2)
			<td colspan="1" style='background-color:yellow;'>Potensial Risk</td>
			@elseif($product->status_product ==3)
			<td colspan="1" style='background-color:green;color:white;'>On Time</td>
			@elseif($product->status_product ==4)
			<td colspan="1" style='background-color:red;color:white;'>Close Beyond</td>
			@elseif($product->status_product ==5)
			<td colspan="1" style='background-color:white;'>Open</td>
			@elseif($product->status_product ==6)
			<td colspan="1" style='background-color:white;'>Open</td>
			@else
			<td colspan="1" style='background-color:teal;color:white;'>Check</td>
			@endif
   		
      
		</tr>
  </table>
  <br>
  	<table border="1">
		<tr>
			<td>No</td>
			<td>Category Control</td>
			<td>Item Control</td>
			<td>PIC</td>
			<td>Plan Start</td>
			<td>Plan Duration</td>
			<td>Plan Finish</td>
			<td>Plan Progress (%)</td>
			<td>Actual Start</td>
			<td>Actual Duration</td>
			<td>Actual Finish</td>
			<td>Actual Progress (%)</td>
			<td>Last Update</td>
      <td>Status</td>
		</tr>
		@foreach($get_item as $value)
		@foreach($value['get_item_control'] as $val)
		<tr>
			<td>{{ $loop->iteration}}</td>
			<td>{{ $val->category->category_name }}</td>
			<td>{{ $val->item->item_name }}</td>
			<td>{{ $val->pic_item }}</td>
			<td>{{ $val->plan_start_item }}</td>
			<td>{{ $val->plan_duration_item }} Hari</td>
			<td>{{ $val->plan_finish_item }}</td>
			<td>{{ $val->plan_progress_item }} %</td>
			<td>{{ $val->actual_start_item }}</td>
			<td>{{ $val->actual_duration_item }} Hari</td>
			<td>{{ $val->actual_finish_item }}</td>
			<td>{{ $val->actual_progress_item }} %</td>
			<td></td>
			<td></td>
		</tr>
		@endforeach
		@endforeach
	</table>
