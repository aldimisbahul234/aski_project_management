<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<div id="container"></div>
</body>
<script src="https://code.highcharts.com/gantt/highcharts-gantt.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script type="text/javascript">
    Highcharts.ganttChart('container', {

 title: {
        text: 'Grafik Schedule Event Project <?php echo$project->project_name; ?>'
    },

    yAxis: {
        uniqueNames: true,
        
    },
    credits:{
        enabled : false
    },

    navigator: {
        enabled: true,
        liveRedraw: true,
        series: {
            type: 'gantt',
            pointPlacement: 0.5,
            pointPadding: 0.25
        },
        yAxis: {
            min: 0,
            max: 3,
            reversed: true,
            categories: []
        }
    },
    scrollbar: {
        enabled: true
    },
    rangeSelector: {
        enabled: true,
        selected: 7
    },
    tooltip : {
        enabled : false
    },
  series: [{
    name: 'Project 1',
    data: [{
      start: Date.UTC(2021),
      end: Date.UTC(2022, 12, 1),
      completed: 1.0,
      name: 'PROJECT <?php echo($project->project_name); ?> <br> <p>--> <?php echo $pengguna->firstname;?> <?php echo $pengguna->lastname;?>',
      id:'1'
    }, 
    <?php foreach ($get_event as $key => $get_event): ?>
      {
      start: Date.UTC(<?php echo $get_event['start_year'];?>, <?php echo $get_event['start_month'];?>, <?php echo $get_event['start_day'];?>),
      end: Date.UTC(<?php echo $get_event['end_year'];?>, <?php echo $get_event['end_month'];?>, <?php echo $get_event['end_day'];?>),
      parent: '1',
      collapsed : true,
      name: '{{ $get_event['event_name'] }} <br> <p>--><?php echo $get_event['start_day'];?> / <?php echo $get_event['showing_start_month'];?> / <?php echo $get_event['start_year'];?></p> <br> <p>--><?php echo $get_event['actual_day_check'];?> / <?php echo $get_event['actual_month_check'];?> / <?php echo $get_event['actual_year_check'];?></p>'
    },
    <?php endforeach ?>
   

    ]
  }]
});
</script>
</html>