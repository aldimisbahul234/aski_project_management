@extends('layouts/root')
@section('main')
<div class="col-lg-12">
<h2>TASK PROJECT HC21</h2>
    <div class="row">
   <!--   <div class="col-sm-6">
        <div class="card">
            <div class="btn-group">
                <button
                class="btn btn-info"
                type="button"
              >
            
                CATEGORY CONTROL LIST 
            
              </button>
              <button
                class="btn btn-primary dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                SELECT CATEGORY CONTROL
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                 <a class="dropdown-item" href="">ALL CATEGORY</a>
               
              </div>
            </div>
        </div>
    </div>
 -->
<div class="col-lg-12">

	<div class="row">  
		<div class="col-lg-6">
		<div class="card">
			<div class="card-body">
        <h3>Task today</h3>
				 <table class="table analistyc" id="today">
                 <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                  </thead>
                 <tbody>
                @if(count($get_task_today) > 0)
                @foreach($get_task_today as $value)
                <tr>
                  <td>{{ $value['pic_item'] }}</td>
                  <td><a href=""> {{ $value['part_name'] }} <br /> <h5>Category name : <br />{{ $value['category_name'] }} </h5> </a></td>
                  <td></td>
                  <td>{{ $name_today }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="4" style="text-align: center;">Tidak Ada task hari ini</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                @endif
                 </tbody>
               </table>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
		<div class="card">
			<div class="card-body">
        <h3>Task next week</h3>
				 <table class="table analistyc" id="next">
                 <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                  </thead>
                 <tbody>
                @if(count($get_task_next_week) > 0)
                @foreach($get_task_next_week as $values)
                <tr>
                  <td>{{ $values['pic_item'] }}</td>
                  <td> <a href=""> {{ $values['part_name'] }} <br /> <h5>Category name : <br />{{ $values['category_name'] }} </h5> </a></td>
                  <td></td>
                  <td>{{ $name_week}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="4" style="text-align: center;">Tidak Ada task minggu depan</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                @endif
                 </tbody>
               </table>
				</div>
			</div>
		</div>

	</div>
		
</div>

@endsection
@push('script')
<script type="text/javascript">
	  $(document).ready(function() {
            $('#today').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                  "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,  
                
            });
             $('#next').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                  "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,  
                
            });
          });
</script>
@endpush

