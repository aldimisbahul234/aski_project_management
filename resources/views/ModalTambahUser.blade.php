             
              <div
                            class="modal fade text-left"
                            id="default"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="myModalLabel1"
                            aria-hidden="true"
                          >
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Tambah User</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                          
                                   <div class="col-md-12 col-12">
                 
    <form class="form-participant" action="{{ url('user') }}" method="post" enctype="multipart/form-data">
      @csrf
                        <div class="row">
                          <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Depan</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Depan"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="firstname"
                            required
                          />
                        </div>
                      </div>
                        <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Nama Belakang</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Nama Belakang"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="lastname"
                            required
                          />
                        </div>
                      </div>
                       </div>
                      
                         <div class="row">
                          <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Username</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Name"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="username"
                            required
                          />
                        </div>
                      </div>
                        <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">Telpon</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Name"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="telepon"
                            required
                          />
                        </div>
                      </div>
                       </div>

                    <div class="row">
                      <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-default-email1">Email</label>
                          <input
                            type="email"
                            id="basic-default-email1"
                            class="form-control"
                            placeholder="john.doe@email.com"
                            aria-label="john.doe@email.com"
                            name="email"
                            required
                          />
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                          <label class="form-label" for="basic-default-password1">Password</label>
                          <input
                            type="password"
                            id="basic-default-password1"
                            class="form-control"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            name="password"
                            required
                          />
                        </div>
                      </div>
                      </div>


                        <div class="form-group">
                          <label for="select-country1">Hak Akses</label>
                          <select class="form-control" id="select-country1" name="hak_akses" required>
                            <option value="">-Pilih Akses-</option>
                            <option value="administrator">Administrator</option>
                            <option value="project_manager">Project Manager</option>
                            <option value="project_leader">Project Leader</option>
                            <option value="cost">Cost</option>
                            <option value="quality">Quality</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <label for="customFile1">Profile pic</label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile1" name="photo" required />
                            <label class="custom-file-label" for="customFile1">Choose profile pic</label>
                          </div>
                        </div>
                    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
