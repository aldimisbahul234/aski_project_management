@extends('layouts/root')
@section('main')
<div class="col-lg-12">
  <div class="row">
@foreach($get_data as $get_data)

    <div class="col-lg-4">
    	<a href="{{ url('pic/project_open_rio_get/'.$project->id_project.'/'.$get_data['part_number']) }}">
        <div class="card-box bg-info" id="cost">
            @if($get_data['get_notif'] > 0)
             <span class="notif" style="width: 8%;">{{ $get_data['get_notif'] }}</span>
             @endif
             <div class="inner">
                <h3 style="color: white">{{ $get_data['part_name'] }}</h3>
                <h4 style="color: white">{{ $get_data['get_issue'] }} Open R.I.O</h4>
            </div>
            <div class="icon">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
          
        </div>
        </a>
    </div>

@endforeach
    
  </div>
</div>
@endsection