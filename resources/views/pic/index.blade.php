@extends('layouts/root')
@section('main')
<div class="col-lg-12" style="margin-bottom: 10px;">
	<div class="row">
		<div class="col-lg-3">
			<a href="{{ url('pic/open_rio') }}" class="btn btn-primary" style="width: 100%">OPEN RIO <button class="btn btn-danger">{{ $count_rio_before }}</button></a><span class="notif" style="padding-left: 9px;padding-top: 3px;right: 4px;">{{ $get_notif_issue }}</span>
		</div>
		<div class="col-lg-3">
			<a href="{{ url('pic/open_item_control') }}" class="btn btn-primary" style="width: 100%">OPEN ITEM CONTROL <button class="btn btn-danger">{{ $count_item_not_complate }}</button></a> <span class="notif" style="padding-left: 9px;padding-top: 3px;right: 4px;">{{ $get_notif_item }}</span>
		</div>
		<div class="col-lg-6">
			<a class="btn btn-info my_category" style="width: 100%">MY TOTAL CATEGORY CONTROL <button class="btn btn-danger">{{ count($my_category) }}</button></a>
		</div>
	</div>
</div>
<div class="col-lg-12"  style="margin-bottom: 80px;">
	<div class="row">
		<div class="col-lg-3">
			<a href="{{ url('pic/open_rio_finish') }}" class="btn btn-success" style="width: 100%">RIO FINISH <button class="btn btn-danger">{{ $count_issue_complate }}</button></a>
		</div>
		<div class="col-lg-3">
			<a href="{{ url('pic/open_item_control_finish') }}" class="btn btn-success" style="width: 100%">ITEM CONTROL FINISH <button class="btn btn-danger">{{ $count_item_complate }}</button></a>
		</div>
		<div class="col-lg-3">
			<a class="btn btn-info my_product" style="width: 100%">MY TOTAL PRODUCT <button class="btn btn-danger">{{ $count_my_product }}</button></a>
		</div>
		<div class="col-lg-3">
			<a class="btn btn-info my_project" style="width: 100%">MY TOTAL PROJECT <button class="btn btn-danger">{{ $count_my_project }}</button></a>
		</div>
	</div>
</div>
</div>

<h3>Quick Access Item Control</h3>
<hr><br><br>
<form action="{{ url('pic') }}" method="GET">
		<div class="row">
			<div class="col-lg-3">
			<label>Project</label>
			<select name="id_project" class="form-controll" id="select_project">
				<option>--Choise--</option>
				@foreach($select_projects as $select_projects)
				<option value="{{ $select_projects->id_project }}">{{ $select_projects->project_name }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-lg-3">
			<label>Product</label>
			<select name="part_number" class="form-controll" id="get_product" disabled>
				<option>--Choise--</option>
			</select>
		</div>
		<div class="col-lg-3">
			<label>Category</label>
			<select name="id_category" class="form-controll" id="get_category_product" disabled>
				<option>--Choise--</option>
			</select>
		</div>
			<div class="col-lg-1">
				<label>&nbsp</label>
				<a href="{{ url('pic') }}" class="btn btn-secondary" style="width: 100%">RESET</a>
			</div>
		<div class="col-lg-2">
				<label>&nbsp</label>
				<button href="" class="btn btn-warning" style="width: 100%">CARI PRODUCT</button>
			</div>
		</div>
</form>
@if($get_item !=null)
<div class="row">
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">ITEM CONTROL</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
      
       
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th>LAST UPDATE</th>
              <th>ITEM CONTROL</th>
              <th>PIC</th>
              <th>PLAN START</th>
              <th>PLAN DURATION</th>
              <th>PLAN FINISH</th>
              <th>PLAN PROGRESS</th>
              <th>ACTUAL START</th>
              <th>ACTUAL DURATION</th>
              <th>ACTUAL FINISH</th>
              <th>ACTUAL PROGRESS</th>
              <th>LAMPIRAN</th>
              <th>OPEN R.I.O</th>
              <th>STATUS</th>
              <th>REMINDER</th>
              <th>EDIT</th>
              <th>HAPUS</th>
              <th>NOTE</th>
            </tr>
          	</thead>
           <tbody>
           @foreach($get_item as $value)
            <tr>
              <td>{{ $value->last_update_item }}</td>
              <td>@if($value->item) {{ $value->item->item_name }} @endif</td>
              <td>{{ $value->pic_item }}</td>
              <td>{{ $value->plan_start_item }}</td>
              <td>{{ $value->plan_duration_item }}</td>
              <td>{{ $value->plan_finish_item }}</td>
              <td>{{ $value->plan_progress_item }}</td>
              <td>{{ $value->actual_start_item }}</td>
              <td>{{ $value->actual_duration_item }}</td>
              <td>{{ $value->actual_finish_item }}</td>
              <td>{{ $value->actual_progress_item }}</td>
              <td>
                @if(!empty($value->bukti_item))
                <a href="" class="btn btn-primary">
                  <i data-feather="download"></i>
                  Download
                </a>
                @else
                 <button class="btn btn-primary" disabled>
                  <i data-feather="download"></i>
                  Download
                </button>
                @endif
              </td>
              <td><a href="{{ url('project/issue/'.$value->id_project.'/'.$value->part_number.'/'.$value->id_category.'/'.$value->id_item.'/'.'1') }}" class="btn btn-danger btn-sm">R.I.O</a></td>
              <td>
                 <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
              </td>
              <td></td>
              <td>
                <a href="{{ url('project/edit_item_control/'.$value->id_project.'/'.$value->id_category.'/'.$value->part_number.'/'.'1'.'/'.$value->id_item) }}" class="btn btn-info btn-sm">
                  <i data-feather="edit"></i>
                  Edit
                </a>
               
              </td>
              <td>
                 <a href="{{ url('project/delete_item_control/'.$value->id_item) }}" class="btn btn-danger btn-sm">
                  <i data-feather="trash"></i>
                  Hapus
                </a>
              </td>
              <td>
                <button href="" class="btn btn-primary note btn-sm">
                  <i data-feather="edit"></i>
                  Note
                </button>
              </td>
            </tr>
           @endforeach
           </tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>
</div>
@endif

@endsection
@include('pic.modal.modal_product')
@include('pic.modal.modal_project')
@include('pic.modal.modal_category')
@push('script')
<script type="text/javascript">
$('.my_project').click(function(){
    $('#modal_my_project').modal('show');
    $('.modal-title').html('My project');
});
$('.my_product').click(function(){
    $('#modal_my_product').modal('show');
    $('.modal-title').html('My product');
});
$('.my_category').click(function(){
    $('#modal_category').modal('show');
    $('.modal-title').html('My category');
});
 $('#select_project').change(function(){
            var i = $(this);
            var id = i.val();
            $.ajax({
                url: "pic/get_product_pic/"+id,
                success: function( response ) {
                    console.log(response);
                    document.getElementById('get_product').disabled = false;
                        $.each(response, function (part_number, part_name) {
                          $('#get_product').append(new Option(part_number, part_name))
                        })
                      }
                  });
               
              });
 $('#get_product').change(function(){
            var i = $(this);
            var id = i.val();
            $.ajax({
                url: "pic/get_category_product/"+id,
                success: function( response ) {
                    console.log(response);
                    document.getElementById('get_category_product').disabled = false;
                        $.each(response, function (id_category, part_number) {
                          $('#get_category_product').append(new Option(id_category, part_number))
                        })
                      }
                  });
               
              });

</script>
  <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
               "scrollX": true,
              
                "scrollCollapse": true,
        
       });
        
        });
        </script>
@endpush