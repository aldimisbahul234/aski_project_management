<div class="modal fade text-left" id="modal_my_product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      @foreach($get_my_product as $get_my_product)
      <h3>{{ $get_my_product->part_name }}</h3>
      @endforeach
    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-danger btn-modal-delete">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</div>
</div>