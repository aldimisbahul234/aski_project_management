<div class="modal fade text-left" id="modal_my_project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      @foreach($my_project as $my_project)
        <h3>{{ $my_project->project_name }}</h3>
      @endforeach
    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-danger btn-modal-delete">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</div>
</div>