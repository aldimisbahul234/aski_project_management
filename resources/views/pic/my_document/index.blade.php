@extends('layouts/root')
@section('main')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<table class="datatables-basic table" id="datatable">
				<thead>
					<tr>
					  <th style="text-align: center;">NO</th>
					  <th style="text-align: center;">DOCUMENT NAME</th>
					  <th style="text-align: center;">PROJECT</th>
					  <th style="text-align: center;">PRODUCT</th>
					  <th style="text-align: center;">STATUS DOCUMENT</th>
						<th style="text-align: center;">ACTION</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $value)
          @php
          $product = App\Http\Models\Masterdata\Product::where(['id_project'=>$value->id_project, 'part_number'=>$value->part_number])->first();
          $project = App\Http\Models\Masterdata\TypeProject::where(['id_project'=>$value->id_project])->first();
          $get_notif =App\Http\Models\Notification::where(['id_project'=>$value->id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'document','document_code'=>$value->document_code])->count();
          @endphp
					<tr>
						<td style="text-align: center;">{{ $loop->iteration }}</td>
						<td style="text-align: center;">{{ $value->document_name }} @if($get_notif > 0) <span class="badge badge-danger">new update</span> @endif</td>
						<td style="text-align: center;"> {{ $project->project_name }}</td>
						<td style="text-align: center;"> {{ $product->part_name }}</td>
						
						<td style="text-align: center;">
							@if($value->status_document =="0")
							<button class="btn btn-danger" style="width: 100%;"> not complate</button>
							@elseif($value->status_document =="1")
							<button class="btn btn-success" style="width: 100%;"> complate</button>
							@else
							<button class="btn btn-warning" style="width: 100%;"> revisi</button>
							@endif 
						</td>
						
					
						<td style="text-align: center;">
                <button class="btn btn-primary show_note" data-id="{{ $value->document_code }}"> show note</button>
              <button class="btn btn-primary upload_document" value="{{ $value->document_name }}" data-id="{{ $value->document_code }}" >upload doc</button>
                @if(!empty($value->filename))
                  <a href="{{ url('project/download_document/'.$value->filename) }}" class="btn btn-primary"><i data-feather="download"></i> download</a>
                @else
                  <button class="btn btn-primary" disabled><i data-feather="download"></i> download</button>
                @endif      
            </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>



@endsection


<div class="modal fade text-left" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="{{ url('pic/upload_document_pic') }}" method="POST" id="addForm" enctype="multipart/form-data"> 
    @csrf
    <div class="modal-body">
	   <span id="body"></span>
	    <label class="form-label">Document <span class="text-danger">*</span></label>
        <input type="file" class="form-control" name="filename" id="filename">

    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-danger btn-modal-delete">Upload</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</form>
  </div>
</div>
</div>
</div>



<div class="modal fade text-left" id="modal_document_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
  <div class="modal-dialog modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Alert</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      
        <div class="modal-body">
           <span id="input_code"></span>
          <span id="get_list_name"></span>
          <hr>
         
         

        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="modal fade text-left" id="modal_document_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
  <div class="modal-dialog modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Alert</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body">
        <span id="get_note"></span>


        </div>
        <div class="modal-footer">
        
          <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
      </div>
   
    </div>
  </div>
</div>
@push('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').dataTable({
         "pageLength": 20,
        "lengthChange": false,
        "paging": true,
        "searching": true,
       "scrollX": true,
      
        "scrollCollapse": true,
	});
	  
});        
  $(".upload_document").click(function(e){
            e.preventDefault();
             var name  = $(this).attr('value');
             var document_code  = $(this).attr('data-id');
           $('#modal_upload').modal('show');
           $('#myModalLabel1').text(name);
            $('#body').html('<input type="hidden" name="document_code" value="'+document_code+'">');
    });
 $('.show_list').click(function(){
                    var document_code = $(this).attr('data-id');
                    $('#input_code').html('<input type="hidden" value="'+document_code+'" name="document_code">');
                    var url = "{{ url('project/get_list_name') }}";
                      $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'document_code': document_code,
                          },
                           success: function(data) {
                            console.log(data);
                         
                               $('#modal_document_list').modal('show');
                               $('.modal-title').html('Check list document');
                               $('#get_list_name').html(data);
                           
                           }
                      }); 
                 
                });
                  $('.show_note').click(function(){
                    var document_code = $(this).attr('data-id');
                    var url = "{{ url('project/get_document_note') }}";
                      $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'document_code': document_code,
                          },
                           success: function(data) {
                            console.log(data);
                         
                               $('#modal_document_note').modal('show');
                               $('.modal-title').html('Note document');
                               $('#get_note').html(data.result.note);
                           
                           }
                      }); 
                 
                });
</script>
@endpush