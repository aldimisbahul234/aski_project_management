@extends('layouts/root')
@section('main')

@include('project.flash.flash')
 <div class="col-lg-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">SETTING PROFILE</h2>
		    			</div>
		  			</div>
				</div>
    		</div>
    		<form action="{{ url('pic/update_my_profile/'.$data->id_pengguna) }}" method="POST">
    			@csrf
      			<div class="card-body"> 
				
			  				<label>Username</label>
			  				<input type="text" name="" class="form_technical" style="width: 100%" value="{{ $data->username }}" disabled>
			  				<label>Password</label>
			  				<input type="password" name="password" class="form_technical" value="{{ $data->password }}" style="width: 100%">
			  				<label>First name</label>
			  				<input type="text" name="firstname" class="form_technical" style="width: 100%" value="{{ $data->firstname }}">
			  				<label>Last name</label>
			  				<input type="text" name="lastname" class="form_technical" style="width: 100%" value="
			  				{{ $data->lastname }}">
			  				<label>Email</label>
			  				<input type="text" name="email" class="form_technical" style="width: 100%" value="{{ $data->email }}">
			  				<label>Telphone</label>
			  				<input type="text" name="telepon" value="{{ $data->telepon }}" class="form_technical" style="width: 100%">
			  				<label>Departement</label>
			  				<select type="text" name="id_departement" class="form_technical" style="width: 100%">
			  					@foreach($departement as $value)
			  					<option value="{{ $value->id_departement }}" {{ $data->id_departement == $value->id_departement ? "selected":"" }}>{{ $value->departement }}</option>
			  					@endforeach
			  				</select>

			  				
      		</div>
      		<div class="card-footer">
      			<button class="btn btn-primary"> Save</button>
			     		
				</form>
      		</div>
    	</div>
	</div>


@endsection