@extends('layouts/root')
@section('main')
<div class="col-lg-12">
  <div class="row">
@foreach($get_data as $get_data)

    <div class="col-lg-4">
        <a href="{{ url('pic/open_item_control_detail_product_finish/'.$get_data['id_project'].'/'.$get_data['part_number']) }}">
        <div class="card-box bg-info" id="cost">
             <div class="inner">
                <h3 style="color: white">{{ $get_data['part_name'] }}</h3>
                <h4 style="color: white">{{ $get_data['count_item'] }} Open item controll</h4>
            </div>
            <div class="icon">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
          
        </div>
        </a>
    </div>

@endforeach
    
  </div>
</div>
@endsection