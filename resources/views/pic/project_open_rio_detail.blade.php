@extends('layouts/root')
@section('main')




 <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
           <center><h4 class="card-title">UPDATE RIO</h4></center><br><br>
       <form class="form-participant" action="{{ url('pic/project_open_rio_update/'.$data->id_issue) }}" method="post" enctype="multipart/form-data">
            @csrf
           <input type="hidden" name="id_project" value="{{ $data->id_project }}">
           <input type="hidden" name="id_item" value="{{ $data->id_item }}">
           <input type="hidden" name="id_category" value="{{ $data->id_category }}">
           <input type="hidden" name="part_number" value="{{ $data->part_number }}">
             <input type="hidden" name="id_process" value="{{ $data->id_process }}">
          <div class="row">
            <div class="col-lg-12">
              <label>SELECT R.I.O</label>
                <select name="category_issue" class="form-control" readonly>
                <option value="Risk" {{ $data->category_issue == 'Risk' ? "selected":"" }}>Risk</option>
                <option value="Issue" {{ $data->category_issue == 'Issue' ? "selected":"" }}>Issue</option>
                <option value="Opportunity" {{ $data->category_issue == 'Opportunity' ? "selected":"" }}>Opportunity</option>
              </select>
            </div>
           <div class="col-lg-12">
               <label>DESCRIPTION</label>
           <textarea class="form-control" required cols="50" rows="5" placeholder="Typing your detail description here..." name="issue" readonly>{{ $data->issue }}</textarea>     
           </div>
            <div class="col-lg-12">
               <label>ANSWER PIC</label>
              <textarea class="form-control" required cols="50" rows="5" placeholder="Typing your detail description here..." name="answer_pic"> {{$get_answer['get_answer']}} </textarea>     
           </div>
             <div class="col-lg-12">
              <label>ITEM CONTROL</label>
                <input type="text" name="item_name" value="{{ $get_item['get_items'] }}" disabled class="form-control">
            </div>
             <div class="col-lg-12">
              <label>PIC</label>
                <input type="text" name="pic" value="{{ $data->pic }}" readonly class="form-control">
            </div>
             <div class="col-lg-12">
              <label>RECIPIENT EMAIL</label>
               <input type="text" name="email_penerima" class="form-control" value="{{ $get_mail['get_email'] }}" readonly>
             
            </div>
                <div class="col-lg-12">
              <label>EMAIL SENDER </label>
               <input type="text" name="email" class="form-control" value="{{ $email->email }}" readonly>
             
            </div>
              <div class="col-lg-12">
              <label>SENDER PASSWORD </label>
               <input type="password" name="password" class="form-control">
             
            </div>
              <div class="col-lg-12">
              <label>ATTACHMENT</label>
               <input type="file" name="bukti" class="form-control">
             
            </div>
             <div class="col-lg-12">
              <label>STATUS ISSUE</label>
             
              <select name="status_rio" class="form-control">
                 <option value="0">UN DONE</option>
                 <option value="1">DONE</option>
                
              </select>
            </div>

            <br><br><br><br>

             <div class="col-lg-12">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>

        </div>
     

        </div>
 </form>
</div>
</div>
@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
            $('.check').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
             $('#pic').change(function(){
              var username = $(this).val();
              var url = "{{ url('project/get_email') }}";
               $.ajax({
                  type: "GET",
                  url: url,
                  data: {
                  'username': username,
                  },
                   success: function(data) {
                    console.log(data);
                 
                       document.getElementById('email').value = data;
                    
                   
                   }
                  }); 
            });
          });
 </script>
 @endpush
 