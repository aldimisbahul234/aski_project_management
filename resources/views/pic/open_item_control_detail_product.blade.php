
@extends('layouts/root')
@section('main')
<!-- @include('project.flash.flash') -->

@if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">ITEM CONTROL</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
      
       
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th>READ</th>
              <th>LAST UPDATE</th>
              <th>ITEM CONTROL</th>
              <th>PIC</th>
              <th>PLAN START</th>
              <th>PLAN DURATION</th>
              <th>PLAN FINISH</th>
              <th>PLAN PROGRESS</th>
              <th>ACTUAL START</th>
              <th>ACTUAL DURATION</th>
              <th>ACTUAL FINISH</th>
              <th>ACTUAL PROGRESS</th>
              <th>LAMPIRAN</th>
              <th>OPEN R.I.O</th>
              <th>STATUS</th>
              <th>REMINDER</th>
              <th>EDIT</th>
              <th>HAPUS</th>
              <th>NOTE</th>
            </tr>
          	</thead>
           <tbody>
           @foreach($data as $value)
           @php
            $get_notif =App\Http\Models\Notification::where(['penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'item','id_item'=>$value->id_item])->count();
            @endphp
            <tr>
              <td>@if($get_notif > 0) <a class="btn btn-primary btn-sm" href="{{ url('project/read_notif/'.'item'.'/'.$value->id_item) }}"> Read</a> @endif</td>
              <td>{{ $value->last_update_item }} @if($get_notif > 0) <span class="badge badge-danger">new update</span> @endif</td>
              <td>@if($value->item) {{ $value->item->item_name }} @endif</td>
              <td>{{ $value->pic_item }}</td>
              <td>{{ $value->plan_start_item }}</td>
              <td>{{ $value->plan_duration_item }} Hari</td>
              <td>{{ $value->plan_finish_item }}</td>
              <td>{{ $value->plan_progress_item }}</td>
              <td>{{ $value->actual_start_item }}</td>
              <td>{{ $value->actual_duration_item }} Hari</td>
              <td>{{ $value->actual_finish_item }}</td>
              <td>{{ $value->actual_progress_item }}</td>
              <td>
                @if(!empty($value->bukti_item))
                <a href="" class="btn btn-primary">
                  <i data-feather="download"></i>
                  Download
                </a>
                @else
                 <button class="btn btn-primary" disabled>
                  <i data-feather="download"></i>
                  Download
                </button>
                @endif
              </td>
              <td><a href="{{ url('project/issue/'.$value->id_project.'/'.$value->part_number.'/'.$value->id_category.'/'.$value->id_item.'/'.'1') }}" class="btn btn-danger btn-sm">R.I.O</a></td>
              <td>
                @if($value->actual_progress_item ==100 ||  $value->actual_progress_item >100)
                 <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                  @else
                   <div class="progress progress-bar-warning">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{$value->actual_progress_item}}%"
                      ></div>
                    </div>
                  @endif
              </td>
              <td></td>
              <td>
                <a href="{{ url('project/edit_item_control/'.$value->id_project.'/'.$value->id_category.'/'.$value->part_number.'/'.'1'.'/'.$value->id_item) }}" class="btn btn-info btn-sm">
                  <i data-feather="edit"></i>
                  Edit
                </a>
               
              </td>
              <td>
                 <a href="{{ url('project/delete_item_control/'.$value->id_item) }}" class="btn btn-danger btn-sm">
                  <i data-feather="trash"></i>
                  Hapus
                </a>
              </td>
              <td>
                <button href="" class="btn btn-primary note btn-sm">
                  <i data-feather="edit"></i>
                  Note
                </button>
              </td>
            </tr>
           @endforeach
           </tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>

@endsection
@include('project.leader.modal_note')
@push('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
               "scrollX": true,
              
                "scrollCollapse": true,
        
       
        
        });
          });
           $('.note').click(function(){
               swal({
                      title: 'Cancelled',
                      text: 'Your product is safe :)',
                      type: 'error',
                      confirmButtonClass: "btn btn-info",
                      buttonsStyling: false
                    })
          });
    </script>
@endpush