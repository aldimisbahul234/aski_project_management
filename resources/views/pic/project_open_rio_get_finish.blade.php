@extends('layouts/root')
@section('main')
@if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="col-lg-12">
		<div class="card">
			
			
			
			<div class="card-body">
				<h3 style="text-align: center;">OPEN R.I.O (Risk/Issue/Opportunity)</h3>
					
				   <table class="table table-striped table-hover nowrap struckture" id="datatable" style="width: 150%">
                 		<thead> 
		                  <tr>
		                    <th>NO</th>
		                    <th>ISSUE</th>
		                  	 <th>PART NUMBER</th>
                         <th>STATUS RIO</th>
                         <th>LAMPIRAN</th>
                         <th>UPDATE</th>
		                  </tr>
                  		</thead>
                 		<tbody>
                 		@foreach($data as $value)
                 		<tr>
                 			<td>{{ $loop->iteration }}</td>
                 			<td>{{ $value->issue }}</td>
                 			<td>{{ $value->part_number }}</td>
                      <td>   
                        <a  class="btn btn-success" style="width: 62%"><i class="fa fa-check" aria-hidden="true"></i> DONE</a>  
                      </td>
                      <td>
                         @if(!empty($value->bukti) AND $value->bukti !="kosong")
                        <a href="{{ url('pic/download_document_issue/'.$value->bukti) }}" class="btn btn-primary"><i data-feather="download"></i> download</a>
                        @else
                        <button class="btn btn-primary" disabled><i data-feather="download"></i> download(No available)</button>
                        @endif
                      </td>
                            <td>
                                <button disabled class="btn btn-primary btn-sm"> update</button>
                            </td>
                 		</tr>
                 		@endforeach
                 		</tbody>
               		</table>
			</div>
		</div>
</div>

@endsection
@push('script')
<script type="text/javascript">
	   $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                  "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,  
                
            });
          });
</script>
@endpush

