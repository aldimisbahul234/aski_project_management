
@extends('layouts/root')
@section('main')

@include('project.flash.flash')
@if(Auth::user()->hak_akses =="project_manager")
 <div class="col-12">     
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#default">
  <i class="fa fa-plus"></i>ADD NEW PROJECT 
</button><br><br><br>
</div>
@endif
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">TYPE PROJECT ( Customer {{$customer->customer_name}} )</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th width="10%">PROJECT NAME</th>
              <th width="5%">TOTAL ITEM</th>
              <th width="15%">NEXT EVENT</th>
              <th width="5%">PROJECT MANAGER</th>
              <th width="5%">PLAN PROGRES</th>
              <th width="5%">ACTUAL PROGRES</th>
              <th width="40%">STATUS</th>
              <th width="5%">LAST UPDATE</th>
              <th width="5%">REPORT</th>
              <th width="5%">PROJECT DETAIL</th>
            </tr>
          	</thead>
           <tbody>
           	@foreach($data as $value)
            @php
                $project_leader = App\Http\Models\Masterdata\MemberProject::where(['id_project'=>$value['id'],'id_position'=>1])->first();
                if($project_leader ==true){
                $getname = App\User::where('username',$project_leader->username)->first();
                }else{
                 $getname = false;
                }
                
                $event = DB::table('event_project')->leftjoin('category_event_project','category_event_project.id_category_event_project','event_project.id_category_event_project')->where('event_project.id_project',$value['id'])->where('jadwal_event','>=',$dateNow)->first();
                if($event ==true){
                
                $selisih = Carbon\Carbon::now()->startOfDay()->diffInDays($event->jadwal_event, false);
              }else{
               $selisih = 0;
            }
            @endphp
           
    	       	<tr>
    	       		<td>{{ $value['project_name'] }}</td>
    	       		<td>{{ $value['count_part'] }}</td>
                <td>
                  <div class="blink">
                    <a href="" class="btn btn-warning">
                      @if($selisih ==0) Today 
                      @elseif($dateNow > $event->jadwal_event) Until Now 
                      @else {{ $event->event_project_name }} {{ $selisih }} Day Left 
                      @endif
                    </a>
                  </div>
                </td>
                <td>@if($getname ==true) {{ $getname->firstname }} {{ $getname->lastname }} @else - @endif</td>
                <td>
                    @if($value['result_plan'] > 100) 100% 
                    @else {{ $value['result_plan'] }}% 
                    @endif
                </td>
                <td>@if($value['result_actual'] > 100) 100% 
                    @else {{ $value['result_actual'] }}% 
                    @endif
                </td>
                <td>
                  <a href="{{ url('project/product_detail/'.$value['id'].'/'.$value['customer']) }}">
                    <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                  </a>
                </td>
               <td></td>
               <td><a href="{{ url('project/project_detail_gantt') }}" class="btn btn-success btn-sm"><i class="fa fa-chart-bar fa-1x"></i></a></td>
               <td><a href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$value['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye fa-1x"></i></a></td>
    	       	</tr>
			       @endforeach
           	</tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>
@include('project.leader.modal_create_project')
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
            });
          });
    </script>
@endpush