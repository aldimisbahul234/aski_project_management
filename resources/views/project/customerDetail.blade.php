
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')

	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">TYPE PROJECT ( Customer {{$customer->customer_name}} )</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	<tr>
              <th colspan="2" style="text-align: center;background-color: #00BCD4;" class="text-white">PROJECT</th>
              <th colspan="{{ count($process) }}" style="text-align: center;background-color: #009688" class="text-white">PROCESS</th>
              <th colspan="4" style="text-align: center;background-color: #00BCD4" class="text-white">ACTION</th>
            
            </tr>
            <tr>
              <th>PROJECT NAME</th>
              <th>TOTAL PART</th>
              @foreach($process as $process)
              		<th style="text-align: center;background-color: {{$process->box_color}};color: black">{{$process->process_name}}</th>
              @endforeach
              <th>REPORT</th>
              <th>ADD PART</th>
              <th>EDIT</th>
              <th>DELETE</th>
            </tr>
          </thead>
           <tbody>

           	@foreach($data as $value)

           	<tr>
           		<td style="text-align: center;">{{ $value['project_name'] }}</td>
           		<td style="text-align: center;">{{ $value['count_part'] }}</td>
           		@foreach($proc as $values)
           		@php
           		$getProc = App\Http\Controllers\Controller::getProcess($value['id'],$values->id_process);
              $getProcId = App\Http\Controllers\Controller::getProcessId($value['id'],$values->id_process);
           		@endphp
           		<td style="text-align: center;">
                @if($getProcId ==true)
                <a href="{{ url('project/export_process/'.$header->id_product.'/'.$value['id'].'/'.$getProcId)}}" class="btn btn-success btn-sm">
                @else 
                <a href="" class="btn btn-success btn-sm"> @endif {{ $getProc }}</a></td>
           		@endforeach
           		<td style="text-align: center;"><a class="btn btn-success btn-sm" href="{{ url('project/export_all/'.$header->id_product.'/'.$value['id']) }}"><i class="fa fa-book"></i></a></td>
           		<td style="text-align: center;"><a class="btn btn-primary btn-sm" href="{{ url('project/add_part/'. $value['id']) }}"><i class="fa fa-plus"></i></a></td>
           		<td style="text-align: center;"><a class="btn btn-secondary btn-sm" href="{{ url('project/edit_project/'.$header->id_product.'/'.$customer->id_customer.'/'.$value['id']) }}"><i class="fa fa-pen"></i></a></td>
           		<td style="text-align: center;"><a class="btn btn-danger btn-sm" href=""><i class="fa fa-trash"></i></a></td>

           	</tr>
@endforeach
           	</tbody>
        </table>
 </div>
    	</div>

      </div>
    </div>
</div>
@include('project.modal_create')
@endsection
@push('script')
<script src="../../../app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../../../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
               
        
       
        
        });
          });
    </script>
@endpush