
@extends('layouts/root')
@section('main')
 <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">{{$header->product_name}}</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')

 <div class="col-lg-12">
@if(Auth::user()->hak_akses =="project_manager")
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#default">
  <i class="fa fa-plus"></i>ADD NEW CUSTOMER 
</button><br><br><br>
@endif
</div>
 <div class="col-lg-12">
 <div class="row">
@foreach($data as $value)
    <div class="col-lg-6">
      <a href="{{url('project/customerDetail/'.$value['id_customer'].'/'.$header->id_product)}}">
      <div class="card bg-success text-white">
        @if($value['notif'] > 0)
           
          <span class="notif" style="padding-left: 15px;">{{ $value['notif'] }}</span>
          @endif
        <div class="card-body customers">
            <div class="row">
              <div class="col-lg-2">
                <i class="fa fa-user-circle fa-5x"></i>
              </div>
              <div class="col-lg-6">
                <h2 class="text-white">{{$value['customer']}}</h2>
                <p class="card-text">{{ $value['project'] }} PROJECT</p>
              </div>
            </div>
        </div>
      </div>
</a>
    </div>
@endforeach
  </div>
</div>
@include('project.modal_create_customer')

@endsection
@push('script')
 
@endpush