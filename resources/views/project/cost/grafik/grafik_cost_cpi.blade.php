<script type="text/javascript">
Highcharts.chart('cpi', {
    chart: {
        type: 'bar'
    },
    title: {
        text: '<b>Grafik Cost Performance Index Project Budget'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories: ['tes']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Budget Product'
        }
    },
    legend: {
        reversed: true
    },
    tooltip: {
        valueSuffix: ' Millions'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                
            }
        }
    },
    series: [{
        name: 'ACTUAL BUDGET',
        color: '#52b251',
        data: [0]
    }, {
        name: 'PLAN BUDGET',
        color: '#73a2ff',
        data: [0]
    }]
});

Highcharts.chart('plus_minus', {
    chart: {
        type: 'column'
    },
    yAxis: {
        title: {
            text: 'Percentage (%)'
        }
    },
    credits:{
        enabled : false
    },
    title: {
        text: '<b>Grafik Cost Performance Index COGM'
    },
    tooltip: {
        valueSuffix: '%'
    },
     plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },
    xAxis: {
        categories: ['tes']
    },
    credits: {
        enabled: false
    },
    series: [
        {
        name: 'tes',
        data: [0]
    },
    ]
});
		</script>