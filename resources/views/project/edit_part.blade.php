
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
                <li class="breadcrumb-item"><a href="">Type Project {{$project->project_name}}</a></li>
                <li class="breadcrumb-item"><a href="">Data</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
 
  <div class="col-12">
    <div class="card">
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
    	  <table class="datatables-basic table" id="datatable">
          <thead>
            <tr>
             
              <th style="text-align: center;">NO</th>
              <th style="text-align: center;">PART NUMBER</th>
              <th style="text-align: center;">PART NAME</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
           <tbody>
          @foreach($data as $value)
            <tr>
              <td style="text-align: center;">{{ $loop->iteration }}</td>
              <td style="text-align: center;">{{ $value->part_number }}</td>
              <td style="text-align: center;">{{ $value->part_name }}</td>
              <td style="text-align: center;">
              
                    <a class="btn" href="{{url('project/edit_part_list/'.$header->id_product.'/'.$project->id_project.'/'.$value->part_number)}}">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="btn" href="{{url('project/edit_part_list/'.$value->part_number)}}">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  
              </td>
            </tr>
          @endforeach
         </tbody>
        </table>
    </div>
    	</div>

      </div>
    </div>
</div>


@endsection
@push('script')
    <script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/jszip.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 dom: 'Bfrtip',
   
                   
              buttons: [
        {
          extend: 'collection',
          className: 'btn btn-outline-secondary dropdown-toggle mr-2',
          text: feather.icons['share'].toSvg({ class: 'font-small-4 mr-50' }) + 'Export',
          buttons: [
            {
              extend: 'print',
              text: feather.icons['printer'].toSvg({ class: 'font-small-4 mr-50' }) + 'Print',
              className: 'dropdown-item',
             
            },
            {
              extend: 'csv',
              text: feather.icons['file-text'].toSvg({ class: 'font-small-4 mr-50' }) + 'Csv',
              className: 'dropdown-item',
              
            },
            {
              extend: 'excel',
              text: feather.icons['file'].toSvg({ class: 'font-small-4 mr-50' }) + 'Excel',
              className: 'dropdown-item',
              
            },
            {
              extend: 'pdf',
              text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 mr-50' }) + 'Pdf',
              className: 'dropdown-item',
              
            },
            {
              extend: 'copy',
              text: feather.icons['copy'].toSvg({ class: 'font-small-4 mr-50' }) + 'Copy',
              className: 'dropdown-item',
              
            }
          ],
          init: function (api, node, config) {
            $(node).removeClass('btn-secondary');
            $(node).parent().removeClass('btn-group');
            setTimeout(function () {
              $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
            }, 50);
          }
        },
        {
          text: feather.icons['plus'].toSvg({ class: 'mr-50 font-small-4' }) + 'Add New Record',
          className: 'create-new btn btn-primary',
          attr: {
            'data-toggle': 'modal',
            'data-target': '#default'
          },
          init: function (api, node, config) {
            $(node).removeClass('btn-secondary');
          }
        }
      ],
        });
          });
    </script>
@endpush