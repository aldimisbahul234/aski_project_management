@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

    
<div class="col-lg-12">
	<div class="card">

  		<div class="card-header">
     		<div class="col-lg-12">
            	<div class="card" style="background-color: rgba(115,103,240,.7)">
              		<div class="card-body">
                  		<h2 style="text-align: center;" class="text-white">ADD PART DIE GO {{ $project->project_name }}</h2>
              		</div>
            	</div>
        	</div>
  		</div>
  	<div class="card-body">
 					<form class="form-participant" action="{{ url('product') }}" method="post" enctype="multipart/form-data">
                           @csrf
                           <input type="hidden" name="id_project" value="{{ $project->id_project }}">
                            	
                            	<div class="form-group">
			                      <label class="form-label" for="basic-addon-name">PART NUMBER</label>
			                      <input
			                        type="text"
			                        id="basic-addon-name"
			                        class="form-control"
			                        placeholder="PART NUMBER"
			                        aria-label="Name"
			                        aria-describedby="basic-addon-name"
			                        name="part_number" 
			                        required
			                      />
			                    </div>

			                    <div class="form-group">
			                      <label class="form-label" for="basic-addon-name">ADD PART</label>
			                      <input
			                        type="text"
			                        id="basic-addon-name"
			                        class="form-control"
			                        placeholder="ADD PART"
			                        aria-label="Name"
			                        aria-describedby="basic-addon-name"
			                        name="part_name"
			                        required
			                      />
			                    </div>

                         <div class="form-group">
                      		<button type="submit" class="btn btn-primary">Submit</button>
                    	</div>
                      </form>

		</div>
	</div>
</div>


@endsection