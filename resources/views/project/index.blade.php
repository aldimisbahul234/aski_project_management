
@extends('layouts/root')
@section('main')
@push('style')
<style type="text/css">
 
</style>
@endpush
@include('project.header.header')
@include('project.flash.flash')
<div class="col-lg-12">
@if(Auth::user()->hak_akses =="project_manager")
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#default">
  <i class="fa fa-plus"></i>ADD NEW SECTOR
</button>
@endif
</div><br><br><br>
 <div class="col-lg-12">
 


 <div class="row">
@foreach($sector as $value)
    <div class="col-lg-6">

<a href="{{url('project/customer/'.$value['id_product'])}}">
      <div class="card {{ $value['color'] }} text-white ">
     @if($value['notif'] > 0)
           <span class="notif" >{{ $value['notif'] }}</span>
      @endif
         
        <div class="card-body sectors">

            <div class="row">
              <div class="col-lg-3">
                <i class="fa fa-{{ $value['logo'] }} fa-5x"></i>
              </div>
              <div class="col-lg-6">
                <h2 class="text-white">{{$value['sector']}}</h2>
                <p class="card-text">{{ $value['customer'] }} CUSTOMER</p>
              </div>
            </div>
        </div>
      </div>
        </a>
    </div>
@endforeach
  </div>
</div>
@include('project.modal_create')

@endsection
@push('script')

@endpush

