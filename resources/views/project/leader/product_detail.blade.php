
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')

	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">PRODUCT DETAIL {{ $project->project_name }}</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th width="5%">NO</th>
              <th width="5%">PART NUMBER</th>
              <th width="5%">PART NAME</th>
              <th width="5%">PROJECT LEADER</th>
              <th width="5%">PLAN PROGRES</th>
              <th width="5%">ACTUAL PROGRES</th>
              <th width="5%">PROCESS</th>
              <th width="5%">OPEN R.I.O</th>
              <th width="15%">STATUS</th>
              <th width="15%">REPORT</th>
            </tr>
          	</thead>
           <tbody>
           	@foreach($product as $value)
            @php
            $countactual = 0;
            $countplan = 0;
            $project_leader = null;
            $get_notif =App\Http\Models\Notification::where(['id_project'=>$projectID,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->count();

              $get_project_leader = App\Http\Models\Masterdata\MemberProject::where('id_project',$project->id_project)->where('part_number',$value->part_number)->where('id_position',2)->first();
              if($get_project_leader ==true){
              $project_leader = App\User::where('username',$get_project_leader->username)->first();
              }
              $process = App\Http\Models\Masterdata\Process::where('id_process',$value->id_process)->first();
              
              $itemcontrolCount = App\Http\Models\Masterdata\ItemControl::where('part_number',$value->part_number)->count();
        
              $sum_plan = App\Http\Models\Masterdata\ItemControl::where('part_number',$value->part_number)->where('status_item',1)->sum('plan_progress_item');
              $sum_actual = App\Http\Models\Masterdata\ItemControl::where('part_number',$value->part_number)->where('status_item',1)->sum('actual_progress_item');


              if($sum_plan > 0){
               $countplan = round($sum_plan / $itemcontrolCount);
            }
             

            
              if($sum_actual > 0){
              $countactual = round($sum_actual / $itemcontrolCount);
            }
            @endphp
    	       	<tr>
    	       		<td>{{ $loop->iteration }}</td>
    	       		<td>{{ $value->part_number }}</td>
                <td>{{ $value->part_name }}</td>
                <td>@if(!empty($project_leader)) {{ $project_leader->firstname }} @endif</td>
                <td>
                  @if($itemcontrolCount ==0)
                  0%
                  @else
                    @if($countplan > 100)
                      100%
                    @else
                    {{ $countplan }} %
                    @endif
                  @endif
                </td>
                <td>
                   @if($itemcontrolCount ==0)
                  0%
                  @else
                    @if($countactual > 100)
                      100%
                    @else
                    {{ $countactual }} %
                    @endif
                  @endif
                </td>
                <td>{{ $process->process_name }}</td>
               <td><a href="{{ url('project/issue/'.$project->id_project.'/'.$value->part_number.'/'.'0'.'/'.'0'.'/'.'0') }}" class="btn btn-danger btn-sm">R.I.O 
                @if($get_notif > 0)
                <span class="badge badge-info">{{ $get_notif }}</span></a>
                @endif
                </td>
               <td> 
             
                @if($countactual > 100 || $countactual ==100)
                <a href="{{ url('project/product_category_control/'.$value->part_number.'/'.$value->id_process.'/'.$project->id_project) }}">
                  <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                  </a>
                  @else
                   <a href="{{ url('project/product_category_control/'.$value->part_number.'/'.$value->id_process.'/'.$project->id_project) }}">
                  <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{$countactual}}%"
                      ></div>
                    </div>
                  </a>
                  @endif
                
                </td>
               <td>
                <div class="row">
                  <div class="col-lg-6">
                     <a class="btn btn-primary btn-sm" href="{{ url('gant_chart_project/'.$projectID.'/'.$value->part_number) }}"><i data-feather="file"></i> Grafik</a>
                  </div>
                  <div class="col-lg-6">
                      <a class="btn btn-primary btn-sm" href="{{ url('project/download_excel_product/'.$project->id_project.'/'.$value->part_number) }}"><i data-feather="file"></i> Excel</a>
                  </div>
                </div>
               
               
               </td>
    	       	</tr>
			       @endforeach
           	</tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>
@include('project.modal_create')
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,   
            });
          });
    </script>
@endpush