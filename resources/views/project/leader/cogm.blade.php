@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-sm-12">
      <div class="card collapse-icon">
        <div class="card-header">

        </div>
        <div class="card-body">
         <h2 style="text-align: center;">COGM</h2>
       
         <button class="btn btn-info" disabled><i class="fa fa-plus fx-5"></i>ADD COGM (USER COST ONLY)</button><br><br>
          @foreach($getData as $key=>$value)
          <div class="collapse-default">
            <div class="card" style="background-color: orange">
              <div
                id="headingCollapse1"
                class="card-header"
                data-toggle="collapse"
                role="button"
                data-target="#collapse_{{ $key }}"
                aria-expanded="false"
                aria-controls="collapse1"
              >
                <span class="lead collapse-title text-white"> {{ $value['product'] }} </span>
              </div>

              <div id="collapse_{{ $key }}" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                <div class="card-body" style="background-color: white">
                  
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table table-striped table-hover nowrap index_{{ $key }}" id="datatable">
                      <thead>
                      <tr>

                      <th>EVENT COGM</th>
                      <th>BOM COST</th>
                      <th>PROCESS COST</th>
                      <th>DEPRECATION COST</th>
                      <th>TOTAL COST</th>
                      <th>ACTION</th>
                      </tr>
                      </thead>
                      <tbody>
                      
                          <tr>

                            <td>{{ $value['detail']['event'] }}</td>
                            <td>{{ $value['detail']['bom'] }}</td>
                            <td>{{ $value['detail']['process']}}</td>
                            <td>{{ $value['detail']['depreciation']}}</td>
                            <td>{{ $value['detail']['total'] }}</td>
                            <td>
                              <a href="{{ url('cost/edit/'.$value['id_cogm']) }}" class="btn btn-info btn-sm"><i data-feather="edit"></i>EDIT</a>
                              <a href="{{ url('cost/hapus/'.$value['id_cogm']) }}" class="btn btn-danger btn-sm"><i data-feather="trash"></i>HAPUS</a>
                            </td>
                          </tr>

                      </tbody>
                      </table>
                  </div>
                </div>
              </div>

                </div>
              </div>
            </div>
            @endforeach


          </div>
        </div>
      </div>
    </div>
    

    <div class="row">
      <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="text-title" style="text-align: center;">COGM (Cost Of Good Manufacture)</h2>

          <div id="grafik_cogm"></div>
          <div id="grafik_cogm2"></div>

        </div>
      </div>
    </div>
    </div>
		
	
@endsection
@push('script')
@include('project.leader.grafik.grafik_project_cogm')
@endpush

