<div class="modal fade text-left modal_documents" id="modal_document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog modal-dialog modal-lg" role="document">
 <form  method="post" enctype="multipart/form-data">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     @csrf
   <input type="hidden" name="part_number" value="{{ $part_number }}">
   <input type="hidden" name="id_project" value="{{ $id_project }}">
   <input type="hidden" name="id_category_event_project" value="{{ $id_category_event_project }}">
   <input type="hidden" name="id_process" value="1">
   <span id="document_code"></span>
   <div class="row">
      <div class="col-lg-12">
        <label>Document name</label>
        <input type="text" name="document_name" id="document_name" class="form_technical" style="width: 100%">
      </div>
      <!--  <div class="col-lg-12">
        <label>List document must be upload</label>
        <div class="row">
          <div class="col-lg-11">
            <input type="text" name="list_name[]" class="form_technical" style="width: 100%" placeholder="List name">
          </div>
          <div class="col-lg-1">
            <button class="btn btn-primary add_list" type="button">+</button>
          </div>
        </div>
         <span id="form_list"></span>
      </div> -->
      <div class="col-lg-12">
        <label>Pic</label>
        <select class="form_technical" name="pic" id="pic">
          @foreach($member as $member)
          <option value="{{ $member->username }}">{{ $member->username }}</option>
          @endforeach
        </select>
      </div>
      <div class="col-lg-12">
        <label>Email</label>
        <input type="email" name="email" class="form_technical" style="width: 100%" id="email">
      </div>
      <div class="col-lg-12">
        <label>Password</label>
        <input type="password" name="" class="form_technical" style="width: 100%">
      </div>
      <div class="col-lg-12">
        <label>Upload</label>
        <input type="file" name="filename" class="form_technical" style="width: 100%">
      </div>
   </div>

    <div class="modal-footer">
       <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</form>
</div>
</div>