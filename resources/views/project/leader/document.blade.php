@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
	<div class="card">
	<div class="card-body">
    <a href="{{ url('project/display_project_detail/'.$project->id_customer.'/'.$id_project) }}" class="btn btn-danger"> back</a>
		<table class="table document" id="datatable">
                 <thead>
                  
                  <tr>
                    <th style="text-align: center;">NO</th>
                    <th style="text-align: center;">EVENT</th>
                    <th style="text-align: center;">ADD DOCUMENT</th>
                   
                  </tr>
                  </thead>
                 <tbody>
                	@foreach($get_event as $get_event)
                	<tr>
                		<td style="text-align: center;">{{ $loop->iteration }}</td>
                		<td style="text-align: center;">{{ $get_event['event'] }}</td>
                		
                			<td style="text-align: center;">
                    
                    <a href="{{ url('project/list_document/'.$get_event['id_project'].'/'.$get_event['part_number'].'/'.$get_event['id_category_event_project']) }}" class="btn btn-primary">Add Document</a> 
                		</td>
                		
                	</tr>
                	@endforeach
                 </tbody>
               </table>
	</div>
</div>
</div>


@endsection
@push('script')
<script type="text/javascript">
	 $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });
</script>
@endpush