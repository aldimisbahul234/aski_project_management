@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-lg-11">
					<h3 style="text-align: center">UPDATE FORM APPROVAL LINE {{ $event->event_project_name }}</h3>
				</div>
				<div class="col-lg-1">
					<a href="{{ url('project/display_project_detail/'.$project->id_customer.'/'.$project->id_project) }}" class="btn btn-danger"> Back</a>
				</div>
			</div>
			
			
			<h3>ASPECT</h3><hr>
			 <form class="form-participant" action="{{ url('project/update_approval_lines/'.$check->id_approval_line) }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_project" value="{{ $check->id_project }}">
             <input type="hidden" name="part_number" value="{{ $check->part_number }}">
              <input type="hidden" name="id_category_event_project" value="{{ $check->id_category_event_project }}">
			<h5>1. Incoming & Suplier Development (2)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done1" min="0" max="2" class="form_technical" value="{{ $check->done1 }}">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject1" min="0" max="2" value="{{ $check->eject1 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang1" min="0" max="2" value="{{ $check->silang1 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>2. Drawing (3)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done2" min="0" max="3" value="{{ $check->done2 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject2" min="0" max="3" value="{{ $check->eject2 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang2" min="0" max="3" value="{{ $check->silang2 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>3. Material & Komponen (3)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done3" min="0" max="3" value="{{ $check->done3 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject3" min="0" max="3" value="{{ $check->eject3 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang3" min="0" max="3" value="{{ $check->silang3 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>4. Man (5)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done4" min="0" max="5" value="{{ $check->done4 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject4" min="0" max="5" value="{{ $check->eject4 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang4" min="0" max="5" value="{{ $check->silang4 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>5. Document (9)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done5" min="0" max="9" value="{{ $check->done5 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject5" min="0" max="9" value="{{ $check->eject5 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang5" min="0" max="9" value="{{ $check->silang5 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>6. Machine (6)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done6" min="0" max="6" value="{{ $check->done6 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject6" min="0" max="6" value="{{ $check->eject6 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang6" min="0" max="6" value="{{ $check->silang6 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>7. Environment (7)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done7" min="0" max="7" value="{{ $check->done7 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject7" min="0" max="7" value="{{ $check-> eject7}}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang7" min="0" max="7" value="{{ $check->silang7 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>8. Inspection (3)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done8" min="0" max="3" value="{{ $check->done8 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject8" min="0" max="3" value="{{ $check->eject8 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang8" min="0" max="3" value="{{ $check->silang8 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>9. Visual (1)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done9" min="0" max="1" value="{{ $check->done9 }}"class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject9" min="0" max="1" value="{{ $check->eject9 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang9" min="0" max="1" value="{{ $check->silang9 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>10. Dimensi (1)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done10" min="0" max="1" value="{{ $check->done10 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject10" min="0" max="1" value="{{ $check->eject10 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang10" min="0" max="1" value="{{ $check->silang10 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>11. Process Performance (2)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done11" min="0" max="2" value="{{ $check->done11 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject11" min="0" max="2" value="{{ $check->eject11 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang11" min="0" max="2" value="{{ $check->silang11 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>12. Material & Komponen incoming (5)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done12" min="0" max="5" value="{{ $check->done12 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject12" min="0" max="5" value="{{ $check->eject12 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang12" min="0" max="5" value="{{ $check->silang12 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>13. Storage FG (5)</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-check fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="done13" min="0" max="5" value="{{ $check->done13 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-angle-double-up fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="eject13" min="0" max="5" value="{{ $check->eject13 }}" class="form_technical">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-window-close fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<input type="number" name="silang13" min="0" max="5" value="{{ $check->silang13 }}" class="form_technical">
				</div>
			</div>
			<br>

			<h5>Attachment FIle Approval Line</h5>
			<div class="row">
				<div class="col-sm-1" style="left: 46px;top: 8px;">
					<i class="fa fa-edit fa-2x"></i>
				</div>
				<div class="col-sm-10">
					<p>{{ $check->attachment }}</p>
					<input type="file" name="attachment" class="form_technical">
				</div>
			</div>
			<br>

			<button class="btn btn-success">SAVE ALL</button>
		</form>
		</div>
	</div>
</div>
@endsection