@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')


 <div class="col-lg-12">
      <div class="card">
     
         
        <div class="card-header">
          <a class="btn btn-danger float-right" href="{{ url('project/display_project_detail/'.$project->id_customer.'/'.$project->id_project) }}"> back</a>
        </div>
        <div class="card-body">
           <center><h4 class="card-title">EDIT MEMBER {{ $data->username }}</h4></center><br><br>
       <form class="form-participant" action="{{ url('project/update_member/'.$data->id_member) }}" method="post" enctype="multipart/form-data">
            @csrf
              <input type="hidden" name="id_project" value="{{ $project->id_project }}">
               <input type="hidden" name="part_number" value="{{ $data->part_number }}">
              <input type="hidden" name="id_process" value="{{ $data->id_process }}">
               <input type="hidden" name="username" value="{{ $data->username }}">
           
              <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="first-name-vertical">POSISTION</label>
                 <select name="id_position" class="form-control">
                   <option>-PILIH POSITION-</option>
                   @foreach($position as $position)
                   <option value="{{ $position->id_position }}" {{ $data->id_position ==$position->id_position ? "selected":"" }}>{{ $position->position_name }}</option>
                   @endforeach
                 </select>
                </div>
              </div>
             </div>

              <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="first-name-vertical">AVAILABILITY (%)</label>
                <input type="text" name="availability" class="form-control" placeholder="Type Here For Availability" value="{{ $data->availability }}">
                </div>
              </div>
             </div>

              <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <center><label><i class="fa fa-plus"></i>STANDAR RATE (Rp)</label></center>
                  <div class="row">
                  <div class="col-6">
                    <input type="text" name="standar_rate" class="form-control" placeholder="Type Here Standar Rate" value="{{ $data->standar_rate }}">
                  </div>
                   <div class="col-6">
                    <select name="duration_rate" class="form-control"> 
                      <option>Select duration rate</option>
                      <option value="days" {{ $data->duration_rate =="days" ? "selected":"" }}>Days</option>
                      <option value="month" {{ $data->duration_rate =="month" ? "selected":"" }}>Month</option>
                      <option value="years" {{ $data->duration_rate =="years" ? "selected":"" }}>Years</option>
                    </select>
                  </div>
                </div>
                </div>
              </div>
             </div>

             <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
             
        </div>
      </div>
    </div>


     
@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
            $('.check').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
 </script>
 @endpush
 