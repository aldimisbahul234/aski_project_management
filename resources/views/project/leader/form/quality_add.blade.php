@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<h3 style="text-align: center">INSERT DATA TRIAL {{ $trial_count }}</h3>
			
			<div class="row">
				<div class="col-sm-12">
					<label>TYPE EVENT</label>
					<input type="text" name="id_category_event_project" value="{{ $event->event_project_name }}" class="form_technical" style="width: 100%" disabled>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<label>PART NUMBER</label>
					<input type="text" name="part_number" value="{{ $product->part_number }}" class="form_technical" style="width: 100%" disabled>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<label>PART NAME</label>
					<input type="text" name="" value="{{ $product->part_name }}" class="form_technical" style="width: 100%" disabled>
				</div>
			</div>

			<div class="row">
				
				<div class="col-sm-6">
					<div class="card" style="background: rgb(0 20 255 / 39%);">
						<div class="card-body">
							<form action="{{ url('project/post_quality') }}" method="post" enctype="multipart/form-data">
								@csrf
							<input type="hidden" name="type" value="save_visual">
							<input type="hidden" name="status" value="0">
								<input type="hidden" name="trial_number" value="{{ $trial_count }}">
							<input type="hidden" name="part_number" value="{{ $product->part_number }}">
							<input type="hidden" name="id_category_event_project" value="{{ $event->id_category_event_project }}">
							<h2 style="text-align: center;">VISUAL</h2><hr>
								<label>NOTES</label>
								<textarea class="form-control" name="note_visual">
									
								</textarea>
								<label>ATTACHMENT</label>
								<input type="file" name="attachment_visual" class="form-control">
								<label>PIC TERKAIT</label>
								<select name="pic_terkait[]" multiple class="form-control">
									<option value="1">PE</option>
									<option value="2">MTTF</option>
									<option value="3">ENG - Process</option>
									<option value="4">ENG - Setting & Moldshop</option>
								</select>

						</div>
						<div class="card-footer">
							<button class="btn btn-primary"><i data-feather="plus"></i> SAVE VISUAL</button>
						</div>
						</form>
					</div>
				</div>


				
				<div class="col-sm-6">
					<div class="card" style="background: #dcab7c96;">
						<div class="card-body">
							<h2 style="text-align: center;">DIMENSI</h2><hr>
							<form action="{{ url('project/post_quality') }}" method="post" enctype="multipart/form-data">
								@csrf
							<input type="hidden" name="type" value="save_dimensi">
							<input type="hidden" name="status" value="0">
							<input type="hidden" name="trial_number" value="{{ $trial_count }}">
							<input type="hidden" name="part_number" value="{{ $product->part_number }}">
							<input type="hidden" name="id_category_event_project" value="{{ $event->id_category_event_project }}">
								<label>NOTES</label>
								<textarea class="form-control" name="note_dimensi">
									
								</textarea>
								<label>ATTACHMENT</label>
								<input type="file" name="attachment_dimensi" class="form-control">
								<label>PIC TERKAIT</label>
								<select name="pic_terkait[]" multiple class="form-control">
									<option value="1">PE</option>
									<option value="2">MTTF</option>
									<option value="3">ENG - Process</option>
									<option value="4">ENG - Setting & Moldshop</option>
								</select>
							
						</div>
						<div class="card-footer">
							<button class="btn btn-primary"><i data-feather="plus"></i> SAVE DIMENSI</button>
						</div>
							</form>
					</div>
				</div>
				
			</div>

			  <table class="table table-striped table-hover nowrap document" id="datatable">
				<thead>
					<tr>
						<th>Notes</th>
						<th>Type</th>
						<th>Pic Terkait</th>
					</tr>
				</thead>
				<tbody>
					@foreach($visual as $visual)
					<tr>
						<th>{{ $visual->notes }}</th>
						<th> Visual </th>
						<th>
							@if($visual->pic_pe =="1")
							PE
							@endif
							@if($visual->pic_mjtf =="2")
							MJTF
							@endif
							@if($visual->pic_eng_process =="3")
							ENG PROCESS
							@endif
							@if($visual->pic_eng_set_mold =="4")
							ENG SET MOLD
							@endif
						</th>
					</tr>
					@endforeach
					@foreach($dimensi as $dimensi)
					<tr>
						<th>{{ $dimensi->notes }}</th>
						<th>Dimensi</th>
						<th>
							@if($dimensi->pic_pe =="1")
							PE
							@endif
							@if($dimensi->pic_mjtf =="2")
							MJTF
							@endif
							@if($dimensi->pic_eng_process =="3")
							ENG PROCESS
							@endif
							@if($dimensi->pic_eng_set_mold =="4")
							ENG SET MOLD
							@endif
						</th>
					</tr>
					@endforeach
				</tbody>
			</table>

	<form action="{{ url('project/post_quality') }}" method="post" enctype="multipart/form-data">
								@csrf
									<input type="hidden" name="trial_number" value="{{ $trial_count }}">
							<input type="hidden" name="part_number" value="{{ $product->part_number }}">
							<input type="hidden" name="id_category_event_project" value="{{ $event->id_category_event_project }}">
			<div class="card">
				<div class="card-body">
			<div class="row">

				<div class="col-sm-6">
				<label>ADJUSTMENT VISUAL</label>
				<select class="form-control" name="adjustment_visual">
					<option value="">---Select Adjustment---</option>
					<option value="OK">OK</option>
					<option value="NG">NG</option>
				</select>
				</div>
				<div class="col-sm-6">
						<label>ADJUSTMENT DIMENSI</label>
						<select class="form-control" name="adjustment_dimensi">
							<option value="">---Select Adjustment---</option>
							<option value="OK">OK</option>
							<option value="NG">NG</option>
						</select>
				</div>
			</div>
		</div>
			</div>


		</div>
	</div>
	<div class="card-footer">

		<input type="hidden" name="type" value="save_all">
		<input type="hidden" name="id_project" value="{{ $id }}">
		<button class="btn btn-success">SAVE ALL</button>
		</form>
	</div>
</div>
@endsection
@push('script')
<script type="text/javascript">
	 $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });
</script>
@endpush
