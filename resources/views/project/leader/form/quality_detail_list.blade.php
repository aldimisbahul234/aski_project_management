@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
      <h2 style="text-align: center;">DISPLAY QUALITY </h2>
			<table class="table document" id="datatable" style="border-spacing: 15px;">
                 <thead>     
	              <tr>
	                <th>NO</th>
	                <th>EVENT</th>
                  <th>CURRENT TRIAL</th>
                  <th>ADJUSTMENT VISUAL</th>
                  <th>ADJUSTMENT DIMENSI</th>
                  <th>DETAIL</th>
                  <th>LINE APPROVAL</th>
	              </tr>
                  </thead>
               
               </table>
		</div>
	</div>
</div>		
@endsection
@push('script')
<script type="text/javascript">
	  $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
                
            });
          });
</script>
@endpush