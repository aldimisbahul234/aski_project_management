@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
      <h2 style="text-align: center;">DISPLAY QUALITY PROJECT {{ $project->project_name }} {{ $product->part_name }}</h2>
			<table class="table document" id="datatable" style="border-spacing: 15px;">
                 <thead>     
	              <tr>
	                <th>NO</th>
	                <th>EVENT</th>
                  <th>CURRENT TRIAL</th>
                  <th>ADJUSTMENT VISUAL</th>
                  <th>ADJUSTMENT DIMENSI</th>
                  <th>DETAIL</th>
                  <th>LINE APPROVAL</th>
	              </tr>
                  </thead>
                 <tbody>
                    @foreach($data as $data)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $data['event'] }}</td>
                      
                   
                        <td>@if($data['trial_number'] ==true) {{ $data['trial_number'] }} @else N/A @endif</td>
                        @if($data['adjustment_visual'] =="NG")
                        <td style="text-align: center;background-color:red;color:white;border: 1px solid white;padding: 5px;">
                          {{ $data['adjustment_visual'] }}
                        @elseif($data['adjustment_visual'] =="OK")
                         <td style="text-align: center;background-color:green;color:white;border: 1px solid white;padding: 5px;">
                          {{ $data['adjustment_visual'] }}
                        @else
                         <td style="text-align: center;background-color:grey;color:white;border: 1px solid white;padding: 5px;">
                          N/A
                        @endif
                        </td>
                        @if($data['adjustment_dimensi'] =="NG")
                        <td style="text-align: center;background-color:red;color:white;border: 1px solid white;padding: 5px;">
                          {{ $data['adjustment_dimensi'] }}
                        @elseif($data['adjustment_dimensi'] =="OK")
                         <td style="text-align: center;background-color:green;color:white;border: 1px solid white;padding: 5px;">
                          {{ $data['adjustment_dimensi'] }}
                        @else
                         <td style="text-align: center;background-color:grey;color:white;border: 1px solid white;padding: 5px;">
                          N/A
                        @endif
                        </td>
                     
                        
                        <td>
                          @if($data['id_quality'] ==true)
                            <a href="{{ url('project/quality_detail_list/'.$data['id_quality']) }}" class="btn btn-success"> Detail</a>
                            @else
                             <button class="btn btn-success" disabled> Detail</button>
                            @endif
                        </td>
                        <td>N/A</td>
                    </tr>
                    @endforeach
                 </tbody>
               </table>
		</div>
	</div>
</div>		
@endsection
@push('script')
<script type="text/javascript">
	  $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
                
            });
          });
</script>
@endpush