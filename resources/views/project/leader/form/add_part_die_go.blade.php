@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')


 <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">ADD PRODUCT DIE GO ( Project {{ $project->project_name }} )</h4>
        </div>
        <div class="card-body">
        <form class="form-participant" action="{{ url('project/post_product') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_project" value="{{ $project->id_project }}">
     

              <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">PART NUMBER</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
          
                      <select class="form-control" name="part_number" id="part_number">
                      	<option>-Pilih Process-</option>
                        @foreach($product as $product)
                          <option value="{{ $product->part_number }}">{{ $product->part_number }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
          </div>

         
<span id="get">
  
</span>
               

              <div class="col-sm-9 offset-sm-3">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
              $('#part_number').on('change',function(e){
                   $('#get').hide();
                var brand_id =  e.target.value;
                console.log(brand_id);

                $.ajax  ({
                url: "{{ url('project/get_prod') }}/"+brand_id,
                success: function(brand_id){
                    console.log(brand_id);
                       $("#get").show();
                       $("#get").html('<div class="row">'+
              '<div class="col-12">'+
                '<div class="form-group row">'+
                  '<div class="col-sm-3 col-form-label">'+
                    '<label for="fname-icon">PART NAME</label>'+
                  '</div>'+
                  '<div class="col-sm-9">'+
                    '<div class="input-group input-group-merge">'+
                      '<div class="input-group-prepend">'+
                        '<span class="input-group-text"><i data-feather="user"></i></span>'+
                      '</div>'+
                      '<input type="text" value="'+brand_id+'" id="fname-icon" class="form-control" name="part_name" placeholder="part name"/>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
             '</div>');
                    }
                });
             });
          });
 </script>
 @endpush
 