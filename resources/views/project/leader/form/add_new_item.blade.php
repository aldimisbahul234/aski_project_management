@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
 @if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


 <div class="col-lg-12">
      <div class="card">
     
         
    <div class="card-header">
      
    </div>
        <div class="card-body">
           <a class="btn btn-danger btn-sm float-right" href="{{ url('project/item_control_detail/'.$id_project.'/'.$categoris.'/'.$part_number.'/'.$id_process) }}"> Back</a>
           @if($data !=null)
           <center><h4 class="card-title">EDIT ITEM {{ $data->part_number }}</h4></center>
           @else
           <center><h4 class="card-title">ADD NEW ITEM</h4></center>
           @endif
           <br><br>
      @if($data !=null)
       <form class="form-participant" action="{{ url('project/update_item_control/'.$data->id_item) }}" method="post" enctype="multipart/form-data">
        @else
         <form class="form-participant" action="{{ url('project/pos_add_new_item/') }}" method="post" enctype="multipart/form-data">
          @endif
            @csrf
            <input type="hidden" name="id_process" value="{{ $id_process }}">  
      <input type="hidden" name="categoris" value="{{ $categoris }}">   
<input type="hidden" name="part_number" value="{{ $part_number }}">
   <input type="hidden" name="id_project" value="{{ $id_project }}">         
              <div class="row">
              <div class="col-12">
                <div class="form-group">
                 <label>ITEM CONTROL</label>
                  
                  @if($data !=null)
                   <input type="text" name="id_category" class="form-control" value="{{ $category->item_name }}" {{ $check }}>
                   
                  @else
                   <input type="text" name="id_category" class="form-control" {{ $check }}>
                    @endif
                </div>
              </div>
             </div>

              <div class="row">
              <div class="col-12">
                <div class="form-group">
                 <label>PIC</label>
                  
                    @if($data !=null)
                    @if(Auth::user()->hak_akses =='project_manager' || Auth::user()->hak_akses =='project_leader')
                    <select name="pic_item" class="form-control" id="pic"> 
                      <option>member</option>
                      @foreach($member as $member)
                      <option value="{{ $member->username }}" {{ $data->pic_item == $member->username ? "selected" : "" }}>{{ $member->username }}</option>
                      @endforeach
                    </select>
                    @else
                    <input name="pic_item" class="form-control" value="{{ $data->pic_item }}" readonly>
                    @endif
                  @else
                      <select name="pic_item" class="form-control" id="pic"> 
                      <option>member</option>
                      @foreach($member as $member)
                      <option value="{{ $member->username }}" >{{ $member->username }}</option>
                      @endforeach
                    </select>
                  @endif
                </div>
              </div>
             </div>


             <div class="row">
              <div class="col-12">
                <div class="form-group">
                  
                  <div class="row">
                    <div class="col-lg-6">
                      <label>PLAN START</label>
                      @if($data !=null)
                      <input type="date" name="plan_start_item" class="form_technical" style="width: 100%" {{ $data->plan_start_item == true ? "value=$data->plan_start_item" : "" }} {{ $check }}>
                      @else
                      <input type="date" name="plan_start_item" class="form_technical" style="width: 100%" {{ $check }}>
                      @endif
                    </div>
                    <div class="col-lg-6">
                      <label>PLAN FINISH</label>
                        @if($data !=null)
                      <input type="date" name="plan_finish_item" class="form_technical" style="width: 100%" {{ $data->plan_finish_item == true ? "value=$data->plan_finish_item" : "" }} {{ $check }}>
                      @else
                      <input type="date" name="plan_finish_item" class="form_technical" style="width: 100%" {{ $check }}>
                      @endif
                    </div>
                     <div class="col-lg-6">
                      <label>ACTUAL START</label>
                        @if($data !=null)
                      <input type="date" name="actual_start_item" class="form_technical" style="width: 100%" {{ $data->actual_start_item == true ? "value=$data->actual_start_item" : "" }} {{ $check }}>
                      @else
                       <input type="date" name="actual_start_item" class="form_technical" style="width: 100%" {{ $check }}>
                      @endif
                    </div>
                    <div class="col-lg-6">
                      <label>ACTUAL FINISH</label>
                         @if($data !=null)
                      <input type="date" name="actual_finish_item" class="form_technical" style="width: 100%" {{ $data->actual_finish_item == true ? "value=$data->actual_finish_item" : "" }} {{ $check }}>
                      @else
                      <input type="date" name="actual_finish_item" class="form_technical" style="width: 100%" {{ $check }}>
                      @endif
                    </div>
                    <div class="col-lg-12">
                      <label>ACTUAL PROGRESS ITEM (%)</label>
                        @if($data !=null)
                      <input type="text" name="actual_progress_item"  class="form_technical" style="width: 100%"{{ $data->actual_progress_item == true ? "value=$data->actual_progress_item" : "" }}>
                      @else
                       <input type="text" name="actual_progress_item"  class="form_technical" style="width: 100%">
                      @endif
                    </div>
                    <div class="col-lg-12">
                      <label>CATATAN</label>
                        @if($data !=null)
                      <input type="text" name="catatan" class="form_technical" style="width: 100%" {{ $data->catatan == true ? "value=$data->catatan" : "" }}>
                      @else
                       <input type="text" name="catatan" class="form_technical" style="width: 100%">
                      @endif
                    </div>
                    <!--  <div class="col-lg-12">
                      <label>DATE DURATION AUTO EMAIL</label>
                        @if($data !=null)
                      <input type="number" name="date_duration_auto_email" class="form_technical" style="width: 100%" {{ $data->date_duration_auto_email == true ? "value=$data->date_duration_auto_email" : "" }}>
                      @else
                       <input type="number" name="date_duration_auto_email" class="form_technical" style="width: 100%">
                      @endif
                    </div> -->

                    <div class="col-lg-12">
                      <label>EMAIL SENDER</label>
                      <input type="email" name="email_pengirim" class="form_technical" style="width: 100%" value="{{ Auth::user()->email }}">
                    </div>
                    <div class="col-lg-12">
                      <label>PASSWORD SENDER</label>
                      <input type="password" name="password_pengirim" class="form_technical" style="width: 100%">
                    </div>
                    @if(Auth::user()->hak_akses =='project_manager' || Auth::user()->hak_akses =='project_leader')
                     <div class="col-lg-12">
                      <label>EMAIL RECIPLE</label>
                      <input type="email" name="email_penerima" class="form_technical" style="width: 100%" value="{{ $email_pic['email_pic'] }}" id="email" readonly>
                    </div>
                    @else
                    <div class="col-lg-12">
                      <label>EMAIL RECIPLE</label>
                      <input type="email" name="email_penerima" class="form_technical" style="width: 100%" value="{{ $email_leader['email_leader'] }}">
                    </div>
                    @endif
                      <div class="col-lg-12">
                      <label>DOCUMENT</label>
                      <input type="file" name="bukti_item" class="form_technical" style="width: 100%">
                    </div>

                     

                  </div>
                </div>

              </div>
             </div>


             <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
             
        </div>
      </div>
    </div>


@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
            $('.check').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });

              $('#pic').change(function(){
              var username = $(this).val();
              var url = "{{ url('project/get_email') }}";
               $.ajax({
                  type: "GET",
                  url: url,
                  data: {
                  'username': username,
                  },
                   success: function(data) {
                    console.log(data);
                 
                       document.getElementById('email').value = data;
                    
                   
                   }
                  }); 
                });

          });
 </script>
 @endpush
 