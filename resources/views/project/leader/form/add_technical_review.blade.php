@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<h2 style="text-align: center">FORM TECHNICAL REVIEW</h2>
				<h3>A. BASIC SPECIFICATION</h3>
				<hr>
          <form class="form-participant" action="{{ url('project/post_technical_review/') }}" method="post" enctype="multipart/form-data">
            @csrf
        <input type="hidden" name="id_customer" value="{{ $IDcustomer->id_customer }}">
        <input type="hidden" name="id_project" value="{{ $project->id_project }}">
        <input type="hidden" name="part_number" value="{{ $product->part_number }}">
        <input type="hidden" name="id_process" value="{{ $process->id_process }}">
					<div class="form-group row">
                		<label for="colFormLabel" class="col-sm-2 col-form-label">CUSTOMER</label> :
                			<div class="col-sm-9">
                  			<input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value="{{ $IDcustomer->customer_name }}"  disabled/>
                		</div>
              		</div>

              		<div class="form-group row">
                		<label for="colFormLabel" class="col-sm-2 col-form-label">PROJECT</label> :
                			<div class="col-sm-9">
                  			<input type="text" class="form_technical" value="{{ $project->project_name }}" placeholder="Normal Input" style="width: 100%"  disabled/>
                		</div>
              		</div>

              		<div class="form-group row">
                		<label for="colFormLabel" class="col-sm-2 col-form-label">PART NUMBER</label> :
                			<div class="col-sm-9">
                  			<input type="text" class="form_technical" value="{{ $product->part_number }}" placeholder="Normal Input" style="width: 100%" disabled />
                		</div>
              		</div>

              		<div class="form-group row">
                		<label for="colFormLabel" class="col-sm-2 col-form-label">PART NAME</label> :
                			<div class="col-sm-9">
                  			<input type="text" class="form_technical" value="{{ $product->part_name }}" placeholder="Normal Input" style="width: 100%" disabled />
                		</div>
              		</div>

              		<div class="form-group row">
                		<label for="colFormLabel" class="col-sm-2 col-form-label">PROCESS</label> :
                			<div class="col-sm-9">
                  			<input type="text" value="{{ $process->process_name }}" class="form_technical" placeholder="Normal Input" style="width: 100%" disabled />
                		</div>
              		</div>

                      <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET MASSPRO</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Normal Input" name="target_masspro" style="width: 100%" />
                    </div>
                  </div>

                    <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">FORECASE</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="result" class="form_technical" name="forcase_year" placeholder="Year"   />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Pcs/year</h5>
                      </div>
                  </div>
               <!-- calculate 2 -->
               <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"></label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get2"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang2()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah2()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi2()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali2()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate2()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="basic_result_2" name="forcase_month" class="form_technical" placeholder="Month"   />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Pcs/month</h5>
                      </div>
                  </div>


                   <!-- calculate 3 -->
               <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"></label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic3_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get3"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang3()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah3()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi3()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali3()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic3_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate3()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="basic_result_3" name="forcase_day" class="form_technical" placeholder="Day"   />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Pcs/day</h5>
                      </div>

                  </div>

  <h3>B. PROCESS SPECIFICATION</h3>
  <hr>
    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
              <a
                class="nav-link active"
                id="home-tab-fill"
                data-toggle="tab"
                href="#home-fill"
                role="tab"
                aria-controls="home-fill"
                aria-selected="true"
                >Plastic Injection</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="profile-tab-fill"
                data-toggle="tab"
                href="#profile-fill"
                role="tab"
                aria-controls="profile-fill"
                aria-selected="false"
                >Line Rear View</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="messages-tab-fill"
                data-toggle="tab"
                href="#messages-fill"
                role="tab"
                aria-controls="messages-fill"
                aria-selected="false"
                >Line Assy Outer Mirror</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="settings-tab-fill"
                data-toggle="tab"
                href="#settings-fill"
                role="tab"
                aria-controls="settings-fill"
                aria-selected="false"
                >Line Foaming</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="settingsi-tab-fill"
                data-toggle="tab"
                href="#settingsi-fill"
                role="tab"
                aria-controls="settingsi-fill"
                aria-selected="false"
                >Line Assy Seat Button</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="line_paint-tab-fill"
                data-toggle="tab"
                href="#line_paint-fill"
                role="tab"
                aria-controls="line_paint-fill"
                aria-selected="false"
                >Line Painting</a
              >
            </li>
          </ul>

           <!-- Tab panes -->
          <div class="tab-content pt-1">
            <div class="tab-pane active" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">

                  <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">BERAT PART</label> :
                        <div class="col-sm-8">
                            <input type="text" class="form_technical" name="beratpartpi" placeholder="Normal Input" style="width: 100%" value=""  />
                        </div>
                         <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Gram</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">BERAT RUNNER</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic4_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get4"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang4()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah4()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi4()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali4()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic4_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate4()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="basic_result_4" name="beratrunnerpi" class="form_technical" placeholder="Normal Input"  style="width: 100%" />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Gram</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">NO CAVITY</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="nocavitypi" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Cavity</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">MESIN INJECTION</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="mesininjectionpi" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Ton</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">FINISHING</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="finishing" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">M/P</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">ASSY</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="assygeneral" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">M/P</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="targetctimepi" placeholder="Normal Input" style="width: 100%" value=""  />
                        </div>
                         <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Seccond</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">STATUS MOLD</label> :
                        <div class="col-sm-9">
                       <select name="statusmoldpi" class="form-control">
                         <option value="mold_aski">Mold Aski</option>
                          <option value="mold_customer">Mold Customer</option>
                       </select>
                    </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME ASSY</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="targetctimeassypi" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Seccond</h5>
                      </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMASI REJECTION</label> :
                        <div class="col-sm-8">
                        <input type="text" class="form_technical" name="estimaterejectionpi" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                     <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">%</h5>
                      </div>
                  </div>


                      <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN MESIN</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic5_angka1" name="estimatepemakaianmesinharipi" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get5"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" type="button" type="button" onclick="basic_kurang5()">-</button>
                                    <button class="dropdown-item" type="button" type="button" type="button" onclick="basic_tambah5()">+</button>
                                    <button class="dropdown-item" type="button" type="button" type="button" onclick="basic_bagi5()">/</button>
                                    <button class="dropdown-item" type="button" type="button" type="button" onclick="basic_kali5()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic5_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate5()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="basic_result_5" class="form_technical" placeholder="Normal Input"   />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Hours / day</h5>
                      </div>
                  </div>
               <!-- calculate 2 -->
               <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"></label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic6_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get6"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_kurang6()">-</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_tambah6()">+</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_bagi6()">/</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_kali6()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic6_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate6()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-2">
                        <input type="text" id="basic_result_6" name="estimatepemakaianmesinbulanpi" class="form_technical" placeholder="Normal Input"   />
                      </div>
                       <div class="col-sm-1">
                       <h5 style="margin-top: 15px;">Hours / month</h5>
                      </div>
                  </div>

                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" >ESTIMATE PEMAKAIAN MESIN</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="kapasitasmesintersediapi" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitasmesintersediapi" value="0">
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio" name="kapasitasmesintersediapi" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                    <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">MESIN No</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" name="nomor_mesin" placeholder="No mesin" style="width: 100%" value=""  />
                    </div>
                  </div>

                  <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">1. TYPE MATERIAL</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" name="typematerial" placeholder="Type material" style="width: 100%" value=""  />
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">- GRADE</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Grade" name="gradematerial" style="width: 100%" value=""  />
                    </div>
                  </div>

                   <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" >-- STATUS MATERIAL</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="statusmaterial" value="1">
                                        <label>New grade</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="statusmaterial" value="0">
                                        <label>Existing</label>
                                      </div>


                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                     <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">-- QTY / MONTH</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic7_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get7"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang7()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah7()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi7()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali7()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic7_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate7()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_7" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>

                    <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">2. MASTER BATCH / ADITIF</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value=""  />
                    </div>
                  </div>

                   <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">-- WARNA</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Warna" name="warnaaditif" style="width: 100%" value=""  />
                    </div>
                  </div>

                     <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">-- GRADE</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Grade" name="gradematerial" style="width: 100%" value=""  />
                    </div>
                  </div>

                    <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">-- DOSAGE</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Dosage" name="dosageaditif" style="width: 100%" value=""  />
                    </div>
                  </div>

                  <div class="form-group row">
                      <label for="colFormLabel" class="col-sm-2 col-form-label">3. PRE-HEATING</label> :
                        <div class="col-sm-9">
                        <input type="text" class="form_technical" placeholder="Pre heating" name="preheating" style="width: 100%" value=""  />
                    </div>
                  </div>

                 <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold">4. HOPPER PRE-HEATING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="hopperpreheating" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="hopperpreheating" value="0">
                                        <label>No</label>
                                      </div>
                                       <div class="col-lg-2">
                                      <input type="radio" name="hopperpreheating" value="2">
                                        <label>Need confirmation</label>
                                      </div>


                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                   <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold">5. MENGGUNAKAN DEHUMIDIFIYING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="ig_radio_menggunakandehumidifiying" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="ig_radio_menggunakandehumidifiying" value="0">
                                        <label>No</label>
                                      </div>
                                       <div class="col-lg-2">
                                      <input type="radio" name="ig_radio_menggunakandehumidifiying" value="2">
                                        <label>Need confirmation</label>
                                      </div>


                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold">6. DEHUMIDIFIYING TERSEDIA</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="ig_radio_dehumidifiyingtersedia" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="ig_radio_dehumidifiyingtersedia" value="0">
                                        <label>No</label>
                                      </div>
                                       <div class="col-lg-2">
                                      <input type="radio" name="ig_radio_dehumidifiyingtersedia" value="2">
                                        <label>Need confirmation</label>
                                      </div>


                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>




            </div>
            <div class="tab-pane" id="profile-fill" role="tabpanel" aria-labelledby="profile-tab-fill">

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">LINE REAR VIEW</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="linerearview"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME REAR VIEW</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical"  name="targetctimerearview" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION REAR VIEW</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionrearview"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN LINE REAR VIEW</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic8_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get8"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_kurang8()">-</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_tambah8()">+</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_bagi8()">/</button>
                                    <button class="dropdown-item" type="button" type="button" onclick="basic_kali8()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic8_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate8()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_8" name="estimatepemakaianrearview" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS REAR VIEW</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasrearview" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERPAKAI</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasterpakairearview" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                  <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label"> KAPASITAS TERSEDIA REAR VIEW</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediarearview" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2" >
                                        <input type="radio" name="kapasitastersediarearview" value="0">
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediarearview" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">FLOAT GLASS</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="materialfloatglass" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">QTY/PCS FLOAT GLASS</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="qtyfloatglass"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

            </div>
            <div class="tab-pane" id="messages-fill" role="tabpanel" aria-labelledby="messages-tab-fill">

              <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">LINE ASSY OUTER MIRROR</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="lineassyoutermirror"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME ASSY OUTER MIRROR</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="targetctimeassyoutermirror"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION ASSY OUTER MIRROR</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionassyoutermirror"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN LINE ASSY OUTER MIRROR</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic9_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get9"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang9()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah9()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi9()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali9()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic9_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate9()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_9" name="estimatepemakaianassyoutermirror" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS ASSY OUTER MIRROR</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasassyoutermirror" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERPAKAI</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasterpakaiassyoutermirror"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>


                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label"> KAPASITAS TERSEDIA OUTER MIRROR</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio"  name="kapasitastersediaassyoutermirror" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediaassyoutermirror" value="0" >
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio"  name="kapasitastersediaassyoutermirror" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

            </div>
            <div class="tab-pane" id="settings-fill" role="tabpanel" aria-labelledby="settings-tab-fill">
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">BERAT PART</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="beratpartfoaming" placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                    <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">BERAT RUNNER</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic10_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get10"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang10()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah10()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi10()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali10()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic10_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate10()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_10" name="beratrunnerfoaming" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>


                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">LINE FOAMING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="linefoaming"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME FOAMING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="targetctimefoaming" placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION FOAMING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionfoaming"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN LINE FOAMING</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic11_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get11"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang11()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah11()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi11()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali11()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic11_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate11()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_11" name="estimatepemakaianfoaming" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS FOAMING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasfoaming"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERPAKAI</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasterpakaifoaming" placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                   <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label"> KAPASITAS TERSEDIA FOAMING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediafoaming" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediafoaming" value="0">
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediafoaming" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">  1. POLYOL</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="materialpolyol"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">  2. ISO CYANALK</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="materialisocyanalk"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">  3. MOLD RELEASE</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="materialmoldrelease" placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

            </div>

              <div class="tab-pane" id="settingsi-fill" role="tabpanel" aria-labelledby="settingsi-tab-fill">

                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">LINE ASSY SEAT BOTTOM</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="lineassyseatbottom"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME ASSY SEAT BOTTOM</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="targetctimeassyseatbottom"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION ASSY SEAT BOTTOM</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionassyseatbottom"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                      <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN LINE ASSY SEAT BOTTOM</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic12_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get12"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang12()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah12()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi12()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali12()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic12_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button" onclick="basic_caluculate12()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_12" name="estimatepemakaianassyseatbottom" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS ASSY SEAT BOTTOM</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasassyseatbottom"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERPAKAI</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasterpakaiassyseatbottom"  placeholder="LINE ASSY SEAT BOTTOM" style="width: 100%"  />
                      </div>
                  </div>

                   <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERSEDIA ASSY SEAT BOTTOM</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediaassyseatbottom" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediaassyseatbottom" value="0">
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediaassyseatbottom" value="1">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

              </div>

              <div class="tab-pane" id="line_paint-fill" role="tabpanel" aria-labelledby="line_paint-tab-fill">

                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">LINE REAR VIEW</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="linepainting"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">TARGET C/TIME PAINTING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="targetctimepainting"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION BUFFING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionbuffing"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION SANDING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionsanding" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION TOUCH UP</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectiontouchup" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION REPAINT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionrepaint" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE REJECTION OUT TOTAL</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="estimaterejectionouttotal"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">ESTIMATE PEMAKAIAN LINE PAINTING</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic13_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                            <div class="demo-inline-spacing">
                                <div class="btn-group">
                                  <button
                                    class="btn btn-primary btn-sm dropdown-toggle"
                                    type="button"
                                    id="get13"

                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    style="left: 66px;top: -16px;"
                                  >

                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button class="dropdown-item" type="button" onclick="basic_kurang13()">-</button>
                                    <button class="dropdown-item" type="button" onclick="basic_tambah13()">+</button>
                                    <button class="dropdown-item" type="button" onclick="basic_bagi13()">/</button>
                                    <button class="dropdown-item" type="button" onclick="basic_kali13()">X</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic13_angka2" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                            <button class="btn btn-primary btn-sm" type="button"  onclick="basic_caluculate13()" style="left: 50px;top: 4px;">Calculate</button>
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_13" name="estimatepemakaianpainting" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>

                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS LINE PAINTING</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitaspainting"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">KAPASITAS TERPAKAI</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="kapasitasterpakaipainting" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                   <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label"> KAPASITAS TERSEDIA PAINTING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediapainting" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediapainting" value="0">
                                        <label>No</label>
                                      </div>
                                        <div class="col-lg-2">
                                        <input type="radio" name="kapasitastersediapainting" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

              </div>
              <h5 style="font-weight: bold">TYPE MATERIAL PAINTING </h5>
               <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">1. CAT UNDER COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="namecatundercoat" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">QTY/PCS CAT UNDER COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="catundercoat"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">  2. CAT TOP COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="namecattopcoat" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">QTY/PCS CAT TOP COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="cattopcoat"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">3. THINNER UNDER COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="namethinnerundercoat" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">  QTY/PCS THINNER UNDER COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="thinnerundercoat"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"> 4. THINNER TOP COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="namethinnertopcoat" placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"> QTY/PCS THINNER TOP COAT</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="thinnertopcoat"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"> 5. HARDENER</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="namehardener"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">QTY/PCS HARDENER</label> :
                      <div class="col-sm-9">
                        <input type="text" class="form_technical" name="hardener"  placeholder="LINE REAR VIEW" style="width: 100%"  />
                    </div>
                  </div>

                 <h3>C. COMPONENT / INSERT</h3>
                    <hr>
                    <div class="row">
                      <div class="col-lg-6">
                        <input type="text" class="form-control" name="nama_component[]" placeholder="Normal Input" value=""  />
                      </div>
                      <div class="col-lg-3">
                        <input type="text" class="form-control" name="qty_component[]" placeholder="Normal Input" value=""  />
                      </div>
                       <div class="col-lg-2">
                        <select  class="form-control" name="satuancomponent[]">
                            <option value="pcs">Pcs</option>
                            <option value="unit">Unit</option>
                            <option value="seat">Seat</option>
                            <option value="lembar">Lembar</option>
                            <option value="box">Box</option>
                            <option value="roll">Metter</option>
                            <option value="centimeter">Centimeter</option>
                            <option value="kilo_gram">Kilo gram</option>
                            <option value="gram">Gram</option>
                        </select>
                      </div>
                      <div class="col-lg-1">
                        <button class="btn btn-primary c_add_component" type="button">+</button>
                      </div>
                    </div>
                    <span id="c_form_component" style="display: none;"></span>
                     <br><br>
                   <h3>D. UTILITY SPECIFICATION</h3>
                    <hr>

                    <button class="btn btn-primary addUtility" type="button">+</button><br><br>
                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">HOT RUNNER</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="hotrunner" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                        </div>
                       <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">FAN/PENDINGIN</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="pendingin" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>

                      <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">CHILLER</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="chiller" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>
                       <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">ANGIN BERTEKANAN</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="anginbertekanan" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>

                      <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">MTC AIR</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="mtcair" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>
                       <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">MEJA ASSY</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="mejaassy" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>

                      <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">MTC OIL</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="mtcoil" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>
                       <div class="col-lg-6">
                                   <div class="form-group row">
                                    <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">OVEN CONVEYOR</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>

                      <div class="col-lg-6">

                                   <div class="form-group row">
                                     <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">RENDAMAN</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="rendaman" placeholder="Normal Input" style="width: 100%" value=""  />
                            </div>
                          </div>
                      </div>
                       <div class="col-lg-6">
                            <div class="form-group row">
                              <input type="checkbox" name="" style="width: 7%;height: 27px;">
                              <label for="colFormLabel" class="col-sm-2 col-form-label">STAPLES</label> :
                                <div class="col-sm-9">
                                <input type="text" class="form_technical" name="staples" placeholder="Normal Input" style="width: 100%" value=""  />
                              </div>
                          </div>
                      </div>

                    
                    </div>
                    <div class="row" id="form_utility" style="display: none;"></div>
                  <h3>E. GAUGE SPECIFICATION</h3>
                  <hr>
 
                  <button class="btn btn-primary addGuage" type="button">+</button>          
                        <div class="row" style="margin-top: 10px;">
                          
                          <div class="col-lg-3">
                              <input type="checkbox" name="jig" value="1">
                              <label>JIG</label>
                          </div>
                          <div class="col-lg-3">
                              <input type="checkbox" name="gonogo" value="1">
                              <label>GO - NO GO</label>
                          </div>
                          <div class="col-lg-3">
                              <input type="checkbox" name="cf" value="1"> 
                              <label>C/F</label>
                          </div>
                          <div class="col-lg-3">
                              <input type="checkbox" name="errorprofing" value="1">
                              <label>ERROR PROFING</label>
                          </div>   
                          
                        
                        </div>
                        <div class="row" id="form_guage" style="display: none;"></div>
              <br>
                  <h3>F. PACKING SPECIFICATION</h3>
                  <hr>
                         <h5>A. BOX PACKING</h5>
                         <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label">1. TYPE</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="typeboxpackaging" value="boxkarton">
                                        <label>Box karton</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="typeboxpackaging" value="boxplastik">
                                         <label>Box karton / Keranjang</label>
                                      </div>
                                          <div class="col-lg-2">
                                            <input type="radio"name="typeboxpackaging" value="boxpartisi" >
                                             <label>Box carrugated</label>
                                          </div>
                                          <div class="col-lg-2">
                                            <input type="radio" name="typeboxpackaging" value="kereta">
                                             <label>Kereta rak / ambalan</label>
                                          </div>
                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>


                          <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">2. DIMENSI BOX PACKING</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic4_angka1" name="panjangdimensiboxpackaging" class="form_technical" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                                X
                        </div>
                      </div>
                    </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic4_angka2" name="lebardimensiboxpackaging" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                           X
                          </div>
                        </div>
                       <div class="col-sm-3">
                        <input type="text" id="basic_result_4" name="tinggidimensiboxpackaging" class="form_technical" placeholder="Normal Input"   />
                      </div>
                  </div>

                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">3. ISI PART / BOX</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="isipartbox" placeholder="Normal Input"   style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">4. JUMLAH TOTAL KEBUTUHAN BOX</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="totalkebutuhanbox" placeholder="Normal Input"   style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>


                         <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold;">B. KANTONG PACKING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="kantongpackaging" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="kantongpackaging" value="0">
                                        <label>No</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>


                         <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label">1. TYPE</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="typekantongpackaging" value="kantongplastik">
                                        <label>Kantong plastik</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="typekantongpackaging" value="typekantongpackaging">
                                        <label>Form layer</label>
                                      </div>
                                          <div class="col-lg-2">
                                            <input type="radio" name="typekantongpackaging" value="typekantongpackaging">
                                            <label>Box carrugated</label>
                                          </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>




                                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">2. DIMENSI KANTONG PACKING</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic4_angka1" class="form_technical" placeholder="Normal Input" style="width: 175%" name="panjangdimensikantongpackaging" />
                        </div>
                        <div class="col-lg-6">
                                X
                        </div>
                      </div>
                    </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic4_angka2" name="lebardimensikantongpackaging" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                           X
                          </div>
                        </div>

                  </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">3. QTY KANTONG/BOX</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="qtykantongpackaging" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>


                         <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold;"> C. LAYER / SKAT</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="layerskat" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="layerskat" value="0">
                                        <label>No</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                             <h5>1. TYPE</h5>
                         <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                      <input type="checkbox" name="" style="height: 32px;">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">KARTON</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="typekartonskat" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                       <input type="checkbox" name="" style="height: 32px;">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">CORRUGATED</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="typecorrugatedskat" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                       <input type="checkbox" name="" style="height: 32px;">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">PLASTIK</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="typeplastikskat" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>


                                 <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label"> 2. DIMENSI LAYER / SKAT</label> :
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-lg-6">
                          <input type="text" id="basic4_angka1" class="form_technical" name="panjangdimensiskat" placeholder="Normal Input" style="width: 175%"  />
                        </div>
                        <div class="col-lg-6">
                                X
                        </div>
                      </div>
                    </div>

                        <div class="row">
                          <div class="col-lg-6">
                            <input type="text" id="basic4_angka2" name="lebardimensiskat" class="form_technical" placeholder="Normal Input"   />
                          </div>
                           <div class="col-lg-6">
                           X
                          </div>
                        </div>

                  </div>

                         <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" style="font-weight: bold;">D. KERETA HANDLING</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="keretahandling" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="keretahandling" value="0">
                                        <label>No</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">1. JUMLAH TUMPUKAN</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="jumlahtumpukankeretahandling" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label"> 2. TOTAL KEBUTUHAN KERETA HANDLING</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="totalkebutuhankeretahandling" placeholder="Normal Input" style="width: 100%"  />
                                            </div>
                                    </div>
                                </div>
                         </div>
                    <h3>G. TESTING REQUIREMENT</h3>
                  <hr>
                  <div class="row">
                      <div class="col-lg-6">
                        <input type="text" class="form-control" placeholder="JENIS PENGUJIAN" name="jenis_pengujian[]" value=""  style="width: 100%" />
                      </div>
                      <div class="col-lg-4">
                        <input type="text" class="form-control" placeholder="TEMPAT PENGUJIAN" name="tempat_pengujian[]" value=""  style="width: 100%" />
                      </div>
                      <div class="col-lg-2">
                        <button class="btn btn-primary g_add_component" type="button">+</button>
                      </div>
                    </div>
                    <span id="g_form_component" style="display: none;"></span>
                    <br><br>
                  <h3>   H. BUILDING SPECIFICATION</h3>
                  <hr>
                  <h5 style="font-weight: bold;">1.  RUANG/SPACE STORE RAW MATERIAL</h5>
                   <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- DIMENSI RUANG</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="dimensiruangrawmaterial" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>

                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" >-- RUANG / SPACE TERSEDIA</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial">
                                        <label>No</label>
                                      </div>
                                      <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                          <h5 style="font-weight: bold;">2. RUANG STORE BOX KOSONG</h5>

                         <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- DIMENSI RUANG</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="dimensiruangboxkosong" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>

                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" >-- RUANG / SPACE TERSEDIA</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                          <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial" value="0">
                                        <label>No</label>
                                      </div>
                                      <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediarawmaterial" value="2">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                           <h5 style="font-weight: bold;">3. RUANG STORE PART FINISH GOOD</h5>

                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- DIMENSI RUANG</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="dimensiruangboxkosong" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>

                    <div class="row">
                                <div class="col-lg-12">
                                       <div class="form-group row">
                                  <label for="colFormLabel" class="col-sm-2 col-form-label" >-- RUANG / SPACE TERSEDIA</label> :
                                    <div class="col-sm-9">
                                      <div class="row">
                                         <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediaboxkosong" value="1">
                                        <label>Yes</label>
                                         </div>
                                          <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediaboxkosong" value="0">
                                        <label>No</label>
                                      </div>
                                      <div class="col-lg-2">
                                        <input type="radio" name="ruangtersediaboxkosong" value="3">
                                        <label>Need confirmation</label>
                                      </div>

                                      </div>
                                </div>
                              </div>
                          </div>
                         </div>

                     <h3>    I. OTHER SPECIFICATION / SPECIAL CHARACTERISTIC</h3>
                  <hr>
                  <label>
OTHER SPECIFICATION DESCRIPTION</label>
                     <div class="row">
                      <div class="col-lg-10">
                        <input type="text" class="form-control" name="description_other_spec[]" placeholder="Normal Input" value=""  />
                      </div>
                      <div class="col-lg-2">
                        <button class="btn btn-primary j_add_component" type="button">+</button>
                      </div>


                    </div>
                    <span id="j_form_component" style="display: none;"></span>
                    <br><br>
                    <h3>   J. LIST OF INVESTMENT</h3>
                  <hr>   <label>
LIST KEBUTUHAN INVESTASI</label>
                       <div class="row">
                      <div class="col-lg-6">
                        <input type="text" class="form-control" name="list_invesment[]" placeholder="Normal Input" value=""  />
                      </div>
                      <div class="col-lg-3">
                        <input type="text" class="form-control" name="qty_list[]" placeholder="Normal Input" value=""  />
                      </div>
                        <div class="col-lg-1">
                               <select class="form-control" name="satuaninvestasi[]">
                                                        <option value="Pcs">Pcs</option>
                                                        <option value="Unit">Unit</option>
                                                        <option value="Set">Set</option>
                                                        <option value="Lembar">Lembar</option>
                                                        <option value="Box">Box</option>
                                                    </select>
                            </div>
                       <div class="col-lg-2">
                        <button class="btn btn-primary k_add_component" type="button">+</button>
                      </div>

                    </div>
                     <span id="k_form_component" style="display: none;"></span>
                    <br><br>
                     <h3>     KETERANGAN, CATATAN & LAIN-LAIN</h3>
                  <hr>
                     <h5 style="font-weight: bold;">Note,Keterangan & Rekomendasi</h5>
                   <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- NOTE</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="note" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- KETERANGAN</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="keterangan" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>

                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- REKOMENDASI</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="rekomendasi" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                           <h5 style="font-weight: bold;">Berdasarkan Analisis dan feasibility study dari team APQP maka diputuskan Project :</h5>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- PRODUK OK</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="produkok" placeholder="Normal Input"  style="width: 100%" />
                                            </div>
                                    </div>
                                </div>
                         </div>
                          <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">-- PRODUK NG</label> :
                                            <div class="col-sm-9">
                                                 <input type="text" class="form_technical" name="produkng" placeholder="Normal Input" style="width: 100%"  />
                                            </div>
                                    </div>
                                </div>
                         </div>

          </div>
          <button class="btn btn-primary" type="submit"> Save</button>
			</div>
		</div>
	</div>
@endsection
@push('script')
@include('project.leader.js.js_basic_spesifikasi')
@endpush
