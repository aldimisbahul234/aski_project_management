@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')


 <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">REGISTER DATA PART</h4>
          <a href="{{ url('project/display_project_detail/'.$IDcustomer->id_customer.'/'.$project->id_project) }}" class="btn btn-danger">BACK</a>
          
        </div>
        <div class="card-body">
        <form class="form-participant" action="{{ url('project/update_rfq/'.$data->part_number) }}" method="post" enctype="multipart/form-data">
            @csrf
            

           <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">QTY ORDER</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="number"
                        id="fname-icon"
                        class="form-control"
                        name="qty_order"
                        value="{{ $data->qty_order }}"
                        placeholder="QTY"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

                <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">LIFE TIME</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="lifetime"
                        placeholder="1 tahun | 1 bulan | 1 hari"
                        value="{{ $data->lifetime }}"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

               <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">REMARKS</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="remarks"
                        placeholder="REMARKS"
                        value="{{ $data->remarks }}"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

              <div class="col-sm-9 offset-sm-3">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

@endsection