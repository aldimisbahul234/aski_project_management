@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">Project</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$customer->id_customer) }}">{{ $customer->customer_name }}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$customer->id_product) }}">{{ $customer->customer_name }}</a></li>
                 <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$customer->id_product) }}">Detail {{ $customer->customer_name }}</a></li>
                  <li class="breadcrumb-item"><a href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project) }}">Detail {{ $project->project_name }}</a></li>


              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


 <div class="col-lg-12">
      <div class="card">
       <!--  <div class="card-header">
          <a href="{{ url()->previous() }}" class="btn btn-danger">Back</a>
        </div> -->
        <div class="card-body">
           <center><h4 class="card-title">ADD NEW ISSUE PROJECT {{ $project->project_name }} PART {{ $product->part_name }}</h4></center><br><br>
       <form class="form-participant" action="{{ url('project/pos_add_issue/') }}" method="post" enctype="multipart/form-data">
            @csrf
           <input type="hidden" name="id_project" value="{{ $id_project }}">
           <input type="hidden" name="part_number" value="{{ $part_number }}">
             <input type="hidden" name="id_process" value="{{ $id_process }}">
          <div class="row">
            <div class="col-lg-12">
              <label>SELECT R.I.O</label>
                <select name="category_issue" class="form-control">
                <option value="Risk">Risk</option>
                <option value="Issue">Issue</option>
                <option value="Opportunity">Opportunity</option>
              </select>
            </div>
           <div class="col-lg-12">
               <label>DESCRIPTION</label>
           <textarea class="form-control" required cols="50" rows="5" placeholder="Typing your detail description here..." name="issue" ></textarea>     
           </div>
            <div class="col-lg-12">
              <label>ITEM CONTROL</label>
                <select name="id_item" class="form-control">
                  @foreach($get_master_item as $item_control)
                    <option value="{{ $item_control->id_item }}">{{ $item_control->item_name }}</option>
                  @endforeach
              </select>
            </div>
             <div class="col-lg-12">
              <label>PIC</label>
                <select name="pic" class="form-control" id="pic">
                  @foreach($member as $member)
                    <option value="{{ $member->username }}">{{ $member->username }}</option>
                  @endforeach
              </select>
            </div>
            <div class="col-lg-12">
              <label>RECEPIENT EMAIL</label>
               <input type="text" name="email" class="form-control" id="email">
            </div>
            <div class="col-lg-12">
              <label>EMAIL SENDER</label>
               <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}" readonly>
            </div>
              
              <div class="col-lg-12">
              <label>SENDER PASSWORD</label>
               <input type="password" name="password" class="form-control" placeholder="ENTER THE SENDER'S EMAIL PASSWORD">
             
            </div>
              <div class="col-lg-12">
              <label>ATTACHMENT</label>
               <input type="file" name="bukti" class="form-control">
              </select>
            </div>
            <br><br><br><br>

             <div class="col-lg-12">
        <button type="submit" class="btn btn-primary" id="sub">Submit</button>
      </div>

        </div>
     

        </div>
 </form>
</div>
</div>
@endsection
 @push('script')
 <script type="text/javascript">
   $(document).ready(function() {
            $('.check').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
    $('#pic').change(function(){
              var username = $(this).val();
              var url = "{{ url('project/get_email') }}";
               $.ajax({
                  type: "GET",
                  url: url,
                  data: {
                  'username': username,
                  },
                   success: function(data) {
                    console.log(data);
                 
                       document.getElementById('email').value = data;
                    
                   
                   }
                  }); 
            });
 $('#sub').click(function(){
       var issue = $('#issue').val();
     if (issue =null) {alert('kosong');}
 });
 </script>
 @endpush
 