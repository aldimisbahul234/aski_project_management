@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')


 <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          
                <h4 class="card-title">REGISTER DATA PART</h4>
             
          
               <a class="btn btn-danger btn-sm" href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project) }}"> Back</a>
             
        </div>
        <div class="card-body">
        <form class="form-participant" action="{{ url('project/add_rfq') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_project" value="{{ $project->id_project }}">
            <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">PART NUMBER</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="part_number"
                        placeholder="PART NUMBER"
                      />
                    </div>
                  </div>
                </div>
              </div>
          </div>
              <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">PART NAME</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="part_name"
                        placeholder="PART NAME"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

              <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">PROCESS</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                    
                      <select class="form-control" name="id_process">
                      	<option>-Pilih Process-</option>
                      	@foreach($process as $process)
                      	<option value="{{ $process->id_process }}">{{ $process->process_name }}</option>
                      	@endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
          </div>

           <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">QTY ORDER</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="number"
                        id="fname-icon"
                        class="form-control"
                        name="qty_order"
                        placeholder="QTY"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

                <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">LIFE TIME</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="lifetime"
                        placeholder="1 tahun | 1 bulan | 1 hari"
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

               <div class="row">
              <div class="col-12">
                <div class="form-group row">
                  <div class="col-sm-3 col-form-label">
                    <label for="fname-icon">REMARKS</label>
                  </div>
                  <div class="col-sm-9">
                    <div class="input-group input-group-merge">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="user"></i></span>
                      </div>
                      <input
                        type="text"
                        id="fname-icon"
                        class="form-control"
                        name="remarks"
                        placeholder="REMARKS"
                        required
                      />
                    </div>
                  </div>
                </div>
              </div>
             </div>

              <div class="col-sm-9 offset-sm-3">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

@endsection
 