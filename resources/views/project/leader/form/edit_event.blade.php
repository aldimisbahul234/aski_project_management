@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')


 <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">EDIT EVENT DIE GO OF PROJECT {{ $project->project_name }}</h4> <a href="{{ url('project/display_project_detail/'.$IDcustomer->id_customer.'/'.$project->id_project) }}" class="btn btn-danger">BACK</a>
        </div>
        <div class="card-body">
       <form class="form-participant" action="{{ url('project/update_event/'.$event->id_event_project) }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_project" value="{{ $project->id_project }}">
             <input type="hidden" name="id_category_event_project" value="{{ $event->id_category_event_project }}">
            
            <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label for="first-name-vertical">START PLAN EVENT</label>
                  <input
                    type="date"
                    id="first-name-vertical"
                    class="form-control"
                    name="jadwal_event"
                    value="{{ $event->jadwal_event }}"
                  />
                </div>
              </div>
              
              <div class="col-6">
                <div class="form-group">
                  <label for="first-name-vertical">START ACTUAL EVENT</label>
                  <input
                    type="date"
                    id="first-name-vertical"
                    class="form-control"
                    name="actual_event"
                    value="{{ $event->actual_event }}"
                  />
                </div>
              </div>


               <div class="col-6">
                <div class="form-group">
                  <label for="first-name-vertical">FINISH PLAN EVENT</label>
                  <input
                    type="date"
                    id="first-name-vertical"
                    class="form-control"
                    name="finish_plan_event"
                    value="{{ $event->finish_plan_event }}"
                  />
                </div>
              </div>
              
              <div class="col-6">
                <div class="form-group">
                  <label for="first-name-vertical">FINISH ACTUAL EVENT</label>
                  <input
                    type="date"
                    id="first-name-vertical"
                    class="form-control"
                    name="finish_actual_event"
                    value="{{ $event->finish_actual_event }}"
                  />
                </div>
              </div>

              <div class="col-12">
                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
              </div>
            </div>
          </form>
         

             
        </div>
      </div>
    </div>

 <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">DATA HISTORY EVENT PROJECT DIE GO</h4>
        </div>
        <div class="card-body">

            <table class="table rfq" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>START PLAN CURRENT</th>
                    <th>FINISH PLAN CURRENT</th>
                    <th>START ACTUAL CURRENT</th>
                    <th>FINISH ACTUAL CURRENT</th>
                    <th>START PLAN CHANGE</th>
                    <th>FINISH PLAN CHANGE</th>
                    <th>START ACTUAL CHANGE</th>
                    <th>FINISH ACTUAL CHANGE</th>
                    <th>UPDATE DATE</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($histori as $histori)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $histori->jadwal_event_current }}</td>
                   <td>{{ $histori->finish_plan_event_current }}</td>
                    <td>{{ $histori->start_actual_event_current }}</td>
                   <td>{{ $histori->finish_actual_event_current }}</td>
                   <td>{{ $histori->jadwal_event_change }}</td>
                   <td>{{ $histori->finish_plan_event_change }}</td>
                   <td>{{ $histori->start_actual_event_change }}</td>
                   <td>{{ $histori->finish_actual_event_change }}</td>
                  <td>{{ $histori->update_on }}</td>    
                 @endforeach
                 </tbody>
               </table>

        </div>
      </div>
    </div>
@endsection
 