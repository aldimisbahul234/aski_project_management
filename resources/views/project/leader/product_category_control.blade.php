
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$product->id_product) }}">Project Detail {{ $project->project_name }}</a></li>
                <li class="breadcrumb-item"><a href="">Product Detail {{ $product->part_name }}</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>


@if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">CATEGORY CONTROL</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
            <div class="card-header">
              @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
              <button class="btn btn-primary add">ADD NEW</button>
              @endif
            </div>
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th width="20%">CATEGORY CONTROL</th>
              <th width="15%">OPEN ITEM CONTROL</th>
              <th width="30%" style="text-align: center">STATUS</th>
              <th width="10%">REMOVE</th>
            </tr>
          	</thead>
           <tbody>
            @foreach($datas as $data)
            <tr>
                <td>
                  <h3>{{ $loop->iteration }}.{{ $data['name'] }}</h3>
                  <p>Plan Progress : {{ $data['result_plan'] }} %</p>
                  <p>Actual Progress : {{ $data['result_actual'] }} % {{ $data['get_notif'] }}</p>
                </td>
                <td>
                  <a href="{{ url('project/get_item_list/'.$data['part_number'].'/'.$data['id_category'].'/'.$id_project) }}" class="btn btn-danger">{{ count($data['open_item']) }} Item Controll</a>

                </td>
                <td>
                  @if($data['result_actual'] ==100 || $data['result_actual'] >100)
                  <div class="row">
                    <div class="col-lg-10">
                         <a href="{{ url('project/item_control_detail/'.$id_project.'/'.$data['id_category'].'/'.$data['part_number'].'/'.$id_process) }}">
                          <div class="progress progress-bar-success">
                              <div
                                class="progress-bar progress-bar-striped progress-bar-animated"
                                role="progressbar"
                                aria-valuenow="100"
                                aria-valuemin="100"
                                aria-valuemax="100"
                                style="width: 100%"
                              ></div>
                            </div>
                          </a>
                    </div>
                    <div class="col-lg-2">
                      @if($data['get_notif'] > 0)
                        <span class="badge badge-danger" style="background-color: red;"> new</span>
                      @endif
                    </div>
                  </div> 
                  @else
                  <div class="row">
                    <div class="col-lg-10">
                        <a href="{{ url('project/item_control_detail/'.$id_project.'/'.$data['id_category'].'/'.$data['part_number'].'/'.$id_process) }}">
                          <div class="progress progress-bar-danger">
                              <div
                                class="progress-bar progress-bar-striped progress-bar-animated"
                                role="progressbar"
                                aria-valuenow="100"
                                aria-valuemin="100"
                                aria-valuemax="100"
                                style="width: {{$data['result_actual']}}%"
                              ></div>
                            </div>
                          </a>
                    </div>
                    <div class="col-lg-2">
                        @if($data['get_notif'] > 0)
                          <span class="badge badge-danger" style="background-color: red;"> new</span>
                        @endif
                    </div>
                  </div>
                  
                  @endif
                </td>
                <td>
                  @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                  <a href="{{ url('project/remove_category_control/'.$data['id_category']) }}" class="btn btn-danger"><i data-feather="trash"></i> Remove</a>
                  @else
                  <button class="btn btn-danger" disabled><i data-feather="trash"></i> Remove</button>
                  @endif
                </td>
            </tr>
            @endforeach
           </tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>

@endsection
@include('project.leader.modal_add_category')
@push('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true, 
            });
             $('.add').click(function(){
                  $('#modal_add_category').modal('show');
                  $('.modal-title').html('Add Category');
                });
          });
    </script>
@endpush