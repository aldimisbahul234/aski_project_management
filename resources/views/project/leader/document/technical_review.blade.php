@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
<div class="col-lg-12">
  <div class="card">

     
    
<div class="card-body">
  <a class="btn btn-danger" href="{{ url('project/display_project_detail/'.$project->id_customer.'/'.$project->id_project) }}">back</a>
<center><h2>TECHNICAL REVIEW HISTORY {{$product->part_name}}</h2></center><br><br>
<table class="datatables-basic table check">
     <thead>
      
      <tr>
        <th width="2%">NO</th>
        <th width="5%">TECHNICAL REVIEW NAME</th>
        <th width="15%">LAST UPDATE</th>
        <th width="5%">UPDATE BY</th>
        <th width="5%">EDIIT</th>
        <th width="5%">HAPUS</th>
        <th width="5%">DOWNLOAD</th>
    
      </tr>
      </thead>
     <tbody>
     @foreach($data as $val)
     <tr>
       <td>{{ $loop->iteration }}</td>
       <td>{{ $val->tr_name }}</td>
       <td>{{ $val->last_update_tr }}</td>
       <td>{{ $val->update_tr_by }}</td>
       <td><a href="{{ url('project/edit_technical_review/'.$val->no_document.'/'.$val->part_number.'/'.$val->id_tr) }}" class="btn btn-info btn-sm"> EDIT</a></td>
       <td><a href="{{ url('project/delete_technical_review/'.$val->no_document.'/'.$val->part_number) }}" class="btn btn-danger btn-sm"> HAPUS</td>
       <td><a href="{{ url('project/print_technical_review/'.$project->id_project.'/'.$val->no_document.'/'.$val->part_number) }}" class="btn btn-success btn-sm"> DOWNLOAD</td>
     </tr>
     @endforeach
     </tbody>
   </table>
 </div>
</div>

@endsection