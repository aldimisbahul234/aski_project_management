<script type="text/javascript">
Highcharts.chart('grafik_event_detail', {

    title: {
        text: 'Date Today :'
    },
    
    yAxis: {
        uniqueNames: true,
        
    },
    credits:{
        enabled : false
    },
    navigator: {
        enabled: true,
        liveRedraw: true,
        series: {
            type: 'gantt',
            pointPlacement: 0.5,
            pointPadding: 0.25
        },
        yAxis: {
            min: 0,
            max: 3,
            reversed: true,
            categories: []
        }
    },
    scrollbar: {
        enabled: true
    },
    rangeSelector: {
        enabled: true,
        selected: 7
    },
    exporting : {
        allowHTML: true,
        printMaxWidth: 60000
    },
    plotLines : {
        color : "blue"
    },
    series: [{
        name: 'PRODUCT',
        data: []
    },]
});

		</script>