<script type="text/javascript">
Highcharts.chart('grafik_budget', {
    chart: {
        type: 'column'
    },
    yAxis: {
        title: {
            text: 'Percentage (%)'
        }
    },
    credits:{
        enabled : false
    },
    title: {
        text: '<b>GRAFIK BUDGET COST CAPEX PLAN & ACTUAL'
    },
    tooltip: {
        valueSuffix: '%'
    },
     plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },
    xAxis: {
        categories: [
                'Plan Budget','Actual Budget'
        ]
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'ACTUAL BUDGET',
        color: '#52b251',
        data: [<?php echo $total_plan_capex;?>]
    }, {
        name: 'PLAN BUDGET',
        color: '#73a2ff',
        data: [<?php echo $total_actual_capex;?>]
    }]
});

Highcharts.chart('grafik_cost_budget', {
    chart: {
        type: 'bar'
    },
    title: {
        text: '<b>GRAFIK BUDGET COST OPEX PLAN & ACTUAL'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories: [ 
                '<?php echo $part_name_opex_get;?>'
                ]
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Budget Product'
        }
    },
    legend: {
        reversed: true
    },
    tooltip: {
        valueSuffix: ' Millions'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                
            }
        }
    },
    series: [{
        name: 'ACTUAL BUDGET',
        color: '#52b251',
        data: [<?php echo $actual_budget_opex;?>]
    }, {
        name: 'PLAN BUDGET',
        color: '#73a2ff',
        data: [<?php echo $plan_budget_opex;?>]
    }]
});
</script>