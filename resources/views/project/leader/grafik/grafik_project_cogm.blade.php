<script type="text/javascript">
Highcharts.chart('grafik_cogm', {
    chart: {
        type: 'column'
    },
    yAxis: {
        title: {
            text: 'Percentage (%)'
        }
    },
    credits:{
        enabled : false
    },
    title: {
        text: '<b>GRAFIK PROCESS COST PROJECT'
    },
    tooltip: {
        valueSuffix: '%'
    },
     plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },
    xAxis: {
        categories: [
                'Plan Budget','Actual Budget'
        ]
    },
    credits: {
        enabled: false
    },
    series: [
   <?php foreach ($getDatas as $key => $getDatas): ?>  
    {
    
        color: '#52b251',
        name:'<?php echo$getDatas['product']; ?>',
        drilldown: "1",
        data: [<?php echo$getDatas['detail']['process']; ?>]
    },
    <?php endforeach ?>
    ]
});
Highcharts.chart('grafik_cogm2', {
    chart: {
        type: 'column'
    },
    yAxis: {
        title: {
            text: 'Percentage (%)'
        }
    },
    credits:{
        enabled : false
    },
    title: {
        text: '<b>GRAFIK DEPRECIATION EVENT PROJECT'
    },
    tooltip: {
        valueSuffix: '%'
    },
     plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },
    xAxis: {
        categories: [
                'Plan Budget','Actual Budget'
        ]
    },
    credits: {
        enabled: false
    },
     series: [
   <?php foreach ($getDatasi as $key => $getDatas): ?>  
    {
    
        color: '#52b251',
        name:'<?php echo$getDatas['product']; ?>',
        drilldown: "1",
        data: [<?php echo$getDatas['detail']['depreciation']; ?>]
    },
    <?php endforeach ?>
    ]
});
</script>