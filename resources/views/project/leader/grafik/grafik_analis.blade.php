<script type="text/javascript">
Highcharts.chart('analis_donut', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: '{{ $total }} Item Control',
        align: 'center',
        verticalAlign: 'middle',
        y: 15
    },
    credits:{
        enabled : false
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -40,
                format: '{point.y}',
                style: {
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize : '20px'
                }
            },
            startAngle: 100,
            endAngle: 100,
            center: ['50%', '50%'],
            size: '100%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Item Control',
        innerSize: '50%',
        data: [
            ['TODAY', <?php echo $counttoday;?>],
            ['NEXT ITEM CONTROL', <?php echo $countnext;?>],
            ['IN PROGRESS', <?php echo $countprogress;?>],
            ['COMPLETED', <?php echo $countcomplate;?>],
            ['DELAY', <?php echo $countdelay;?>]
        ],
                colors: [
                '#69afff', 
                'black', 
                'orange', 
                '#2ec74a', 
                '#db4621'
                ]
    }]
});
</script>
<script type="text/javascript">
// Create the chart
Highcharts.chart('analis_bar', {
    chart: {
        type: 'column'
    },
    title: false,
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Item Control'
        }

    },
    credits:{
        enabled : false
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} Item Control'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    series: [
        {
            name: "Item Control",
            colorByPoint: true,
            data: [
                {
                    name: "Today",
                    y:<?php echo $counttoday;?>,
                    color : '#69afff',
                    drilldown: "Today"
                },
                {
                    name: "Next Item Control",
                    y: <?php echo $countnext;?>,
                    color : 'black',
                    drilldown: "Next"
                },
                {
                    name: "In Progress",
                    y: <?php echo $countprogress;?>,
                    color : 'orange',
                    drilldown: "Progress"
                },
                {
                    name: "Completed",
                    y: <?php echo $countcomplate;?>,
                    color : '#2ec74a',
                    drilldown: "Completed"
                },
                {
                    name: "Delay",
                    y: <?php echo $countdelay;?>,
                    color : '#d92323',
                     drilldown: "Delay"
                }
            ]
        }
    ],
drilldown: {
    series: [

      {
        name: "Delay",
        id: "Delay",
        data: [
         <?php foreach ($getdelay as $key => $delay): ?>   
          [
            "<?php echo $delay['part_number'].'_'.$key++;?>",
             <?php echo $delay['plan_progress_item'];?>
          ],
         <?php endforeach ?>
        ]
      },
      {
        name: "Completed",
        id: "Completed",
        data: [
         <?php foreach ($getcomplate as $key => $complate): ?>   
          [
            "<?php echo $complate['part_number'].'_'.$key++;?>",
             <?php echo $complate['plan_progress_item'];?>
          ],
         <?php endforeach ?>
        ]
      },
      {
        name: "Progress",
        id: "Progress",
        data: [
         <?php foreach ($getprogress as $key => $progress): ?>   
          [
            "<?php echo $progress['part_number'].'_'.$key++;?>",
             <?php echo $progress['plan_progress_item'];?>
          ],
         <?php endforeach ?>
        ]
      },
    {
        name: "Next",
        id: "Next",
        data: [
         <?php foreach ($getnext as $key => $next): ?>   
          [
            "<?php echo $next['part_number'].'_'.$key++;?>",
             <?php echo $next['plan_progress_item'];?>
          ],
         <?php endforeach ?>
        ]
      },
    {
        name: "Today",
        id: "Today",
        data: [
         <?php foreach ($gettoday as $key => $today): ?>   
          [
            "<?php echo $today['part_number'].'_'.$key++;?>",
             <?php echo $today['plan_progress_item'];?>
          ],
         <?php endforeach ?>
        ]
      }
 
    ]
  }
});
        </script>

                <script type="text/javascript">
Highcharts.chart('analis_progress', {
    chart: {
        type: 'bar'
    },
    title: false,
    credits:{
        enabled : false
    },
    xAxis: {
        categories: [
            <?php if($item_names > 0): ?>
            <?php foreach ($item_names as $key => $value): ?>
             '<?php echo $value['item_name'];?>',
            <?php endforeach ?>
             <?php endif ?>
        ]
    },
    yAxis: {
        min: 0,
        max:100,
        title: {
            text: 'PROGRESS ACTUAL'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}%</b><br/>'
    },
    series: [ 
        {
        name: 'Complete',
        color : "green",
        data: [ 
                <?php if($item_names > 0): ?>
              <?php foreach ($item_actual as $key => $actual): ?>
             <?php echo $actual['actual_progress_item'];?>,
            <?php endforeach ?>
             <?php endif ?>
         ]
    }]
});
        </script>