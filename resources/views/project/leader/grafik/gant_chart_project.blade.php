<!DOCTYPE html>
<html>
<head>
    <title></title>
        <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.min.css')}}">
     <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/bordered-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/themes/semi-dark-layout.min.css')}}">
</head>
<body>
<form>
   <div class="row">
     <div class="col-sm-6">
        <div class="card">
            <div class="btn-group">
                <button
                class="btn btn-info"
                type="button"
              >
              @if($part_number =='null')
            SELECT PRODUCT
            @else
            {{ $product->part_name }}
            @endif
              </button>
              <button
                class="btn btn-primary dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
               SELECT PRODUCT
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
               @foreach($get_all_product as $get_all_product)
                 <a class="dropdown-item" href="{{ url('gant_chart_project/'.$id_project.'/'.$get_all_product->part_number) }}">{{ $get_all_product->part_name }}</a>
              @endforeach
               
                
              </div>
            </div>
        </div>
    </div>
  </div>
</form>
@if($part_number !='null')
<div id="container"></div>
@endif
</body>
<script src="https://code.highcharts.com/gantt/highcharts-gantt.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
  <script src="{{asset('app-assets/vendors/js/vendors.min.js')}}"></script>
  @if($product ==true)
<script type="text/javascript">
    Highcharts.ganttChart('container', {

 title: {
        text: 'Date today : <?php echo$date; ?> <br>Gantt chart product <?php echo$product->part_name; ?>'
    },

    yAxis: {
        uniqueNames: true,
        
    },
    credits:{
        enabled : false
    },

    navigator: {
        enabled: true,
        liveRedraw: true,
        series: {
            type: 'gantt',
            pointPlacement: 0.5,
            pointPadding: 0.25
        },
        yAxis: {
            min: 0,
            max: 3,
            reversed: true,
            categories: []
        }
    },
    scrollbar: {
        enabled: true
    },
    rangeSelector: {
        enabled: true,
        selected: 7
    },
    tooltip : {
        enabled : false
    },
  series: [{
    name: 'Project 1',
    data: [
          
    {
      start: Date.UTC(2021, 1, 1),
      end: Date.UTC(2022, 12, 1),
      completed: 1.0,
     
      name: '<?php echo $product->part_name;?> <br> <p>--->Project leader : <?php echo$firstname; ?> <?php echo$lastname; ?>',
   
      id:'1'
    }, 
   
     
    <?php foreach ($get_category as $key => $get_category_value): ?>   
     {
      id:'<?php echo $get_category_value['id_category'];?>',
      start: Date.UTC(<?php echo $get_category_value['plan_start_year'];?>, <?php echo $get_category_value['plan_start_month'];?>, <?php echo $get_category_value['plan_start_day'];?>),
      end: Date.UTC(<?php echo $get_category_value['plan_finish_year'];?>, <?php echo $get_category_value['plan_finish_month'];?>, <?php echo $get_category_value['plan_finish_day'];?>),
      completed: <?php 
      if ($get_category_value['count_progress'] > 100) {
         echo "1.0";
      }elseif($get_category_value['count_progress'] >=10 || $get_category_value['count_progress'] < 100){
         echo "0.";echo $get_category_value['count_progress'];      
      }else{
         echo "0";
      }
      

      ?>,
      name: '<?php echo $get_category_value['category_name'];?> <br> <p>Plan start item : <?php echo $get_category_value['plan_start_item'];?> - Plan finish item : <?php echo $get_category_value['plan_finish_item'];?></p> <br> <p>Actual start item : <?php echo $get_category_value['actual_start_item'];?> - Acutal finish item<?php echo $get_category_value['actual_finish_item'];?></p>',
      parent: '1',
      collapsed : true
    },
     <?php foreach ($get_category_value['get_item'] as $key => $value): ?> 
     {
      id:'<?php echo $value->id_control;?>', 
      completed: 100,
      name: '<?php echo $value->item->item_name;?> <br> <p>Pic item : <?php echo $value->pic_item;?></p>',
      parent: '<?php echo $get_category_value['id_category'];?>',
      collapsed : true
    },
     {
     
      completed: 100,
      name: '<p>Plan start item : <?php echo $value->plan_start_item;?> - Plan finish item : <?php echo $value->plan_finish_item;?></p> <br> <p> Actual start item :<?php echo $value->actual_start_item;?> - Actual finish item : <?php echo $value->actual_finish_item;?></p>',
      parent: '<?php echo $value->id_control;?>',
      collapsed : true
    },
    <?php endforeach ?>
    <?php endforeach ?>
    ]
  }]
});
</script>
@endif
</html>