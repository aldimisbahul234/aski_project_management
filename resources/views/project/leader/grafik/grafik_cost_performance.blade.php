


<script type="text/javascript">
Highcharts.chart('grafik_cost', {
    chart: {
        type: 'column'
    },
    yAxis: {
        title: {
            text: 'Percentage (%)'
        }
    },
    credits:{
        enabled : false
    },
    title: {
        text: '<b>Grafik Cost Performance Index COGM'
    },
    tooltip: {
        valueSuffix: '%'
    },
     plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },
    xAxis: {
        categories: [
                <?php foreach ($get_performance as $key => $get_performance): ?>
                    '<?php echo $get_performance['get_part_name'];?>',
                <?php endforeach ?>
        ]
    },
    credits: {
        enabled: false
    },
    
});
Highcharts.chart('grafik_cost_budget', {
    chart: {
        type: 'bar'
    },
    title: {
        text: '<b>Grafik Cost Performance Index Project Budget'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories: [ 
                <?php foreach ($product_name as $key => $product_name): ?>
                    '<?php echo $product_name['product_name'];?>',
                <?php endforeach ?>
                ]
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Budget Product'
        }
    },
    legend: {
        reversed: true
    },
    tooltip: {
        valueSuffix: ' Millions'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                
            }
        }
    },
    series: [{
        name: 'ACTUAL BUDGET',
        color: '#52b251',
        data: [<?php echo $actual_budget;?>]
    }, {
        name: 'PLAN BUDGET',
        color: '#73a2ff',
        data: [<?php echo $plan_budget;?>]
    }]
});
</script>