@extends('layouts/root')
@section('main')
<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
        <table class="datatables-basic table" id="datatable">
           <thead>
            <tr>
              <th>ID</th>
              <th>ACTIVITY</th>
              <th>UPDATE ON</th>
              <th>PIC USERNAME</th>
              <th>PROJECT</th>
              <th>PRODUCT</th>
              <th>DESCRIPTION</th>
              <th>HISTORY DETAIL</th>
              <th>HAPUS</th>
            </tr>
           </thead>
           <tbody>
             @foreach($data as $value)
              <tr>
                <td>{{ $value['id_escalation'] }}</td>
                <td>{{ $value['escalation_name'] }}</td>
                <td>{{ $value['tgl_escalation'] }}</td>
                <td>{{ $value['pic'] }}</td>

                <td>{{ $value['part_name'] }}</td>
                <td>{{ $value['project'] }}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
             @endforeach
           </tbody>
</table>
    </div>
  </div>
</div>


@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
            });
          });
    </script>
@endpush