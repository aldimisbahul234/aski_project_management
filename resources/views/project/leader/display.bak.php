
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
                 <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Type Project {{$project->project_name}}</a></li>
                  <li class="breadcrumb-item"><a href="">Project Detail</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
<!-- header -->
    <div class="col-md-3 mb-2 mb-md-0">
      <ul class="nav nav-pills flex-column nav-left">
        <!-- general -->
        <li class="nav-item">
          <a
            class="nav-link active"
            id="account-pill-general"
            data-toggle="pill"
            href="#account-vertical-general"
            aria-expanded="true"
          >
            <i data-feather="info" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">INFO GENERAL PROJECT</span>
          </a>
        </li>

       <!-- event -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="event"
            data-toggle="pill"
            href="#account-vertical-event"
            aria-expanded="true"
          >
            <i data-feather="calendar" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">EVENT</span>
          </a>
        </li>

         <!-- rpq -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="rpq"
            data-toggle="pill"
            href="#account-vertical-rpq"
            aria-expanded="true"
          >
            <i data-feather="message-square" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">RPQ</span>
          </a>
        </li>

        <!-- technical -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="technical"
            data-toggle="pill"
            href="#account-vertical-technical"
            aria-expanded="true"
          >
            <i data-feather="briefcase" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">TECHNICAL REVIEW</span>
          </a>
        </li>

        <!-- struckture -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="struckture"
            data-toggle="pill"
            href="#account-vertical-struckture"
            aria-expanded="true"
          >
            <i data-feather="settings" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">STRUCKTURE TEAM</span>
          </a>
        </li>

          <!-- product -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="product"
            data-toggle="pill"
            href="#account-vertical-product"
            aria-expanded="true"
          >
            <i data-feather="box" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">PRODUCT</span>
          </a>
        </li>

         <!-- quality -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="quality"
            data-toggle="pill"
            href="#account-vertical-quality"
            aria-expanded="true"
          >
            <i data-feather="check" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">QUALITY</span>
          </a>
        </li>

         <!-- document -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="document"
            data-toggle="pill"
            href="#account-vertical-document"
            aria-expanded="true"
          >
            <i data-feather="folder" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">DOCUMENT</span>
          </a>
        </li>

         <!-- cost -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="cost"
            data-toggle="pill"
            href="#account-vertical-cost"
            aria-expanded="true"
          >
            <i data-feather="clipboard" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">COST</span>
          </a>
        </li>

         <!-- analistyc -->
       <li class="nav-item">
          <a
            class="nav-link"
            id="analistyc"
            data-toggle="pill"
            href="#account-vertical-analistyc"
            aria-expanded="true"
          >
            <i data-feather="bar-chart" class="font-medium-3 mr-1"></i>
            <span class="font-weight-bold">ANALISTYC</span>
          </a>
        </li>

      </ul>
    </div>
    <!--/ left menu section -->


    <!-- right content section -->
    <div class="col-md-9">
      <div class="card">
          <div class="card-header">
            <h2 style="text-align: right;">PROJECT DETAIL {{ $project->project_name }}</h2>
          </div>
        <div class="card-body">
        
          <div class="tab-content">


            <!-- general tab -->
            <div
              role="tabpanel"
              class="tab-pane active"
              id="account-vertical-general"
              aria-labelledby="account-pill-general"
              aria-expanded="true"
            >
           
              <table class="table">
                 
                    
                    <tr>
                      <th>PROJECT NAME</th>  
                      <th>{{ $project->project_name }}</th> 
                      <th></th>      
                    </tr>
                    <tr>
                      <th>CUSTOMER NAME</th>    
                      <th>{{ $customer->customer_name }}</th>  
                      <th></th>   
                    </tr>
                    @foreach($summary as $value)
                    <tr>
                      <th>{{ $value->caption_info }}</th>
                      <th>{{ $value->description }}</th>
                      <th></th>
                    </tr>
                    @endforeach
                   
              </table>
              <div class="card-footer">
                 <a href="{{ url('project/report_general/'.$project->id_project) }}" class="btn btn-success"><i data-feather='file-plus'></i>REPORT</a>
                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#general_modal"><i data-feather='edit-3'></i>EDIT INFO GENERAL PROJECT</button>
                 <a href="" class="btn btn-danger"><i data-feather='trash'></i>DELETE ALL INFO GENERAL PROJECT</a>
               </div>
            </div>
            @include('project.leader.modal.modal_general')
            <!--/ general tab -->
            <!-- event tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-event"
              aria-labelledby="event"
              aria-expanded="true"
            >

            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#event_modal"><i data-feather='file-plus'></i>ADD EVENT</button>
           <a href="{{ url('project/detail_event_grafik') }}" class="btn btn-success"><i data-feather='activity'></i> VIEW GRAFIK EVENT</a>
              <table class="table event" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>EVENT</th>
                    <th>EVENT SCHADULE</th>
                    <th>EDIT</th>
                    <th>REMOVE</th>
                  </tr>
                  </thead>
                 <tbody>
                  @foreach($event as $even)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $even->category_name->event_project_name }}</td>
                    <td>{{ $even->jadwal_event }}</td>
                    <td><a class="btn btn-info btn-sm" href="{{ url('project/edit_event/'.$even->id_event_project) }}">EDIT</a></td>
                    <td><a href="{{ url('project/delete_event/'.$even->id_event_project) }}" class="btn btn-danger btn-sm">Remove</a></td>
                  </tr>
                  @endforeach
                 </tbody>
               </table>
 @include('project.leader.modal.modal_event')

            </div>
            <!-- end -->

            <!-- rpq tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-rpq"
              aria-labelledby="rpq"
              aria-expanded="true"
            >
           <button type="button" class="btn btn-success"><i data-feather='file'></i>RPQ DOCUMENT</button>
           <a href="{{ url('project/create_rfq/'.$project->id_project) }}" class="btn btn-info"><i data-feather='chevrons-down'></i>REGISTER DATA PART</a>
           <a href="{{ url('project/export_rfq/'.$project->id_project) }}" class="btn btn-warning"><i data-feather='chevrons-down'></i>REPORT</a>
             <table class="table table-striped table-hover nowrap rfq" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROCESS</th>
                    <th>QTY ORDER</th>
                    <th>LIFE TIME</th>
                    <th>REMARKS</th>
                    <th></th>
                    <th></th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_rfq as $rfq)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $rfq['part_number'] }}</td>
                   <td>{{ $rfq['part_name'] }}</td>
                  <td>{{ $rfq['process_name'] }}</td>
                  <td>{{ $rfq['qty'] }}</td>
                  <td>{{ $rfq['lifetime'] }}</td>
                  <td>{{ $rfq['remarks'] }}</td>
                  <td><a class="btn btn-info btn-sm" href="{{ url('project/edit_rfq/'.$rfq['part_number']) }}">edit</a></td>
                  <td><a href="" class="btn btn-danger btn-sm">hapus</a></td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
            </div>
            <!-- end  -->

             <!-- technical tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-technical"
              aria-labelledby="technical"
              aria-expanded="true"
            >
           
               <table class="table technical" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>NEW TECHNICAL REVIEW</th>
                    <th>ATTACHMENT</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_technical as $technical_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $technical_value['part_number'] }}</td>
                   <td>{{ $technical_value['part_name'] }}</td>
                  <td><a class="btn btn-primary" href="{{ url('project/add_technical_review/'.$project->id_project.'/'.$technical_value['part_number']) }}"><i class="fa fa-book"></i>FORM TECHNICAL REVIEW</a></td>
                  <td>
                    @if($technical_value['technical_review'] > 0)
                    <a class="btn btn-warning" href="{{ url('project/technical_review/'.$project->id_project.'/'.$technical_value['part_number']) }}"><i class="fa fa-check"></i>DOCUMENT TECHNICAL REVIEW</a>
                    @else
                    <button class="btn btn-warning" disabled><i class="fa fa-close"></i>NOT AVAILABLE</button>
                    @endif

                  </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

            </div>
            <!-- end  -->

              <!-- struckture tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-struckture"
              aria-labelledby="struckture"
              aria-expanded="true"
            >
            <h3>Project Manager : {{ $getmanager->firstname }} {{ $getmanager->lastname }}</h3>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#all_member"><i data-feather='chevrons-down'></i>VIEW ALL MEMBER PROJECT</button>
            <a href="{{ url('project/add_part_die_go/'.$project->id_project) }}" class="btn btn-info"><i data-feather='file-plus'></i>ADD PRODUCT DIE GO</a>
           <table class="table table-striped table-hover nowrap struckture" id="datatable" style="width: 150%">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROJECT LEADER</th>
                    <th>PROCESS</th>
                    <th>TOTAL PIC</th>
                    <th>MEMBER DETAIL</th>
                    <th>ADD MEMBER</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_structure as $structure_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $structure_value['part_number'] }}</td>
                   <td>{{ $structure_value['part_name'] }}</td>
                  <td>@foreach($structure_value['get_leader'] as $val) <p>{{ $val->username }}</p> @endforeach</td>
                  <td>{{  $structure_value['process_name']  }}</td>
                  <td>{{  $structure_value['count_pic'] }}</td>
                  <td><a href="{{ url('project/detail_member/'.$project->id_project.'/'.$structure_value['part_number']) }}" class="btn btn-success"><i class="fa fa-eye">DETAIL</i></a></td>
          <td><a href="{{ url('project/add_new_member/'.$project->id_project.'/'.$structure_value['part_number'].'/'.$structure_value['id_process']) }}" class="btn btn-info"><i class="fa fa-plus">ADD NEW MEMBER</i></a></td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
            </div>
          
            <!-- end  -->

              <!-- product tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-product"
              aria-labelledby="product"
              aria-expanded="true"
            >
           
             <table class="table table-striped table-hover nowrap product" id="datatable" style="width: 200%">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROJECT LEADER</th>
                    <th>PLAN PROGRESS</th>
                    <th>ACTUAL PROGRESS</th>
                    <th>OPEN R.I.O</th>
                    <th>STATUS</th>
                    <th>REPORT</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_product as $product_value)
                 <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $product_value['part_number'] }}</td>
                  <td>{{ $product_value['part_name'] }}</td>
                  <td>
                  @foreach($product_value['get_leader'] as $val) 
                    {{ $val->username }}
                  @endforeach<br>
                  </td>
                  <td>{{ $product_value['result_plan'] }} %</td>
                  <td>{{ $product_value['result_actual'] }} %</td>
                  <td><a href="{{ url('project/issue/'.$project->id_project.'/'.$product_value['part_number']) }}" class="btn btn-danger btn-sm">R.I.O</a></td>
                  <td><a href="{{ url('project/product_category_control/'.$product_value['part_number'].'/'.$product_value['id_process_product'].'/'.$project->id_project) }}">
                  <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                  </a>
                </td>
                  <td><a href="" class="btn btn-success btn-sm">Report</a></td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
            </div>
            <!-- end  -->

              <!-- quality tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-quality"
              aria-labelledby="quality"
              aria-expanded="true"
            >
           
             <table class="table quality" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>CURRENT EVENT QUALITY</th>
                    <th>CURRENT TRIAL</th>
                    <th>ADJUSMENT VISUAL</th>
                    <th>ADJUSMENT DIMENSI</th>
                    <th>APPROVAL LINE</th>
                    
                    @if(Auth::user()->hak_akses =="quality")
                    <th>ADD</th>
                    <th>DETAIL</th>
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($quality as $quality)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $quality['part_number'] }}</td>
                   <td>{{ $quality['part_name'] }}</td>
                  <td><button class="btn btn-primary">{{ $quality['event_name'] }} <br><br> {{ $quality['current']}}</button></td>
                  <td>N/A</td>
                  <td >N/A</td>
                  <td>N/A</td>
                  <td>
                    @if(Auth::user()->hak_akses =="quality")
                      <div class="row">
                        <div class="col-lg-12">
                          <a href="{{ url('project/approval_line/'.$project->id_project) }}"><i data-feather='check-circle'></i></a> | 
                          <a href="{{ url('project/approval_line/'.$project->id_project) }}"><i data-feather='external-link'></i></a> |
                          <a href="{{ url('project/approval_line/'.$project->id_project) }}"><i data-feather='alert-circle'></i></a>
                        </div>
                        <div class="col-lg-12">
                          <button class="btn-primary btn-sm" style="width: 85%"> N\A </button>
                        </div>
                      </div>
                      @else
                       <div class="row">
                        <div class="col-lg-12">
                          <a><i data-feather='check-circle'></i></a> | 
                          <a><i data-feather='external-link'></i></a> |
                          <a><i data-feather='alert-circle'></i></a>
                        </div>
                        <div class="col-lg-12">
                          <button class="btn-primary btn-sm" style="width: 85%"> N\A </button>
                        </div>
                      </div>
                      @endif

                     
                  </td>
                  @if(Auth::user()->hak_akses =="quality")
                  <td>
                    <a href="{{ url('project/quality_add/'.$project->id_project) }}" class="btn btn-success"><i data-feather="edit"></i>Add</a>
                  </td>
                  @endif
                  <td>
                    <a href="{{ url('project/quality_detail/'.$project->id_project) }}" class="btn btn-success"><i data-feather="eye"></i>Detail</a>
                  </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

            </div>
            <!-- end  -->

              <!-- document tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-document"
              aria-labelledby="document"
              aria-expanded="true"
            >
           
             <table class="table document" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th></th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_document as $document_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $document_value['part_number'] }}</td>
                   <td>{{ $document_value['part_name'] }}</td>
                  <td style="background-color: red;color: white">
                    @if($document_value['document'] ==0)
                    <i data-feather='x-square'></i>
                    NOT AVAILABLE
                    @elseif($document_value['countnot'] > 0 || $document_value['document'] !=0)
                   <a href="{{ url('project/document/'.$project->id_project) }}" style="color: white"><i data-feather='alert-circle'></i>
                    NOT COMPLATE</a>
                    @else
                    <i data-feather='check-square'></i>
                    COMPLATE
                    @endif
                  </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

            </div>
            <!-- end  -->

              <!-- cost tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-cost"
              aria-labelledby="cost"
              aria-expanded="true"
            >
            
<div class="col-lg-12">
  <div class="row">

            <div class="col-lg-4">
                <div class="card-box bg-danger">
                    <div class="inner">
                        <h3 style="color: white"> COGM </h3>
                        <p style="color: white"> {{ $cogm }} COGM </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    <a href="{{ url('project/cogm/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
             <div class="col-lg-4">
                <div class="card-box bg-danger">
                    <div class="inner">
                        <h3 style="color: white"> PROJECT BUDGET </h3>
                        <p style="color: white"> {{ $budget }} Item Invesment </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </div>
                    <a href="{{ url('project/project_budget/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
             <div class="col-lg-4">
                <div class="card-box bg-danger">
                    <div class="inner">
                        <h3 style="color: white">COST</h3>
                        <p style="color: white">PERFORMACE INDEX</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <a href="{{ url('project/cost_performance/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
  </div>
</div>

            
            </div>
            <!-- end  -->

              <!-- analistyc tab -->
             <div
              role="tabpanel"
              class="tab-pane"
              id="account-vertical-analistyc"
              aria-labelledby="analistyc"
              aria-expanded="true"
            >
           
             <table class="table analistyc" id="datatable">
                 <thead>
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>ANALISTYC</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_analistyc as $analistyc_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $analistyc_value['part_number'] }}</td>
                   <td>{{ $analistyc_value['part_name'] }}</td>
                  <td><a href="{{ url('project/analis/'.$project->id_project.'/'.$analistyc_value['part_number'].'/'.'all') }}" class="btn btn-success btn-sm"><i data-feather='activity'></i> SEE DETAIL</td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
            </div>
            <!-- end  -->

          </div>
        </div>
      </div>
    </div>
  @include('project.leader.modal.modal_all_member')
  @endsection
  @push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.event').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
         $(document).ready(function() {
            $('.rfq').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true, 
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true, 
            });
          });
          $(document).ready(function() {
            $('.struckture').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
            });
          });
           $(document).ready(function() {
            $('.product').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
                
            });
          });
            $(document).ready(function() {
            $('.all_member').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
                
            });
          });
            $(document).ready(function() {
            $('.analistyc').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });
          });

             $(document).ready(function() {
            $('.quality').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                  "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,  
                
            });
          });
               $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });

           var i=1;  
      $('#tambah').click(function(){
           i++;  
          

         $('#add').append('<div class="row"  id="row'+i+'">'+ 
    '<div class="col-lg-6">'+
        '<div class="form-group">'+
            '<a id="'+i+'" class="btn_remove delete'+i+'">remove</a>'+
            '<input type="text"  class="form-control" placeholder="caption" name="caption_info[]=" required />'+
        '</div>'+
    '</div>'+
        '<div class="col-lg-6">'+
            '<div class="form-group">'+
            '<label></label>'+
                '<input type="text" class="form-control" placeholder="description" name="description[]" required />'+
                '</div>'+
            '</div>'+
        '</div>');  
      });  
    $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
            $('.delete'+button_id+'').remove();  
      });  
    </script>
@endpush

  



