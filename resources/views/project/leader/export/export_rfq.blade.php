<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
</head>
<body>
	<center><h2> PT Astra Komponen Indonesia </h2></center>
	<center><small>Tanggal : {{ $datetime }}</small></center>
	<hr>
	<p><h4>SECTOR       : {{ $sector->product_name }}</h4></p>
	<p><h4>CUSTOMER     :{{ $customer->customer_name }}</h4></p>
	<p><h4>PROJECT NAME :{{ $project->project_name }}</h4></p>
<table id="customers">
	<thead>
		<tr>
		<th>NO</th>
		<th>PART NUMBER</th>
		<th>PART NAME</th>
		<th>PROCESS NAME</th>
		<th>QTY</th>
		<th>LIFE TIME</th>
		<th>REMARKS</th>
	</tr>
	</thead>
	<tbody>
		@foreach($data as $value)
		<tr>
			  	  <td>{{ $loop->iteration }}</td>
                  <td>{{ $value['part_number'] }}</td>
                  <td>{{ $value['part_name'] }}</td>
                  <td>{{ $value['process_name'] }}</td>
                  <td>{{ $value['qty'] }}</td>
                  <td>{{ $value['lifetime'] }}</td>
                  <td>{{ $value['remarks'] }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
</body>
</html>