@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
 @if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="col-lg-12">
	<div class="card">
	<div class="card-body">
    @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
    <button class="btn btn-primary modal_document"><i data-feather="edit"></i> Add document</button>
    @endif
    <a class="btn btn-danger float-right" href="{{ url('project/display_project_detail/'.$project->id_customer.'/'.$project->id_project) }}"> back</a>
		<table class="table document" id="datatable">
                 <thead>
                  
                  <tr>
                    <th style="text-align: center;">NO</th>
                    <th>READ</th>
                    <th style="text-align: center;">DOCUMENT NAME</th>
                    <th style="text-align: center;">PIC</th>
                    <th style="text-align: center;">STATUS DOCUMENT</th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                  </tr>
                  </thead>
                 <tbody>
                	@foreach($data as $value)
                  @php
                    $get_notif =App\Http\Models\Notification::where(['id_project'=>$value->id_project,'part_number'=>$value->part_number,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'document','document_code'=>$value->document_code])->count();
                  @endphp
                		<tr>
                			<td style="text-align: center;">{{ $loop->iteration }}</td>
                      <td>
                         @if($get_notif > 0) <a class="btn btn-primary btn-sm" href="{{ url('project/read_notif/'.'document'.'/'.$value->document_code) }}"> Read</a> @endif
                      </td>
                			<td style="text-align: center;">{{ $value->document_name }} @if($get_notif > 0)<span class="badge badge-danger">new update</span> @endif</td>
                			<td style="text-align: center;">{{ $value->pic }}</td>
                		
                			<td style="text-align: center;">
                        @if($value->status_document ==0)
                         <button class="btn btn-danger dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 96%;">NOT COMPLETE</button>
                        @elseif($value->status_document ==1)
                         <button class="btn btn-success dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 96%;">COMPLETE</button>
                        @else
                         <button class="btn btn-warning dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 96%;">NEED REVISION</button>
                        @endif
                       

                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ url('project/change_status_doc/'.$value->document_code.'/'.'1'.'/'.$value->id_project.'/'.$value->part_number) }}">COMPLATE</a>
                        <a class="dropdown-item" href="{{ url('project/change_status_doc/'.$value->document_code.'/'.'0'.'/'.$value->id_project.'/'.$value->part_number) }}">NOT COMPLATE</a>
                        <a class="dropdown-item" href="{{ url('project/change_status_doc/'.$value->document_code.'/'.'2'.'/'.$value->id_project.'/'.$value->part_number) }}">NEED REVISION</a>
                 
                 
                  </div>
                			</td>
                       <td style="text-align: right;">
                         <button class="btn btn-primary show_note" data-id="{{ $value->document_code }}" type="button"><i data-feather="edit"></i> add comment</button>
                         
                      </td>
                      <td>
                          <button class="btn btn-primary editDocument" data-id="{{ $value->document_code }}" type="button"><i data-feather="edit"></i> edit document</button>
                          
                      </td>
                      <td>
                         <button class="btn btn-primary upload_document" data-id="{{ $value->document_code }}" data-href="{{ $value->document_name }}"><i data-feather="upload"></i> upload</button>
                        
                      </td>
                      <td>
                          @if(!empty($value->filename))
                        <a href="{{ url('project/download_document/'.$value->filename) }}" class="btn btn-primary"><i data-feather="download"></i> download</a>
                        @else
                        <button class="btn btn-primary" disabled><i data-feather="download"></i> download</button>
                        @endif
                      </td>
                		</tr>
                	@endforeach
                 </tbody>
               </table>
	</div>
</div>
</div>


@endsection
@include('project.leader.modal_add_document')
@include('project.leader.modal_add_document_list')
@include('project.leader.modal_add_document_note')
<div class="modal fade text-left" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="{{ url('pic/upload_document_pic') }}" method="POST" id="addForm" enctype="multipart/form-data"> 
    @csrf
    <div class="modal-body">
     <span id="body"></span>
      <label class="form-label" id="document_name"> <span class="text-danger">*</span></label>
        <input type="file" class="form-control" name="filename" id="filename">

    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-success">Upload</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</form>
  </div>
</div>
</div>
</div>

@push('script')
<script type="text/javascript">
	 $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
              $(".upload_document").click(function(e){
            e.preventDefault();
             var name  = $(this).attr('data-href');
             var document_code  = $(this).attr('data-id');
           $('#modal_upload').modal('show');
           $('#document_name').html(name);
            $('#body').html('<input type="hidden" name="document_code" value="'+document_code+'">');
    });
            $('#pic').change(function(){
              var username = $(this).val();
              var url = "{{ url('project/get_email') }}";
               $.ajax({
                  type: "GET",
                  url: url,
                  data: {
                  'username': username,
                  },
                   success: function(data) {
                    console.log(data);
                 
                       document.getElementById('email').value = data;
                    
                   
                   }
                  }); 
            });
          });
   
                $('.get_status_list').change(function(){
                      var checkstatus = $(this).val();
                      console.log(checkstatus);
                      if (checkstatus ==1) {
                           $('#status_list').show();
                           $('#status_list').html('<label>Note</label><textarea class="form-control" name="note"></textarea>');
                      }else{
                           $('#status_list').hide();
                      }
                     
                });
               $('.modal_document').click(function(){
                  $('.modal_documents').modal('show');
                  $('.modal-title').html('Add New Document');
                   $('#document_name').val();
                   $('#pic').val();
                    $('.modal_documents form').attr('action', "{{ url('project/post_document_project') }}");
                });
                 $('.show_list').click(function(){
                    var document_code = $(this).attr('data-id');
                    $('#input_code').html('<input type="hidden" value="'+document_code+'" name="document_code">');
                    var url = "{{ url('project/get_list_name') }}";
                      $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'document_code': document_code,
                          },
                           success: function(data) {
                            console.log(data);
                         
                               $('#modal_document_list').modal('show');
                               $('.modal-title').html('Check list document');
                               $('#get_list_name').html(data);
                           
                           }
                      }); 
                 
                });
                  $('.show_note').click(function(){
                    var document_code = $(this).attr('data-id');
                    var url = "{{ url('project/get_document_note') }}";
                      $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'document_code': document_code,
                          },
                           success: function(data) {
                            console.log(data);
                         
                               $('#modal_document_note').modal('show');
                               $('.modal-title').html('Note document');
                               $('#get_note').val(data.result.note);
                                  $('#document_codes').html('<input name="document_code" type="hidden" value="'+data.result.document_code+'">');
                           }
                      }); 
                 
                });
                 
                   $('#get_status').change(function(){
                      var status = $(this).val();
                      var url = "{{ url('project/change_status_document') }}";
                         $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'status': status,
                          },
                           success: function(data) {
                            console.log(data);
                         
                               document.getElementById('email').value = data;
                            
                           
                           }
                      }); 
                  });


          var g=1; 
          $('.add_list').click(function(){
           g++;  
           $('#form_list').append('<div class="row"  id="row'+g+'">'+
                      '<div class="col-lg-11">'+
                        '<input type="text" class="form_technical" placeholder="List name"  name="list_name[]"  value="" style="width: 100%" />'+
                      '</div>'+
                
                      '<div class="col-lg-1">'+
                        '<a id="'+g+'" class="btn btn-danger g_btn_remove delete'+g+'">X</a>'+
                      '</div>'+
                    '</div>');  
      });  
        $(document).on('click', '.g_btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
            $('.delete'+button_id+'').remove();  
      });  
 $('.editDocument').click(function(){

       var id = $(this).attr('data-id');
       var url = "{{ url('project/get_document_note') }}";

        $('.modal_documents form').attr('action', "{{ url('project/update_document') }}");

        $.ajax({
            type : 'GET',
            url : url,
            data: {
                          'document_code': id,
            },
            dataType : 'JSON',
            success : function(data) {
                console.log(data);

          
                   $('#document_code').html('<input name="document_code" type="hidden" value="'+data.result.document_code+'">')
                   $('#document_name').val(data.result.document_name);
                   $('#pic').val(data.result.pic);
                    $('#myModalLabel1').html('Edit Document'); 
                    $('.modal_documents').modal('show');

                
                        
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error : Gagal mengambil data'); 
            }
        });

    });
</script>
@endpush