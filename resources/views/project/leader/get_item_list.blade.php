
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

@include('project.flash.flash')

	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">OPEN ITEM CONTROL</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">

  		<div class="col-12">
  			  <div class="card">
          <!--   <div class="card-header">
                    <a class="btn btn-primary" href="{{ url('project/add_new_item/'.$category->id_category_list.'/'.$id_project) }}">ADD NEW ITEM</a>
            </div> -->
          
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th>LAST UPDATE</th>
              <th>ITEM CONTROL</th>
              <th>PIC</th>
              <th>PLAN START</th>
              <th>PLAN DURATION</th>
              <th>PLAN FINISH</th>
              <th>PLAN PROGRESS</th>
              <th>ACTUAL START</th>
              <th>ACTUAL DURATION</th>
              <th>ACTUAL FINISH</th>
              <th>ACTUAL PROGRESS</th>
              <th>LAMPIRAN</th>
              <th>OPEN R.I.O</th>
              <th>STATUS</th>
              <th>REMINDER</th>
              @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
              <th>EDIT</th>
              <th>DELETE</th>
              @endif
              <th>NOTE</th>
            </tr>
          	</thead>
           <tbody>
           @foreach($data as $value)
            <tr>
              <td>{{ $value->last_update_item }}</td>
              <td>@if($value->item ==true) {{ $value->item->item_name }} @endif</td>
              <td>{{ $value->pic_item }}</td>
              <td>{{ $value->plan_start_item }}</td>
              <td>{{ $value->plan_duration_item }} Hari</td>
              <td>{{ $value->plan_finish_item }}</td>
              <td>{{ $value->plan_progress_item }}</td>
              <td>{{ $value->actual_start_item }}</td>
              <td>{{ $value->actual_duration_item }} Hari</td>
              <td>{{ $value->actual_finish_item }}</td>
              <td>{{ $value->actual_progress_item }}</td>
              <td>
                  <a href="" class="btn btn-primary btn-sm">
                  <i data-feather="download"></i>
                  Download
                </a>
              </td>
            
              <td>R.I.O</td>
                <td>
                 <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
              </td>
              <td></td>
              @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_manager")
              <td>
                <a href="{{ url('project/edit_item_control/'.$value->id_project.'/'.$id_category.'/'.$value->part_number.'/'.$id_process->id_process.'/'.$value->id_item) }}" class="btn btn-info btn-sm">
                  <i data-feather="edit"></i>
                  Edit
                </a>
              </td>
               <td>
                <a href="" class="btn btn-danger btn-sm">
                  <i data-feather="trash"></i>
                  Hapus
                </a>
              </td>
              @endif
              <td>
                <a href="" class="btn btn-primary btn-sm">
                  <i data-feather="edit"></i>
                  Note
                </a>
              </td>
            </tr>
           @endforeach
           </tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>

@endsection
@push('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
               "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
        
       
        
        });
          });
    </script>
@endpush