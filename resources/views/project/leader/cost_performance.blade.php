@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    
<div class="col-lg-12">

			
	
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h2 class="text-title">COST PERFORMANCE INDEX (CPI)</h2>
					 <a href="{{ url('project/display_project_detail/'.$IDcustomer->id_customer.'/'.$project->id_project) }}" class="btn btn-danger">BACK</a>
				</div>
				<div class="card-body">
					<div id="grafik_cost"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					@if($get_performance ==true)
					<div id="grafik_cost_budget"></div>
					@else
					<i>No record ....</i>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
    @include('project.leader.grafik.grafik_cost_performance')
@endpush

