             
<div
class="modal fade text-left"
id="event_modal"
tabindex="-1"
role="dialog"
aria-labelledby="myModalLabel1"
aria-hidden="true"
>
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1" style="text-align: center">ADD EVENT</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
          <div class="modal-body">                
                <div class="col-md-12 col-12">
                    <form class="form-participant" action="{{ url('project/add_event') }}" method="post" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="id_project" value="{{ $project->id_project }}">
                  
                     <div class="row"> 
                      <div class="col-lg-12">
                        <div class="form-group">
                         <label class="form-label" for="basic-addon-name">Nama Event</label>
                            <select class="form-control" name="id_category_event_project">
                              @foreach($event_all as $value)
                              <option value="{{ $value->id_category_event_project }}">{{  $value->event_project_name }}</option>
                              @endforeach
                            </select>
                            </div>
                          </div>

                          <div class="col-lg-6">
                        <div class="form-group">
                           <label class="form-label" for="basic-addon-name">Plan start event</label>
                            <input
                                type="date"
                                id="basic-addon-name"
                                class="form-control"
                                placeholder="description"
                                aria-label="Name"
                                aria-describedby="basic-addon-name"
                                name="jadwal_event"
                          
                                required
                              />
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                               <label class="form-label" for="basic-addon-name">Actual start event</label>
                                <input
                                    type="date"
                                    id="basic-addon-name"
                                    class="form-control"
                                    placeholder="description"
                                    aria-label="Name"
                                    aria-describedby="basic-addon-name"
                                    name="actual_event"
                              
                                    required
                                  />
                                </div>
                              </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                               <label class="form-label" for="basic-addon-name">Plan finish event</label>
                                <input
                                    type="date"
                                    id="basic-addon-name"
                                    class="form-control"
                                    placeholder="description"
                                    aria-label="Name"
                                    aria-describedby="basic-addon-name"
                                    name="finish_plan_event"
                              
                                    required
                                  />
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                   <label class="form-label" for="basic-addon-name">Actual finish event</label>
                                    <input
                                        type="date"
                                        id="basic-addon-name"
                                        class="form-control"
                                        placeholder="description"
                                        aria-label="Name"
                                        aria-describedby="basic-addon-name"
                                        name="finish_actual_event"
                                  
                                        required
                                      />
                                    </div>
                                  </div>

                        </div>
    
                    </div>
                  </div>
   

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
