             

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="all_member">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<div class="col-lg-12">
  <div class="card">
    <div class="card-header">
      <h2>ALL MEMBER</h2>
    </div>
    <div class="card-body">
            <table class="datatables-basic table all_member" style="width: 100%">
                         <thead>
                          
                          <tr>
                            <th width="10%">NAME</th>
                            <th width="5%">PROCESS</th>
                            <th width="15%">POSITION</th>
                            <th width="5%">PRODUCT</th>
                            <th width="5%">DEPARTEMENT</th>
                            <th width="5%">AVAILABILITY</th>
                            <th width="20%">STANDAR RATE</th>
                            <th width="5%">EDIT</th>
                            <th width="5%">DELETE</th>
                        
                          </tr>
                          </thead>
                         <tbody>
                          @foreach($all_member_get as $member)
                          <tr>
                            <td>{{ $member['name'] }}</td>
                            <td>{{ $member['process_name'] }}</td>
                            <td>{{ $member['position'] }}</td>
                            <td>{{ $member['part_name'] }}</td>
                            <td>{{ $member['departement'] }}</td>
                            <td>{{ $member['availability'] }}</td>
                            <td>{{ $member['standar_rate'] }}</td>
                            <td>
                            <a href="{{ url('project/edit_member_detail/'.$member['id_member']) }}" class="btn btn-primary btn-sm"> Edit</a>
                            </td>
                            <td>
                            <a href="{{ url('project/delete_member_detail/'.$member['id_member']) }}" class="btn btn-danger btn-sm"> Delete</a>
                          </td>
                          </tr>
                          @endforeach
                         </tbody>
                       </table>
                     </div>
        </div>
    </div>
  </div>
</div>
                     
                  