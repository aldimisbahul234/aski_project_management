             
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_rfq">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-body">
<form class="form-participant" action="{{ url('project/upload_document_rfq') }}" method="post" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="id_project" value="{{ $project->id_project }}">
       	<div class="card">
       		<div class="card-body">
       				<div class="row">
					<div class="col-lg-12">	
					<label>EMAIL</label>
						<input type="text" name="email" class="form_technical" style="width: 100%">
					</div>
					<div class="col-lg-12">	
					<label>PASSWORD</label>
						<input type="password" name="password" class="form_technical" style="width: 100%">
					</div>
					<div class="col-lg-6">
						<input type="file" name="rfq_file" class="form_technical" style="width: 100%">
					</div>
						<div class="col-lg-6">
							<input type="date" name="date_created" class="form_technical" style="width: 100%">
						</div>
						<div class="col-lg-12">
							<button class="btn btn-success">UPLOAD</button>
						</div>
						<div class="col-lg-12">
							
							 <table class="table document_rfq" id="datatable">
					             <thead>
					              
					              <tr>
					                <th>NO</th>
					                <th>DOCUMENT</th>
					                <th>LAST UPDATE</th>
					                <th>DELETE</th>
					              </tr>
					              </thead>
					             <tbody>
					             	@foreach($modal_rfq_document as $modal_rfq_document)
					             	<tr>
					             		<td>{{ $loop->iteration }}</td>
					             		<td>{{ $modal_rfq_document->rfq_file }}</td>
					             		<td>{{ $modal_rfq_document->date_created }}</td>
					             		<td><a href="" class="btn btn-danger btn-sm"> Hapus</a></td>
					             	</tr>
					             	@endforeach
					            </tbody>
					           </table>


						</div>

					</div>
       		</div>
       	</div>
       </form>
       
       </div>    
    </div>
  </div>
</div>
                     
                  