             
<div
class="modal fade text-left"
id="general_modal"
tabindex="-1"
role="dialog"
aria-labelledby="myModalLabel1"
aria-hidden="true"
>
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1" style="text-align: center">EDIT INFO GENERAL</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
          <div class="modal-body">                
                    <form class="form-participant" action="{{ url('project/update_general/'.$project->id_project) }}" method="post" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="id_customer" value="{{ $customer->id_customer }}">
                      <input type="hidden" name="id_project" value="{{ $project->id_project }}">
                      <div class="row"> 
                          <div class="col-lg-6">
                            <div class="card bg-primary">
                              <div class="card-header">
                            <h5 style="color: white">CAPTION</h5>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="card bg-danger">
                             <div class="card-header">
                              <h5 style="color: white">DESCRIPTION</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                @foreach($summary as $value)
                     <div class="row"> 
                      <div class="col-lg-6">
                        <div class="form-group">
                            <input
                                type="text"
                                id="basic-addon-name"
                                class="form-control"
                                placeholder="description"
                                aria-label="Name"
                                aria-describedby="basic-addon-name"
                                name="caption_info[]"
                                value="{{ $value->caption_info }}"
                                required
                              />
                            </div>
                          </div>

                        <div class="col-lg-6">
                        <div class="form-group">
                            <input
                                type="text"
                                id="basic-addon-name"
                                class="form-control"
                                placeholder="description"
                                aria-label="Name"
                                aria-describedby="basic-addon-name"
                                name="description[]"
                                value="{{ $value->description }}"
                                required
                              />
                            </div>
                          </div>
                        </div>
                      @endforeach
                        <a class="btn btn-primary" id="tambah"><i class="fa fa-plus"></i>ADD</a>
                        <span id="add"></span>
  
                    </div>

 

                    
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>
                  </div>
                </div>
              </div>
