@extends('layouts/root')

@section('main')

<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                 <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">Project</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$customer->id_customer) }}">{{ $customer->customer_name }}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$customer->id_product) }}">{{ $customer->customer_name }}</a></li>
                 <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$customer->id_product) }}">Detail {{ $customer->customer_name }}</a></li>
                  <li class="breadcrumb-item"><a href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$project->id_project) }}">Detail {{ $project->project_name }}</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

 
<div class="col-lg-12">
		<div class="card">
			 <!-- <div class="card-header">
          <a href="{{ url()->previous() }}" class="btn btn-danger float-right">Back</a>
        </div> -->
			
			
			<div class="card-body">

				<h3 style="text-align: center;">OPEN R.I.O (Risk/Issue/Opportunitys) @if($id_process =="dashboard") IN {{ $get_part->project_name }} @else {{ $get_part->part_name }} @endif</h3>
					
              @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
               
                    <a href="{{ url('project/create_issue/'.$id_project.'/'.$part_number.'/'.$id_category.'/'.$id_item.'/'.$id_process) }}" class="btn btn-primary">+ Add New</a>
               
              @endif
            
				   <table class="table table-striped table-hover nowrap struckture" id="datatable" style="width: 150%">
                 		<thead> 
		                  <tr>
		                    <th>NO</th>
                        <th>READ</th>
		                    <th>ISSUE</th>
		                  	 <th>CATEGORY ISSUE</th>
                         <th>PIC</th>
                         <th>PART NUMBER</th>
                         <th>LAMPIRAN</th>
                         <th>STATUS ISSUE</th>
                         <th>ANSWER PIC</th>
                         @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                         <th>REMOVE</th>
                         @endif
		                  </tr>
                  		</thead>
                 		<tbody>
                 		@foreach($data as $value)
                    @php
                     $get_notif =App\Http\Models\Notification::where(['id_issue'=>$value->id_issue,'id_project'=>$id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'issue'])->count();
                     @endphp
                 		<tr>
                 			<td>{{ $loop->iteration }}</td>
                      <td>@if($get_notif > 0) <a class="btn btn-primary btn-sm" href="{{ url('project/read_notif/'.'issue'.'/'.$value['id_issue']) }}"> Read</a> @endif</td>
                 			<td>{{ $value->issue }}  @if($get_notif > 0) <span class="badge badge-danger" style="background-color: red;">{{ $get_notif }}</span> @endif</td>
                      <td>{{ $value->category_issue }}</td>
                      <td>{{ $value->pic }}</td>
                 			<td>{{ $value->part_number }}</td>
                      <td>
                        @if(!empty($value->bukti) AND $value->bukti !="kosong")
                        <a href="{{ url('pic/download_document_issue/'.$value->bukti) }}" class="btn btn-primary"><i data-feather="download"></i> download</a>
                        @else
                        <button class="btn btn-primary" disabled><i data-feather="download"></i> download(No available)</button>
                        @endif
                      </td>
                      <td>
                          @if($value->status_rio ==0)
                          <button class="btn btn-danger" style="width: 62%"><i class="fa fa-window-close" aria-hidden="true"></i> UN DONE</button>
                          @else
                          <button class="btn btn-success" style="width: 62%"><i class="fa fa-check" aria-hidden="true"></i> DONE</button>
                          @endif
                      </td>
                      <td>
                        <button type="button" class="btn btn-primary jawaban" data-id="{{ $value->id_issue }}" type="button"><i data-feather="edit"></i> answer pic</button>
                      </td>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <td><a href="{{ url('project/issue_remove/'.$value->id_issue) }}" class="btn btn-danger"><i data-feather="trash"></i> remove</a></td>
                    @endif
                 		</tr>

                 		@endforeach
                 		</tbody>
               		</table>
			</div>
		</div>
</div>
<!-- modal -->
             
<div class="modal fade text-left " id="model_answer_pic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">ANSWER PIC/h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
      <div class="modal-body">                
               
              <h1 class="get_answer"></h1>   
   

                    
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- end -->
@endsection
@push('script')
<script type="text/javascript">
	   $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                  "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,  
                
            });
          });
        $('.jawaban').click(function(){
                    var id_issue = $(this).attr('data-id');
                    var url = "{{ url('project/get_answer_pic') }}";
                      $.ajax({
                          type: "GET",
                          url: url,
                          data: {
                          'id_issue': id_issue,
                          },
                           success: function(data) {
                            console.log(data);
                             
                               $('#model_answer_pic').modal('show');
                               $('.modal-title').html('Note document');
                              $('.get_answer').html(data.answer_pic);
                           }
                      }); 
                 
                });
</script>
@endpush

