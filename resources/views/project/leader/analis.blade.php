@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
<div class="col-lg-12">
<h2>ANALYTICS PROJECT {{ $project->project_name }}  {{ $product->part_name }}</h2>
    <div class="row">
     <div class="col-sm-6">
        <div class="card">
            <div class="btn-group">
                <button
                class="btn btn-info"
                type="button"
              >
              @if($status =="all")
                PRODUCT {{ $product->part_name }}
                @else
                CATEGORY CONTROL LIST {{ $status_category->category_name }}
              @endif
              </button>
              <button
                class="btn btn-primary dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                SELECT CATEGORY CONTROL
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                 <a class="dropdown-item" href="{{ url('project/analis/'.$project->id_project.'/'.$product->part_number.'/'.'all') }}">ALL CATEGORY</a>
                @foreach($category as $category)
                <a class="dropdown-item" href="{{ url('project/analis/'.$project->id_project.'/'.$product->part_number.'/'.$category->id_category) }}">{{ $category->category_name }}</a>
                @endforeach
              </div>
            </div>
        </div>
    </div>

<div class="col-sm-1">
     <div class="card"  style="left: 580px;">
       <a href="{{ url('project/display_project_detail/'.$IDcustomer->id_customer.'/'.$project->id_project) }}" class="btn btn-danger">BACK</a>
     </div>
</div>

</div>
<div class="row">



    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <span id="analis_donut"></span><br><br>
                
                <p>
                 <div class="row">
                    <div class="col-sm-6">
                        <span class="btn bg-blue"> </span>
                        <span>Today </span>
                    </div>
                    <div class="col-sm-6">
                        <span>{{ $counttoday }} Item Control</span>
                    </div>
                </div>
                </p><hr>

                 <p>
                     <div class="row">
                        <div class="col-sm-6">
                            <span class="btn bg-black"> </span>
                            <span>Next Item Control</span>
                        </div>
                        <div class="col-sm-6">
                            <span>{{ $countnext }} Item Control</span>
                        </div>
                    </div>
                </p><hr>
                 <p>
                     <div class="row">
                        <div class="col-sm-6">
                            <span class="btn bg-orange"> </span>
                            <span>In Progress </span>
                        </div>
                        <div class="col-sm-6">
                            <span>{{ $countprogress }} Item Control</span>
                        </div>
                    </div>
                </p><hr>
                 <p>
                     <div class="row">
                        <div class="col-sm-6">
                            <span class="btn bg-green"> </span>
                            <span>Completed </span>
                        </div>
                        <div class="col-sm-6">
                            <span>{{ $countcomplate }} Item Control</span>
                        </div>
                    </div>
                </p><hr>             
                <p>
                    <div class="row">
                        <div class="col-sm-6">
                              <span class="btn bg-red"></span>
                            <span>Delay </span>
                        </div>
                        <div class="col-sm-6">
                            <span>{{$countdelay}} Item Control</span>
                        </div>
                    </div>
                </p><hr>  
                
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h2>STATISTIK</h2>
                <div class="row">

                    <div class="col-lg-4">
                    
                          <div class="card shadow-none bg-transparent border-primary">
                            <div class="card-body">
                              <h2>{{ $statistik_hasil }}  Days</h2><br>
                              <h4>DURATION.</h4>
                            </div>
                          </div>
   
                    </div>

                     <div class="col-lg-6">
                       <div class="card shadow-none bg-transparent border-primary">
                            <div class="card-body">
                                  <h2 style="text-align: center">TOP PERFORMANCE</h2><br>
                                  <h2 style="text-align: center">@if($top_performance ==true) {{ $top_performance->firstname }} @else - @endif</h2>
                                </div>
                          </div>
                    </div>

                </div><br><br>
                
                <h2>DATE</h2>
                <div class="row">
                     <div class="col-lg-12">
                         <div class="card shadow-none bg-transparent border-primary">
                            <div class="card-body">
                                  <h2 style="text-align: center">{{ $parse_to_date_start }}</h2><br>
                                <h2 style="text-align: center">Until</h2><br>
                                  <h2 style="text-align: center">{{ $parse_to_date_finish }} </h2>
                             </div>
                          </div>
                    </div>
                </div>

            </div>
        </div>
         
              <a href="{{ url('gant_chart_project/'.$IDproject.'/'.$part_number) }}" target="_blank" class="btn btn-success" style="
    width: 100%;
    height: 93px;
    padding: 36px;
"><h3>GANT CHART</h3></a>
            
           
    </div>

    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <h2>PERFORMANCE CHART</h2>
              <span id="analis_bar" ></span>
              <h2>PERFORMANCE CHART</h2>
              <div id="analis_progress" style="min-width: 310px; height: 1200px; margin: 0 auto"></div>
            </div>
        </div>
    </div>

</div>
 </div>
@endsection
@push('script')
    @include('project.leader.grafik.grafik_analis')
@endpush