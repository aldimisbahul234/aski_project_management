
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
@include('project.flash.flash')
@if(Auth::user()->hak_akses =="project_manager")
 <div class="col-12">     
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#default">
  <i class="fa fa-plus"></i>ADD NEW PROJECT 
</button><br><br><br>
</div>
@endif
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">TYPE PROJECT ( Customer {{$customer->customer_name}} )</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
    
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th width="10%">PROJECT NAME</th>
              <th width="5%">TOTAL PART</th>
              <th width="15%">NEXT EVENT</th>
              <th width="5%">PROJECT MANAGER</th>
              <th width="5%">PLAN PROGRES</th>
              <th width="5%">ACTUAL PROGRES</th>
              <th width="30%">STATUS</th>
              <th width="5%">LAST UPDATE</th>
              <th width="5%">REPORT</th>
              <th width="30%">PROJECT DETAIL</th>
            </tr>
          	</thead>
           <tbody>
           	@foreach($data as $value)
            @php

                $project_leader = App\Http\Models\Masterdata\MemberProject::where(['id_project'=>$value['id'],'id_position'=>1])->first();
                if($project_leader ==true){
                $getname = App\User::where('username',$project_leader->username)->first();
                }else{
                 $getname = false;
                }
                $event = DB::table('event_project')->leftjoin('category_event_project','category_event_project.id_category_event_project','event_project.id_category_event_project')->where('event_project.id_project',$value['id'])->where('jadwal_event','>=',$dateNow)->first();
                if($event ==true){
                
                $selisih = Carbon\Carbon::now()->startOfDay()->diffInDays($event->jadwal_event, false);
              }else{
               $selisih = 0;
            }
            @endphp
           
    	       	<tr>
    	       		<td>{{ $value['project_name'] }}</td>
    	       		<td>{{ $value['count_part'] }}</td>
                <td>
                  <div class="blink">
                    <a  class="btn btn-warning" style="width: 100%;height: 49px;">
                      @if($selisih ==0) Today 
                      @elseif($dateNow > $event->jadwal_event) Until Now 
                      @else {{ $event->event_project_name }} {{ $selisih }} Day Left 
                      @endif
                    </a>
                  </div>
                </td>
                <td>@if($getname ==true) {{ $getname->firstname }} {{ $getname->lastname }} @else - @endif</td>
                <td>
                    @if($value['result_plan'] > 100) 100% 
                    @else {{ $value['result_plan'] }}% 
                    @endif
                </td>
                <td>@if($value['result_actual'] > 100) 100% 
                    @else {{ $value['result_actual'] }}% 
                    @endif
                </td>
                <td>
                   @if($value['check'] =='true')
                    @if($value['result_actual'] == 100 || $value['result_actual'] > 100)
                  <a href="{{ url('project/product_detail/'.$value['id'].'/'.$value['customer']) }}">
                    <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                  </a>
                  @elseif( $value['result_actual'] !=0)
                   <a href="{{ url('project/product_detail/'.$value['id'].'/'.$value['customer']) }}">
                    <div class="progress progress-bar-warning">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{ $value['result_actual'] }}%"
                      ></div>
                    </div>
                  </a>
                  @else
                     <a href="{{ url('project/product_detail/'.$value['id'].'/'.$value['customer']) }}">
                    <div class="progress progress-bar-secondary">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{$value['result_actual']}}%"
                      ></div>
                    </div>
                  </a>
                  @endif
                  @else
                    @if($value['result_actual'] == 100 || $value['result_actual'] > 100)
                  <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                    @elseif($value['result_actual'] < 100 || $value['result_actual'] != 0)
                       <div class="progress progress-bar-warning">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="{{$value['result_actual']}}"
                        aria-valuemin="{{$value['result_actual']}}"
                        aria-valuemax="100"
                        style="width: {{$value['result_actual']}}%"
                      ></div>
                    </div>
                    @else
                     <div class="progress progress-bar-secondary">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{$value['result_actual']}}%"
                      ></div>
                    </div>
                    @endif

                  @endif
                </td>
               <td>{{ $value['last_update_item'] }}</td>
               <td>
                 @if($value['check'] =='true')
                <a href="{{ url('gant_chart_project/'.$value['id'].'/'.'null') }}" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-chart-bar fa-1x"></i> Grafik</a>
                @else
                 <button class="btn btn-success btn-sm" disabled><i class="fa fa-chart-bar fa-1x"></i> Grafik</button>
                @endif
              </td>
               <td>
               
                @if($value['check'] =='true')
                 <div class="row">
                  <div class="col-lg-6">
                     <a href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$value['id']) }}" class="btn btn-primary btn-sm"><i data-feather="edit"></i> Detail</a>
                  </div>
                  <div class="col-lg-6">
                      @if($value['notif'] > 0)
                        <span class="badge badge-danger">new</span> 
                      @endif
                  </div>
                </div>
                 
                 
                @elseif(Auth::user()->hak_akses =="quality")
                <div class="row">
                  <div class="col-lg-10">
                    <a href="{{ url('project/display_project_detail/'.$customer->id_customer.'/'.$value['id']) }}" class="btn btn-primary btn-sm"><i data-feather="edit"></i></a>  
                  </div>
                   <div class="col-lg-2">
                    @if($value['notif'] > 0)
                      <span class="badge badge-danger">new</span> 
                    @endif
                  </div>
                </div>
                @else
                 <div class="row">
                  <div class="col-lg-6">
                <button class="btn btn-primary btn-sm" disabled><i data-feather="edit"></i> Detail</button>  
              </div>
              <div class="col-lg-6">
                @if($value['notif'] > 0)<span class="badge badge-danger">new</span> @endif
              </div>
                @endif
                </td>
    	       	</tr>
			       @endforeach
           	</tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>

<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
        <table class="datatables-basic table" id="datatable-dashboard">
           <thead>
            
            <tr>
              <th style="text-align: center;">PROJECT NAME</th>
              <th style="text-align: center;">CURRENT</th>
              <th style="text-align: center;">NEXT EVENT</th>
              <!-- <th style="text-align: center;">TIME</th> -->
              <th style="text-align: center;">QUALITY</th>
              <th style="text-align: center;">COST</th>
              <th style="text-align: center;">BUDGET</th>
            <!--   <th style="text-align: center;">OPEN R.I.O</th> -->
              <th style="text-align: center;">ANALISTYIC</th>
              <th style="text-align: center;">PROGRESS ACTUAL</th>
            </tr>
            </thead>
           <tbody>
             @foreach($getProject as $project)
            <tr>
              <td>{{ $project['project_name'] }}</td>
              <td>
                <button disabled type="button" class="btn btn-info" style="width: 165px;height: 80px;">{{ $project['event_name_current'] }} - {{ $project['get_current'] }}</button>
              </td>
              <td>
                <button disabled type="button" class="btn btn-warning" style="width: 165px;height: 80px;">{{ $project['event_name'] }} {{ $project['next'] }} Days Left</button>
              </td>
             <!--  <td><a href="{{ url('type_project_dashboard/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-danger waves-effect"><i data-feather="eye"></i></a></td> -->
              <td>
               
                   <button class="btn btn-primary dropdown-toggle"  id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false" style="width: 100%">APPROVAL LINE 
                            N/A</button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item"></a>
                  @foreach($project['get_product'] as $vals)
                    <a class="dropdown-item" href="{{ url('project/quality_detail/'.$vals->id_project.'/'.$vals->part_number) }}">{{ $vals->part_name }}</a>
                  @endforeach
              </div>
                        <!--     <a class="btn btn-primary" href="" style="width: 100%">
                            APPROVAL LINE 
                            N/A
                          </a> -->
                           <a class="btn btn-primary" style="margin-top: 10px;width: 100%">
                            TRIAL
                            N/A
                          </a>
              
                     
                
                     
                              
               
              </td>
              <td>
                  <a href="{{ url('cost/cogm/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-warning waves-effect"><i data-feather="eye"></i></a></td>
              </td>
              <td>
                <a href="{{ url('project/project_budget/'.$project['id_project']) }}" class="btn btn-icon btn-icon rounded-circle btn-outline-warning waves-effect"><i data-feather="eye"></i></a></td>
              </td>
             <!--  <td>
                <a href="{{ url('project/issue/'.$project['id_project'].'/'.'null'.'/'.'null'.'/'.'null'.'/'.'dashboard') }}" class="btn btn-danger">R.I.O</a>
              </td> -->
              <td>
                @if($project['part_number'] ==true)
                <a class="btn btn-primary" href="{{ url('project/analis/'.$project['id_project'].'/'.$project['part_number'].'/'.'all') }}">ANALYTIC</a>
                @else
                <button class="btn btn-primary" disabled>ANALYTIC</button>
                @endif
              </td>
              <td>
                <a href="{{ url('type_project_dashboard/'.$project['id_project']) }}">
                  @if($project['total_actual'] == 100 || $project['total_actual'] > 100)
               <div class="progress progress-bar-success">
                 <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      >{{ $project['total_actual'] }} %</div>
                    
                    </div>
                    @else
                    <div class="progress progress-bar-warning">
                 <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{ $project['total_actual'] }}%"
                      >{{ $project['total_actual'] }} %</div>
                    
                    </div>

                    @endif

                  </a>
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

<!-- grafik -->
<div class="col-lg-12">
  <div class="row">

    <div class="col-lg-6">
       
            <div id="grafik_customer"></div>
          
    </div>
  
   
    <div class="col-lg-6">
      
          <div id="grafik_rfq"></div>
      
    </div>
    <br><br>

    <div class="col-lg-4" style="top: 15px;">
     
          <div id="progress_project"></div>
      
    </div>

     <div class="col-lg-4" style="top: 15px;">
     
          <div id="project_budget"></div>
        
    </div>

     <div class="col-lg-4" style="top: 15px;">
     
          <div id="project_cost"></div>
       
    </div>

  </div>
</div>
<!-- end -->
@include('project.leader.modal_create_project')
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
            });
             $('#datatable-dashboard').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                 "scrollX": true,
                "scrollY": "500px",
                "scrollCollapse": true,
            });
            Highcharts.chart('progress_project', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Progress Project'
    },
    credits:{
        enabled : false
    },
    xAxis: {
        categories: [
         <?php foreach ($get_grafik_progress_project as $key => $get_grafik_progress_project): ?>
           '<?php echo $get_grafik_progress_project['project_name'];?>',
        <?php endforeach ?>
        ]
    },
    yAxis: {
        min: 0,
        max:100,
        title: {
            text: 'PROGRESS PROJECT ACTUAL'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}%</b><br/>'
    },
    series: [ {
        name: 'Complete',
        color : "skyblue",
        data: [ 
        <?php foreach ($get_grafik_progress_actual as $key => $get_grafik_progress_actual): ?>
           <?php echo $get_grafik_progress_actual['sum_actual'];?>,
        <?php endforeach ?>
        ]
    }]
});


Highcharts.chart('project_budget', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Budget Complete Project'
    },
    credits:{
        enabled : false
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        <?php foreach ($get_grafik_budget as $key => $get_grafik_budget): ?>
           '<?php echo $get_grafik_budget['project_name'];?>',
        <?php endforeach ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Budget Complete'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">Project {point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },
    series: [{
        name: 'Budget Complete',
        color : "skyblue",
        data: [
            <?php foreach ($get_grafik_budget_sum as $key => $get_grafik_budget_sum): ?>
           <?php echo $get_grafik_budget_sum['total'];?>,
            <?php endforeach ?>
        ]

    }]
});


// Create the chart
Highcharts.chart('project_cost', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Cost Current Event'
    },
    subtitle: {
        text: 'Click Name of Project Below to see Detail'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Cost'
        }

    },
    credits:{
        enabled : false
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: ''
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">Project {point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0"> Percentage : </td>' +
            '<td style="padding:0"><b>{point.y}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },

    series: [
        {
            name: "PROJECT",
            colorByPoint: true,
            data: [
              
            ]
        }
    ],
    drilldown: {
        series: [
           
        ]
    }
});



          });
    </script>
@endpush