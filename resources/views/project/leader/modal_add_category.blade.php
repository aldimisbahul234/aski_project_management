<div class="modal fade text-left" id="modal_add_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
<div class="modal-dialog modal-dialog modal-lg" role="document">
 <form action="{{ url('project/add_category_control') }}" method="post" enctype="multipart/form-data">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel1">Alert</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     @csrf
    <input type="hidden" name="part_number" value="{{ $part_number }}">
    <input type="hidden" name="id_process" value="{{ $id_process }}">
    <div class="col-lg-12">
      <select class="form-technical" name="category_name">
        @foreach($category_list as $category)
        <option>{{ $category->category_list_name }}</option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="modal-footer">
       <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</form>
</div>
</div>