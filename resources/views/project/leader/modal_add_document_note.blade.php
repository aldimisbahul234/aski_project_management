<div class="modal fade text-left" id="modal_document_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
  <div class="modal-dialog modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Alert</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('project/save_note') }}" method="post">
        @csrf
        <div class="modal-body">
       <input type="text" name="note" class="form-control" id="get_note">
       <span id="document_codes"></span>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
      </div>
   </form>
    </div>
  </div>
</div>