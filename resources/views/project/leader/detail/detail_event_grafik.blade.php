@extends('layouts/root')
@section('main')
<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div id="grafik_event_detail"></div>
			</div>
		</div>
</div>

@endsection
@push('script')
@include('project.leader.grafik.event_detail_grafik')
@endpush

