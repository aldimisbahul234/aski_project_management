

@extends('layouts/root')
@section('main')
<div class="col-lg-12">
  <div class="card">
    <div class="card-header">
      <h2>Member Detail of Product {{$product->part_name}}</h2>
     <!--  <button class="btn btn-success" style="left: 223px;">ADD MEMBER</button> -->
       <a href="{{ url('project/display_project_detail/'.$IDcustomer->id_customer.'/'.$project->id_project) }}" class="btn btn-danger">BACK</a>
    </div>
    <div class="card-body">
<table class="datatables-basic table all_member">
       <thead>
        
        <tr>
          <th width="10%">NAME</th>
          <th width="5%">PROCESS</th>
          <th width="15%">POSITION</th>
          <th width="5%">PRODUCT</th>
          <th width="5%">DEPARTEMENT</th>
          <th width="5%">AVAILABILITY</th>
          <th width="20%">STANDAR RATE</th>
           @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
          <th width="5%">EDIT</th>
          <th width="5%">DELETE</th>
          @endif
        </tr>
        </thead>
       <tbody>
        @foreach($all_member_get as $member)
        <tr>
          <td>{{ $member['name'] }}</td>
          <td>{{ $member['process_name'] }}</td>
          <td>{{ $member['position'] }}</td>
          <td>{{ $member['part_name'] }}</td>
          <td>{{ $member['departement'] }}</td>
          <td>{{ $member['availability'] }}</td>
          <td>{{ $member['standar_rate'] }}</td>
           @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
          <td><a class="btn btn-primary" href="{{ url('project/edit_member_detail/'.$member['id_member']) }}">EDIT</a></td>
          <td><a href="{{ url('project/delete_member_detail/'.$member['id_member']) }}" class="btn btn-danger">DELETE</td>
            @endif
        </tr>
        @endforeach
       </tbody>
     </table>
   </div>
</div>
</div>
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.all_member').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
    </script>
@endpush