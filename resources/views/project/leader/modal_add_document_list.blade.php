<div class="modal fade text-left" id="modal_document_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none">
  <div class="modal-dialog modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Alert</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <form action="{{ url('project/update_status_document') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
           <span id="input_code"></span>
          <span id="get_list_name"></span>
          <hr>
          <label>Status</label>
          <select class="form_technical get_status_list" name="status_document">
            <option>pilih status</option>
            <option value="1">Revisi</option>
            <option value="2">Lengkap</option>
          </select>
          <span id="status_list" style="display: none"></span>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>