@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>

    
		<div class="col-lg-12">
			

					<div class="row">
						<div class="col-lg-6">
							<div class="card bg-warning">
								<div class="card-body budget">
									<span style="color: white">BUGET COMPLETE</span>
									<p><h1 style="color: white">{{ $result }} %</h1></p>
								</div>
							</div>
						</div>	
						<div class="col-lg-6">
							<div class="card bg-warning">
								<div class="card-body budget">
									<span style="color: white">STATUS BUDGET</span>
									<p><h1><div class="blink" style="color: white">{{ $status }}</div></h1></p>
								</div>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="card bg-warning">
								<div class="card-body budget">
									<span style="color: white">PLAN TOTAL BUDGET</span>
									<p><h1><div class="blink" style="color: white">{{ number_format($budget_plan,2) }}</div></h1></p>
								</div>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="card bg-warning">
								<div class="card-body budget">
									<span style="color: white">ACTUAL TOTAL BUDGET</span>
									<p><h1><div class="blink" style="color: white">{{ number_format($budget_actual,2) }}</div></h1></p>
								</div>
							</div>
						</div>

					</div>
			
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
				<h2 style="text-align: center;">PROJECT BUDGET</h2>	
				

				 <div class="col-sm-12">
      <div class="card collapse-icon">
        <div class="card-header">

        </div>
        <div class="card-body">
         <h2 style="text-align: center;">COGM</h2>
          @if(Auth::user()->hak_akses =="cost")
           <a class="btn btn-info" href="{{ url('cost/budget_create/'.$project->id_project) }}"><i class="fa fa-plus fx-5"></i>ADD COGM</a><br><br>
          @else      
         <button class="btn btn-info" disabled><i class="fa fa-plus fx-5"></i>ADD COGM (User Cost Only)</button>
         @endif
         <br><br>
          @foreach($getData as $key=>$value)
          <div class="collapse-default">
            <div class="card" style="background-color: orange">
              <div
                id="headingCollapse1"
                class="card-header"
                data-toggle="collapse"
                role="button"
                data-target="#collapse_{{ $key }}"
                aria-expanded="false"
                aria-controls="collapse1"
              >
                <span class="lead collapse-title text-white"> {{ $value['product'] }} (Qty order = {{ $value['qty'] }}) </span>
                 <span class="lead collapse-title text-white">Status Budget : {{ $status }} </span>
              </div>

              <div id="collapse_{{ $key }}" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                <div class="card-body" style="background-color: white">
                  
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table index_{{ $key }}" id="datatable">
                      <thead>
                      
                      <tr>
                      <th>NO</th>
                      <th>ITEM INVESMENT</th>
                      <th>FUNDING</th>
                      <th>CATEGORY</th>
                      <th>TOTAL PRICE PLAN</th>
                      <th>TOTAL PRICE ACTUAL</th>
                      <th>ACTION</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($value['budget'] as $values)
                      	<tr>
                      		<td>{{ $loop->iteration }}</td>
                      		<td>{{ $values->item_invesment }}</td>
                      		<td>{{ $values->funding }}</td>
                      		<td>{{ $values->category }}</td>
                      		<td>{{ number_format($values->total_price_plan,2) }}</td>
                      		<td>{{ number_format($values->total_price_actual,2) }}</td>
                      		<td>
                    
                              <a href="{{ url('cost/hapus_budget/'.$values['id_project_budget']) }}" class="btn btn-danger btn-sm"><i data-feather="trash"></i>HAPUS</a>
                          </td>
                      	</tr>
                      @endforeach
                      </tbody>
                      </table>
                  </div>
                </div>
              </div>

                </div>
              </div>
            </div>
            @endforeach


          </div>
        </div>
      </div>
 


				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
		<div id="grafik_budget"></div>
	</div>
</div>
</div>
<!-- 
  <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
    <div id="grafik_cost_budget"></div>
  </div>
</div> -->
</div>
@endsection
@push('script')
@include('project.leader.grafik.grafik_project_budget')
@endpush

