
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>


@if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

	
 <div class="col-12">
    <div class="card">
    	<div class="card-header">
				 <div class="col-lg-12">
		  			<div class="card" style="background-color: rgba(115,103,240,.7)">
		    			<div class="card-body">
		      				<h2 style="text-align: center;" class="text-white">ITEM CONTROL / {{ $project->project_name }} / {{ $category->category_list_name }} /{{ $product->part_name }}</h2>
		    			</div>
		  			</div>
				</div>
    	</div>
      <div class="card-body">
        <div class="col-lg-12">
           @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
           <a class="btn btn-primary btn-sm" href="{{ url('project/add_new_item/'.$category->id_category_list.'/'.$id_project.'/'.$part_number.'/'.$id_category.'/'.$id_process) }}">ADD NEW ITEM</a>
           @endif
            <a class="btn btn-danger btn-sm float-right" href="{{ url('project/product_category_control/'.$part_number.'/'.$id_process.'/'.$id_project) }}"> Back</a>
        </div>
       
  		<div class="col-12">
  			  <div class="card">
  				<table class="datatables-basic table" id="datatable">
         	 <thead>
          	
            <tr>
              <th>READ</th>
              <th>LAST UPDATE</th>
              <th>ITEM CONTROL</th>
              <th>PIC</th>
              <th>PLAN START</th>
              <th>PLAN DURATION</th>
              <th>PLAN FINISH</th>
              <th>PLAN PROGRESS</th>
              <th>ACTUAL START</th>
              <th>ACTUAL DURATION</th>
              <th>ACTUAL FINISH</th>
              <th>ACTUAL PROGRESS</th>
              <th>LAMPIRAN</th>
              <!-- <th>OPEN R.I.O</th> -->
              <th>STATUS</th>
              <th>STATUS ITEM</th>
              @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
              <th>EDIT</th>
              <th>HAPUS</th>
              @endif
              <th>NOTE</th>
            </tr>
          	</thead>
           <tbody>
           @foreach($data as $value)
           @php
           $get_notif =App\Http\Models\Notification::where(['id_item'=>$value->id_item,'id_project'=>$id_project,'penerima'=>Auth::user()->id_pengguna,'status_read'=>'un_read','tipe'=>'item'])->count();
           @endphp
            <tr>
              <td>
                 @if($get_notif > 0) <a class="btn btn-primary btn-sm" href="{{ url('project/read_notif/'.'item'.'/'.$value->id_item) }}"> Read</a> @endif
              </td>
              <td>
                <div class="row">
                  <div class="col-lg-10">
                     {{ $value->last_update_item }}
                  </div>
                  <div class="col-lg-2">
                    @if($get_notif > 0)
                     <span class="badge badge-danger" style="background-color: red;">new update</span>
                    @endif
                  </div>
                </div>
               </td>
              <td>@if($value->item) {{ $value->item->item_name }} @endif</td>
              <td>{{ $value->pic_item }}</td>
              <td>{{ $value->plan_start_item }}</td>
              <td>{{ $value->plan_duration_item }} Hari</td>
              <td>{{ $value->plan_finish_item }}</td>
              <td>{{ $value->plan_progress_item }}</td>
              <td>{{ $value->actual_start_item }}</td>
              <td>{{ $value->actual_duration_item }} Hari</td>
              <td>{{ $value->actual_finish_item }}</td>
              <td>{{ $value->actual_progress_item }}</td>
              <td>
                @if(!empty($value->bukti_item))
                <a href="{{ url('project/download_document/'.$value->bukti_item) }}" class="btn btn-primary">
                  <i data-feather="download"></i>
                  Download
                </a>
                @else
                 <button class="btn btn-primary" disabled>
                  <i data-feather="download"></i>
                  Download
                </button>
                @endif
              </td>
         <!--      <td><a href="{{ url('project/issue/'.$value->id_project.'/'.$value->part_number.'/'.$id_category.'/'.$value->id_item.'/'.$id_process) }}" class="btn btn-danger btn-sm">R.I.O</a></td> -->
              <td>
                @if($value->actual_progress_item ==100 || $value->actual_progress_item > 100)
                 <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      ></div>
                    </div>
                    @else
                       <div class="progress progress-bar-warning">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{$value->actual_progress_item}}%"
                      ></div>
                    </div>
                    @endif
              </td>
              <td>
                      @if($value->status_item ==1)
                      <button class="btn btn-danger dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"aria-expanded="false" style="width: 96%;">PROGRESS</button>
                      @else
                       <button class="btn btn-success dropdown-toggle"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"aria-expanded="false" style="width: 96%;">FINISH</button>
                       @endif
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ url('project/update_status_item/'.$value->id_item.'/'.'progress') }}">PROGRESS</a>
                        <a class="dropdown-item" href="{{ url('project/update_status_item/'.$value->id_item.'/'.'finish') }}">FINISH</a>
                      </div>
                    </td>
               @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
              <td>
                <a href="{{ url('project/edit_item_control/'.$value->id_project.'/'.$id_category.'/'.$value->part_number.'/'.$id_process.'/'.$value->id_item) }}" class="btn btn-info btn-sm">
                  <i data-feather="edit"></i>
                  Edit
                </a>
               
              </td>
              <td>
                 <a href="{{ url('project/delete_item_control/'.$value->id_item) }}" class="btn btn-danger btn-sm">
                  <i data-feather="trash"></i>
                  Hapus
                </a>
              </td>
              @endif
              <td>
                <button href="" class="btn btn-primary note btn-sm">
                  <i data-feather="edit"></i>
                  Note
                </button>
              </td>
            </tr>
           @endforeach
           </tbody>
        </table>
         </div>
    	</div>

      </div>
    </div>
</div>

@endsection
@include('project.leader.modal_note')
@push('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
               "scrollX": true,
              
                "scrollCollapse": true,
        
       
        
        });
          });
           $('.note').click(function(){
                  $('#modal_note').modal('show');
                  $('.modal-title').html('List Note');
                });
    </script>
@endpush