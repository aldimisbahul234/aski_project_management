
@extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
                 <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Type Project {{$project->project_name}}</a></li>
                  <li class="breadcrumb-item"><a href="">Project Detail</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
<!-- header -->

  @if(Session::get('status') == '200')
<div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Success!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@elseif(Session::get('status') == '400')
<div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 98%;
    height: 58px;
    left: 15px;
    padding-top: 15px;
    padding-left: 15px;">
  <strong>Error!</strong>{{ Session::get('msg') }}.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


   <!-- Tabs with Icon starts -->
    <div class="col-xl-12 col-lg-12">
      <div class="card">
        <div class="card-body">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a
                class="nav-link active"
                id="general-tab"
                data-toggle="tab"
                href="#general"
                aria-controls="user"
                role="tab"
                aria-selected="true"
                ><i data-feather="info" ></i> INFO GENERAL PROJECT </a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="event-tab"
                data-toggle="tab"
                href="#event"
                aria-controls="profile"
                role="tab"
                aria-selected="false"
                ><i data-feather="calendar" ></i> EVENT</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="rfq-tab"
                data-toggle="tab"
                href="#rfq"
                aria-controls="profile"
                role="tab"
                aria-selected="false"
                ><i data-feather="message-square" ></i> RFQ</a
              >
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="technical-tab"
                data-toggle="tab"
                href="#technical"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="briefcase" ></i> TECHNICAL REVIEW</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="structure-tab"
                data-toggle="tab"
                href="#structure"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="settings" ></i> STRUCTURE TEAM</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="product-tab"
                data-toggle="tab"
                href="#product"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="box" ></i> PRODUCT @if($count_notif_product > 0) <span class="badge badge-danger" style="background-color: red;"> new</span> @endif</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="quality-tab"
                data-toggle="tab"
                href="#quality"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="check" ></i> QUALITY</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="document-tab"
                data-toggle="tab"
                href="#document"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="folder" ></i> DOCUMENT  @if($count_notif_document > 0) <span class="badge badge-danger" style="background-color: red;"> new</span> @endif</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="cost-tab"
                data-toggle="tab"
                href="#cost"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="user" ></i> COST</a
              >
            </li>
             <li class="nav-item">
              <a
                class="nav-link"
                id="analistyc-tab"
                data-toggle="tab"
                href="#analistyc"
                aria-controls="about"
                role="tab"
                aria-selected="false"
                ><i data-feather="user" ></i> ANALYTIC</a
              >
            </li>
          </ul>
          <div class="tab-content">

            <!-- general -->
            <div class="tab-pane active" id="general" aria-labelledby="general-tab" role="tabpanel">

               <div class="card">
                  <div class="card-header">
                      <h2 style="text-align: right;">PROJECT DETAIL {{ $project->project_name }}</h2>
                  </div>
                <div class="card-body">
                <table class="table">
                    <tr>
                      <th>PROJECT NAME</th>  
                      <th>{{ $project->project_name }}</th> 
                      <th></th>      
                    </tr>
                    <tr>
                      <th>CUSTOMER NAME</th>    
                      <th>{{ $customer->customer_name }}</th>  
                      <th></th>   
                    </tr>
                    @foreach($summary as $value)
                    <tr>
                      <th>{{ $value->caption_info }}</th>
                      <th>{{ $value->description }}</th>
                      <th></th>
                    </tr>
                    @endforeach
              </table>
            </div>
             <div class="card-footer">
                 <a href="{{ url('project/report_general/'.$project->id_project) }}" class="btn btn-success"><i data-feather='file-plus'></i>REPORT</a>
                 @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#general_modal"><i data-feather='edit-3'></i>EDIT INFO GENERAL PROJECT</button>
                 <a href="{{ url('project/delete_project_summary/'.$project->id_project.'/'.$project->id_customer) }}" class="btn btn-danger"><i data-feather='trash'></i>DELETE ALL INFO GENERAL PROJECT</a>
                 @endif
               </div>
          </div>
            @include('project.leader.modal.modal_general')
            </div>
            <!-- end general -->

            <!-- event -->
            <div class="tab-pane" id="event" aria-labelledby="event-tab" role="tabpanel">
             @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#event_modal"><i data-feather='file-plus'></i>ADD EVENT</button>
              @endif
           <a href="{{ url('dashboard_detail_schedule/'.$project->id_project) }}" class="btn btn-success" target="_blank"><i data-feather='activity'></i> VIEW GRAFIK EVENT</a>
              <table class="table event" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>EVENT</th>
                    <th>START PLAN EVENT</th>
                    <th>FINISH PLAN EVENT</th>
                    <th>START ACTUAL EVENT</th>
                    <th>FINISH ACTUAL EVENT</th>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <th>EDIT</th>
                    <th>REMOVE</th>
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                  @foreach($event as $even)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $even->category_name->event_project_name }}</td>
                    <td>{{ $even->jadwal_event }}</td>
                    <td>{{ $even->finish_plan_event }}</td>
                    <td>{{ $even->actual_event }}</td>
                    <td>{{ $even->finish_actual_event }}</td>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <td><a class="btn btn-info btn-sm" href="{{ url('project/edit_event/'.$even->id_event_project) }}">EDIT</a></td>
                    <td><a href="{{ url('project/delete_event/'.$even->id_event_project) }}" class="btn btn-danger btn-sm">Remove</a></td>
                    @endif
                  </tr>
                  @endforeach
                 </tbody>
               </table>
               @include('project.leader.modal.modal_event')
            </div>
            <!-- end event -->

            <!-- start rfq -->
            <div class="tab-pane" id="rfq" aria-labelledby="rfq-tab" role="tabpanel">
                 @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
               <button type="button" class="btn btn-success rfq_document"><i data-feather='file'></i>RFQ DOCUMENT</button>
           <a href="{{ url('project/create_rfq/'.$project->id_project) }}" class="btn btn-info"><i data-feather='chevrons-down'></i>REGISTER DATA PART</a>
           @endif
           <a href="{{ url('project/export_rfq/'.$project->id_project) }}" class="btn btn-warning"><i data-feather='chevrons-down'></i>REPORT</a>
             <table class="table table-striped table-hover nowrap rfq" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROCESS</th>
                    <th>QTY ORDER</th>
                    <th>LIFE TIME</th>
                    <th>REMARKS</th>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <th></th>
                    <th></th>
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_rfq as $rfq)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $rfq['part_number'] }}</td>
                   <td>{{ $rfq['part_name'] }}</td>
                  <td>{{ $rfq['process_name'] }}</td>
                  <td>{{ $rfq['qty'] }}</td>
                  <td>{{ $rfq['lifetime'] }}</td>
                  <td>{{ $rfq['remarks'] }}</td>
                     @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                  <td><a class="btn btn-info btn-sm" href="{{ url('project/edit_rfq/'.$rfq['part_number']) }}">edit</a></td>
                  <td><a href="" class="btn btn-danger btn-sm">hapus</a></td>
                  @endif
                 </tr>
                 @endforeach
                 </tbody>
               </table>

            </div>
            <!-- end rfq -->

            <!-- start technical -->
            <div class="tab-pane" id="technical" aria-labelledby="technical-tab" role="tabpanel">
           
               <table class="table technical" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <th>NEW TECHNICAL REVIEW</th>
                    @endif
                    <th>ATTACHMENT</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_technical as $technical_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $technical_value['part_number'] }}</td>
                   <td>{{ $technical_value['part_name'] }}</td>
                      @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                  <td><a class="btn btn-primary" href="{{ url('project/add_technical_review/'.$project->id_project.'/'.$technical_value['part_number']) }}"><i class="fa fa-book"></i>FORM TECHNICAL REVIEW</a></td>
                  @endif
                  <td>
                    @if($technical_value['technical_review'] > 0)
                    <a class="btn btn-warning" href="{{ url('project/technical_review/'.$project->id_project.'/'.$technical_value['part_number']) }}"><i class="fa fa-check"></i>DOCUMENT TECHNICAL REVIEW</a>
                    @else
                    <button class="btn btn-warning" disabled><i class="fa fa-close"></i>NOT AVAILABLE</button>
                    @endif

                  </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

            </div>
            <!-- end technical -->

            <!-- start structure -->
             <div class="tab-pane" id="structure" aria-labelledby="structure-tab" role="tabpanel">

               <h3>Project Manager : {{ $getmanager->firstname }} {{ $getmanager->lastname }}</h3>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#all_member"><i data-feather='chevrons-down'></i>VIEW ALL MEMBER PROJECT</button>
               @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
            <a href="{{ url('project/add_part_die_go/'.$project->id_project) }}" class="btn btn-info"><i data-feather='file-plus'></i>ADD PRODUCT DIE GO</a>
            @endif
           <table class="table table-striped table-hover nowrap struckture" id="datatable" >
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROJECT LEADER</th>
                    <th>PROCESS</th>
                    <th>TOTAL PIC</th>
                    <th>MEMBER DETAIL</th>
                       @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <th>ADD MEMBER</th>
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_structure as $structure_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $structure_value['part_number'] }}</td>
                   <td>{{ $structure_value['part_name'] }}</td>
                  <td>@foreach($structure_value['get_leader'] as $val) <p>{{ $val->username }}</p> @endforeach</td>
                  <td>{{  $structure_value['process_name']  }}</td>
                  <td>{{  $structure_value['count_pic'] }}</td>
                  <td><a href="{{ url('project/detail_member/'.$project->id_project.'/'.$structure_value['part_number']) }}" class="btn btn-success"><i class="fa fa-eye">DETAIL</i></a></td>
                     @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
          <td><a href="{{ url('project/add_new_member/'.$project->id_project.'/'.$structure_value['part_number'].'/'.$structure_value['id_process']) }}" class="btn btn-info"><i class="fa fa-plus">ADD NEW MEMBER</i></a></td>
          @endif
                 </tr>
                 @endforeach
                 </tbody>
               </table>
                
            </div>
            <!-- end structure -->

              <!-- start product -->
             <div class="tab-pane" id="product" aria-labelledby="product-tab" role="tabpanel">

               <table class="datatables-basic table product" style="width: 200%">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>PROJECT LEADER</th>
                    <th>PLAN PROGRESS</th>
                    <th>ACTUAL PROGRESS</th>
                    <th>OPEN R.I.O</th>
                    <th>STATUS</th>
                    <th>REPORT GANT</th>
                    <th>REPORT EXCEL</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_product as $product_value)
                 <tr>
                  <td style="text-align: left;">{{ $loop->iteration }}</td>
                  <td style="text-align: left;">{{ $product_value['part_number'] }}</td>
                  <td style="text-align: left;">{{ $product_value['part_name'] }}</td>
                  <td style="text-align: left;">
                  @foreach($product_value['get_leader'] as $val) 
                    <p>{{ $val->username }}</p>
                  @endforeach
                  </td>
                  <td style="text-align: left;">{{ $product_value['result_plan'] }} %</td>
                  <td style="text-align: left;">{{ $product_value['result_actual'] }} %</td>
                  <td style="text-align: left;">
                    @if($product_value['notif_issue'] > 0)
                    <div class="row">
                      <div class="col-lg-6">
                         <a href="{{ url('project/issue/'.$project->id_project.'/'.$product_value['part_number'].'/'.'0'.'/'.'0'.'/'.'part_number') }}" class="btn btn-danger btn-sm">R.I.O </a>
                      </div>
                      <div class="col-lg-6">
                        <span class="badge badge-danger" style="background-color: red;"> new</span>
                      </div>
                      @else
                        <a href="{{ url('project/issue/'.$project->id_project.'/'.$product_value['part_number'].'/'.'0'.'/'.'0'.'/'.'part_number') }}" class="btn btn-danger btn-sm">R.I.O</a>
                      @endif
                  </td>
                  <td style="text-align: left;"><a href="{{ url('project/product_category_control/'.$product_value['part_number'].'/'.$product_value['id_process_product'].'/'.$project->id_project) }}">
                  <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{ $product_value['result_actual'] }}%"
                      ></div>
                    </div>
                  </a>
                    @if($product_value['notif_item'] > 0)
                   <span class="badge badge-danger" style="background-color: red;"> new</span>
                   @endif
                </td>
                  <td style="text-align: left;">
                 
                         <a href="{{ url('gant_chart_project/'.$project->id_project.'/'.$product_value['part_number']) }}" class="btn btn-success btn-sm">View grafik</a>
                     
                         
                       
                    
                  </td>
                  <td style="text-align: left;">
                     <a href="{{ url('project/download_excel_product/'.$project->id_project.'/'.$product_value['part_number']) }}" class="btn btn-success btn-sm">Download excel</a>
                  </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

             </div>
             <!-- end product -->

             <!-- start quality -->
             <div class="tab-pane" id="quality" aria-labelledby="quality-tab" role="tabpanel">

                <table class="table quality" id="datatable">
                 <thead>
                  
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>CURRENT EVENT QUALITY</th>
                    <th>CURRENT TRIAL</th>
                    <th>ADJUSMENT VISUAL</th>
                    <th>ADJUSMENT DIMENSI</th>
                    <th>APPROVAL LINE</th>
                    <th>DETAIL</th>
                    @if(Auth::user()->hak_akses =="quality")
                    <th>ADD</th>
                    
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($quality as $quality)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $quality['part_number'] }}</td>
                   <td>{{ $quality['part_name'] }}</td>
                  <td><button class="btn btn-primary">{{ $quality['event_name'] }} <br><br> {{ $quality['current']}}</button></td>
                  <td>@if($quality['trial_number'] ==true) {{ $quality['trial_number'] }} @else N/A @endif</td>
                  @if($quality['adjustment_visual'] ==true) 
                      @if($quality['adjustment_visual'] =="OK") 
                        <td style="background-color: green;color: white">
                        {{ $quality['adjustment_visual'] }} 
                        </td>
                        @else
                        <td style="background-color: red;color: white;">
                             {{ $quality['adjustment_visual'] }} 
                        </td>
                        @endif
                    
                  @else
                  <td>N/A</td>
                  @endif
                  @if($quality['adjustment_dimensi'] ==true) 
                      @if($quality['adjustment_dimensi'] =="OK") 
                        <td style="background-color: green;color: white">
                        {{ $quality['adjustment_dimensi'] }} 
                        </td>
                        @else
                        <td style="background-color: red;color: white;">
                             {{ $quality['adjustment_dimensi'] }} 
                        </td>
                        @endif
                    
                  @else
                  <td>N/A</td>
                  @endif
           
                  <td>
                    @if(Auth::user()->hak_akses =="quality")
                      <div class="row">
                        <div class="col-lg-12">
                          <a href="{{ url('project/approval_line/'.$quality['part_number'].'/'.$project->id_project.'/'.$quality['id_category_event_project']) }}"><i data-feather='check-circle'></i> {{ $quality['line_done'] }} %</a> | 
                          <a href="{{ url('project/approval_line/'.$quality['part_number'].'/'.$project->id_project.'/'.$quality['id_category_event_project']) }}"><i data-feather='external-link'></i> {{ $quality['line_eject'] }} %</a> |
                          <a href="{{ url('project/approval_line/'.$quality['part_number'].'/'.$project->id_project.'/'.$quality['id_category_event_project']) }}"><i data-feather='alert-circle'></i> {{ $quality['line_silang'] }} %</a>
                        </div>
                        <div class="col-lg-12">
                          @if(is_null($quality['approval_line']))
                          <button class="btn-primary btn-sm" style="width: 85%"> N\A </button>
                          @else
                            @if($quality['status_line_done'] =="OK")
                            <button class="btn-success btn-sm" style="width: 85%"> {{ $quality['status_line_done'] }} </button>
                            @else
                            <button class="btn-danger btn-sm" style="width: 85%"> {{ $quality['status_line_done'] }} </button>
                            @endif
                          @endif
                        </div>
                      </div>
                      @else
                       <div class="row">
                        <div class="col-lg-12" style="width: 129px;">
                          <a><i data-feather='check-circle'></i> {{ $quality['line_done'] }} %</a> | 
                          <a><i data-feather='external-link'></i> {{ $quality['line_eject'] }} %</a> |
                          <a><i data-feather='alert-circle'></i> {{ $quality['line_silang'] }} %</a>
                        </div>
                        <div class="col-lg-12 mt-1 ml-1">
                            @if(is_null($quality['approval_line']))
                          <button class="btn-primary btn-sm" style="width: 85%"> N\A </button>
                          @else
                            @if($quality['status_line_done'] =="OK")
                            <button class="btn-success btn-sm" style="width: 85%"> {{ $quality['status_line_done'] }} </button>
                            @else
                            <button class="btn-danger btn-sm" style="width: 85%"> {{ $quality['status_line_done'] }} </button>
                            @endif
                          @endif
                        </div>
                      </div>
                      @endif

                     
                  </td>
                
                  <td>
                    <a href="{{ url('project/quality_detail/'.$project->id_project.'/'.$quality['part_number']) }}" class="btn btn-success"><i data-feather="eye"></i>Detail</a>
                  </td>
                    @if(Auth::user()->hak_akses =="quality")
                  <td>
                    <a href="{{ url('project/quality_add/'.$quality['part_number'].'/'.$project->id_project.'/'.$quality['id_category_event_project']) }}" class="btn btn-success"><i data-feather="edit"></i>Add</a>
                  </td>
                  @endif
                 </tr>
                 @endforeach
                 </tbody>
               </table>

             </div>
             <!-- end quality -->

             <!-- start document -->
             <div class="tab-pane" id="document" aria-labelledby="document-tab" role="tabpanel">

                <table class="table document" id="datatable">
                 <thead>
                  
                  <tr>
                    <th style="text-align: center;">NO</th>
                    <th style="text-align: center;">PART NUMBER</th>
                    <th style="text-align: center;">PART NAME</th>
                    <th style="text-align: center;">PROGRESS DOCUMENT</th>
                    <th style="text-align: center;">TOTAL DOCUMENT</th>
                      @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                    <th style="text-align: center;">ADD DOCUMENT</th>
                    @endif
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_document as $document_value)
                 <tr>
                   <td style="text-align: center;">{{ $loop->iteration }}</td>
                   <td style="text-align: center;">{{ $document_value['part_number'] }}</td>
                   <td style="text-align: center;">{{ $document_value['part_name'] }}</td>
                   <td style="text-align: center;"> 
                    @if($document_value['progress'] == 100 || $document_value['progress'] > 80)
                    <div class="progress progress-bar-success">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: 100%"
                      >{{ $document_value['progress'] }} %</div>
                    </div>
                    @elseif($document_value['progress'] < 100 || $document_value['progress'] > 31)
                    <div class="progress progress-bar-warning">
                      <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        aria-valuenow="60"
                        aria-valuemin="60"
                        aria-valuemax="100"
                        style="width: {{ $document_value['progress'] }}%"
                      >{{ $document_value['progress'] }} %</div>
                    </div>
                    @endif
                   </td>
                   <td style="text-align: center;">
                      {{ $document_value['countdocument'] }}
                   </td>
                   @if(Auth::user()->hak_akses =="project_manager" || Auth::user()->hak_akses =="project_leader")
                   <td style="text-align: center;">
                    @if($document_value['notif'] > 0)
                    <a href="{{ url('project/list_document/'.$project->id_project.'/'.$document_value['part_number']) }}" class="btn btn-primary">Submit document</a> 
                    @else
                    <a href="{{ url('project/list_document/'.$project->id_project.'/'.$document_value['part_number']) }}" class="btn btn-primary" style="right: 17px;">Submit document</a> 
                    @endif
                    @if($document_value['notif'] > 0)
                    <span class="notif" style="width: 2%;height: 3%;top: -13px;
    right: 17px;padding: 5px 10px;position: relative;">new</span>
                    @endif
                     
                   </td>
                   @endif
                 </tr>
                 @endforeach
                 </tbody>
               </table>

             </div>
             <!-- end document -->

              <!-- start cost -->
             <div class="tab-pane" id="cost" aria-labelledby="cost-tab" role="tabpanel">

                <div class="col-lg-12">
                <div class="row">

                          <div class="col-lg-4">
                              <div class="card-box bg-danger">
                                  <div class="inner">
                                      <h3 style="color: white"> COGM </h3>
                                      <p style="color: white"> {{ $cogm }} COGM </p>
                                  </div>
                                  <div class="icon">
                                      <i class="fa fa-cog" aria-hidden="true"></i>
                                  </div>
                                  <a href="{{ url('project/cogm/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                              </div>
                          </div>
                           <div class="col-lg-4">
                              <div class="card-box bg-danger">
                                  <div class="inner">
                                      <h3 style="color: white"> PROJECT BUDGET </h3>
                                      <p style="color: white"> {{ $budget }} Item Invesment </p>
                                  </div>
                                  <div class="icon">
                                      <i class="fa fa-calculator" aria-hidden="true"></i>
                                  </div>
                                  <a href="{{ url('project/project_budget/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                              </div>
                          </div>
                           <div class="col-lg-4">
                              <div class="card-box bg-danger">
                                  <div class="inner">
                                      <h3 style="color: white">COST</h3>
                                      <p style="color: white">PERFORMACE INDEX</p>
                                  </div>
                                  <div class="icon">
                                      <i class="fa fa-user" aria-hidden="true"></i>
                                  </div>
                                  <a href="{{ url('project/cost_performance/'.$project->id_project) }}" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                              </div>
                          </div>
                </div>
                </div>

             </div>
             <!-- end cost -->

             <!-- start analistyc -->
             <div class="tab-pane" id="analistyc" aria-labelledby="analistyc-tab" role="tabpanel">

                  <table class="table analistyc" id="datatable">
                 <thead>
                  <tr>
                    <th>NO</th>
                    <th>PART NUMBER</th>
                    <th>PART NAME</th>
                    <th>ANALISTYC</th>
                  </tr>
                  </thead>
                 <tbody>
                 @foreach($data_analistyc as $analistyc_value)
                 <tr>
                   <td>{{ $loop->iteration }}</td>
                   <td>{{ $analistyc_value['part_number'] }}</td>
                   <td>{{ $analistyc_value['part_name'] }}</td>
                  <td><a href="{{ url('project/analis/'.$project->id_project.'/'.$analistyc_value['part_number'].'/'.'all') }}" class="btn btn-success btn-sm"><i data-feather='activity'></i> SEE DETAIL</td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>

             </div>
             <!-- end analistyc -->


            </div>
          </div>
        </div>
      </div>
    </div>
     @include('project.leader.modal.rfq_document')
  @include('project.leader.modal.modal_all_member')
 
  @endsection
  @push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.event').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
         $(document).ready(function() {
            $('.document_rfq').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
            });
          });
         $(document).ready(function() {
            $('.rfq').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true, 
               
            });
          });
          $(document).ready(function() {
            $('.struckture').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
               
            });
          });
           $(document).ready(function() {
            $('.product').dataTable({
                "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
              
                
            });
          });
            $(document).ready(function() {
            $('.all_member').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                 "scrollX": true,
                "scrollY": "300px",
                "scrollCollapse": true,
                
            });
          });
            $(document).ready(function() {
            $('.analistyc').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,  
                
            });
          });

             $(document).ready(function() {
            $('.quality').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
                   
                
            });
          });
               $(document).ready(function() {
            $('.document').dataTable({
                 "pageLength": 20,
                "lengthChange": false,
                "paging": true,
                "searching": true,
              
                
            });
          });

           var i=1;  
      $('#tambah').click(function(){
           i++;  
          

         $('#add').append('<div class="row"  id="row'+i+'">'+ 
    '<div class="col-lg-6">'+
        '<div class="form-group">'+
            '<a id="'+i+'" class="btn_remove delete'+i+'">remove</a>'+
            '<input type="text"  class="form-control" placeholder="caption" name="caption_info[]=" required />'+
        '</div>'+
    '</div>'+
        '<div class="col-lg-6">'+
            '<div class="form-group">'+
            '<label></label>'+
                '<input type="text" class="form-control" placeholder="description" name="description[]" required />'+
                '</div>'+
            '</div>'+
        '</div>');  
      });  
    $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
            $('.delete'+button_id+'').remove();  
      });  
        $('.rfq_document').click(function(){
                  $('#modal_rfq').modal('show');
                  $('.modal-title').html('Add New Rfq');
                });
    </script>
@endpush

  
