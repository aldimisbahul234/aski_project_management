 @extends('layouts/root')
@section('main')
<div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h2 class="content-header-title float-left mb-0">Project List</h2>
          <div class="d-flex justify-content-start breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ml-1">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project') }}">project</a></li>
              <li class="breadcrumb-item"><a href="{{ url('project/customer/'.$header->id_product) }}">Sector {{$header->product_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/customerDetail/'.$customer->id_customer.'/'.$header->id_product) }}">Customer {{$customer->customer_name}}</a></li>
                <li class="breadcrumb-item"><a href="{{ url('project/edit_project/'.$header->id_product.'/'.$customer->id_customer.'/'.$project->id_project) }}">Type Project{{$project->project_name}}</a></li>
                <li class="breadcrumb-item"><a href="">Edit Part({{$data->part_name}})</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
 

<div class="col-lg-12">
<div class="card">
  <div class="card-header">

     <div class="col-lg-12">
            <div class="card" style="background-color: rgba(115,103,240,.7)">
              <div class="card-body">
                  <h2 style="text-align: center;" class="text-white">EDIT PART</h2>
              </div>
            </div>
        </div>

  </div>
  <div class="card-body">

       <form class="form-participant" action="{{ url('project/update_part/'.$data->part_number) }}" method="post" enctype="multipart/form-data">
                           @csrf
                        
                    
                        <div class="form-group">
                          <label class="form-label" for="basic-addon-name">EDIT PART</label>
                          <input
                            type="text"
                            id="basic-addon-name"
                            class="form-control"
                            placeholder="Event Project"
                            aria-label="Name"
                            aria-describedby="basic-addon-name"
                            name="event_project_name"
                            value="{{ $data->part_name }}"
                            required
                          />
                        </div>
                    
                    </div>
                  </div>
   

                    
                         <div class="form-group">
                      <button type="submit" class="btn btn-primary">Accept</button>
                    </div>
                      </form>


                  </div>
              </div>

@endsection